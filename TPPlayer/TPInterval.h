//
//  TPInterval.h
//
//  Copyright © 2013, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)
//
//  Compatibility: iOS 5.1+
//
//  SUMMARY
//  Metrics tracking requires notification at certain intervals. Sometimes it is necessary to emit a message every 60 seconds for the first 5 minutes
//  then a message every 3 minutes for the next 10 minutes, followed by a message every 15 minutes for the duration
//  An TPInterval encapsulates the information necessary to track those intervals, including a means to deal with pause/ restart conditions
//  
//  An TPInterval with an interval length of 60 seconds and an endPoint of 300, would fire at the end of each minute for the first 5 minutes
//  An TPInterval with an intervalLength of 300 seconds and an endPoint of 0, would fire at the end of every 5 minutes until the video ends


#import <UIKit/UIKit.h>
@class TPInterval;
@class TPIntervalManager;

// the interval notifies its owner when it fires
@protocol TPIntervalOwner
- ( void )intervalFired:( TPInterval *)interval;
@end


@interface TPInterval : NSObject

@property(nonatomic, assign)CGFloat intervalLength; // in seconds
@property(nonatomic, assign)CGFloat endPoint; // in seconds
@property(nonatomic, assign)BOOL hasEndpoint; // if endPoint > 0, to make it infinite set endpoint to 0
@property(nonatomic, weak)id<TPIntervalOwner>manager;
@property(nonatomic, assign)BOOL isComplete;
@property(nonatomic, assign)CGFloat accumulatedTime;

- ( id )initWithLength:( CGFloat)anInterval endPoint:( CGFloat)anEndPoint;

- ( NSString *)messageForStage;
- (CGFloat )intervalElapsedTime;

- ( void )start;
- ( void )pause;
- ( void )stopTiming;
- ( void )reset;

@end

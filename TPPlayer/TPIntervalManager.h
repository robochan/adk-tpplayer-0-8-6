//
//  TPIntervalManager.h
//
//  Copyright © 2013, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)
//
//  Compatibility: iOS 5.1+
//
//  SUMMARY
//  Metrics tracking requires notification at certain intervals. Sometimes it is necessary to emit a message every 60 seconds for the first 5 minutes
//  then a message every 3 minutes for the next 10 minutes, followed by a message every 15 minutes for the duration
//  An TPInterval encapsulates the information necessary to track those intervals, including a means to deal with pause/ restart conditions
//  
//  An TPIntervalManager is initiated with an array of TPInterval objects.  
//  like this
// if you have livePlay, create intervals to monitor for tracking and pass this array to TPPlayerViewController
/*
- ( NSArray *)intervalsToMonitor;
{
    TPInterval *oneMinuteInterval = [[ TPInterval alloc] initWithLength:60 endPoint:300];
    TPInterval *fiveMinuteInterval = [[ TPInterval alloc] initWithLength:300 endPoint:0 ];
    return [ NSArray arrayWithObjects:oneMinuteInterval, fiveMinuteInterval, nil];
}
 */

#import <Foundation/Foundation.h>
#import "TPInterval.h"

// an TPIntervalManager will notify it delegate (TPIntervalManagerDelegate)when the interval fired
@protocol TPIntervalManagerDelegate
- ( void )intervalFired:( TPInterval *)interval;
@end

@interface TPIntervalManager : NSObject<TPIntervalOwner>

@property(nonatomic, weak )id<TPIntervalManagerDelegate>delegate;

- ( id )initWithIntervals:( NSArray *)intervals;
- ( void )start;
- ( void )restart;
- ( void )pause;
- ( void )stopTiming;
- ( CGFloat)intervalElapsedTime;


@end

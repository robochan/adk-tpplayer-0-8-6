//
//  TPMetricsTracker.m
//
//  Copyright © 2013, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)
//

#import "TPMetricsTracker.h"

@implementation TPMetricsTracker

- ( id )initWithInfo:( NSDictionary *)info;
{
    if( self = [ super init ] )
    {
        self.infoDictionary = info;
    }
    return self;
}



- ( NSDictionary *)parametersToReportFromDictionaryInfo:( NSDictionary *)controllerParams controller:( UIViewController *)reportingViewController;
{
    NSMutableDictionary *paramsToReport = [ NSMutableDictionary dictionary ];
    
    NSArray *keysForTrackPage = [ controllerParams allKeys ];
    
    for( NSString *aKeyForTrackPage in keysForTrackPage )
    {
        //        NSDictionary *infoForParam = [ controllerParams objectForKey:aKeyForTrackPage ];
        id infoForParam = [ controllerParams objectForKey:aKeyForTrackPage ];
        NSString *stringToReport = @"";
        
        if ([ infoForParam isKindOfClass:[ NSDictionary class ] ] )
        {
            DLog(@"stringToReport = %@ for key %@", stringToReport, aKeyForTrackPage);
            stringToReport = [ self makeStringFromDictionary:infoForParam reportingController:reportingViewController ];
        }
        else {
            if ([ infoForParam isKindOfClass:[ NSString class ] ] )
            {
                stringToReport = infoForParam;
            }
        }
        [ paramsToReport setObject:stringToReport forKey:aKeyForTrackPage ];
    }
    return paramsToReport;
}

- ( NSString *)makeStringFromDictionary:( NSDictionary *)infoForParam reportingController:( UIViewController *)reportingViewController;
{
    int i = 0;
    NSString *stringToReport = @"";
    for( NSString *aFormat in [infoForParam objectForKey:@"formats" ] )
    {
        NSString *newString;
        NSArray *associatedSelectors = [ infoForParam objectForKey:@"selectors" ];
        if (associatedSelectors && associatedSelectors.count - 1 >= i ) {
            NSString *selectorString = [associatedSelectors objectAtIndex:i++ ];
            // look into swapping use of the selector out for valueForKey:  I think it will for for
            // all the values I have seen so far.
            SEL selectorToUse = NSSelectorFromString(selectorString);
            
            newString = [ stringToReport stringByAppendingFormat:aFormat, [ reportingViewController performSelector:selectorToUse ] ];
        }
        else
        {
            newString = [ stringToReport stringByAppendingFormat:aFormat ];
        }
        stringToReport = newString;
    }
    return stringToReport;
}

#pragma mark - tracking methods

- ( void )trackPageView:( UIViewController *)reportingViewController;
{
    DLog(@"class is %@", [ self class ] );
}

- ( void )trackVideoView:( UIViewController *)reportingController stage:( NSString *)videoStage;
{
    DLog(@"class is %@", [ self class ] );
}
- ( void )trackVideoView:( UIViewController *)reportingController withTime:( NSUInteger)seconds stage:( NSString *)videoStage;
{
    DLog(@"class is %@", [ self class ] );
}
- ( void)trackLink:( UIViewController *)reportingController linkName:( NSString *)linkName forType:( NSString *)type;
{
    DLog(@"class is %@", [ self class ] );
}

- (void) trackSocialShare:(UIViewController *)reportingController sharedItem:(NSString *)sharedItem;
{
    DLog(@"class is %@", [ self class ] );
}

-(void) trackSearch:(UIViewController *)reportingViewController searchTerm:(NSString *)searchTerm{
    DLog(@"class is %@", [ self class ] );
}

- ( void )trackAdView:( UIViewController *)reportingViewController;
{
    DLog(@"class is %@", [ self class ] );
}

- (void)trackVideoAdStarted:(UIViewController *)reportingController adPosition:( NSString *)adPosition additionalInformation:( NSDictionary *)notificationDictionary;
{
    DLog(@"put in whatever you want here - adSource is %@ additionalInformation is %@",adPosition, notificationDictionary);
}
- (void)trackVideoAdCompleted:(UIViewController *)reportingController adPosition:( NSString *)adPosition additionalInformation:( NSDictionary *)notificationDictionary;
{
    DLog(@"put in whatever you want here - adSource is %@ additionalInformation is %@",adPosition, notificationDictionary);
}

- ( void )trackVideoAdPodStarted:(UIViewController *)reportingController adPod:( TPAdPod *)adPod;
{
    DLog(@"put in whatever you want here - adPod is %@",adPod );
    
}
- ( void )trackVideoAdPodCompleted:(UIViewController *)reportingController adPod:( TPAdPod *)adPod;
{
    DLog(@"put in whatever you want here - adPod is %@",adPod );
}
- ( void )trackVideoAdStarted:(UIViewController *)reportingController adObject:( TPAdObject *)adObject;
{
    DLog(@"put in whatever you want here - adObject is %@",adObject );
}

- ( void )trackVideoAdCompleted:(UIViewController *)reportingController adObject:( TPAdObject *)adObject;
{
    DLog(@"put in whatever you want here - adObject is %@",adObject );
}

- ( void )trackLogin:( UIViewController *)reportingController stage:( NSString *)loginStage;
{
    DLog(@"put in whatever you want here - loginStage is %@",loginStage );
}


- ( void )didFinishLaunching;
{
}

- ( void )applicationWillResignActive;
{
}


- ( void )applicationDidEnterBackground;
{
}

- ( void )applicationWillEnterForeground;
{
}

- ( void )applicationDidBecomeActive;
{
}

- ( void )applicationWillTerminate;
{
}


@end

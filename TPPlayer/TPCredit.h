//
//  TPCredit.h
//
//  Copyright © 2013, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)
//


#import <Foundation/Foundation.h>

@interface TPCredit : NSObject

@property(nonatomic, strong)NSString    *role;
@property(nonatomic, strong)NSString    *value;
@property(nonatomic, strong)NSString    *scheme;

- (id)initWithDictionaryFromJSON:(NSDictionary *)creditDictionary;


@end

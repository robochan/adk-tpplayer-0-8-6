//
//  NSStringWithURLParsingSupport.m
//  TPPlayer
//


#import "NSStringWithURLParsingSupport.h"

@implementation NSString (TP_URL_PARSING)

- ( NSMutableDictionary *)parametersDictionary;
{
    NSArray *components = [ self componentsSeparatedByString:@"?"];
    if (components.count > 1) 
    {
        NSString *parametersString = components[1];
        NSMutableDictionary *newParameters = [ NSMutableDictionary dictionary ];
        NSArray *currentParameters = [ parametersString componentsSeparatedByString:@"&" ];
        
        for( NSString *param in currentParameters)
        {
            NSArray *pieces = [ param componentsSeparatedByString:@"="];
            if (pieces.count > 1) {
                newParameters[pieces[0] ] = pieces[1];
            }
        }
        return newParameters;
    }   
    return nil;
}

- ( NSString *)baseURL;
{
    NSArray *components = [ self componentsSeparatedByString:@"?"];
    return components[0];
}

// split the string and make a parameters dictionary from any existing params.  
// merge the parameters dictionary with the one you just created -- values in parameters will override 
// any parameters with the same key that were previously included
// reconstruct the string from the resulting dictionary
- ( NSString *)urlStringWithNewParameters:( NSDictionary *)parameters;
{
    NSMutableString *finalString = nil;
    if( self.length > 0 )
    {
        finalString = [ NSMutableString stringWithFormat:@"%@?", [self baseURL]];
        // put the parameters into a dictionary so we can merge the new ones
        NSMutableDictionary *newParameters = self.parametersDictionary ? [ self parametersDictionary ] : [ NSMutableDictionary dictionary ];
        
        // parameters passed in override matching parameters that are there already
        [ newParameters addEntriesFromDictionary:parameters];
        
        NSMutableArray *tmpArray = [ NSMutableArray array ];      
        [newParameters enumerateKeysAndObjectsUsingBlock:^(NSString *key, NSString *aParameter, BOOL *stop) {
            [ tmpArray addObject:[ NSString stringWithFormat:@"%@=%@",  key, aParameter ] ];
        }];
        NSString *parametersString = [ tmpArray componentsJoinedByString:@"&"];
        [finalString appendString:parametersString ];
    }
    DLog(@"finalString = %@", finalString);
    return finalString;
}

@end

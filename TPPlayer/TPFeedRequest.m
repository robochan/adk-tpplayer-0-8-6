//
//  TPFeedRequest.m
//
//  Copyright © 2013, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)
//

#import "TPFeedRequest.h"
#import "TPRange.h"
#import "TPSort.h"
#import "TPMasterDataManager.h"
#import "TPDataHandler.h"
#import "NSStringWithURLParsingSupport.h"

@interface TPFeedRequest()

// redefining this property to be readwrite internally
@property(nonatomic, strong, readwrite)NSDictionary *defaultParameters;

@end


@implementation TPFeedRequest

- ( id)initWithURL:( NSString *)url delegate:(id<TPFeedRequestDelegate>)delegate;
{
    if( self = [ super init ])
    {
        _url = url;
        _delegate = delegate;
        _defaultParameters = @{@"form": @"json", @"count":@"true", @"validFeed":@"false", @"types":@"none"};
    }
    return self;
}
- ( NSMutableDictionary *)contentFormSpecifier;
{
    if (_contentFormSpecifier) {
        return _contentFormSpecifier;
    }
    _contentFormSpecifier = [@{@"byFormat":@[@"m3u", @"mpeg4"]}mutableCopy ];
    return _contentFormSpecifier;
}

- ( id)copyWithZone:(NSZone *)zone;
{
    TPFeedRequest *newCopy = [[[ self class]alloc]init];
    if (newCopy) {
        newCopy.url = [ self.url copy ];
        newCopy.delegate = self.delegate;
        newCopy.category = [ self.category copy ];
        newCopy.search = [ self.search copy];
        newCopy.sortingSpecification = [ self.search copy ];
        newCopy.range = [ self.range copy ];
        newCopy.fields = [ self.fields copy ];
        newCopy.fileFields = [ self.fileFields copy];
        newCopy.contentFormSpecifier = [self.contentFormSpecifier copy ];
        newCopy.parameters = [ self.parameters copy ];
        newCopy.defaultParameters = self.defaultParameters;
        newCopy.pathName = [self.pathName copy ];
        newCopy.contentItemClassName = [ self.contentItemClassName copy];
    }
    return newCopy;
}

- ( void )requestFeed;  
{
    NSString *finalStringForURL = [ self constructURL ];
    if (! self.pathName) {
        self.pathName = @"ContentList";
    }
    if (! self.contentItemClassName) {
        self.contentItemClassName = @"TPContentItem";
    }
    [ [ TPMasterDataManager sharedInstance ]getContentListForURL:finalStringForURL requester:self selector:@selector(feedLoaded:) path:self.pathName contentItemSubclass:self.contentItemClassName ];

}

- ( void)feedLoaded:( TPDataHandler *)dataHandler;
{
    if (dataHandler.error == nil ) {
        [ self.delegate feedRequest:self isLoaded:dataHandler.returnedObject];
    }
    else{
        [ self.delegate feedRequest:self failedWithError:dataHandler.error ];
    }
}

- ( NSString *)contentFormString;
{
    NSMutableString *contentFormString = [[ NSMutableString alloc ]init ];
    
    [self.contentFormSpecifier enumerateKeysAndObjectsUsingBlock:^(NSString * key, NSArray *obj, BOOL *stop) {
        NSString *stringForKey = [ NSString stringWithFormat:@"%@=%@", key, [ obj componentsJoinedByString:@"|"]];
        [contentFormString appendString:stringForKey ];
    }];
    return contentFormString;
}

- ( NSString *)constructURL;
{
//    NSMutableString *finalString = [self.url mutableCopy ];
//    if (NSNotFound == [ finalString rangeOfString:@"?" ].location) {
//        [finalString appendString:@"?"];
//    }
//    else{
//        [finalString appendString:@"&" ];
//    }
    NSMutableDictionary *finalParameters = [ self.defaultParameters mutableCopy];
    // merge user-provided parameters
    [ finalParameters addEntriesFromDictionary:self.parameters ];
    finalParameters[ @"byContent" ] = [ self contentFormString ];

    if (self.category) {
        finalParameters[@"byCategories"] = self.category;
    }
    if (self.search) {
        finalParameters[@"q"] = self.search;
    }   
    if (self.sortingSpecification) {
        finalParameters[@"sort"] = self.sortingSpecification.isDescending ? [ NSString stringWithFormat:@"%@|desc", self.sortingSpecification.fieldName] : self.sortingSpecification.fieldName;
    }      
    if (self.range) {
        finalParameters[@"range"] = [ NSString stringWithFormat:@"%lu-%lu", (unsigned long)self.range.startIndex, (unsigned long)self.range.endIndex ];
    }  
    if (self.fields) {
        finalParameters[@"fields"] = [self.fields componentsJoinedByString:@","];
    }   
    
    if (self.fileFields) {
        finalParameters[@"fileFields"] = [self.fileFields componentsJoinedByString:@","];
    }   
    
    NSString *finalString = [ self.url urlStringWithNewParameters:finalParameters];
    DLog(@"TPFeedRequest finalString = %@", finalString);

    return finalString;
}


@end

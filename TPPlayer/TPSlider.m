//
//  TPSlider.m
//
//  Copyright © 2013, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)
//
//

#import "TPSlider.h"

@interface TPSlider()


@end

@implementation TPSlider

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (CGRect)thumbRectForBounds:(CGRect)bounds trackRect:(CGRect)rect value:(float)value{
    self.thumbRect = [ super thumbRectForBounds:bounds trackRect:rect value:value];
//    NSLog(@"thumbRect = %@", NSStringFromCGRect(self.thumbRect));
//    NSLog(@"trackRect = %@", NSStringFromCGRect(rect));
    [self.overlayView adjustMarkVisibility:self.thumbRect];
   return self.thumbRect;
}


@end

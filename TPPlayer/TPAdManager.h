//
//  TPAdManager.h
//
//  Copyright © 2013, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)
//


#import <UIKit/UIKit.h>
#import <CoreMedia/CoreMedia.h>
#import <AVFoundation/AVFoundation.h>
#import "TPPlayerView.h"
#import "TPAdManagerOwner.h"
#import "TPAdObject.h"
#import "TPHeartbeatObserver.h"

#define NSEC_PER_SEC 1000000000ull
#define kMidRollAdTimesChanged @"MidRollAdTimesChanged"


@class TPAdManager;

@interface TPAdManager : UIViewController<HeartbeatObserver>

@property (weak, nonatomic) UIViewController<TPAdManagerOwner> * videoManager;
@property (nonatomic, assign)BOOL shouldSkipPreRoll;
@property(nonatomic, strong) NSArray *midrollAdTimes;
// Moves the overlay ad placements based on whether the Closed Captions are currently live.
// Default value when no CC is live is "bc" (bottom center) and "tc" (top center) when live.
@property (nonatomic, assign)BOOL shouldPositionForClosedCaptions;
@property (weak, nonatomic) TPPlayerView *playerView;
@property(nonatomic, assign)BOOL userScrubbed;
@property(nonatomic, assign)NSTimeInterval endScrubValue;
@property(nonatomic, assign)NSTimeInterval beginScrubValue;
@property(nonatomic, strong)NSString *adTypePlaying;
@property(nonatomic, strong)NSString *adManagerContentID;

- (void)submitAdRequest:( NSString *)adManagerId videoManager: (UIViewController<TPAdManagerOwner> *)videoManager;
// cleanup removes us from the default notification center and sends cleanup to the videoManager
- ( void )cleanup;
// Moves the overlay ad placements based on whether the Closed Captions are currently live.
// Default value when no CC is live is "bc" (bottom center) and "tc" (top center) when live.
- ( void )closedCaptionsAreNowShowing:( BOOL )closedCaptionsAreShowing;
// player played to end - now show postRoll ads
- ( void )playerDidReachEnd;
- ( void )playerStateChanged:( NSNotification *)notification;

- ( void )onAdStarted:( NSNotification *)notification;
- ( void )onAdEnded:( NSNotification *)notification;
- ( void )onSlotStarted:( NSNotification *)notification;
- ( void )onSlotEnded:( NSNotification *)notification;

- ( void )userScrubbedForward:( NSTimeInterval)endTime playbackTimeAtStart:( NSTimeInterval)playbackTimeAtStart;

@end

//
//  TPAdObject.h
//  TPPlayer
//
//  Copyright © 2014, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, TPAdType) 
{
    TPAdTypeUnknown = 0,
    TPAdTypePreRoll,
    TPAdTypeOverlay,
    TPAdTypeMidRoll,
    TPAdTypePostRoll
};


@interface TPAdObject : NSObject

@property(nonatomic, assign)TPAdType adType;
@property(nonatomic, assign) NSUInteger adPodPosition;
@property(nonatomic, strong) NSString *primaryAssetName;
@property(nonatomic, assign) NSTimeInterval adDuration;
@property(nonatomic, strong) NSString *adPodID;
@property(nonatomic, strong) NSString *adID;

- ( NSString *)adTypeAsString;

@end

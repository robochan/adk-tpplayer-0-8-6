//
//  TPPlayerConfiguration.m
//  TPPlayer
//
//  Copyright © 2014, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)
//

#import "TPPlayerConfiguration.h"

@implementation TPPlayerConfiguration

- ( NSString *)playerControlClassName;
{
    if (!_playerControlClassName) {
        _playerControlClassName = @"TPPlayerControls";
    }
    return _playerControlClassName;
}

@end

//
//  TPUserList.m
//  TPPlayer
//
//  Copyright (c) 2014 thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)


#import "TPUserList.h"
#import "TPUserListItem.h"
#import "TPAppSpecificInformationManager.h"
#import "TPEndUserServicesAgent.h"


@interface TPUserList()

@property(nonatomic, strong)NSDate *lastRefreshTime;
@property(nonatomic, assign)NSUInteger secondsForRefreshRate;
@property(nonatomic, strong, readwrite)NSMutableArray *userListItems;
@property(nonatomic, strong, readwrite)NSMutableDictionary *userListItemsDictionary;

@end
 
@implementation TPUserList

- ( id )initFromDictionary:( NSDictionary *)entries;
{
    if (self = [super init ]) {
        NSMutableArray *tmpArray = [ @[]mutableCopy];
        NSArray *allItemInfo = entries[@"pluserlist$items"];
        for( NSDictionary *itemInfo in allItemInfo )
        {
            TPUserListItem *listItem = [[ TPUserListItem alloc]initFromDictionary:itemInfo];
            [ tmpArray addObject:listItem];
        }
        self.userListItems = tmpArray;
        self.identifier = entries[@"id"];
        NSUInteger minutesForRefreshRate = [ [ [ TPAppSpecificInformationManager sharedInstance]objectForKey:@"TPResumeUserListRefreshMinutes" ]integerValue];
        self.secondsForRefreshRate = minutesForRefreshRate * 60;
        self.lastRefreshTime = [ NSDate date];
    }
    return self;
}

- ( void )addUserListItem:( TPUserListItem *)userListItem;
{
    [ self.userListItems insertObject:userListItem atIndex:0 ];
    self.userListItemsDictionary[userListItem.aboutID] = userListItem;
}

- ( void )removeUserListItem:( TPUserListItem *)aUserListItem;
{
    NSString *aboutID = aUserListItem.aboutID;
    TPUserListItem *userListItem = self.userListItemsDictionary[aboutID];
    [ self.userListItems removeObject:userListItem ];
    [self.userListItemsDictionary removeObjectForKey:aboutID];
}

- ( void )updateUserListItem:( TPUserListItem *)prototypeUserListItem;
{
    NSString *aboutID = prototypeUserListItem.aboutID;
    TPUserListItem *userListItem = self.userListItemsDictionary[aboutID];
    userListItem.title = [prototypeUserListItem title ];
    userListItem.itemDescription = [ prototypeUserListItem itemDescription];
}


- ( NSMutableDictionary *)userListItemsDictionary;
{
    if (!_userListItemsDictionary) {
        _userListItemsDictionary = [ self itemsDictionaryFromArray ];
    }
    return _userListItemsDictionary;
}

- ( NSValue *)timeToStartRangeValue:( NSString *)aboutID; 
{
    TPUserListItem *item = self.userListItemsDictionary[aboutID];
    if (item && item.title && item.description) {
        NSUInteger valueAdjustment = [[ TPEndUserServicesAgent sharedInstance]shouldStoreTimeValuesAsSeconds] ? 1 : 1000;
        NSUInteger playheadPosition = (NSUInteger)[ item.title longLongValue]/valueAdjustment;
        NSUInteger duration = (NSUInteger)[ item.itemDescription longLongValue]/valueAdjustment;
        NSRange timeToStartRange = NSMakeRange(playheadPosition, duration);
        return [ NSValue valueWithRange:timeToStartRange];
    }
    return nil;
}

- ( NSMutableDictionary *)itemsDictionaryFromArray;
{
    NSMutableDictionary *tmpDict = [ @{} mutableCopy];
    // userListItems is ordered by date - oldest last, so we don't want an older one replacing a newer one
    [ self.userListItems enumerateObjectsWithOptions:NSEnumerationReverse usingBlock:^(TPUserListItem *item, NSUInteger idx, BOOL *stop) {
        tmpDict[item.aboutID] = item;
    }];
    return tmpDict;
}

@end

//
//  TPExternalWindowViewController.h
//  TPPlayer
//
//  Copyright © 2014, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)
//

#import <UIKit/UIKit.h>

@interface TPExternalWindowViewController : UIViewController

@end

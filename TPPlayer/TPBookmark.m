//
//  TPBookmark.m
//  TPPlayer
//
//  Copyright © 2013, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)
//

#import "TPBookmark.h"
#import "TPEndUserServicesAgent.h"

@implementation TPBookmark

- ( NSString *)description;
{
    return [ NSString stringWithFormat:@"identifier = %@   platformUserListID = %@   aboutID = %@  playheadPosition = %ld  duration = %ld updated = %@", self.identifier, self.platformUserListID,  self.aboutID, (long)self.playheadPosition, (long)self.duration, self.updated];
}

-(NSString *)title;
{
    NSUInteger valueAdjustment = [[ TPEndUserServicesAgent sharedInstance]shouldStoreTimeValuesAsSeconds] ? 1 : 1000;
    return [ NSString stringWithFormat:@"%@", @(self.playheadPosition * valueAdjustment)];
}

-(NSString *)itemDescription;
{
    NSUInteger valueAdjustment = [[ TPEndUserServicesAgent sharedInstance]shouldStoreTimeValuesAsSeconds] ? 1 : 1000;
    return [ NSString stringWithFormat:@"%@", @(self.duration * valueAdjustment)];
}

- ( NSDate *)dateUpdated;
{
    if (self.updated) {
        return [[ NSDate alloc ]initWithTimeIntervalSince1970:([ self.updated longLongValue ]/1000.0)];
   }
    return [ NSDate date];
}

@end

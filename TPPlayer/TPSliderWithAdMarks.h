//
//  TPSliderWithAdMarks.h
//
//  Copyright © 2013, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)
//
//  This is just a wrapper view that holds an TPSlider and an TPTickMarkOverlayView
//  On awakeFromNib, it gets the tick mark image and creates the overlayView
//
//  In the nib, hook this to its datasource and its slider.  
//  You can hook the target/action for the slider directly in the nib
//
//  If you know there will never be tick marks, don't give it a datasource
//  if there should sometimes be tickmarks and sometimes not, the tickMarksArray returns nil or
//  an empty array when there should not be tickmarks and an array of values when there should.

#import <UIKit/UIKit.h>
#import "TPSlider.h"

@protocol TPTickMarkOverlayDataSource

- ( NSString *)tickMarkImageName;
- ( NSArray *)tickMarksArray;

@end

@interface TPSliderWithAdMarks : UIView

@property( nonatomic, strong )IBOutlet TPSlider *horizontalSlider;
@property( nonatomic, weak)IBOutlet UIViewController<TPTickMarkOverlayDataSource> *datasource;

- ( void)setUpAdTicks:( NSArray *)tickMarkPositions;

@end

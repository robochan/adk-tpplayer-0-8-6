//
//  TPNetworkMonitor.m
//
//  Copyright © 2013, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)
//

#import "TPNetworkMonitor.h"
#import <UIKit/UIKit.h>

NSString *const kTPReachabilityOfInterestChangedNotification = @"TPReachabilityOfInterestChangedNotification";

@interface TPNetworkMonitor()

@end

@implementation TPNetworkMonitor
{
    TPReachability *hostReach;
    TPReachability *internetReach;
    TPReachability *wifiReach;
}

@synthesize currentNetworkStatus;

+( TPNetworkMonitor *)sharedInstance
{
    static dispatch_once_t once;
    static TPNetworkMonitor *shared = nil;
    
    dispatch_once( &once, ^{
        shared = [ [ TPNetworkMonitor alloc ]init ];
        [ shared setup ];
    } );
    return shared;
}

- (void)removeNotificationCenterObserver
{
    NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
    [notificationCenter removeObserver:self];
}

- (void)dealloc
{
    [self performSelectorOnMainThread:@selector(removeNotificationCenterObserver) withObject:self waitUntilDone:YES];
}


- ( void )setup;
{
    NSNumber *wifiRequiredForPlayAsDefault = [ [ NSUserDefaults standardUserDefaults]objectForKey:@"wifiRequiredForPlay" ];
    if (wifiRequiredForPlayAsDefault) {
        self.wifiRequiredForPlay = [ wifiRequiredForPlayAsDefault boolValue];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(reachabilityChanged:) name: kTPReachabilityChangedNotification object: nil];
    [[ NSNotificationCenter defaultCenter]addObserver:self selector:@selector(enteredForeground:) name:UIApplicationWillEnterForegroundNotification object:nil ];  

    
    //Change the host name here to change the server your monitoring
    self.serverToMonitor = self.serverToMonitor.length ? self.serverToMonitor : @"www.apple.com";
    hostReach = [TPReachability reachabilityWithHostName: self.serverToMonitor];
    [hostReach startNotifier];
    //[self updateInterfaceWithReachability: hostReach];
    
    internetReach = [TPReachability reachabilityForInternetConnection];
    [internetReach startNotifier];
    //[self updateInterfaceWithReachability: internetReach];
    
    wifiReach = [TPReachability reachabilityForLocalWiFi];
    [wifiReach startNotifier];
    //[self updateInterfaceWithReachability: wifiReach];
    
    [ self updateCurrentStatus ];
}

- ( void )setWifiRequiredForPlay:( BOOL)wifiRequiredForPlay;
{
    if (_wifiRequiredForPlay != wifiRequiredForPlay) {
        _wifiRequiredForPlay = wifiRequiredForPlay;
        [ [ NSUserDefaults standardUserDefaults]setObject:[ NSNumber numberWithBool:wifiRequiredForPlay] forKey:@"wifiRequiredForPlay" ];
        [ [ NSUserDefaults standardUserDefaults] synchronize ];
    }
}

- ( void )updateCurrentStatus;
{
    switch([ hostReach currentReachabilityStatus])
    {
        case TPNotReachable:
            DLog(@"hostReach notReachable");
            break;
        case TPReachableViaWiFi:
            DLog(@"hostReach ReachableViaWiFi");
            break;
        default:
            DLog(@"hostReach something else %i", [ hostReach currentReachabilityStatus]);
    }
    switch([ internetReach currentReachabilityStatus])
    {
        case TPNotReachable:
            DLog(@"internetReach notReachable");
            break;
        case TPReachableViaWiFi:
            DLog(@"internetReach ReachableViaWiFi");
            break;
        default:
            DLog(@"internetReach something else %i", [ internetReach currentReachabilityStatus]);
    }
    switch([ wifiReach currentReachabilityStatus])
    {
        case TPNotReachable:
            DLog(@"wifiReach notReachable");
            break;
        case TPReachableViaWiFi:
            DLog(@"wifiReach ReachableViaWiFi");
            break;
        default:
            DLog(@"wifiReach something else %i", [ wifiReach currentReachabilityStatus]);
    }
    TPNetworkStatus netStatus = [hostReach currentReachabilityStatus];
    self.currentWiFiNetworkStatus = [ wifiReach currentReachabilityStatus];
    if (netStatus == TPNotReachable) {
        netStatus = [internetReach currentReachabilityStatus];
        DLog(@"internetReach currentReachabilityStatus reachable = %@", netStatus == TPNotReachable ? @"NO" : @"YES");
    }
    if (self.wifiRequiredForPlay) {
        netStatus = [wifiReach currentReachabilityStatus];
        DLog(@"wifiReach currentReachabilityStatus reachable = %@", netStatus == TPNotReachable ? @"NO" : @"YES");
    }
    self.currentNetworkStatus = netStatus;
    DLog(@"updating currentStatus reachable = %@", netStatus == TPNotReachable ? @"NO" : @"YES");
}

- ( void )showNoConnectivityAlert;
{
    // TODO When we go to iOS8 only, these will have to be handled differently
    NSString *alertMessage = NSLocalizedString( @"Internet Connectivity is not Available", @"Internet Connectivity is not Available" );
    NSString *alertTitle = NSLocalizedString(@"No Internet Access", @"Internet Connectivity is not Available");
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:alertTitle message:alertMessage  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
}    

- ( void )showRenewedConnectivityAlert;
{
    // TODO When we go to iOS8 only, these will have to be handled differently
    NSString *alertMessage = NSLocalizedString( @"Internet Connectivity is now Available", @"Internet Connectivity is now Available");
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Internet Access", @"Internet Connectivity is now Available") message:alertMessage  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
}   

- ( void )enteredForeground:( NSNotification *)note
{
    [ self compareAndUpdate];
}

//Called by TPReachability whenever status changes.
- (void) reachabilityChanged: (NSNotification* )note
{
	TPReachability* curReach = [note object];
	NSCAssert(curReach != nil, @"curReach was nil in reachabilityChanged");
    if (curReach == hostReach) {
        DLog(@"curReach is hostReach");
    }
    if (curReach == wifiReach) {
        DLog(@"curReach is wifiReach");
    }
    if (curReach == internetReach) {
        DLog(@"curReach is internetReach");
    }
//	NSParameterAssert([curReach isKindOfClass: [TPReachability class]]);
	// do something with this knowledge
//    DLog(@"reachabilityChanged %d", [[ note object]currentReachabilityStatus]);
    // we are getting nlsreachability notifications - I assume from nielsen
    if ([curReach isKindOfClass: [TPReachability class]]) {
        [ self compareAndUpdate];
    }
}

- ( BOOL )networkIsReachable;
{
    return self.currentNetworkStatus != TPNotReachable;
}

- ( void )compareAndUpdate;
{
    BOOL networkWasReachable = [ self networkIsReachable];
    [ self updateCurrentStatus ];
    BOOL networkIsNowReachable = [ self networkIsReachable];
    DLog(@"double checking current reachable = %@ was reachable = %@", networkIsNowReachable ? @"YES" : @"NO", networkWasReachable ? @"YES" : @"NO");
    if( networkWasReachable != networkIsNowReachable )
    {
        DLog(@"Reachability may have changed currently is reachable = %@ was reachable = %@", networkIsNowReachable ? @"YES" : @"NO", networkWasReachable ? @"YES" : @"NO");
       [self.interestedParty reachabilityChanged:networkIsNowReachable ];
        [[ NSNotificationCenter defaultCenter]postNotificationName:kTPReachabilityOfInterestChangedNotification object:nil ];
//        if (networkWasReachable) {
//            [ self showNoConnectivityAlert];
//        }
//        else{ 
//                [ self showRenewedConnectivityAlert];
//        }
    }
}


- ( BOOL )networkIsAvailableForVideoPlay;
{
    if (self.wifiRequiredForPlay ) {
        if([ self networkIsReachable])
        {
            return YES;
        }
        return NO;
    }
    BOOL networkIsNowReachable = [ self networkIsReachable];
    return networkIsNowReachable;
}


@end

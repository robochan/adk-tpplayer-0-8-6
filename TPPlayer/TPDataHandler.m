//
//  TPDataHandler.m
//
//
//  Copyright © 2013, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)
//

#import "TPDataHandler.h"

#import "GDataXMLNode.h"
#import <CommonCrypto/CommonDigest.h>
#import "TPMasterDataManager.h"

@interface TPDataHandler()


@end

@implementation TPDataHandler



- (id)initWithPath:( NSString *)pathName url:( NSString *)url metadata:( id )metadata delegate:( id )delegate selector:( SEL ) selector contentType:( DataManagerContentType)contentType shouldEncodeURL:( BOOL )shouldEncodeURL;
{
    self = [super init];
    if (self) 
    {
        // Initialization code here.
        _pathName = pathName;
        _delegate = delegate;
        _selector = selector;
        _metadata = metadata;
        _url = url;
        _contentType = contentType;
        _shouldEncodeUrl = shouldEncodeURL;
    }
    return self;
}

- (void)cancelRequest
{
    if (self.connection)
    {
        [self.connection cancel];
        self.connection = nil;
    }
    
    self.delegate = nil;
    self.selector = nil;
}




- (void)loadResourceUsingDelegate:(id)aDelegate selector:(SEL)aSelector
{
    self.delegate = aDelegate;
    self.selector = aSelector;
    
    [self loadResource];
}

- (void)loadResource
{    
    DLog(@"in load resource metaData = %@", self.metadata);
    
    NSString *encodedUrl;
    if (self.shouldEncodeUrl) 
    {
        encodedUrl = [self.url stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding];
        DLog(@"encodedURL = %@", encodedUrl);
//       NSString *alternatelyEncodedUrl = (__bridge NSString *)CFURLCreateStringByAddingPercentEscapes( kCFAllocatorDefault,
//                                                                                          (__bridge CFStringRef)self.url,
//                                                                                          NULL,
//                                                                                          (CFStringRef)@"!’\"();:@&=+$,/?%#[]% ",
//                                                                                          kCFStringEncodingUTF8);
//        DLog(@"encodedURL = %@", alternatelyEncodedUrl);
    } 
    else 
    {
        encodedUrl = self.url;
    }
    
    // Send Asynchronous request
    NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:encodedUrl] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:60.0];
    [ theRequest setHTTPShouldUsePipelining:YES ];
    // create the connection with the request
    // and start loading the data
    NSURLConnection *theConnection=[[NSURLConnection alloc] initWithRequest:theRequest delegate:self startImmediately:NO];
    [ theConnection scheduleInRunLoop:[ NSRunLoop currentRunLoop] forMode:NSRunLoopCommonModes];
    [ theConnection start];
    
    if (theConnection) 
    {
        self.receivedData = [NSMutableData data];
        self.connection = theConnection;
    } 
    else 
    {
        // Inform the user that the connection failed.
        NSString *errorString = [NSString stringWithFormat:@"Connection failed! Url - %@", self.url];
        DebugLog(@"%@", errorString);
        
        // Send notificaton
        NSMutableDictionary *errorInfo = [NSMutableDictionary dictionaryWithCapacity:1];
        [errorInfo setObject:errorString forKey:NSURLErrorFailingURLStringErrorKey];
        
        self.error = [NSError errorWithDomain:NSURLErrorDomain code:-1 userInfo:errorInfo];
        
        [self performErrorNotification];
        
    }
}

#pragma mark - URLConnection Delegate

- (void)connection:(NSURLConnection *)aConnection didReceiveResponse:(NSURLResponse *)response
{
    [self.receivedData setLength:0];
    
    if ([response isKindOfClass:[NSHTTPURLResponse class]])
    {
        NSInteger statusCode = [((NSHTTPURLResponse *)response) statusCode];
        
        if (statusCode != 200)
        {
            [self.connection cancel];
            
            // release the connection, and the data object
            self.connection = nil;
            self.receivedData = nil;
            
            // Inform the user that the connection failed.
            NSString *errorString = [NSString stringWithFormat:@"Connection failed! Invalid status %ld Url - %@", (long)statusCode, self.url];
            DebugLog(@"%@", errorString);
            
            // Send notificaton            
            NSMutableDictionary *errorInfo = [NSMutableDictionary dictionaryWithCapacity:1];
            [errorInfo setObject:errorString forKey:NSURLErrorFailingURLStringErrorKey];
            
            self.error = [NSError errorWithDomain:NSURLErrorDomain code:-1 userInfo:errorInfo];
            
            [self performErrorNotification];
            
        }
    }
}

- (void)connection:(NSURLConnection *)aConnection didReceiveData:(NSData *)data
{
    [self.receivedData appendData:data];
}

- (void)connection:(NSURLConnection *)aConnection didFailWithError:(NSError *)anError
{
    // release the connection, and the data object
    self.connection = nil;
    
    // receivedData is declared as a method instance elsewhere
    self.receivedData = nil;
    
    // inform the user
    DebugLog(@"Connection failed! Error - %@ %@", [anError localizedDescription], [[anError userInfo] objectForKey:NSURLErrorFailingURLStringErrorKey]);
    
    // Send notificaton
    self.error = anError;
    [self performErrorNotification];
    
}

- (void)connectionDidFinishLoading:(NSURLConnection *)aConnection
{
    // release the connection
    self.connection = nil;
    
    // do something with the data
    // receivedData is declared as a method instance elsewhere
    DebugLog(@"Succeeded! Url %@ received %lu bytes of data",self.url, [self.receivedData length]);
    /*
     // uncomment to help debug response data
     NSString *datastr = [NSString stringWithUTF8String:[self.receivedData bytes]];
     DebugLog(@"datastr = %@", datastr);
     */
    if (self.contentType == DataManagerContentTypeXml)
    {
        NSError *anError = nil;
        
        self.xmlDocument = [[GDataXMLDocument alloc] initWithData:self.receivedData options:0 error:&anError];
        
        if (self.xmlDocument == nil)
        {
            self.receivedData = nil;
            self.error = anError;
            
            if (anError) {
                [self performErrorNotification];
            }
            else {
                [self performDelegateSelector];
            }
            return;
        }
    }
    if (self.contentType == DataManagerContentTypeJSON)
    {
        NSError *anError = nil;
        self.jsonObjects = [NSJSONSerialization JSONObjectWithData:self.receivedData options:NSJSONReadingMutableContainers error:&anError];
        
        if (self.jsonObjects == nil)
        {
            self.receivedData = nil;
            self.error = anError;
            
            if (anError) {
                [self performErrorNotification];
            }
            else {
                [self performDelegateSelector];
            }
            return;
        }
    }
    // call delegate
    [self performDelegateSelector];

    self.xmlDocument = nil;
    self.receivedData = nil;    
}

- (void) performDelegateSelector
{
    [ [ TPMasterDataManager sharedInstance ] dataHandlerLoaded:self ];
}

- ( void )performErrorNotification
{
    [ [ TPMasterDataManager sharedInstance ] errorCreatingObject:self ];
}

@end

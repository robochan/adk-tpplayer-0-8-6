//
//  TPCategory.m
//
//  Copyright © 2013, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)
//

#import "TPCategory.h"

@implementation TPCategory

- (id)initWithDictionaryFromJSON:(NSDictionary *)categoryDictionary
{
    if( self = [ super init])
    {
        _label = categoryDictionary[@"media$label"];
        _name = categoryDictionary[@"media$name"];
        _scheme = categoryDictionary[@"media$scheme"];
    }
    return self;
}

- ( id )initWithCoder:(NSCoder *)aDecoder
{
    self = [ super init ];
    self.label = [ aDecoder decodeObjectForKey:@"label" ];
    self.name = [ aDecoder decodeObjectForKey:@"name" ];
    self.scheme = [ aDecoder decodeObjectForKey:@"scheme" ];
    return self;
}

- ( void )encodeWithCoder:(NSCoder *)aCoder;
{
    [ aCoder encodeObject:self.label forKey:@"label" ];
    [ aCoder encodeObject:self.name forKey:@"name" ];
    [ aCoder encodeObject:self.scheme forKey:@"scheme" ];
}

@end

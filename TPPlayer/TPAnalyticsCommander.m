//
//  TPAnalyticsCommander.m
//
//  Copyright © 2013, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)
//

#import "TPAnalyticsCommander.h"
#import "TPMetricsTracker.h"

@interface TPAnalyticsCommander()

@property(nonatomic, copy )NSString *uniqueIdentifier;
@property(nonatomic, strong )NSArray *analyticsReporters;

@end

@implementation TPAnalyticsCommander

+( TPAnalyticsCommander *)sharedInstance
{
    static dispatch_once_t once;
    static TPAnalyticsCommander *shared = nil;
    
    dispatch_once( &once, ^{
        shared = [ [ TPAnalyticsCommander alloc ]init ];
        [ shared setup ];
    } );
    return shared;
}



- ( void )makeTrackers;
{
    NSArray *metricsTrackerURLs = [ [ NSBundle mainBundle ]URLsForResourcesWithExtension:@"plist" subdirectory:@"Metrics" ];
    NSMutableArray *tmpArray = [ NSMutableArray array ];
    for (NSURL *metricsURL in metricsTrackerURLs )
    {
        NSDictionary *infoForMetricsTracker = [ [ NSDictionary alloc ]initWithContentsOfURL:metricsURL ];
        if( infoForMetricsTracker )
        {
            NSString *className = infoForMetricsTracker[@"subclassToUse"];
            if (! className) {
                NSString *fileName = [ [ metricsURL lastPathComponent ]stringByDeletingPathExtension ];
                NSString *className = nil;
                if ( [ fileName isEqualToString:@"Omniture" ])  
                {
                    className = @"DevAppOmnitureTracker";
                }
                if ( [ fileName isEqualToString:@"Localytics" ])  
                {
                    className = @"DevAppLocalyticsTracker";
                }
                if ( [ fileName isEqualToString:@"Google" ])  
                {
                    className = @"DevAppGoogleTracker";
                }
            }
            if (className) {
                Class  classToUse = NSClassFromString(className);
                TPMetricsTracker *tracker = [[ classToUse alloc ]initWithInfo:infoForMetricsTracker ];
                tracker.uniqueIdentifier = self.uniqueIdentifier;
                [ tmpArray addObject:tracker];
            }
            // make one for each kind of analytics tracker you are going to use.
        }
    }
    self.analyticsReporters = tmpArray;
    
}

- ( void )setup;
{    
    [ self getUniqueIdentifier ];
    [ self makeTrackers];
    //This is the ONLY event sent to the census object so there is no reason to include it in the AnalyticsCommander trackers array
//    [CSComScore Start];
}

- (  void )getUniqueIdentifier;
{
    self.uniqueIdentifier = [ [NSUserDefaults standardUserDefaults] objectForKey:@"metricsUniqueIdentifier" ];
    if (!self.uniqueIdentifier) {
        CFUUIDRef uuid = CFUUIDCreate(kCFAllocatorDefault );
        CFStringRef uuidString = CFUUIDCreateString(kCFAllocatorDefault, uuid);
        self.uniqueIdentifier = [ NSString stringWithFormat:@"%@", uuidString ];
        [[ NSUserDefaults standardUserDefaults ]setObject:self.uniqueIdentifier forKey:@"metricsUniqueIdentifier" ];
        [ [ NSUserDefaults standardUserDefaults ] synchronize ];
        CFRelease(uuid);
        CFRelease(uuidString);
    }
}

#pragma mark - trampoline any  messages we don't already know about

- ( NSMethodSignature *)methodSignatureForSelector:(SEL)aSelector;
{
    // Check the trampoline itself
    NSMethodSignature *signature = [super methodSignatureForSelector:aSelector];
    if (signature) {
        return signature;
    }
    return  [[self.analyticsReporters lastObject ] methodSignatureForSelector:aSelector ];
}

- ( void )forwardInvocation:(NSInvocation *)anInvocation;
{
    SEL selector = [ anInvocation selector ];
    for( TPMetricsTracker *analyticsReporter in self.analyticsReporters)
    {
        if ([ analyticsReporter respondsToSelector:selector]) {
            [ anInvocation setTarget:analyticsReporter];
            [ anInvocation invoke];
        }
    }
}

#pragma mark - general messages

- ( void )trackPageView:( UIViewController *)reportingViewController;
{
    // tell everyone -- each tracker can decide what they need to do about the event
    DLog( );
    for (TPMetricsTracker *analyticsReporter in self.analyticsReporters)
    {
        [ analyticsReporter trackPageView:reportingViewController ];
    }
}

- ( void )trackVideoView:( UIViewController *)reportingController stage:( NSString *)videoStage;
{
    // tell everyone -- each tracker can decide what they need to do about the event
    DLog( );
    for (TPMetricsTracker *analyticsReporter in self.analyticsReporters)
    {
        [ analyticsReporter trackVideoView:reportingController stage:videoStage ];
    }
}

- ( void )trackVideoView:( UIViewController *)reportingController withTime:( NSUInteger)seconds stage:( NSString *)videoStage;
{
    // tell everyone -- each tracker can decide what they need to do about the event
    DLog( );
    for (TPMetricsTracker *analyticsReporter in self.analyticsReporters)
    {
        [ analyticsReporter trackVideoView:reportingController withTime:seconds stage:videoStage ];
    }
}


- ( void)trackLink:( UIViewController *)reportingController linkName:( NSString *)linkName forType:( NSString *)type;
{
    DLog( );
    for (TPMetricsTracker *analyticsReporter in self.analyticsReporters)
    {
        [ analyticsReporter trackLink:reportingController linkName:linkName forType:type];
    }
}

- (void)trackVideoAdStarted:(UIViewController *)reportingController  adPosition:( NSString *)adPosition additionalInformation:(NSDictionary *)notificationDictionary;
{
    for (TPMetricsTracker *analyticsReporter in self.analyticsReporters)
    {
        [ analyticsReporter trackVideoAdStarted:reportingController  adPosition:adPosition additionalInformation:notificationDictionary ];
    }
}

- (void)trackVideoAdCompleted:(UIViewController *)reportingController  adPosition:( NSString *)adPosition additionalInformation:(NSDictionary *)notificationDictionary;
{
    for (TPMetricsTracker *analyticsReporter in self.analyticsReporters)
    {
        [ analyticsReporter trackVideoAdCompleted:reportingController  adPosition:adPosition additionalInformation:notificationDictionary];
    }
}

- ( void )trackVideoAdPodStarted:(UIViewController *)reportingController adPod:( TPAdPod *)adPod;
{
    for (TPMetricsTracker *analyticsReporter in self.analyticsReporters)
    {
        [ analyticsReporter trackVideoAdPodStarted:reportingController  adPod:adPod ];
    }
}

- ( void )trackVideoAdPodCompleted:(UIViewController *)reportingController adPod:( TPAdPod *)adPod;
{
    for (TPMetricsTracker *analyticsReporter in self.analyticsReporters)
    {
        [ analyticsReporter trackVideoAdPodCompleted:reportingController  adPod:adPod ];
    }
}

- ( void )trackVideoAdStarted:(UIViewController *)reportingController adObject:( TPAdObject *)adObject;
{
    for (TPMetricsTracker *analyticsReporter in self.analyticsReporters)
    {
        [ analyticsReporter trackVideoAdStarted:reportingController  adObject:adObject ];
    }
}

- ( void )trackVideoAdCompleted:(UIViewController *)reportingController adObject:( TPAdObject *)adObject;
{
    for (TPMetricsTracker *analyticsReporter in self.analyticsReporters)
    {
        [ analyticsReporter trackVideoAdCompleted:reportingController  adObject:adObject ];
    }
}

- ( void )trackLogin:( UIViewController *)reportingController stage:( NSString *)loginStage;
{
    for (TPMetricsTracker *analyticsReporter in self.analyticsReporters)
    {
        [ analyticsReporter trackLogin:reportingController stage:loginStage ];
    }
}

- (void) trackSearch:(UIViewController *)reportingViewController searchTerm:(NSString *)searchTerm;
{
    for (TPMetricsTracker *analyticsReporter in self.analyticsReporters)
    {
        [ analyticsReporter trackSearch:reportingViewController searchTerm:searchTerm ];
    }
}

- (void) trackSocialShare:(UIViewController *)reportingController sharedItem:(NSString *)sharedItem;
{
    for (TPMetricsTracker *analyticsReporter in self.analyticsReporters)
    {
        [ analyticsReporter trackSocialShare:reportingController sharedItem:sharedItem ];
    }
}



- ( void )didFinishLaunching;
{
    // tell everyone -- each tracker can decide what they need to do about the event
    DLog( );
   for (TPMetricsTracker * analyticsReporter in self.analyticsReporters)
    {
        [ analyticsReporter didFinishLaunching ];
    }
}

- ( void )applicationWillResignActive;
{
    // tell everyone -- each tracker can decide what they need to do about the event
    for (TPMetricsTracker * analyticsReporter in self.analyticsReporters)
    {
        [ analyticsReporter applicationWillResignActive ];
    }
}


- ( void )applicationDidEnterBackground;
{
    // tell everyone -- each tracker can decide what they need to do about the event
    for (TPMetricsTracker * analyticsReporter in self.analyticsReporters)
    {
        [ analyticsReporter applicationDidEnterBackground ];
    }
}

- ( void )applicationWillEnterForeground;
{
    // tell everyone -- each tracker can decide what they need to do about the event
    for (TPMetricsTracker * analyticsReporter in self.analyticsReporters)
    {
        [ analyticsReporter applicationWillEnterForeground ];
    }
}

- ( void )applicationDidBecomeActive;
{
    // tell everyone -- each tracker can decide what they need to do about the event
    for (TPMetricsTracker * analyticsReporter in self.analyticsReporters)
    {
        [ analyticsReporter applicationDidBecomeActive ];
    }
}

- ( void )applicationWillTerminate;
{
    // tell everyone -- each tracker can decide what they need to do about the event
    for (TPMetricsTracker * analyticsReporter in self.analyticsReporters)
    {
        [ analyticsReporter applicationWillTerminate ];
    }
}



@end

//
//  TPVideoManagerViewController.m
//
//  Copyright © 2013, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)
//

#import "TPVideoManagerViewController.h"
#import <AVFoundation/AVFoundation.h>
#import <CoreMedia/CoreMedia.h>
#import "TPClosedCaptionManager.h"
#import "TPMasterDataManager.h"
#import "TPAnalyticsCommander.h"
#import "TPEndUserServicesAgent.h"
#import "TPAppSpecificInformationManager.h"


typedef  void (^TPFullScreenRemovalBlock)(TPVideoManagerViewController *videoManagerVC);
static void *TPAVPlayerItemStatusContext = &TPAVPlayerItemStatusContext;
static void *TPAdManagerMidrollsAvailableContext = &TPAdManagerMidrollsAvailableContext;
static void *TPAVPlayerAirplayActiveContext = &TPAVPlayerAirplayActiveContext;
static void *TPTimedMetadataAvailableContext = &TPTimedMetadataAvailableContext;
static void *TPPlaybackLikelyToKeepUpContext = &TPPlaybackLikelyToKeepUpContext;
static void *TPBufferEmptyContext = &TPBufferEmptyContext;

@interface TPVideoManagerViewController ()

@property(nonatomic, copy)TPFullScreenRemovalBlock removalBlock;
@property(nonatomic, assign)CGRect originalBounds;
@property(nonatomic, strong)TPPlayerView *playerView;

@property(nonatomic, strong)AVPlayerItem *playerItem;
@property(nonatomic, strong)NSMutableArray *audioOptions;
@property(nonatomic, strong)NSMutableArray *legibleOptions;
@property(nonatomic, strong)id notificationToken;
@property(nonatomic, weak )id timeObserverToken;
//@property(nonatomic, weak )id quartileObserverToken;
@property(nonatomic, assign)float lastPlaybackRate;

@property (strong, nonatomic)TPClosedCaptionManager *closedCaptionManager; // optional

@property(nonatomic, assign)BOOL firstTimeHasPassed;
@property(nonatomic, strong)NSTimer *heartbeatTimer;
@property(nonatomic, strong)NSMutableArray *heartbeatObservers;

@property(nonatomic, strong)NSTimer *concurrencyTimer;

@property(nonatomic, assign, readwrite)NSTimeInterval currentPlaybackTime;
@property(nonatomic, assign, readwrite)NSTimeInterval duration;
@property(nonatomic, assign)NSTimeInterval preScrubTime;
@property(nonatomic, assign)NSTimeInterval postScrubTime;
@property(nonatomic, assign)NSTimeInterval scrubbedPlaybackTime;
@property(nonatomic, assign)BOOL awaitingReturnFromPassedOverAds;

@property(nonatomic, assign)BOOL switchingToFullScreen;
@property(nonatomic, assign)BOOL scrubbedForward;

@property(nonatomic, assign)BOOL seekingIsUnderway;

@property(nonatomic, weak)UIViewController *rootViewController;

@property(nonatomic, assign )CGRect initialFrame;
@property(nonatomic, strong )NSURL *urlForMoviePlayer;
@property(nonatomic, strong)TPIntervalManager *intervalManager;
@property(nonatomic, assign)BOOL shouldShowFullCaptions;

@property(nonatomic, strong)NSDictionary *quartileTimes;
@property(nonatomic, strong)NSMutableDictionary *unsentQuartileTimes;
@property(nonatomic, strong)NSArray *quartileMessages;

@property(nonatomic, assign)BOOL adIsPlaying;
@property(nonatomic, assign)BOOL adIsMidroll;
@property(nonatomic, strong)UIView *externalBaseView;
@property(nonatomic, strong)NSArray *availableLegibleOptions;
@property(nonatomic, assign)BOOL observingPlayerItem;
//@property(nonatomic, assign)BOOL observingAdTimes;
@property(nonatomic, assign)BOOL observingStatus;
@property(nonatomic, assign)BOOL networkIsUnavailable;
@property(nonatomic, assign)CGFloat rateWhenReachabilityChanged;
@property(nonatomic, assign)NSTimeInterval lastPlayheadPosition;
@property(nonatomic, assign)UIBackgroundTaskIdentifier task;

//@property(nonatomic, assign) NSTimeInterval pauseTime;

@end

@implementation TPVideoManagerViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

// this is the initializer we use from TPPlayer
- (id)initWithFrame:(CGRect)frame
{
    self = [ super init ];
    self.shouldShowAdTicks = YES;
    self.clearedToDismiss = YES;
    frame.origin.x = 0.0;
    frame.origin.y = 0.0;
    self.initialFrame = frame;
    if (self.shouldAllowAirplay) {
        [ self checkForExternalScreen];
    }
    return self;
}

- ( void )checkForExternalScreen;
{
    BOOL externalScreenIsConnected = [[ UIScreen screens] count ] > 1;
    if (externalScreenIsConnected)
    {
        [ self screenDidConnect:nil ];
    }
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(screenDidConnect:) name:UIScreenDidConnectNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(screenDidDisconnect:) name:UIScreenDidDisconnectNotification object:nil];
}

- ( void )checkQuartiles:(NSTimer *)quartileTimer;
{
    DLog( );
}


- (void)viewDidLoad
{
    [super viewDidLoad];

    [self.view setFrame:self.initialFrame];
    self.showsFullScreen = self.playsFullScreenOnly;
    [[ NSNotificationCenter defaultCenter]addObserver:self selector:@selector(applicationWillResignActive:) name:UIApplicationWillResignActiveNotification object:nil ];
    self.quartileMessages = @[kTPMoviePassed25PerCentNotification, kTPMoviePassed50PerCentNotification, kTPMoviePassed75PerCentNotification ];
}

- (void)screenDidConnect: (NSNotification *) notification
{
    DLog(@"Screen connected");
    UIScreen *screen = [[UIScreen screens] lastObject];
    [ self updateScreen];
//    if (_displayLink)
//    {
//        [_displayLink removeFromRunLoop:[NSRunLoop currentRunLoop] forMode:NSRunLoopCommonModes];
//        [_displayLink invalidate];
//        self.displayLink = nil;
//    }
//    
//    self.displayLink = [screen displayLinkWithTarget:self selector:@selector(updateScreen)];
//    [_displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSRunLoopCommonModes];
}

- ( void )screenDidDisconnect:( NSNotification *)notification;
{
	DLog(@"Screen disconnected.");
    [ self updateScreen];
//    if (_displayLink)
//    {
//        [_displayLink removeFromRunLoop:[NSRunLoop currentRunLoop] forMode:NSRunLoopCommonModes];
//        [_displayLink invalidate];
//        self.displayLink = nil;
//    }
}

- ( void )updateScreen;
{
    BOOL externalScreenIsConnected = ([[ UIScreen screens] count ] > 1);

    // remove external window if the screen has been disconnected
	if ((!externalScreenIsConnected) && self.externalWindow)
    {
        DLog(@"should take down external screen");
		[ self takeDownExternalScreen ];
    }
	
	// (Re)initialize if there's no output window
	if (externalScreenIsConnected && !self.externalWindow)
    {
        DLog(@"should set up external screen");
		[self setupExternalScreen];
    }
	
	// Abort if we have encountered some weird error
	if (!self.externalWindow) {
        return;
    }
	
	// Go ahead and update
//    [ self updateExternalView:self.view ];
//    [[[self.externalWindow subviews]firstObject]setBackgroundColor:[ UIColor yellowColor]];
}

- ( void )takeDownExternalScreen;
{
    [self.playerView removeFromSuperview ];
    self.playerView.frame = self.view.bounds;
    [ self.view addSubview:self.playerView];
    [ self.playerControlsController targetViewChanged:self.playerView];
    [ self.view bringSubviewToFront:self.playerControlsController.view ];
    self.externalWindow.hidden = YES;
    self.externalWindowViewController = nil;
    self.externalWindow = nil;
    self.externalBaseView = nil;
}

- (void)setupExternalScreen
{
	// Check for missing screen
    BOOL externalScreenIsConnected = ([[ UIScreen screens] count ] > 1);

	if (!externalScreenIsConnected)
    {
        return;
    }
	
	// Set up external screen
	UIScreen *secondaryScreen = [UIScreen screens][1];
    UIScreenMode *screenMode = [secondaryScreen preferredMode];
	CGRect rect = (CGRect){.size = screenMode.size};
	DLog(@"Extscreen size: %@", NSStringFromCGSize(rect.size));
    DLog(@"screen size: %@", NSStringFromCGSize(secondaryScreen.bounds.size));

	// Create new outputWindow
	self.externalWindow = [[UIWindow alloc] initWithFrame:secondaryScreen.bounds];
	self.externalWindow.screen = secondaryScreen;
	self.externalWindow.screen.currentMode = screenMode; // Thanks Scott Lawrence
	[self.externalWindow makeKeyAndVisible];
	self.externalWindow.frame = rect;
    
	// Add base video view to outputWindow
    
    self.externalWindowViewController = [ [ TPExternalWindowViewController alloc ]init ];
	self.externalBaseView = self.externalWindowViewController.view;
    self.externalBaseView.frame = rect;
//    self.externalBaseView.backgroundColor = [ UIColor yellowColor];
    self.externalWindow.rootViewController = self.externalWindowViewController;
//    [self.externalWindow addSubview:self.externalBaseView ];
    if(self.playerView)
    {
        [self.playerView removeFromSuperview ];
        self.playerView.frame = self.externalBaseView.bounds;
        [ self.externalBaseView addSubview:self.playerView];  
        [ self.playerControlsController targetViewChanged:self.view];
    }

    
	// Restore primacy of main window
	[self.view.window makeKeyAndVisible];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- ( void )didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation;
{
    DLog( );
    [ self.closedCaptionManager resetFrameToUse:self.showsFullScreen ];
}

- ( void )viewDidLayoutSubviews;
{
    DLog( );
//    [ self.closedCaptionManager resetFrameToUse:self.showsFullScreen ];
}

- ( void )viewWillAppear:(BOOL)animated;
{
    [ super viewWillAppear:animated ];
    [ self.closedCaptionManager resetFrameToUse:self.showsFullScreen ];
}

- ( void )viewWillDisappear:(BOOL)animated
{
    [ super viewWillDisappear:animated ];
    if (self.switchingToFullScreen) {
        self.switchingToFullScreen = NO;
        return;
    }
    [[ TPMasterDataManager sharedInstance ]cancelRequests:self ];
    if (! self.shouldKeepAlive) {
        [ self stopPlayer ];
    }
}

- (BOOL)canBecomeFirstResponder;
{
    return YES;
}

- ( void )airplayStarted;
{
    DLog( );
//    [ [UIApplication sharedApplication] beginReceivingRemoteControlEvents];
//    [ self becomeFirstResponder];
    [ self.playerControlsController airplayStarted];
}

- ( void )airplayEnded;
{
    DLog( );
//    [ [UIApplication sharedApplication] endReceivingRemoteControlEvents];
//    [ self resignFirstResponder];
   [ self.playerControlsController airplayEnded];
}

//-  (void) remoteControlReceivedWithEvent: (UIEvent *) receivedEvent {
//    if (receivedEvent.type == UIEventTypeRemoteControl) {
//        DLog(@"receivedEvent.subtype = %ld", receivedEvent.subtype );
//       switch (receivedEvent.subtype) {
//            case UIEventSubtypeRemoteControlTogglePlayPause:
//                [self.playerControlsController syncPlayPauseButton];
//                break;
//            case UIEventSubtypeRemoteControlEndSeekingForward:
//                self.scrubbedForward = YES;
//                DLog(@"seeing forward %g", CMTimeGetSeconds(self.playerItem.currentTime));
////                [self.playerControlsController syncPlayPauseButton];
//                break;
//            case UIEventSubtypeRemoteControlEndSeekingBackward:
//                self.scrubbedForward = NO;
//                DLog(@"seeing backwards %g", CMTimeGetSeconds(self.playerItem.currentTime));
////                [self.playerControlsController syncPlayPauseButton];
//                break;
//        }
//    }
//}



- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    DLog(@"clicked Button");
}

- ( void)setUpClosedCaptioning:( TPClosedCaption *)closedCaptionInfo;
{
    if (closedCaptionInfo) {
        self.closedCaptionManager = [[ TPClosedCaptionManager alloc]init ];
        self.closedCaptionManager.closedCaptionInfo = closedCaptionInfo;
    }

}

//  use this version WITHOUT AdManager
- ( void )playVideoWithURL:( NSURL *)movieURL;
{
    
    DLog(@"screens = %lu", (unsigned long)[[UIScreen screens] count] );
    if (movieURL) {
        self.player = [[ AVPlayer alloc]initWithURL:movieURL ];
        self.playerItem = self.player.currentItem;
        [ self setupPlayerView];
        [ self.player addObserver:self forKeyPath:@"externalPlaybackActive" options:0 context:&TPAVPlayerAirplayActiveContext ];

        self.player.allowsExternalPlayback = self.shouldAllowAirplay;
//        self.player.usesExternalPlaybackWhileExternalScreenIsActive = self.shouldAllowAirplay;
        self.urlForMoviePlayer = movieURL;
        [ self.player.currentItem addObserver:self forKeyPath:@"status" options:0 context:&TPAVPlayerItemStatusContext ];
        self.observingStatus = YES;
        self.showsFullScreen = self.playsFullScreenOnly;
    }
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
	if (context == TPAVPlayerItemStatusContext) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (self.playerItem.status == AVPlayerItemStatusReadyToPlay) {
                [ self.playerItem removeObserver:self forKeyPath:@"status"];
                self.observingStatus = NO;
                self.duration = CMTimeGetSeconds(self.playerItem.duration) ;

                if (self.startTime > 0.0) {
                    [ self setPlayerTime:self.startTime];
                }
                if (self.shouldTrackQuartiles) {
                    [ self buildQuartileTimes ];
                }
                [ self finishPreparation];
//                [ self setupPlayerView];
                [ self checkForLegibleOptions ];
                [ self startWatching];
//                [ self setPlayerTime:self.startTime];

                self.playerControlsController.duration  = CMTimeGetSeconds(self.playerItem.duration);
                [[ NSNotificationCenter defaultCenter]postNotificationName:kTPMovieReadyToPlayNotification object:[ NSValue valueWithCMTime:self.playerItem.duration] ];
               [[ NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playbackStalled:) name:AVPlayerItemPlaybackStalledNotification object:nil ];
                // if we implement avqueuePlayer, this will need to change, as we will be dealing with more than one playerItem
                if (!self.observingPlayerItem) {
                    [self.playerItem addObserver:self forKeyPath:@"playbackLikelyToKeepUp" options:0 context:TPPlaybackLikelyToKeepUpContext];
//                    [ self.playerItem addObserver:self forKeyPath:@"playbackBufferEmpty" options:0 context:TPBufferEmptyContext];
                    self.observingPlayerItem = YES;
                }
                
                for( id<HeartbeatObserver>externalObserver in self.externalHeartbeatObservers )
                {
                    [ self addHeartbeatObserver:externalObserver];
                }
                if (! self.adManager) {
                    [self startPlayer];
                }
                else{
                    [ self addHeartbeatObserver: self.adManager];
                    [self.adManager submitAdRequest:nil videoManager:self ];  // we have already set the contentID
                    if ( self.shouldShowAdTicks ) {
                        [[ NSNotificationCenter defaultCenter]addObserver:self selector:@selector(midrollAdTimesChanged:) name:kMidRollAdTimesChanged object:self.adManager ];
//                        [ self.adManager addObserver:self forKeyPath:@"midrollAdTimes" options:0 context:TPAdManagerMidrollsAvailableContext];
//                        self.observingAdTimes = YES;
                    }
                }
            }
            else{
                [ self loadFailed ];
            }
        });
	}
	else {
        if (context == TPAdManagerMidrollsAvailableContext ) {
//            [ self.adManager removeObserver:self forKeyPath:@"midrollAdTimes"];
//            self.observingAdTimes = NO;
//            if (self.adManager.midrollAdTimes.count) {
//                [ self.playerControlsController setUpAdTicks:self.adManager.midrollAdTimes];
//            }
        }
        else{
            if (context == TPAVPlayerAirplayActiveContext) {
                if (self.player.externalPlaybackActive) {
                    [ self airplayStarted];
                }
                else{
                    [ self airplayEnded];
                }
            }
            else{
                if (context == TPTimedMetadataAvailableContext) {
                    NSArray *timedMetadata = self.player.currentItem.timedMetadata;
                    [[ NSNotificationCenter defaultCenter]postNotificationName:kTPTimedMetaDataChangedNotification object:timedMetadata ];
                }
                else{
                    if (context == TPPlaybackLikelyToKeepUpContext) {
                        [ self playbackLikelyToKeepUpChanged ];
                    }
                    else{
                        // This gives unexpected results - not reliable
//                        if (context == TPBufferEmptyContext) {
//                            [ self bufferContentsEmptyChanged ];
//                        }
//                        else
                        {
                            [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
                        }
                    }
               }
            }
        }
	}
}

- ( void )midrollAdTimesChanged:( NSNotification *)notification
{
    if (self.adManager.midrollAdTimes.count) {
        [ self.playerControlsController setUpAdTicks:self.adManager.midrollAdTimes];
    }
}

- ( void )playbackLikelyToKeepUpChanged;
{
    BOOL likelyToKeepUp = self.playerItem.playbackLikelyToKeepUp;
    DLog(@"likelyToKeepUp = %@", likelyToKeepUp ? @"YES" : @"NO");
    if (! likelyToKeepUp) {
        self.lastPlayheadPosition = [ self currentPlaybackTime];
        // turns out you can get this before playerDidReachEnd notification
        // so we check and don't send this if we are pretty much at the end
        NSUInteger absoluteDifference = abs(self.duration - self.lastPlayheadPosition);
        if (absoluteDifference < 2) {
            return;
        }
    }

    dispatch_async(dispatch_get_main_queue(), ^{
        [ self.delegate videoLoading:likelyToKeepUp];
    });

}
- ( void )setPlayerItem:( AVPlayerItem *)playerItem;
{
    if (_playerItem == playerItem) {
        return;
    }
    if (self.observingStatus) {
        [ self.playerItem removeObserver:self forKeyPath:@"status"];
        self.observingStatus = NO;
    }
    if (self.observingPlayerItem) {
        self.observingPlayerItem = NO;
        [ _playerItem removeObserver:self forKeyPath:@"playbackLikelyToKeepUp"];
    }
    _playerItem = playerItem;
}

- ( void )checkForLegibleOptions;
{
    AVAsset *currentAsset = self.playerItem.asset;
    AVMediaSelectionGroup *legibleGroup = [currentAsset mediaSelectionGroupForMediaCharacteristic:AVMediaCharacteristicLegible];
    if ([[legibleGroup options] count] > 0) {
        self.availableLegibleOptions = [AVMediaSelectionGroup mediaSelectionOptionsFromArray:[legibleGroup options] withoutMediaCharacteristics:@[AVMediaCharacteristicContainsOnlyForcedSubtitles, AVMediaCharacteristicIsAuxiliaryContent]];
        for( AVMediaSelectionOption *selectionOption in self.availableLegibleOptions)
        {
            DLog(@"selectionOption = %@", selectionOption);
            DLog(@"selectionOption = %@", selectionOption.propertyList);
            DLog(@"selectionOption  is playable = %@", selectionOption.playable ? @"YES": @"NO");
        }
        // TODO keep checking to see if Apple fixed this, so we don't get false positives
//        if ([self.availableLegibleOptions  count ] > 0) {
//            self.player.closedCaptionDisplayEnabled = YES;
//            if (self.closedCaptionManager) {
//               self.closedCaptionManager = nil;
//            }
//        }
    }
}

- ( void )markSlider;
{
       if ( self.shouldShowAdTicks )
       {
           [ self.playerControlsController setUpAdTicks:self.adManager.midrollAdTimes];
       }
}

// we only get this if they play to the end.
- ( void )playerDidReachEnd:( NSNotification *)notification;
{
    [ self notifyResume];
    [ self stopPlayer];
    [[ NSNotificationCenter defaultCenter]postNotificationName:kTPMoviePlayedToEndNotification object:nil ];
    [ self.delegate moviePlaybackDidFinish:notification  ]; 
    if (self.adManager) {
        [ self.adManager playerDidReachEnd ];
    }
//    else{
//        [ self.delegate moviePlaybackDidFinish:notification  ]; 
//    }

}

- ( void )buildQuartileTimes;
{
    CMTime duration = self.player.currentItem.duration;
    if (CMTIME_IS_VALID(duration)) {
        CMTime firstQuarter = CMTimeMultiplyByFloat64(duration, 0.25);
        CMTime secondQuarter = CMTimeMultiplyByFloat64(duration, 0.50);
        CMTime thirdQuarter = CMTimeMultiplyByFloat64(duration, 0.75);
        self.quartileTimes = @{@"0":[NSValue valueWithCMTime:firstQuarter], @"1":[NSValue valueWithCMTime:secondQuarter], @"2":[NSValue valueWithCMTime:thirdQuarter]};
        [ self restoreQuartiles:self.startTime];
    }
}


- ( void )loadFailed;
{
    DLog(@"player item error = %@", self.playerItem.error);
    UIAlertView *alertView = [[ UIAlertView alloc ]initWithTitle:@"Error" message:@"Problems with Load" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil ];
    [ alertView show ];
}

- ( void )setupPlayerView;
{
    self.playerView = [[ TPPlayerView alloc]initWithFrame:self.view.bounds ];
    self.playerView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    self.playerView.player = self.player;
    
    
    NSString *className = self.playerControlClassName;
    DLog(@"playerControlClassName = %@", self.playerControlClassName );
    Class classToUse = NSClassFromString(className);
    self.playerControlsController = [[ classToUse alloc]initWithPlayerView:self.playerView fullScreen:NO ];
    self.playerControlsController.delegate = self;
    self.playerControlsController.duration = CMTimeGetSeconds(self.player.currentItem.duration);
    self.playerControlsController.interestedParty = self.interestedParty;
    self.playerControlsController.controlsShouldFade = !self.playerControlsRequireTapToHide;
    self.playerControlsController.videoTitle = self.delegate.videoTitle;
    self.playerControlsController.playsFullScreenOnly = self.playsFullScreenOnly;
    self.playerControlsController.view.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    
    if (self.closedCaptionManager) {
        self.shouldShowFullCaptions = UIAccessibilityIsClosedCaptioningEnabled();
        self.playerControlsController.closedCaptionsAreAvailable = YES;
        self.closedCaptionManager.playerView = self.playerView;
    }
    else{
        if (self.inlineClosedCaptionsPresent) {
            self.shouldShowFullCaptions = UIAccessibilityIsClosedCaptioningEnabled();
            self.playerControlsController.closedCaptionsAreAvailable = YES;
        }
    }
    
    if (self.externalWindow) {
        DLog(@"have an external window -- mirroredScreen == main screen = %@", ([self.externalWindow.screen mirroredScreen] == [ UIScreen mainScreen]) ? @"YES" : @"NO" );
                                                                                
        self.playerView.frame = self.externalWindow.bounds;
        [ self.externalBaseView addSubview:self.playerView];
//        self.player.allowsExternalPlayback = NO;
        [self.playerControlsController targetViewChanged:self.view ];
//        self.player.usesExternalPlaybackWhileExternalScreenIsActive = NO;
    }
    else{
        DLog(@"DO NOT have an external window -- externalScreen == mail screen = %@", ([self.view.window.screen mirroredScreen] == [ UIScreen mainScreen]) ? @"YES" : @"NO" );
        self.player.allowsExternalPlayback = self.shouldAllowAirplay;
        [ self.view addSubview:self.playerView];
        [self.playerControlsController targetViewChanged:self.playerView ];
    }

    
    DLog(@"just put playerView in superview playerViewSuperview = %@ baseView is %@, myView subviews = %@", self.playerView.superview, self.externalBaseView, [self.view subviews] );
    if (self.player.externalPlaybackActive) {
        [ self.playerControlsController airplayStarted];
    }
    if( ! self.adManager)
    {
        [ self finishPreparation];
    }
    else
    {
        self.adManager.playerView = self.playerView;
    }
}

- ( NSString *)playerControlClassName;
{
    if (!_playerControlClassName) {
        return @"TPPlayerControls";
    }
    return _playerControlClassName;
}

- ( void )finishPreparation;
{
    // set up returning to zero after we hit the end
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerDidReachEnd:)
                                                 name:AVPlayerItemDidPlayToEndTimeNotification
                                               object:self.playerItem];
    
    [ self addChildViewController:self.playerControlsController];
    self.playerControlsController.view.frame = self.view.bounds;
    [self.view addSubview:self.playerControlsController.view ];
    [ self.playerControlsController didMoveToParentViewController:self ];
    DLog(@"player sublayers = %@", [[self.playerView layer ]sublayers]);
    DLog(@"self.externalWindowViewController.view.superview (window)= %@ mainScreen window = %@ self.externalBaseView.window = %@  self.playerView.superview  = %@ self.externalBaseView = %@", self.externalWindowViewController.view.superview, self.view.window, self.externalBaseView.window, self.playerView.superview, self.externalBaseView );
    DLog(@"player is %@ playerView is %@ playerLayer is %@ window is %@ window is hidden %@ vc view is %@ vc view is hidden = %@ external window is hidden %@ externalWindowViewController.view.superview %@ playerView window = %@ playerView window screen = %@ externalBaseView window screen = %@ ", self.player, self.playerView, [self.playerView layer ], self.externalWindow, self.externalWindow.hidden ? @"YES":@"NO", self.externalWindowViewController.view, self.externalWindowViewController.view.hidden ? @"YES":@"NO", self.externalWindow.hidden ? @"YES":@"NO", self.externalWindowViewController.view.superview, self.playerView.window, self.playerView.window.screen, self.externalBaseView.window.screen  );
}



- ( void )stopVideo;
{
    [ self notifyResume ];
   [self stopAllTimers ];
    
    if (self.adManager) {
//        if (self.observingAdTimes) {
//            [ self.adManager removeObserver:self forKeyPath:@"midrollAdTimes"];
//            self.observingAdTimes = NO;
//        }
        [ self.adManager cleanup ];
    }
    else{
        [ self stopPlayer];
    }
    [[ NSNotificationCenter defaultCenter ]removeObserver:self ];
}


- (void)didEnterFullscreen:(NSNotification *)notification
{
    DLog(@"VideoManager didEnterFullScreenVideo  +++++++++++++++++++++++++++++++++++");
    [ self.delegate videoEnteredFullScreen ];    
}

// in case we want to track it in the future
//- ( void )didExitFullscreen:( NSNotification *)notification
//{
//}

//- ( void )resumePlay
//{
//    DLog( );
//    [ self stopPlayer];
//    [ self setPlayerTime:self.startTime];
//    [self startPlayer];
//}




#pragma - mark - methods for AdManager

//// only use this version with adManager
- ( void )playVideoWithURL:( NSURL *)movieURL adManagerId:( NSString *)adManagerContentId;
{
    
    DLog( );
    if (movieURL) {
        self.player = [[ AVPlayer alloc]initWithURL:movieURL ];
        self.playerItem = self.player.currentItem;
        [ self setupPlayerView];

        [ self.player addObserver:self forKeyPath:@"externalPlaybackActive" options:0 context:&TPAVPlayerAirplayActiveContext ];
        self.player.allowsExternalPlayback = self.shouldAllowAirplay;
//        self.player.usesExternalPlaybackWhileExternalScreenIsActive = YES;
        self.urlForMoviePlayer = movieURL;
        [ self.player.currentItem addObserver:self forKeyPath:@"status" options:0 context:&TPAVPlayerItemStatusContext ];
        
        // The code below causes the video player to ignore the Mute switch
        NSError *_error = nil;
        [[AVAudioSession sharedInstance] setCategory: AVAudioSessionCategoryPlayback error:&_error];
        // End mute switch code
        
        
        self.adManager.shouldPositionForClosedCaptions  = self.shouldShowFullCaptions;
        BOOL useResume = self.startTime > 0;
        self.adManager.shouldSkipPreRoll = self.adManager.shouldSkipPreRoll || useResume;
        self.adManager.adManagerContentID = adManagerContentId;
        //    self.adManager.adSiteId = self.adSiteID;
        }  
}


- (void)playContentMovie;
{
    if (self.externalWindow) {
        self.externalWindow.hidden = NO;
    }
    DLog(@"player sublayers = %@", [[self.playerView layer ]sublayers]);
    DLog(@"self.externalWindowViewController.view.superview (window)= %@ mainScreen window = %@ self.externalBaseView.window = %@  self.playerView.superview  = %@ self.externalBaseView = %@", self.externalWindowViewController.view.superview, self.view.window, self.externalBaseView.window, self.playerView.superview, self.externalBaseView );
    DLog(@"player is %@ playerView is %@ playerLayer is %@ window is %@ window is hidden %@ vc view is %@ vc view is hidden = %@ external window is hidden %@ externalWindowViewController.view.superview %@ playerView window = %@ playerView window screen = %@ externalBaseView window screen = %@ ", self.player, self.playerView, [self.playerView layer ], self.externalWindow, self.externalWindow.hidden ? @"YES":@"NO", self.externalWindowViewController.view, self.externalWindowViewController.view.hidden ? @"YES":@"NO", self.externalWindow.hidden ? @"YES":@"NO", self.externalWindowViewController.view.superview, self.playerView.window, self.playerView.window.screen, self.externalBaseView.window.screen  );
    [ self finishPreparation];

    [ self startPlayer];
}

- ( void )playbackStalled:( NSNotification *)notification;
{
    if([[UIApplication sharedApplication] applicationState] == UIApplicationStateBackground)
    {
        self.task = [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:^(void) {
        }];
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        [ self.delegate videoLoading:self.playerItem.playbackLikelyToKeepUp];
    });
    DLog(@"*&*&*&*&*&*&*&*&*&   playbackStalled  *&*&*&*&*&*&*&*&*&*&  player rate = %g", self.player.rate);
    DLog(@"video stalled player error = %@", self.playerItem.error );

}


- (void)cleanup 
{
    DLog( );
    [ self stopPlayer];
    if (self.clearedToDismiss) {
        [ self.delegate moviePlaybackAndAdsDidFinish ];
    }
    else{
        self.removalBlock = ^(TPVideoManagerViewController *videoManagerVC){
            [ videoManagerVC.delegate moviePlaybackAndAdsDidFinish ];
        };
    }
}

- ( void )notifyResume;
{
    DLog(@"notify resume");
    [[ NSNotificationCenter defaultCenter]postNotificationName:kTPPlayerResumeConditionNotification object:self.aboutID userInfo:@{@"duration" : @(self.duration), @"playbackTime" : @(self.currentPlaybackTime)} ];
    if (self.shouldOfferResume && self.aboutID.length ) {
        [[ TPEndUserServicesAgent sharedInstance ]movieStopped:self.aboutID playheadPosition:self.currentPlaybackTime duration:self.duration ];
        DLog(@"notify resume playback is %g duration is %g", self.currentPlaybackTime, self.duration);
    }
}

- ( void )reachabilityChanged:( BOOL )networkIsReachable;
{
    DLog(@"*&*&*&*&*&*&*&*&*&   reachabilityChanged  *&*&*&*&*&*&*&*&*&*&  player rate = %g network is reachable = %@", self.player.rate, networkIsReachable ? @"YES" : @"NO");
    if (networkIsReachable ) {
        if (self.task) {
            DLog(@"*&*&*&*&*&*&*&*&*&   self.task  *&*&*&*&*&*&*&*&*&*& ");
            if([[UIApplication sharedApplication] applicationState] == UIApplicationStateBackground)
            {
                [[UIApplication sharedApplication] endBackgroundTask:self.task];
                self.task = 0;
            }
        }
        if (self.networkIsUnavailable) {
            self.networkIsUnavailable = NO;
            DLog(@"we didn't have connectivity - now we do");
            if (self.player.rate == 0 && self.rateWhenReachabilityChanged > 0.0 ) {
                [ self startPlayer];
                NSTimeInterval timeToPlay = fmax([ self currentPlaybackTime], self.lastPlayheadPosition);
                [ self setPlayerTime:timeToPlay];
            }
        }
    }
    else{
        self.rateWhenReachabilityChanged = self.player.rate;
        self.networkIsUnavailable = YES;
        DLog(@"we did have connectivity - now we don't  player rate is %g", self.rateWhenReachabilityChanged);
        [self stopPlayer ];  
    }
}

- ( BOOL )prefersStatusBarHidden;
{
    return NO;
}

- ( BOOL)modalPresentationCapturesStatusBarAppearance;
{
    return YES;
}

#pragma - mark AppDelegate notification
- ( void )applicationWillResignActive:( NSNotification *)notification;
{
    DLog(@"resigning active");
    [ self notifyResume];
}


#pragma - mark TPPlayerControlsDelegate related methods (defined in PlayerControls.h )

- ( void )userPausedPlayer;
{
    [ self notifyResume ];
    if( [ self.delegate respondsToSelector:@selector(userPausedPlayer) ] )
    {
        [ self.delegate userPausedPlayer ];
    }
}

- ( void )userSelectedClosedCaptionToggleButton:( BOOL )shouldShowClosedCaptions;
{
    [ [ NSNotificationCenter defaultCenter]postNotificationName:kTPClosedCaptionButtonHitNotification object:nil ];
    if (self.closedCaptionManager) {
        if (shouldShowClosedCaptions) {
            DLog(@"start showing closed captions again");
            self.player.closedCaptionDisplayEnabled = YES;
            self.shouldShowFullCaptions = YES;
            [ self addHeartbeatObserver: self.closedCaptionManager];
            [ self.adManager closedCaptionsAreNowShowing:YES ];
            [ self.closedCaptionManager resetFrameToUse:self.showsFullScreen];
        }
        else{
            DLog(@"stop showing closed captions - regions should be cleared");
            self.shouldShowFullCaptions = NO;
            self.player.closedCaptionDisplayEnabled = NO;
            [ self removeHeartbeatObserver:self.closedCaptionManager];
            [ self.adManager closedCaptionsAreNowShowing:NO ];
        }
    }
    else{
        if(self.inlineClosedCaptionsPresent)
        {
            if (shouldShowClosedCaptions) {
                self.player.closedCaptionDisplayEnabled = YES;
                self.shouldShowFullCaptions = YES;
                [ self.adManager closedCaptionsAreNowShowing:YES ];
            }
            else{
                self.player.closedCaptionDisplayEnabled = NO;
                self.shouldShowFullCaptions = NO;
                [ self.adManager closedCaptionsAreNowShowing:NO ];
            }
        }
        else{
            if ([ self.availableLegibleOptions count ]> 0) {
                if (shouldShowClosedCaptions) {
                    AVMediaSelectionGroup *legibleGroup = [self.playerItem.asset mediaSelectionGroupForMediaCharacteristic:AVMediaCharacteristicLegible];
                    DLog(@"selectedOptions = %@", [ self.playerItem selectedMediaOptionInMediaSelectionGroup:legibleGroup]);
                    self.player.closedCaptionDisplayEnabled = YES;
                    self.shouldShowFullCaptions = YES;
                    [ self.adManager closedCaptionsAreNowShowing:YES ];
                    [self.playerItem selectMediaOption:[self.availableLegibleOptions firstObject] inMediaSelectionGroup:legibleGroup];
                    DLog(@"selectedOptions = %@", [ self.playerItem selectedMediaOptionInMediaSelectionGroup:legibleGroup]);
                }
                else{
                    self.shouldShowFullCaptions = NO;
                    self.player.closedCaptionDisplayEnabled = NO;
                    [self.playerItem selectMediaOption:nil inMediaSelectionGroup:[self.playerItem.asset mediaSelectionGroupForMediaCharacteristic:AVMediaCharacteristicLegible]];
                }
            }
        }
    }
}

- ( void )userSelectedDoneButton;
{
    if( [ self.delegate respondsToSelector:@selector(userSelectedDoneButton) ] )
    {
        [ self.delegate userSelectedDoneButton ]; // just for tracking
    }
    if (self.clearedToDismiss) {
        [ self stopVideo ]; 
        [ self.delegate moviePlaybackAndAdsDidFinish ];
    }
    else{
        self.removalBlock = ^(TPVideoManagerViewController *videoManagerVC){
            [ videoManagerVC stopVideo ]; 
            [ videoManagerVC.delegate moviePlaybackAndAdsDidFinish ];
        };
    }

}

- ( void )userSelectedAspectToggleButton;
{
    if (self.playerView.videoFillMode == AVLayerVideoGravityResizeAspect) {
        self.playerView.videoFillMode = AVLayerVideoGravityResizeAspectFill;
    }
    else{
        self.playerView.videoFillMode = AVLayerVideoGravityResizeAspect;
    }
    [ self.closedCaptionManager resetFrameToUse:self.showsFullScreen ];
}


-(NSUInteger)supportedInterfaceOrientations{
    DLog(@"showsFullScreen = %@", self.showsFullScreen ? @"YES" : @"NO");
    return (self.showsFullScreen || self.playsFullScreenOnly) ? UIInterfaceOrientationMaskLandscape : UIInterfaceOrientationMaskAll;
}

- ( BOOL )shouldAutorotate;
{
    return YES;
}

- ( void )userSelectedFullScreenButton:( BOOL )shouldShowFullScreen;
{
    DLog(@"shouldShowFullScreen = %@", YES_OR_NO(shouldShowFullScreen));
    DLog(@"movie natural size before %@", NSStringFromCGSize(self.playerView.playerNaturalSize));
    DLog(@"view before %@", NSStringFromCGSize(self.playerView.bounds.size));
    self.originalBounds = self.view.bounds;
    self.switchingToFullScreen = YES;

    if (shouldShowFullScreen) {
        
        // we only make a custom viewController to have control over autorotate
        self.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        self.modalPresentationStyle = UIModalPresentationFullScreen;
        self.showsFullScreen = shouldShowFullScreen;
         [ self.delegate presentFullScreenVideoManagerVC:self ];
        CGRect frame = CGRectMake(0,0, CGRectGetWidth(self.view.bounds), CGRectGetHeight(self.view.bounds));
        self.playerView.frame = frame;
            DLog(@"fullScreenPresented my view = %@ my view view = %@", self.view, self.view);
            DLog(@"fullScreenPresented player frame = %@ player view superview = %@", NSStringFromCGRect(self.playerView.frame), self.playerView.superview);
  
        [ self didEnterFullscreen:nil ];
        DLog(@"my view = %@ my view self.videoWrapperView = %@", self.view, self.view);
        DLog(@"player frame = %@ player view superview = %@", NSStringFromCGRect(self.playerView.frame), self.playerView.superview);
//        DLog(@"self.fullScreenViewController.view = %@ self.fullScreenViewController.view superview = %@", NSStringFromCGRect(self.fullScreenViewController.view.frame), self.fullScreenViewController.view.superview);
        DLog(@"player is hidden %@ ",self.playerView.hidden ? @"YES"  : @"NO" );
        
    }
    else
    {
            [ self stopShowingFullscreen ];
    }
    [ self.closedCaptionManager resetFrameToUse:self.showsFullScreen ];
}

- ( void )setClearedToDismiss:(BOOL)clearedToDismiss;
{
    _clearedToDismiss = clearedToDismiss;
    if (clearedToDismiss) {
        if (self.removalBlock) {
            self.removalBlock(self);
        }
    }
}

- ( void )stopShowingFullscreen;
{
    if (self.showsFullScreen)
    {        
        if (! self.playsFullScreenOnly) {
            self.showsFullScreen = NO;
            self.view.transform = CGAffineTransformIdentity;
            self.view.frame = self.originalBounds;
            self.playerView.frame = self.originalBounds;
            [ self.delegate restoreFullScreenVideoManagerVC:self ];

            DLog(@"movie natural size after %@", NSStringFromCGSize(self.playerView.playerNaturalSize));
        }
        [ self.delegate userExitedFullscreen ];
    }
}

#pragma  - mark time tracking related methods

- ( void )setupHeartbeatObservers;
{
    if (self.intervalManager) {
        [ self.heartbeatObservers addObject:self.intervalManager ];
        [ self.intervalManager start];
    }
}

- ( NSMutableArray *)heartbeatObservers;
{
    if( ! _heartbeatObservers)
    {
        _heartbeatObservers = [ [NSMutableArray alloc ]init ];
    }
    return _heartbeatObservers;
}

- ( void )addHeartbeatObserver:( id )newObserver;
{
    [ self.heartbeatObservers addObject:newObserver];
    [ newObserver heartbeatStarted];
}

- ( void )removeHeartbeatObserver:( id )observerToRemove;
{
    [ observerToRemove heartbeatStopped];
    [ self.heartbeatObservers removeObject:observerToRemove];
}

- ( void )stopAllTimers;
{
    DLog(@"videoManagerPlayer = %@", self);
    [ self stopConcurrencyTimer];

    if (self.timeObserverToken) {
        [ self stopWatching];
        [ self unlockConcurrency ];
    }
    if (self.heartbeatObservers.count) {
        [ self.heartbeatObservers removeAllObjects];
    }

    if (self.intervalManager) {
        [ self.intervalManager stopTiming];
    }
}

/*
- ( BOOL)timeHasPassedTimeMark:( NSArray *)times withMarker:(NSUInteger)timeMarker;
{
    DLog( );
	NSUInteger count = [times count];
	if (timeMarker < count)
    {
		NSUInteger timecode = [[times
                                objectAtIndex:timeMarker] unsignedIntValue];
		if (self.currentPlaybackTime >= timecode)
        {
            NSNumber *timeOfInterest =  [ times objectAtIndex:timeMarker];
            timeOfInterest = [ NSNumber numberWithUnsignedInt:NSIntegerMax ];
			return YES;
		}
	}
	return NO;
}
 */

- ( void )intervalFired:( TPInterval *)interval;
{
    DLog( );
    [ self.delegate intervalFired:interval ];
}

- ( void )prepareForLiveStream:( BOOL)videoIsLiveStream;
{
    // if there are other things you want to do differently for live streaming, put them here
    self.shouldTrackQuartiles = !videoIsLiveStream;
    self.shouldOfferResume = NO;
    if (videoIsLiveStream) {
        self.intervalManager = [[ TPIntervalManager alloc]initWithIntervals:self.intervalsToMonitor  ];
        self.intervalManager.delegate = self;
    }
}


- ( void )adStarted:( NSDictionary *)notificationDictionary adPosition:( NSString *)adPosition;
{
    DLog( );
    [ self.delegate adStarted:notificationDictionary adPosition:adPosition ];
}

- ( void )adEnded:( NSDictionary *)notificationDictionary adPosition:( NSString *)adPosition;
{
    DLog( );
    [ self.delegate adEnded:notificationDictionary adPosition:adPosition];
}

- ( void )slotStarted:( NSDictionary *)notificationDictionary adPosition:( NSString *)adPosition;
{
    DLog( );
    [self.playerControlsController adSlotStarted:adPosition ];

}

// would like to use slotEnded to activate/deactivate controls, but we get it for overlay ads too.
- ( void )slotEnded:( NSDictionary *)notificationDictionary adPosition:( NSString *)adPosition;
{
    DLog( );
    if (self.awaitingReturnFromPassedOverAds && ![adPosition isEqualToString:@"Overlay"]) {
        self.awaitingReturnFromPassedOverAds = NO;
        [ self startPlayer];
    }
    [self.playerControlsController adSlotEnded:adPosition ];

}

#pragma - mark fancier notifications from adManager
- ( void )slotStarted:( TPAdPod *)adPod;
{
    if ([ self.delegate respondsToSelector:@selector(slotStarted:)]) {
        [ self.delegate slotStarted:adPod];
    }
    [[ NSNotificationCenter defaultCenter]postNotificationName:kTPAdSlotStartedNotification object:adPod ];
}
- ( void )slotEnded:( TPAdPod *)adPod;
{
    if ([ self.delegate respondsToSelector:@selector(slotEnded:)]) {
        [ self.delegate slotEnded:adPod];
    }
    [[ NSNotificationCenter defaultCenter]postNotificationName:kTPAdSlotEndedNotification object:adPod ];

}
- ( void )adStarted:( TPAdObject *)adObject;
{
    if ([ self.delegate respondsToSelector:@selector(adStarted:)]) {
        [ self.delegate adStarted:adObject];
    }
    [[ NSNotificationCenter defaultCenter]postNotificationName:kTPAdStartedNotification object:adObject];
}
- ( void )adEnded:( TPAdObject *)adObject;
{
    if ([ self.delegate respondsToSelector:@selector(adEnded:)]) {
        [ self.delegate adEnded:adObject];
    }
    [[ NSNotificationCenter defaultCenter]postNotificationName:kTPAdEndedNotification object:adObject];
}

#pragma - mark with AVPlayer, this class handles the player based on user interaction with the controls

- ( void )stopPlayer;
{
    if (self.player.rate != 0.0) {
        [ self.player pause];
        if (self.intervalManager) {
            [ self.intervalManager pause];
        }
        NSDictionary *relatedInfo = @{@"playerRate":[ NSNumber numberWithFloat:self.player.rate], @"currentTime":[ NSValue valueWithCMTime:self.player.currentTime]};
        [[ NSNotificationCenter defaultCenter]postNotificationName:kTPPlayerDidChangeRateNotification object:relatedInfo ];
    }
}

- ( void )startPlayer;
{
    if (self.player.rate == 0.0) {
        if (self.adManager && self.scrubbedForward) {
            DLog(@"scrubbed");
            self.scrubbedForward = NO;
            self.awaitingReturnFromPassedOverAds = YES;
            [ self.adManager userScrubbedForward:self.postScrubTime playbackTimeAtStart:self.preScrubTime];
            return;
        }
        else{
            CMTime currentTime = self.player.currentTime;
            [ self.player play];
            if (self.intervalManager) {
                DLog(@"restarting interval manager");
                [ self.intervalManager restart];
            }
            if (0 == CMTimeCompare(currentTime, kCMTimeZero)) {
                [ self.delegate videoStartedPlayingFromBeginning];
            }
            NSDictionary *relatedInfo = @{@"playerRate":[ NSNumber numberWithFloat:self.player.rate], @"currentTime":[ NSValue valueWithCMTime:currentTime]};
            [[ NSNotificationCenter defaultCenter]postNotificationName:kTPPlayerDidChangeRateNotification object:relatedInfo ];
        }
        self.scrubbedForward = NO;
    }
}

- ( TPPlayerConfiguration *)configuration;
{
    if (!_configuration) {
        self.configuration = [[ TPPlayerConfiguration alloc]init ];
    }
    return _configuration;
}



- (void)stopLoadingAnimationAndHandleError:(NSError *)error
{
    //	[[self loadingSpinner] stopAnimation:self];
    //	[[self loadingSpinner] setHidden:YES];
	if (error)
	{
		[self presentError:error ];
	}
}


- ( void )presentError:( NSError *)error;
{
    // TODO put an alertView here
}

// support for controls

- (NSTimeInterval)currentPlaybackTime
{
    DLog(@"currentTime = %g currentItem time =  %g", CMTimeGetSeconds(self.player.currentTime), CMTimeGetSeconds(self.player.currentItem.currentTime));
    return CMTimeGetSeconds(self.player.currentTime);
}

- ( void )addEndItemObserverForPlayerItem;
{
    
}

- ( void )startWatching;
{
    if (self.timeObserverToken)
    {
        return;
    }
	
	__weak TPVideoManagerViewController *weakSelf = self;
	self.timeObserverToken = [self.player addPeriodicTimeObserverForInterval:CMTimeMakeWithSeconds(0.5f, NSEC_PER_SEC) queue:dispatch_get_main_queue() usingBlock:
                              ^(CMTime time) {
                                  [weakSelf playerTimeDidChange:time ];
                              }];
    
    [ self.heartbeatObservers enumerateObjectsUsingBlock:^(id<HeartbeatObserver> heartbeatObserver, NSUInteger idx, BOOL *stop) {
        [ heartbeatObserver heartbeatStarted ];
    }];
    // this doesn't seem to be useful when the user has scrubbed - replaced with different approach
//    if (self.shouldTrackQuartiles) {
//        self.quartileObserverToken = [ self.player addBoundaryTimeObserverForTimes:self.quartileTimes queue:nil usingBlock:^{
//            CMTime time = weakSelf.player.currentTime;
//            [weakSelf playerBoundaryDidChange:time];
//        }];
//    }
}

- (void)playerTimeDidChange:(CMTime)time ;
{
//    DLog(@"playerTimeDidChange  time = %g", CMTimeGetSeconds(time) );
    [self.playerControlsController playerTimeDidChange:CMTimeGetSeconds(time)];
    if (self.shouldTrackQuartiles && self.unsentQuartileTimes.count) {
        [ self checkRemainingQuartiles:time];
    }
    [ self.heartbeatObservers enumerateObjectsUsingBlock:^(id<HeartbeatObserver> heartbeatObserver, NSUInteger idx, BOOL *stop) {
        [ heartbeatObserver pulseHeartbeat:time ];
    }];
}

- ( void )checkRemainingQuartiles:( CMTime )time;
{    
    NSMutableArray *quartilesToRemove = [@[]mutableCopy];
//    DLog(@"time = %g", CMTimeGetSeconds(time) );
    [ self.unsentQuartileTimes enumerateKeysAndObjectsUsingBlock:^(NSString *key, NSValue *timeAsValue, BOOL *stop) {
//        DLog(@"timeAsValue = %g", CMTimeGetSeconds([ timeAsValue CMTimeValue ]));
        BOOL timeAtIndexIsLessThanPlayhead = CMTIME_COMPARE_INLINE([ timeAsValue CMTimeValue ], <=, time );
        if( timeAtIndexIsLessThanPlayhead)
        {
            NSUInteger quartileIndex = [ key integerValue];
            NSString *quartileMessage = self.quartileMessages[quartileIndex];
            [[ NSNotificationCenter defaultCenter]postNotificationName:quartileMessage object:timeAsValue ];
            [self.delegate timeHasPassedQuartile:quartileIndex ];
            [ quartilesToRemove addObject:key ];
            DLog(@"quartileIndex = %lu", (unsigned long)quartileIndex );
        }
    }];
    if (quartilesToRemove.count) {
        [ self.unsentQuartileTimes removeObjectsForKeys:quartilesToRemove ];
    }
}

- ( void )restoreQuartiles:( NSTimeInterval)baseTime;
{
    NSMutableDictionary *restoredQuartiles = [@{}mutableCopy];
    [ self.quartileTimes enumerateKeysAndObjectsUsingBlock:^(NSString *indexAsString, NSValue *quartileTimeAsValue, BOOL *stop) {
        BOOL timeAtQuartileIsGreaterThanPlayhead = CMTIME_COMPARE_INLINE([ quartileTimeAsValue CMTimeValue ], >, CMTimeMakeWithSeconds(baseTime, NSEC_PER_SEC ) );
        if( timeAtQuartileIsGreaterThanPlayhead)
        {
            restoredQuartiles[indexAsString] = quartileTimeAsValue;
        }
    }];
    self.unsentQuartileTimes = restoredQuartiles;
}

- ( void )stopWatching;
{
	if (self.timeObserverToken) {
		[self.player removeTimeObserver:self.timeObserverToken];
		self.timeObserverToken = nil;
        [ self.heartbeatObservers enumerateObjectsUsingBlock:^(id<HeartbeatObserver> heartbeatObserver, NSUInteger idx, BOOL *stop) {
            [ heartbeatObserver heartbeatStopped ];
        }];
	}
}

- ( void )startScrubbing;
{
    self.lastPlaybackRate = self.player.rate;
    self.scrubbedForward = NO;
    DLog(@"lastPlaybackRate = %g", self.lastPlaybackRate);
    self.preScrubTime = self.currentPlaybackTime;
    self.postScrubTime = self.preScrubTime;
    
    [ self stopWatching];
    if (self.lastPlaybackRate > 0.0f) {
        [ self stopPlayer];
    }
    if ([self.delegate respondsToSelector:@selector(userStartedScrubbing)]) {
        [ self.delegate userStartedScrubbing];
    }

}

- ( void )stopScrubbing;
{
    DLog(@"before startWatching lastPlaybackRate = %g", self.lastPlaybackRate);
    self.postScrubTime = self.scrubbedPlaybackTime;
    self.scrubbedForward = (self.preScrubTime < self.postScrubTime);
    NSDictionary *scrubValues = @{@"preScrubValue":[ NSValue valueWithCMTime:CMTimeMakeWithSeconds(self.preScrubTime, NSEC_PER_SEC)],@"postScrubValue":[ NSValue valueWithCMTime:CMTimeMakeWithSeconds(self.postScrubTime, NSEC_PER_SEC) ]};
    [[ NSNotificationCenter defaultCenter]postNotificationName:kTPUserScrubbedNotification object:scrubValues ];
    BOOL scrubbedBackward = (self.postScrubTime < self.preScrubTime);
    if (scrubbedBackward) {
        [ self restoreQuartiles:self.postScrubTime ];
    }
    
    [self startWatching ];
    DLog(@"after startWatching lastPlaybackRate = %g", self.lastPlaybackRate);
	if (self.lastPlaybackRate > 0.0f) {
		[self startPlayer ];
	}
}

- ( void )scrub:( NSTimeInterval )sliderValue;
{
    self.scrubbedPlaybackTime = sliderValue;
    [ self setPlayerTime:sliderValue];
}

- ( void )setPlayerTime:( NSTimeInterval)newPlaybackTime;
{
    DLog(@"newPlaybackTime %g", newPlaybackTime);
    CMTime newTime = CMTimeMakeWithSeconds(newPlaybackTime, 1);
    [ self.player seekToTime:newTime toleranceBefore:kCMTimeZero toleranceAfter:kCMTimeZero];
    //    [self.player seekToTime:newTime completionHandler:^(BOOL finished) {
    //        if (finished) {
    //            DLog(@"after seek currentPlaybackTime = %g", self.currentPlaybackTime);
    //        }
    //    }];
}

- ( BOOL )playerIsPlaying;
{
    return (self.player && self.player.rate > 0.0);
}


- ( CGFloat )togglePlayPause;
{   
    DLog( );
    NSValue *playheadPosition = [ NSValue valueWithCMTime:self.player.currentTime ];
    if (self.player.rate != 1.f) {
        [[ NSNotificationCenter defaultCenter]postNotificationName:kTPPlayButtonHitNotification object:playheadPosition ];
		[self startPlayer];
        return 1.0;
    }
    else{
        [[ NSNotificationCenter defaultCenter]postNotificationName:kTPPauseButtonHitNotification object:playheadPosition ];
       [ self stopPlayer];
        [ self userPausedPlayer];
        return 0.0;
    }
    return  self.player.rate;
}

- ( void )userSelectedDone;
{
    [ self stopPlayer];
	[[NSNotificationCenter defaultCenter] removeObserver:self ];
    
}

- (void)dealloc
{
    self.playerItem = nil;  // removes observer
    self.adManager = nil;
    [ self.player removeObserver:self forKeyPath:@"externalPlaybackActive" ];
    [ self stopWatching];
    [ self screenDidDisconnect:nil ];
    self.externalWindow = nil;
    // on the chance we didn't get to the point where removeObserver happened...
	[[NSNotificationCenter defaultCenter] removeObserver:self ];
}

#pragma - mark concurrency support

- ( void )setConcurrencyInfo:(TPConcurrencyInfo *)concurrencyInfo;
{
    _concurrencyInfo = concurrencyInfo;
    if (!_concurrencyInfo) {
        [ self stopConcurrencyTimer ];
        _concurrencyInfo.concurrencyObserver = nil;
    }
    else{
        _concurrencyInfo.concurrencyObserver = self.delegate;
        if (! self.concurrencyTimer ) {
            NSInteger lockInterval = [self.concurrencyInfo.updateLockInterval integerValue];
            self.concurrencyTimer = [NSTimer scheduledTimerWithTimeInterval:lockInterval target:self selector:@selector(updateConcurrency:) userInfo:nil repeats:YES];
        }
    }
}

- ( void )stopConcurrencyTimer;
{
    if (self.concurrencyTimer) {
        [ self.concurrencyTimer invalidate];
        self.concurrencyTimer = nil;
    }
}

- (  void )updateConcurrency:( NSTimer *)timer;
{
    [ [ TPEndUserServicesAgent sharedInstance]updateConcurrency:self.concurrencyInfo];
}

- ( void )unlockConcurrency;
{
    if (self.concurrencyInfo) {
        dispatch_queue_t low = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
        
        dispatch_async(low , ^{
            [[ TPEndUserServicesAgent sharedInstance ]unlockConcurrency:self.concurrencyInfo ];        
        });
    }
}

@end

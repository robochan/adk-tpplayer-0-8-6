//
//  TPUserListItem.m
//  TPPlayer
//
//  Copyright (c) 2014 thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)


#import "TPUserListItem.h"

@implementation TPUserListItem

- ( id )initFromDictionary:( NSDictionary *)entries;
{
    if (self = [super init ]) {
        self.identifier = entries[@"id"];
        self.itemDescription = entries[@"description"];
        self.title = entries[@"title"];
        self.updated = entries[@"updated"];
        self.aboutID = entries[@"pluserlistitem$aboutId"];
//        DLog(@"date = %@",[[ NSDate alloc ]initWithTimeIntervalSince1970:([ self.updated longLongValue ]/1000.0)]);
    }
    return self;
}

@end

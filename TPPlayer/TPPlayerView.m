//
//  TPPlayerView.m
//  tryAVPlayer
//
//  Copyright © 2014, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)

//

#import "TPPlayerView.h"

@implementation TPPlayerView

+ (Class)layerClass
{
	return [AVPlayerLayer class];
}

- (id)initWithFrame:(CGRect)frame {
	self = [super initWithFrame:frame];
	if (self) {
//		self.backgroundColor = [UIColor orangeColor];
		self.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
	}
	return self;
}

- (AVPlayer*)player
{
	return [(AVPlayerLayer*)[self layer] player];
}

- (void)setPlayer:(AVPlayer*)player
{
	[(AVPlayerLayer*)[self layer] setPlayer:player];
}

- ( CGSize )playerNaturalSize;
{
    return self.player.currentItem.presentationSize;
}

/* Specifies how the video is displayed within a player layer’s bounds. 
 (AVLayerVideoGravityResizeAspect is default) */

- (NSString*)videoFillMode {
    return [(AVPlayerLayer *)[self layer] videoGravity];
}

- (void)setVideoFillMode:(NSString *)fillMode
{
	AVPlayerLayer *playerLayer = (AVPlayerLayer*)[self layer];
	playerLayer.videoGravity = fillMode;
}



@end

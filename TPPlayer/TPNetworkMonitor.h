//
//  TPNetworkMonitor.h
//
//  Copyright © 2013, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)
//

#import "TPReachability.h"

@protocol TPReachabilityInterestedParty
- ( void )reachabilityChanged:( BOOL )networkIsReachable;
@end

// When this notification is emitted, there is no object.  It is posted obviously when the reachability changes
//  interestedParty is notified directly using the reachabilityChanged method defined above
//  if you are running iOS7.x you can call showNoConnectivityAlert or showRenewedConnectivityAlert
//  these methods use UIAlertView and cannot be used under iOS8

extern NSString *const kTPReachabilityOfInterestChangedNotification;

@interface TPNetworkMonitor : NSObject

@property(assign ) TPNetworkStatus currentNetworkStatus;
@property(assign ) TPNetworkStatus currentWiFiNetworkStatus;
@property(nonatomic, weak)id<TPReachabilityInterestedParty> interestedParty;
@property(nonatomic, assign)BOOL wifiRequiredForPlay;
@property(nonatomic, strong)NSString *serverToMonitor;  // default is @"www.apple.com"

+( TPNetworkMonitor *)sharedInstance;

- ( BOOL )networkIsReachable;
- ( BOOL )networkIsAvailableForVideoPlay;  // if wifiRequiredForPlay needs currentWiFiNetworkStatus to be available to return YES. Otherwise only internet availability is considered.

//  DEPRECATION WARNING:  these methods use UIAlertView and exhibit less than perfect behavior under iOS8
//  Since this object is not a UIViewController subclass, direct replacement with UIAlertController isn't an option.
- ( void )showNoConnectivityAlert;
- ( void )showRenewedConnectivityAlert;

@end

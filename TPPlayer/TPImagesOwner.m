//
//  TPImagesOwner.m
//
//  Copyright © 2013, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)
//

#import "TPImagesOwner.h"
#import "TPDataHandler.h"
#import "TPLoadedImageRecipient.h"
#import "TPMasterDataManager.h"
#import "TPSmartImage.h"

@interface TPImagesOwner()

@property(nonatomic, strong)NSMutableDictionary *interestedParties;
@property(nonatomic, strong)NSMutableSet *allRecipients;
@property(nonatomic, strong)NSMutableSet *imagesCurrentlyLoading;

@end

@implementation TPImagesOwner
@synthesize images = _images;
@synthesize title =  _title;

- ( UIImage *)imageForRole:( NSString *)role;
{
    NSString *adjustedRole = [ [ TPMasterDataManager sharedInstance] adjustedRole:role ];
    TPSmartImage *aSmartImage = [ self.images objectForKey:adjustedRole ];
    if( aSmartImage )
    {
        return aSmartImage.image;
    }
    // if we don't have one use the low-res version
    aSmartImage = [ self.images objectForKey:role ];
    if( aSmartImage )
    {
        return aSmartImage.image;
    }
    return nil;
}

- ( NSMutableSet *)imagesCurrentlyLoading;
{
    if (!_imagesCurrentlyLoading) {
        _imagesCurrentlyLoading = [NSMutableSet set];
    }
    return _imagesCurrentlyLoading;
}

- ( NSMutableDictionary *)interestedParties;
{
    if (!_interestedParties) {
        _interestedParties = [@{}mutableCopy];
    }
    return _interestedParties;
}

- ( NSMutableSet *)allRecipients;
{
    if (!_allRecipients) {
        _allRecipients = [ NSMutableSet set ];
    }
    return _allRecipients;
}

- ( NSString *)imagePathForRole:( NSString *)role;
{
    NSString *adjustedRole = [ [ TPMasterDataManager sharedInstance] adjustedRole:role ];
    TPSmartImage *aSmartImage = [ self.images objectForKey:adjustedRole ];
    if( aSmartImage )
    {
        return aSmartImage.imagePath;
    }
    // if we don't have one use the low-res version
    aSmartImage = [ self.images objectForKey:role ];
    if( aSmartImage )
    {
        return aSmartImage.imagePath;
    }
    
    return nil;
}

- ( void )setImage:( UIImage *)anImage forRole:( NSString *)role;
{
    // TODO check this to be sure it uses the right role
    NSString *adjustedRole = [ [ TPMasterDataManager sharedInstance] adjustedRole:role ];
    TPSmartImage *aSmartImage = [ self.images objectForKey:adjustedRole ];
    if( aSmartImage )
    {
        aSmartImage.image = anImage;
    }
}

- ( TPSmartImage *)smartImageForRole:( NSString *)role;
{
    DLog(@"role = %@", role);
    NSString *adjustedRole = [ [ TPMasterDataManager sharedInstance] adjustedRole:role ];
    DLog(@"adjustedRole = %@", adjustedRole);
   TPSmartImage *aSmartImage = [ self.images objectForKey:adjustedRole ];
    DLog(@"aSmartImage path = %@", aSmartImage.imagePath);
    if( !aSmartImage )
    {
        aSmartImage = [ self.images objectForKey:role ];
        DLog(@" no smartImage - make one from original role aSmartImage = %@", aSmartImage);
    }
    if (! aSmartImage)
    {
        DLog(@"still no smartImage - make one from defaultForRole:role aSmartImage = %@", aSmartImage);
        TPSmartImage *aSmartImage = [ [ TPMasterDataManager sharedInstance ]defaultForRole:role ];
       return aSmartImage;
    }
    return aSmartImage;
}

- ( void )goGetImageForRole:(NSString *)role metadata:(id)metadata delegate:(id)delegate selector:(SEL)selector activityIndicator:(UIActivityIndicatorView *)activityIndicator;
{
    TPSmartImage *imageToLoad = [ self smartImageForRole:role ];
    UIImage *currentImage = imageToLoad.image;
    if (currentImage) {
        TPDataHandler *simpleDataHandler = [ [ TPDataHandler alloc]init ];
        simpleDataHandler.metadata = metadata;
        simpleDataHandler.returnedObject = currentImage;
        [ delegate performSelector:selector withObject:simpleDataHandler];
        [activityIndicator stopAnimating ];
        return;
    }
    NSString *imagePathForRole = imageToLoad.imagePath;
    if(imagePathForRole )
    {
        //        DLog(@"imagePathForRole = %@", imagePathForRole);
        // if we are already loading this image, don't set up another request
        BOOL weAreCurrentlyLoadingThisImage = [ self.imagesCurrentlyLoading containsObject:imagePathForRole ];
        DLog(@"We are %@ currently loading this image", weAreCurrentlyLoadingThisImage ? @"" : @"NOT" );
        TPLoadedImageRecipient *interestedRecipient = [ self findOrMakeRecipientForDelegate:delegate selector:selector activityIndicator:activityIndicator];
        if (! self.interestedParties[imagePathForRole]) {
            self.interestedParties[imagePathForRole] = [ NSMutableSet set];  // using set because it doesn't take duplicates
        }
        [ self.interestedParties[imagePathForRole] addObject:interestedRecipient ];
        if (activityIndicator) {
            activityIndicator.hidden = NO;
            [activityIndicator startAnimating];
        }
        if(!weAreCurrentlyLoadingThisImage)
        {
            DLog(@"metadata is %@ image path is %@", metadata, imagePathForRole);
            [ self.imagesCurrentlyLoading addObject:imagePathForRole];
            [ [ TPMasterDataManager sharedInstance ] getImageForPath:imagePathForRole  metadata:metadata owner:imageToLoad  delegate:self selector:@selector(imageLoaded:)];
        }
    }
}

- ( TPLoadedImageRecipient *)findOrMakeRecipientForDelegate:( id)delegate selector:( SEL)selector activityIndicator:( UIActivityIndicatorView *)activityIndicator;
{
    for( TPLoadedImageRecipient *loadedRecipient in self.allRecipients)
    {
        if(loadedRecipient == delegate && [ NSStringFromSelector(loadedRecipient.imageLoadSelector) isEqualToString:NSStringFromSelector(selector) ] )
        {
            return loadedRecipient;
        }
    }
    TPLoadedImageRecipient *imageRecipient = [ [ TPLoadedImageRecipient alloc]init];
    imageRecipient.imageLoadingDelegate = delegate;
    imageRecipient.imageLoadSelector = selector;
    imageRecipient.activityIndicator = activityIndicator;
    [ self.allRecipients addObject:imageRecipient];
    return imageRecipient;
}


- ( void )imageLoaded:( TPDataHandler *)dataHandler;
{
    NSString *imagePathToLoad = dataHandler.url;
    [ self.imagesCurrentlyLoading removeObject:imagePathToLoad ];
    NSMutableSet *observersToRemove= [ NSMutableSet set];  // just in case
    for (TPLoadedImageRecipient *imageRecipient in self.interestedParties[imagePathToLoad]) {
        [ imageRecipient.activityIndicator stopAnimating ];
        [ imageRecipient.imageLoadingDelegate performSelectorOnMainThread:imageRecipient.imageLoadSelector withObject:dataHandler waitUntilDone:YES ];
        [ observersToRemove addObject:imageRecipient];
    }
    [ self.interestedParties[imagePathToLoad] minusSet:observersToRemove ];
    [ self cleanUpRecipients:imagePathToLoad];
}

- ( void )cleanUpRecipients:( NSString *)imagePathToLoad;
{
    NSMutableSet *remainingObservers = self.interestedParties[imagePathToLoad];
    if (remainingObservers.count == 0) {
        [self.interestedParties removeObjectForKey:imagePathToLoad ];
    }
    NSMutableSet *remainingRecipients = [ NSMutableSet set];
    for( NSMutableSet *recipients in [self.interestedParties allValues])
    {
        [ remainingRecipients unionSet:recipients ];
    }
    self.allRecipients = remainingRecipients;
}

- ( id )initWithCoder:(NSCoder *)aDecoder
{
    self = [ super init ];
    self.images = [ aDecoder decodeObjectForKey:@"images" ];
    self.title = [ aDecoder decodeObjectForKey:@"title" ];

    return self;
}

- ( void )encodeWithCoder:(NSCoder *)aCoder;
{
    [ aCoder encodeObject:self.images forKey:@"images" ];
    [ aCoder encodeObject:self.title forKey:@"title" ];
}


@end

//
//  TPSmartImage.m
//
//  Copyright © 2013, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)
//

#import "TPSmartImage.h"
#import "TPAppSpecificInformationManager.h"
#import "TPMasterDataManager.h"

@implementation TPSmartImage

+ ( TPSmartImage *)smartImageFromString:( NSString *)url withParentId:( NSString *)parentId;
{
    if (url) {
        TPSmartImage *newSmartImage = [ [ TPSmartImage alloc ]init ];
        newSmartImage.parentID = parentId;
        newSmartImage.imagePath = url;
        return  newSmartImage;
    }
    return nil;
}

- ( UIImage *)image;
{
//    DLog(@"imageLocalURL = %@ ", self.imageLocalURL);
    if ( self.imageLocalURL ) 
    {
        NSString *imageLocalURLPath = [ self.imageLocalURL path];
        NSString *pathForSourceName = [ NSString stringWithFormat:@"%@_sourceName.txt", [ imageLocalURLPath stringByDeletingPathExtension ] ];
        
        NSError *error;
        NSString *sourceName = [ NSString stringWithContentsOfFile:pathForSourceName encoding:NSUTF8StringEncoding error:&error ];
//        DLog(@"imageLocalURL = %@ and sourceName = %@", self.imageLocalURL, sourceName);
        if (sourceName) {
            if ([ sourceName isEqualToString:self.imagePath ]) {
                return [ self freshLocalImage ];
            }
            else
            {
                // remove files for the image and its source name
                NSURL *currentImagePath = [[ TPMasterDataManager sharedInstance ].imagesDirectoryURL URLByAppendingPathComponent:[ self.imageLocalURL lastPathComponent]];
                NSURL *sourceNameImagePath = [[ TPMasterDataManager sharedInstance ].imagesDirectoryURL URLByAppendingPathComponent:pathForSourceName];
                DLog(@"no match  removing files imagePath = %@  sourceNamePath = %@", [ currentImagePath path ], [ sourceNameImagePath path]);
               [[ NSFileManager defaultManager]removeItemAtPath:[sourceNameImagePath path] error:&error ];
                [[ NSFileManager defaultManager]removeItemAtPath:[currentImagePath path] error:&error ];
            }
        }
        
    }
    return nil;
}

- ( UIImage *)freshLocalImage;
{
    if (_image)
    {
        return _image;
    }
    // don't bother if the image has expired
    if ([self localImageIsFresh])
    {
        NSString *imageIdentifier = [ self.imageLocalURL lastPathComponent];
        NSURL *currentImagePath = [[ TPMasterDataManager sharedInstance ].imagesDirectoryURL URLByAppendingPathComponent:imageIdentifier];
//        NSData *imageData = [[ NSData alloc ]initWithContentsOfURL:self.imageLocalURL ];
        NSData *imageData = [[ NSData alloc ]initWithContentsOfURL:currentImagePath ];
        //            DLog(@"imageLocalURL = %@", self.imageLocalURL )
        if( imageData )
        {
            UIImage *localImage = [ UIImage imageWithData:imageData ];
            if (localImage) {
                self.image = localImage;
                if (NSNotFound != [self.role rangeOfString:@"_retina" ].location ) {
                    //this image is retina
                    //                        DLog(@"scale of image is %g", self.image .scale);
                }
                
                return localImage;
            }
        }
    }
    return nil;
}

- ( BOOL )localImageIsFresh;
{
    NSNumber *hoursTilExpiration = [[ TPAppSpecificInformationManager sharedInstance]objectForKey:@"TPImageExpiresInHours" ];
    NSUInteger hoursToUse = hoursTilExpiration ? [ hoursTilExpiration integerValue] : 30 * 24;
    NSDictionary *attributes = [ [ NSFileManager defaultManager ]attributesOfItemAtPath:[ self.imageLocalURL path ] error:nil ];
    NSDate *fileModificationDate = [ attributes fileModificationDate ];
    NSTimeInterval expirationToUse = ( [ fileModificationDate timeIntervalSinceReferenceDate ]) + (hoursToUse * 60 * 60 );
    NSTimeInterval nowTime = [ [ NSDate date ] timeIntervalSinceReferenceDate ];
    
    return  nowTime < expirationToUse;
}


#pragma mark - FNImageLoading protocol

- ( NSString * )pathForImage;
{
    if (! self.parentID) {
        return nil;
    }
    NSString *aPath = [NSString stringWithFormat:@"%@_%@", self.role, self.parentID ];
    if (NSNotFound != [self.role rangeOfString:@"_retina" ].location ) {
        //this image is retina
//        DLog(@"scale of image is %g", self.image .scale);
        aPath = [ aPath stringByAppendingString:@"@2x" ];
    }
    
    return aPath;
}

- ( void )setNewImage:( UIImage *)newImage;
{
    self.image = newImage;
    if (NSNotFound != [self.role rangeOfString:@"_retina" ].location ) {
        //this image is retina
//        DLog(@"scale of image is %g", self.image .scale);
    }
}

- ( id )initWithJsonDictionary:( NSDictionary *)imageDictionary withParentId:( NSString *)parentId
{
    if (self = [ super init ]) {
        _filesize = [[imageDictionary objectForKey:@"plfile$filesize"] integerValue];
        _height = [[imageDictionary objectForKey:@"plfile$height"] integerValue];
        _width  = [[imageDictionary objectForKey:@"plfile$width"] integerValue];
        _url    = [imageDictionary objectForKey:@"plfile$url"];
        _format = [imageDictionary objectForKey:@"plfile$format"];
        _assetTypes = [imageDictionary objectForKey:@"plfile$assetTypes"];
        _isDefault = (BOOL)imageDictionary[@"plfile$isDefault"];
        _contentType    = [imageDictionary objectForKey:@"plfile$contentType"];
        _parentID = parentId;
        
        //        _adManagerId = jsonObjects[@"pl1$freewheelAssetId"];
        // MORE TO COME
        
        // TODO - remove/merge these with above
        _imagePath = _url;
        
        if (_assetTypes.count) {
            NSString *asstTypeForRole = [_assetTypes objectAtIndex:0];
            _role = asstTypeForRole;
        }
        else
        {
            _role = [NSString stringWithFormat:@"image%ldx%ld", (long)_width,(long) _height]; 
        }
    }
    return self;
}


//Notice these objects do not archive the image itself
- ( id )initWithCoder:(NSCoder *)aDecoder
{
    self = [ super init ];
    self.parentID = [ aDecoder decodeObjectForKey:@"parentID" ];
    self.role = [ aDecoder decodeObjectForKey:@"role" ];
    self.imageLocalURL = [ aDecoder decodeObjectForKey:@"imageLocalURL" ];
    self.imagePath = [ aDecoder decodeObjectForKey:@"imagePath" ];
    self.photoKey = [ aDecoder decodeObjectForKey:@"photoKey"];
    self.filesize = [ aDecoder decodeIntegerForKey:@"filesize"];
    self.height = [ aDecoder decodeIntegerForKey:@"height"];
    self.width = [ aDecoder decodeIntegerForKey:@"width"];
    self.url = [ aDecoder decodeObjectForKey:@"url"];
    self.format = [ aDecoder decodeObjectForKey:@"format"];
    self.assetTypes = [ aDecoder decodeObjectForKey:@"assetTypes"];
    self.isDefault = [ aDecoder decodeBoolForKey:@"isDefault"];
    self.contentType = [ aDecoder decodeObjectForKey:@"contentType"];
    return self;
}

- ( void )encodeWithCoder:(NSCoder *)aCoder;
{
    [ aCoder encodeObject:self.parentID forKey:@"parentID" ];
    [ aCoder encodeObject:self.role forKey:@"role" ];
    [ aCoder encodeObject:self.imageLocalURL forKey:@"imageLocalURL" ];
    [ aCoder encodeObject:self.imagePath forKey:@"imagePath" ];
    [ aCoder encodeObject:self.photoKey forKey:@"photoKey" ];
    [ aCoder encodeInteger:self.filesize forKey:@"filesize" ];
    [ aCoder encodeInteger:self.height forKey:@"height" ];
    [ aCoder encodeInteger:self.width forKey:@"width" ];
    [ aCoder encodeObject:self.url  forKey:@"url" ];
    [ aCoder encodeObject:self.format forKey:@"format" ];
    [ aCoder encodeObject:self.assetTypes forKey:@"assetTypes" ];
    [ aCoder encodeBool:self.isDefault forKey:@"isDefault" ];
    [ aCoder encodeObject:self.contentType forKey:@"contentType"];
    
}



@end

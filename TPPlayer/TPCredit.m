//
//  TPCredit.m
//
//  Copyright © 2013, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)
//


#import "TPCredit.h"

@implementation TPCredit


- (id)initWithDictionaryFromJSON:(NSDictionary *)creditDictionary
{
    if( self = [ super init])
    {
        _role = creditDictionary[@"media$role"];
        _value = creditDictionary[@"media$value"];
        _scheme = creditDictionary[@"media$scheme"];
    }
    return self;
}

- ( id )initWithCoder:(NSCoder *)aDecoder
{
    self = [ super init ];
    self.role = [ aDecoder decodeObjectForKey:@"role" ];
    self.value = [ aDecoder decodeObjectForKey:@"value" ];
    self.scheme = [ aDecoder decodeObjectForKey:@"scheme" ];
    return self;
}

- ( void )encodeWithCoder:(NSCoder *)aCoder;
{
    [ aCoder encodeObject:self.role forKey:@"role" ];
    [ aCoder encodeObject:self.value forKey:@"value" ];
    [ aCoder encodeObject:self.scheme forKey:@"scheme" ];
}


@end

//
//  TPCategory.h
//
//  Copyright © 2013, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)
//


#import <Foundation/Foundation.h>

@interface TPCategory : NSObject

@property(nonatomic, strong)NSString *label;
@property(nonatomic, strong)NSString *name;
@property(nonatomic, strong)NSString *scheme;

- (id)initWithDictionaryFromJSON:(NSDictionary *)categoryDictionary;

@end

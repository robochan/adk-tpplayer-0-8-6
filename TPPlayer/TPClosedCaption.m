//
//  TPClosedCaption.m
//
//  Copyright © 2013, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)
//

#import "TPClosedCaption.h"
#import <AVFoundation/AVFoundation.h>

@implementation TPClosedCaption

- ( NSArray *)closeCaptionTimeCollection;
{
    // would store it and check before creating but this will only be called once
    NSMutableArray *timesCollection = [ NSMutableArray array ];
    NSMutableArray *linesArray = [ NSMutableArray array ];
    TPCCTimeInfo *timeInfo;
    CMTime currentBeginTime;

    for( TPCCLine *ccLine in self.closedCaptionLines )
    {
        if( !timeInfo ) // first one
        {
            timeInfo = [[ TPCCTimeInfo alloc ] init];
            [ timesCollection addObject:timeInfo];

            [linesArray addObject:ccLine ];
            timeInfo.timeToShow = ccLine.beginTime;
            currentBeginTime = ccLine.beginTime;
        }
        else // not the first one
        {
            BOOL currentLineHasSameTime = CMTIME_COMPARE_INLINE(ccLine.beginTime, ==, timeInfo.timeToShow);
            if (currentLineHasSameTime) 
            {
                [ linesArray addObject:ccLine];
            }
            else // new time
            {
                // save the array and clear it out
                timeInfo.ccLines = linesArray;  
                linesArray = [ NSMutableArray arrayWithObject:ccLine ];
                  
                // make a new TPCCTimeInfo object
                timeInfo = [[ TPCCTimeInfo alloc ] init];
                [ timesCollection addObject:timeInfo];

                timeInfo.timeToShow = ccLine.beginTime;
                currentBeginTime = ccLine.beginTime;
            }
        }
    }
    // deal with the last line
    timeInfo.ccLines = linesArray;
    
    return timesCollection;
}
@end

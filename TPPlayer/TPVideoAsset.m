//
//  TPVideoAsset.m
//
//  Copyright © 2013, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)
//

#import "TPVideoAsset.h"
#import "TPClosedCaption.h"

@implementation TPVideoAsset


- ( id )initFromPreviewJson:( NSDictionary *)jsonObjects andBasicURLString:( NSString *)basicURLString;
{
    if (self = [ super init ]) {
        _videoURLString = basicURLString;
        _guid = jsonObjects[@"guid"];
//        _adManagerId = jsonObjects[@"pl1$freewheelAssetId"];
        // MORE TO COME
    }
    return  self;
}

- ( id )initWithJsonDictionary:( NSDictionary *)videoDictionary
{
    if (self = [ super init ]) {
        _videoURLString = [videoDictionary objectForKey:@"plfile$url"];
        _bitrate = [[videoDictionary objectForKey:@"plfile$bitrate"] integerValue];
        _duration = [[videoDictionary objectForKey:@"plfile$duration"] floatValue];
        _filesize = [[videoDictionary objectForKey:@"plfile$filesize"] integerValue];
        _height = [[videoDictionary objectForKey:@"plfile$height"] integerValue];
        _width  = [[videoDictionary objectForKey:@"plfile$width"] integerValue];

        //        _adManagerId = jsonObjects[@"pl1$freewheelAssetId"];
        // MORE TO COME
    }
    return  self;
}

- ( id )initWithCoder:(NSCoder *)aDecoder
{
    self = [ super init ];
    self.videoURLString = [ aDecoder decodeObjectForKey:@"videoURLString" ];
    self.inlineClosedCaptionsPresent = [ aDecoder decodeBoolForKey:@"inlineClosedCaptionsPresent"];
    self.needsAuthorization = [ aDecoder decodeBoolForKey:@"needsAuthorization" ];
    self.guid = [ aDecoder decodeObjectForKey:@"guid" ];
    self.adManagerId = [ aDecoder decodeObjectForKey:@"adManagerId" ];
    
    self.bitrate = [ aDecoder decodeIntegerForKey:@"bitrate" ];
    self.duration = [ aDecoder decodeFloatForKey:@"duration" ];
    self.filesize = [ aDecoder decodeIntegerForKey:@"filesize" ];
    self.height = [ aDecoder decodeIntegerForKey:@"height" ];
    self.width = [ aDecoder decodeIntegerForKey:@"width" ];
    return self;
}

- ( void )encodeWithCoder:(NSCoder *)aCoder;
{
    [ aCoder encodeObject:self.videoURLString forKey:@"videoURLString" ];
    [ aCoder encodeBool:self.inlineClosedCaptionsPresent forKey:@"inlineClosedCaptionsPresent" ];
    [ aCoder encodeBool:self.needsAuthorization forKey:@"needsAuthorization" ];
    [ aCoder encodeObject:self.guid forKey:@"guid" ];
    [ aCoder encodeObject:self.adManagerId forKey:@"adManagerId" ];
    
    [ aCoder encodeInteger:self.bitrate forKey:@"bitrate" ];
    [ aCoder encodeFloat:self.duration forKey:@"duration" ];
    [ aCoder encodeInteger:self.filesize forKey:@"filesize" ];
    [ aCoder encodeInteger:self.height forKey:@"height" ];
    [ aCoder encodeInteger:self.width forKey:@"width" ];

}


@end

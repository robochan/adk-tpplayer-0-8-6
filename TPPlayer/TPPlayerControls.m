//
//  TPPlayerControls.m
//  tryAVPlayer
//
//  Copyright © 2014, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)

//

#import "TPPlayerControls.h"


@interface TPPlayerControls ()
@property(nonatomic, assign)BOOL scrubbing;
@property(nonatomic, strong )NSTimer *fadeTimer;
@property(nonatomic, assign)BOOL playerIsInFullScreenMode;

@end

@implementation TPPlayerControls

// using initWithPlayer instead of initWithNib because we want a pointer to the player when we load the view
-(id)initWithPlayerView:(TPPlayerView *)playerView fullScreen:( BOOL )playerIsInFullScreenMode;
{
	self = [super init ];
	if (self) {
		_playerView = playerView;
        _playerIsInFullScreenMode = playerIsInFullScreenMode;
        _controlsShouldFade = YES;
        UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc]  initWithTarget:self action:@selector(handleTapFrom:)];
        _controlsVisibleTapRecognizer = tapRecognizer;
        [ tapRecognizer setNumberOfTapsRequired:1];
        [_playerView addGestureRecognizer:tapRecognizer];
        [[ NSNotificationCenter defaultCenter]addObserver:self selector:@selector(playerStateChanged:) name:kTPPlayerDidChangeRateNotification object:nil ];

	}
	return self;
}

- ( void )viewWillAppear:(BOOL)animated
{
    self.doneButton.titleLabel.text = NSLocalizedString(@"Done", @"Button label to close fullscreen video");
    // Hide the Duration display until we receive the Duration Available notification
    self.durationLabel.hidden = ! self.duration > 0;    
    self.statusLabel.text = self.videoTitle;
    [ self setupFullScreenButton ];    
    if (self.controlsShouldFade) {
        [ self startFadeTimer];
    }
    [ super viewWillAppear:animated];
}

- ( void )setupFullScreenButton;
{
    // on iphone, you can't toggle between full-screen or not
//    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ) {
        if (self.playsFullScreenOnly) {
            [self.fullscreenButton removeFromSuperview ];
        }
//    }
}

- ( void )viewDidDisappear:(BOOL)animated;
{
    [ super viewDidDisappear:animated];
    [ self cancelFadeTimer];
}


- ( void )viewDidLoad;
{
    [ super viewDidLoad];
    self.view.alpha = 0.0f;
    
    self.fullscreenButton.hidden = self.playsFullScreenOnly;
    self.fullscreenButton.selected = self.playerIsInFullScreenMode;
    self.ccButton.hidden = !self.closedCaptionsAreAvailable;
    self.ccButton.selected = NO;
    
    self.sliderWithAdMarks.horizontalSlider.value = 0.0;
    [[ NSNotificationCenter defaultCenter]addObserver:self selector:@selector(appBecameActive:) name:UIApplicationDidBecomeActiveNotification object:nil ];
    if (! self.controlsShouldFade) {
        UITapGestureRecognizer *hideTapGestureRecognizer = [[ UITapGestureRecognizer alloc]initWithTarget:self action:@selector(hideControls:) ];
        [self.view addGestureRecognizer:hideTapGestureRecognizer ];
        hideTapGestureRecognizer.delegate = self;
    }
}

#pragma - mark gestureRecognizer delegate methods
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    if ([touch.view isKindOfClass:[ UIControl class]]) {
        return NO;
    }
    return YES;
}

- ( void )dealloc;
{
    DLog(@"in dealloc");
    [self performSelectorOnMainThread:@selector(removeNotificationCenterObserver) withObject:self waitUntilDone:YES];
    DLog(@"in dealloc");
}

- ( void )removeNotificationCenterObserver;
{
    [[ NSNotificationCenter defaultCenter]removeObserver:self ];
}

- ( void )appBecameActive:( NSNotification *)notification;
{
    [ self syncPlayPauseButton];
}

- ( void )setClosedCaptionsAreAvailable:(BOOL)closedCaptionsAreAvailable;
{
    _closedCaptionsAreAvailable = closedCaptionsAreAvailable;
    if (closedCaptionsAreAvailable) {
        self.ccButton.hidden = !self.closedCaptionsAreAvailable;
    }
}


- (void)handleTapFrom:(UITapGestureRecognizer *)recognizer {
    [self cancelFadeTimer];
    [ UIView animateWithDuration:1.5 animations:^{
        self.view.alpha = 1.0;
    } completion:^(BOOL finished) {
        [ self controlsWereMadeVisible ];
    }];
}

- ( void )controlsWereMadeVisible;
{
    if (self.controlsShouldFade) {
        [ self startFadeTimer];
    }
}

- ( void )hideControls:( UIGestureRecognizer *)gestureRecognizer;
{
    [ UIView animateWithDuration:1.5 animations:^{
        self.view.alpha = 0.0;
    } completion:^(BOOL finished) {
        [ self controlsWereHidden ];
    }];
}

- ( void )controlsWereHidden; 
{
    DLog(@"controlsWereHidden"); 
}


- (void)handleFadeTimer:(NSTimer *)timer
{
	[self removeControls];
}

-(void)removeControls {
    if (self.controlsShouldFade && !self.airplayIsActive) {
        [ self hideControls:nil ];
    }
}

-(void)cancelFadeTimer {
	if (self.fadeTimer) {
		[self.fadeTimer invalidate];
		self.fadeTimer = nil;
	}
}

- ( void )startFadeTimer;
{
    [ self cancelFadeTimer];
    self.fadeTimer = [NSTimer scheduledTimerWithTimeInterval:4.0 target:self selector:@selector(handleFadeTimer:) userInfo:nil repeats:NO ];
}


- (void)playerTimeDidChange:(NSTimeInterval)timeInSeconds 
{
//    DLog(@"timeInSeconds = %g", timeInSeconds);
    if (!self.scrubbing) 
    {
        [self syncScrubberView:timeInSeconds];
    }
}

//- (NSTimeInterval)duration
//{
//    if (_duration == 0.0)
//    {
//        if (CMTIME_IS_NUMERIC(self.playerView.player.currentItem.duration))
//        {
//            _duration = CMTimeGetSeconds(self.playerView.player.currentItem.duration);
//        }
//    }
//    return _duration;
//}

- (void)syncScrubberView:(NSTimeInterval)currentTime {
	if (self.duration) {
        UISlider *slider = self.sliderWithAdMarks.horizontalSlider;
		slider.minimumValue = 0;
		slider.maximumValue = self.duration;
        
		if (!self.scrubbing) {
			slider.value = currentTime;
		}
        
		[self updateScrubberLabelsWithDuration:self.duration andCurrentTime:currentTime];
	} else {
		self.playheadPositionLabel.text = @"-- : --";
	}
}

- (void)updateScrubberLabelsWithDuration:(double)duration andCurrentTime:(double)currentTime {
	NSInteger currentSeconds = ceilf(currentTime);
	double remainingTime = duration - currentTime;
	self.playheadPositionLabel.text = [self formatSeconds:currentSeconds];
    self.durationLabel.text = [ self formatSeconds:remainingTime];
}

- (NSString *)formatSeconds:(NSInteger)value {
	NSInteger seconds = value % 60;
	NSInteger minutes = value / 60;
	return [NSString stringWithFormat:@"%02ld:%02ld", (long)minutes, (long)seconds];
}

#pragma mark - Scrubber Event Handling

- (IBAction)scrubbingDidStart:(id)sender {
    [ self cancelFadeTimer];
    
	self.playheadPositionLabel.text = @"-- : --";
	self.durationLabel.text = @"-- : --";
	self.scrubbing = YES;
    [ self.delegate startScrubbing];
}


- (IBAction)playbackSliderDone:(id)sender {
	self.scrubbing = NO;
    [ self.delegate stopScrubbing];
    if (self.controlsShouldFade) {
        [ self startFadeTimer];
    }
}


// could also observe the value of the slider
- ( IBAction)playbackSliderMoved:(id)sender
{
    if (!self.scrubbing) {
        [ self scrubbingDidStart:nil ];
    }
    else{
        NSTimeInterval sliderValue = self.sliderWithAdMarks.horizontalSlider.value;
        [ self.delegate scrub:sliderValue];
        [ self syncScrubberView:sliderValue ];
    }
}


- ( IBAction )handlePlayAndPauseButton:( UIButton  *)sender;  // if you override this, you should call super.
{
    CGFloat playerRate = [ self.delegate togglePlayPause ];
    BOOL playerIsPaused = (playerRate == 0.0);
    self.playPauseButton.selected = playerIsPaused;
    if (playerRate > 0) {
        [self removeControls];
    }
    if (self.controlsShouldFade) {
        [ self startFadeTimer];
    }
}

- ( void )viewDidAppear:(BOOL)animated;
{
    [ super viewDidAppear:animated];
    [ self syncPlayPauseButton];
}

- ( void )playerStateChanged:( NSNotification *)notification;
{
    [ self syncPlayPauseButton];
}

- ( void )syncPlayPauseButton;
{
    self.playPauseButton.selected = ![self.delegate playerIsPlaying];
    DLog(@"playPauseButton is selected %@", self.playPauseButton.selected ? @"YES" : @"NO");
}



- (IBAction)userHitAspectButton:(id)sender {
    if ([(NSObject *)self.delegate respondsToSelector:@selector(userSelectedAspectToggleButton)]) {
        [ self.delegate userSelectedAspectToggleButton ];
    }
}

- ( IBAction)fullScreenButtonSelected:(id)sender
{
    // take us to fullScreen
//    [self removeControls];
    [ self handleFullscreenButton];

}

-(IBAction)handleFullscreenButton 
{
    // toggle fullScreen mode
    self.playerIsInFullScreenMode = !self.playerIsInFullScreenMode;
    self.fullscreenButton.selected = self.playerIsInFullScreenMode;
    [ self.delegate userSelectedFullScreenButton:self.playerIsInFullScreenMode];
    
//    [ self handleDoneButtonVisibility ];
    [self removeControls];
}

- ( void )handleDoneButtonVisibility;
{
    self.doneButton.hidden = !self.playerIsInFullScreenMode;
}

- (IBAction)doneButtonSelected:(id)sender;
{
    self.fullscreenButton.selected =NO;
    [ self cancelFadeTimer];
    if ([(NSObject *)self.delegate respondsToSelector:@selector(userSelectedDoneButton)]) {
        [ self.delegate userSelectedDoneButton];
    }
}

- ( IBAction)ccButtonSelected:(id)sender;
{
    self.ccButton.selected = !self.ccButton.selected;
    [ self.delegate userSelectedClosedCaptionToggleButton:(self.ccButton.selected  == YES) ];
}


#pragma - mark TPTickMarkOverlayDataSource

- ( NSString *)tickMarkImageName;
{
    return TICK_IMAGE_NAME;

}
- ( NSArray *)tickMarksArray;
{
    return @[];
}

#pragma - mark more tick mark management
//  called when the adManager  has the information
- ( void)setUpAdTicks:( NSArray *)midrollAdTimes;
{
    if (self.duration > 0) {
        NSMutableArray *tickMarkPositions = [ NSMutableArray arrayWithCapacity:midrollAdTimes.count];
        for( NSValue *adTimeAsValue  in midrollAdTimes)
        {
            CMTime adTime = [ adTimeAsValue CMTimeValue ];
            CGFloat timeToShow = CMTimeGetSeconds(adTime) ;
            DLog(@"tick marks timeToShow = %g", timeToShow);
            CGFloat percentForAd = timeToShow / self.duration;
            DLog(@"tick marks percentForAd = %g", percentForAd);
            [tickMarkPositions addObject:[ NSNumber numberWithFloat:percentForAd ] ];
        }
        [ self.sliderWithAdMarks setUpAdTicks:tickMarkPositions ];
    }
    
}

- ( void )adSlotStarted:( NSString *)adPosition;
{
    if (![adPosition isEqualToString:@"Overlay"]) {
        if ( !self.airplayIsActive) {
            self.view.alpha = 0.0;
            self.controlsVisibleTapRecognizer.enabled = NO;
        }

    }
}
- ( void )adSlotEnded:( NSString *)adPosition;
{
    if (![adPosition isEqualToString:@"Overlay"]) {
        if (!self.airplayIsActive) {
            self.controlsVisibleTapRecognizer.enabled = YES;
        }
    }
}

#pragma - mark code related to external display
- ( void )targetViewChanged:(UIView *)newView;
{
    UIView *currentView = self.controlsVisibleTapRecognizer.view;
    if (currentView == newView) {
        return;
    }
    DLog(@"targetViewChanged");

    [ currentView removeGestureRecognizer:self.controlsVisibleTapRecognizer ];
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc]  initWithTarget:self action:@selector(handleTapFrom:)];
    self.controlsVisibleTapRecognizer = tapRecognizer;
    [ tapRecognizer setNumberOfTapsRequired:1];
    [newView addGestureRecognizer:self.controlsVisibleTapRecognizer ];
}

#pragma  - mark code related to airplay
- ( void )airplayStarted;
{
    DLog( );
    [ self setupAirplayRouteName ];
    self.airplayIsActive = YES;
    self.view.alpha = 1.0;
    self.controlsVisibleTapRecognizer.enabled = NO;
    self.airplayActiveView.hidden = NO;
}

- ( void )airplayEnded;
{
    DLog( );
    self.airplayIsActive = NO;
    self.controlsVisibleTapRecognizer.enabled = YES;
    [ self removeControls];
    self.airplayActiveView.hidden = YES;
}

-( void )setupAirplayRouteName;
{
    AVAudioSessionRouteDescription *currentRoute = [[ AVAudioSession sharedInstance ] currentRoute];
    NSArray *outputs = [ currentRoute outputs];
    AVAudioSessionPortDescription *portDescription = [ outputs firstObject];
    self.airplayDeviceName = portDescription.portName;
    
//    self.airplayDeviceName = currentRoute
//    CFDictionaryRef currentRouteDescriptionDictionary = nil;
//    UInt32 dataSize = sizeof(currentRouteDescriptionDictionary);
//    AudioSessionGetProperty(kAudioSessionProperty_AudioRouteDescription, &dataSize, &currentRouteDescriptionDictionary);
//    
//    self.airplayDeviceName = nil;
//    
//    if (currentRouteDescriptionDictionary) {
//        CFArrayRef outputs = CFDictionaryGetValue(currentRouteDescriptionDictionary, kAudioSession_AudioRouteKey_Outputs);
//        if(CFArrayGetCount(outputs) > 0) {
//            CFDictionaryRef currentOutput = CFArrayGetValueAtIndex(outputs, 0);
//            
//            //Get the output type (will show airplay / hdmi etc
////            CFStringRef outputType = CFDictionaryGetValue(currentOutput, kAudioSession_AudioRouteKey_Type);
//            
//            //If you're using Apple TV as your ouput - this will get the name of it (Apple TV Kitchen) etc
//            CFStringRef outputName = CFDictionaryGetValue(currentOutput, @"RouteDetailedDescription_Name");
//            
//            self.airplayDeviceName = (__bridge NSString *)outputName;
//        }
//    }
}



@end

//
//  TPNetworkAwareViewController.m
//
//  Copyright © 2013, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)
//

#import "TPNetworkAwareViewController.h"
#import "TPMasterDataManager.h"

@interface TPNetworkAwareViewController ()

@end

@implementation TPNetworkAwareViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [ [ NSNotificationCenter defaultCenter]addObserver:self selector:@selector(appEnteredForeground:) name:UIApplicationWillEnterForegroundNotification object:nil ];
    
    [ self setup ];
    [ self checkDataLoad ];
}

- (void)removeNotificationCenterObserver
{
    NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
    [notificationCenter removeObserver:self];
}

- (void)dealloc
{
    [self performSelectorOnMainThread:@selector(removeNotificationCenterObserver) withObject:self waitUntilDone:YES];
}


- ( void )appEnteredForeground:( NSNotification *)notification;
{
    [ self checkDataLoad];
}

- ( void )viewWillAppear:(BOOL)animated;
{
    [ super viewWillAppear:animated];
    
    [ TPNetworkMonitor sharedInstance].interestedParty = self;
    // every subclass must ensure dataLoadIsComplete is managed!!
    // we don't want to start loading again if we are currently in the processof loading
    [ self checkDataLoad ];
    BOOL networkIsAvailable = ( [ [ TPNetworkMonitor sharedInstance] networkIsReachable  ]);
    if (!networkIsAvailable) {
        [[ TPNetworkMonitor sharedInstance]showNoConnectivityAlert ];
    }
    [ self adjustUIForReachability:networkIsAvailable ];
}

- (void) viewWillDisappear:(BOOL)animated
{
    [[ TPMasterDataManager sharedInstance ]cancelRequests:self ];
    self.dataLoadingIsInProgress = NO;
    [super viewWillDisappear:animated];
}

- ( void )reachabilityChanged:( BOOL )networkIsReachable;
{
    [ self checkDataLoad];
    [ self adjustUIForReachability:networkIsReachable];
}

- ( void)adjustUIForReachability:( BOOL )networkIsAvailable;
{
    DLog( );
}

- ( void )checkDataLoad;
{
    if ([ self needToLoadData]) 
    {
        if(  [ [ TPNetworkMonitor sharedInstance] networkIsReachable  ])
        {
            [ self startLoadingData];
        } 
    }
}

- ( BOOL )needToLoadData;
{
    if (!self.dataLoadIsComplete) {
        if (self.dataLoadingIsInProgress) {
            return NO;
        }
        return YES;
    }
    else{
        if ([self dataNeedsRefresh]) {
            return YES;
        }
    }
    return NO;
}

- ( BOOL )dataNeedsRefresh;
{
    // subclasses need to override this & check is their contentList isFresh
    // they obviously shouldn't reference super unless they want to return no;
    return NO;
}


- ( void )setup;
{
    //  subclasses override
}

- ( void )startLoadingData
{
    self.dataLoadingIsInProgress = YES;
}

- (void)showAlertView
{
    NSString *networkError = NSLocalizedString(@"Network Error", @"Network Error");
    NSString *networkMessage = NSLocalizedString(@"A network connection could not be established.  Please try again or check your settings", @"");
    NSString *tryAgain = NSLocalizedString(@"Try Again", @"Try Again");
    NSString *cancelButtonTitle = NSLocalizedString(@"Cancel", @"Cancel");
    
    if (NSClassFromString(@"UIAlertController")) {
        UIAlertController *alertController = [ UIAlertController alertControllerWithTitle:networkError message:networkMessage preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *tryAgainAction = [ UIAlertAction actionWithTitle:tryAgain style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            if (  [ TPNetworkMonitor sharedInstance ].currentNetworkStatus == TPNotReachable) 
            {
                [self performSelector:@selector(showAlertView) withObject:nil afterDelay:2.5];
            } 
            else 
            {
                [self startLoadingData];
            }
        }];
        UIAlertAction *cancelAction = [ UIAlertAction actionWithTitle:cancelButtonTitle style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
            DLog(@"user says Cancel");
        }];
        [ alertController addAction:tryAgainAction];
        [ alertController addAction:cancelAction];
        [ self presentViewController:alertController animated:NO completion:nil ];
    }
    else{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:networkError message:networkMessage  delegate:self cancelButtonTitle:cancelButtonTitle otherButtonTitles:tryAgain, nil];
    [alert show];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{    
    if (buttonIndex != [ alertView cancelButtonIndex ] ) {
        if (  [ TPNetworkMonitor sharedInstance ].currentNetworkStatus == TPNotReachable) 
        {
            [self performSelector:@selector(showAlertView) withObject:nil afterDelay:2.5];
        } 
        else 
        {
            [self startLoadingData];
        }
    }
}


@end

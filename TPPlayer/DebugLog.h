//
//  DebugLog.h
//  Created by Kurt Bosselman on 11/17/11.


// Comment out the next line to disable global logging
#define ENABLE_TP_LOGGING     0
#define YES_OR_NO(x) (( x )? @"YES":@"NO")
#ifndef DLog
#ifdef DEBUG 
#define DLog(fmt, ...) NSLog((@"%s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__);

#ifdef ENABLE_TP_LOGGING
#define DebugLog(s, ...) NSLog(s, ##__VA_ARGS__)
#else
#define DebugLog(s, ...)
#endif
#else
#define DebugLog(s, ...)
#define DLog(...)
#endif
#endif
//
//  TPCCTimeInfo.m
//
//
//  Copyright © 2013, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)
//

#import "TPCCTimeInfo.h"
#import "TPCCLine.h"

@implementation TPCCTimeInfo


- ( NSString *)description;
{
    NSString *allCaptions = @"";
    for( TPCCLine *ccLine in self.ccLines)
    {
        NSString *stringToAppend = [ NSString stringWithFormat:@" -- %@ -- ", ccLine.theLine.strings];
        allCaptions = [ allCaptions stringByAppendingString:stringToAppend ];
    }
    
    return [ NSString stringWithFormat:@"timeToShow = %g, numberOfLines = %lu  %@", CMTimeGetSeconds(self.timeToShow), (unsigned long)self.ccLines.count, allCaptions];
}

@end

//
//  TPDataHandler.h
//
//
//  Copyright © 2013, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)
//


// We still need to re-examine how the cache is used.  The temporary files may be useful for the smilData -- the rest of it we don't need.


#import <Foundation/Foundation.h>
//#import "XMLDocumentWrapper.h"

@class GDataXMLDocument;
@class TPDataHandler;

typedef enum 
{
    DataManagerCacheTypeNone = 0,
    DataManagerCacheTypeMemory,
    DataManagerCacheTypeFile,
    DataManagerCacheTypeTmpFile,
    DataManagerCacheTypeInBundle
    
} DataManagerCacheType;

typedef enum 
{
    DataManagerContentTypeData = 0,
    DataManagerContentTypeXml,
    DataManagerContentTypeJSON
} DataManagerContentType;

@interface TPDataHandler : NSObject

@property (nonatomic, retain) NSURLConnection *connection;
@property (nonatomic, retain) NSString *url;
@property (nonatomic, retain) id metadata;
@property (nonatomic) DataManagerContentType contentType;
@property (nonatomic) DataManagerCacheType cacheType;
@property (nonatomic, assign) BOOL shouldEncodeUrl;
@property (nonatomic, retain) id delegate;
@property (nonatomic, assign) SEL selector;
@property (nonatomic, retain) NSMutableData *receivedData;
@property (nonatomic, retain) GDataXMLDocument *xmlDocument;
@property (nonatomic, retain) id jsonObjects;
@property (nonatomic, retain) NSError *error;
@property (nonatomic, retain) id returnedObject;
@property (nonatomic, retain ) NSString *pathName;
@property (nonatomic, retain ) id owner;
@property (nonatomic, assign ) BOOL auxiliary;
@property (nonatomic, strong ) NSString *contentItemSubclass;
@property(nonatomic, strong ) NSString *classToBuild;
@property(nonatomic, assign)NSUInteger minutesToExpiration;

- (id)initWithPath:( NSString *)pathName url:( NSString *)url metadata:( id )metadata delegate:( id )delegate selector:( SEL ) selector contentType:( DataManagerContentType)contentType shouldEncodeURL:( BOOL )shouldEncodeURL;

- (void)cancelRequest;

- (void)loadResourceUsingDelegate:(id)aDelegate selector:(SEL)aSelector;
- (void)loadResource;

@end

//
//  TPTickMarkOverlayView.m
//
//  Copyright © 2013, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)
//
//

#import "TPTickMarkOverlayView.h"
#import <QuartzCore/QuartzCore.h>

@interface TPTickMarkOverlayView()

// these should be percents stored as NSNumbers
@property(nonatomic, strong)NSArray *markLocations;
@property(nonatomic, strong)NSMutableArray *rectsForMarks;
@property(nonatomic, strong)UIImage *tickImage;
@end

@implementation TPTickMarkOverlayView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

// so it will pass the hits through
-(id)hitTest:(CGPoint)point withEvent:(UIEvent *)event 
{
    id hitView = [super hitTest:point withEvent:event];
    if (hitView == self) return nil;
    else return hitView;
}

- ( void )layoutSubviews;
{
    [ self adjustMarkPositioning ];
}


- ( void )makeMarks:( NSArray *)locationsToMark withImage:(UIImage *)tickImage;
{
    self.tickImage = tickImage;
    [ self makeRects:locationsToMark ];
    [ self makeLayers ];
}

- ( void )makeRects:( NSArray *)locationsToMark
{
    NSUInteger widthToUse = self.tickImage.size.width;
    NSInteger heightToUse = self.tickImage.size.height;
    NSInteger yToUse = (self.bounds.size.height - self.tickImage.size.height )/2.0;
    self.markLocations = locationsToMark;
    NSMutableArray *tmpArray =[ NSMutableArray arrayWithCapacity:locationsToMark.count];
    CGFloat sliderLength = self.sliderFrame.size.width;
    CGFloat sliderX = self.sliderFrame.origin.x - (widthToUse/2.0);
    
    for( NSNumber *percentAsNumber in locationsToMark )
    {
        CGFloat percentToUse = [ percentAsNumber floatValue];
        CGFloat xPosition = sliderX + (sliderLength * percentToUse);
        CGRect frameRect = CGRectMake(xPosition, yToUse, widthToUse, heightToUse );
        NSValue *rectAsValue = [ NSValue valueWithCGRect:frameRect ];
        [ tmpArray addObject:rectAsValue];
    }
    self.rectsForMarks = tmpArray;
}

- ( void )makeLayers;
{
    for( NSValue *rectAsValue in self.rectsForMarks)
    {
        CGRect frameRect = [ rectAsValue CGRectValue];
        CALayer *layer = [ CALayer layer ];
        layer.frame = frameRect;
        layer.contents = (id)self.tickImage.CGImage;
//        layer.backgroundColor = [color CGColor ];
        [self.layer addSublayer:layer ];
    }
}

#pragma  - mark subsuequent adjustments ( position and visibility)

- ( void )adjustMarkPositioning;
{
    NSUInteger widthToUse = self.tickImage.size.width;
    CGFloat sliderLength = self.sliderFrame.size.width;
    CGFloat sliderX = self.sliderFrame.origin.x - (widthToUse/2.0);
    
    NSUInteger index = 0;
    for( CALayer *layer in self.layer.sublayers)
    {
        NSValue *rectAsValue = [ self.rectsForMarks objectAtIndex:index];
        CGRect frameRect = [rectAsValue CGRectValue  ];
        
        NSNumber *percentAsNumber = [self.markLocations objectAtIndex:index ]; 
        CGFloat percentToUse = [ percentAsNumber floatValue];
        CGFloat xPosition = sliderX + (sliderLength * percentToUse);
        frameRect.origin.x = xPosition;
        NSValue *newRectValue = [ NSValue valueWithCGRect:frameRect ];
        [ self.rectsForMarks replaceObjectAtIndex:index withObject:newRectValue ];
        
        layer.frame = frameRect;
        index += 1;
    }
}

- ( void )adjustMarkVisibility:( CGRect)thumbRect;
{
//    NSLog(@"adjustMarkVisibility %@", NSStringFromCGRect(thumbRect));
    for( CALayer *layer in self.layer.sublayers)
    {
         CGRect frameRect = layer.frame;
        layer.hidden = CGRectIntersectsRect(thumbRect, frameRect);
    }
}



@end

//
//  TPPlayerViewController.h
//
//  Copyright © 2013, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)
//


#import <UIKit/UIKit.h>
#import "TPVideoProtocols.h"
#import "TPAdManager.h"
#import "TPCustomContentItemHolder.h"
#import "TPMetadataView.h"
#import "TPBookmark.h"
#import "TPBookmarkConsumer.h"
#import "TPEndUserServicesAgent.h"
#import "TPNetworkMonitor.h"

@class TPAdManager;
@class TPContentItem;
@class TPDataHandler;
@class TPVideoAsset;
@class TPIdentityServicesViewController;

extern NSString *const TPPlayerDidReceiveContentItemNotification;

@protocol TPPlayerFullScreenDelegate
@optional // necessary if shouldPlayFullScreenImmediately is YES  - these methods will need to remove the player from the UIViewController heirarchy
- (void)playerCancelled;  // hit cancel button
- (void)playbackDidFinish;

// these are optional under all conditions - implement them if you wish to handle alertViews in another viewController
// this will only be called if you have set delegateShouldHandleLoadFailureAlert to true
- ( void )userReceivedLoadError:( NSString *)errorDescription;

// this will only be called if you have set delegateShouldHandleConcurrencyAlert to true
- ( void )userReceivedConcurrencyError:( NSString *)errorDescription;

// this will only be called if you have set delegateShouldHandleNoWiFiAlert to true
- ( void )userReceivedNoWifiCondition;

@end


@interface TPPlayerViewController : UIViewController<TPURLAdjustmentRequester, TPMetaDataURLAdjustmentRequester, TPCustomContentItemHolder, TPBookmarkConsumer, TPConcurrencyObserver, TPReachabilityInterestedParty>

@property(nonatomic, strong)NSArray *urlAdjusters;
@property(nonatomic, strong)NSArray *metadaUrlAdjusters;
@property(nonatomic, strong)TPContentItem *contentItem;  // can use a subclass of TPContentItem
@property(nonatomic, strong)TPVideoAsset *videoAsset;
@property(nonatomic, strong)TPMetadataView *metadataView;
@property(nonatomic, strong)TPIdentityServicesViewController *identityServicesViewController;  // use a subclass of TPIdentityServices

@property(nonatomic, strong)TPAdManager *adManager; // use a subclass of TPAdManager
@property(nonatomic, weak)id< TPPlayerFullScreenDelegate>delegate;  //  necessary if player goes full-Screen

@property (weak, nonatomic) IBOutlet UIView *imageContainerView;
@property (weak, nonatomic) IBOutlet UIImageView *videoImage;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *imageActivityIndicator;
@property (weak, nonatomic) IBOutlet UIButton *playButton;
@property (weak, nonatomic) IBOutlet UILabel *noContentLabel;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *mainActivityIndicator;
@property(nonatomic, strong)UIView *authorizationOverlay;
@property(nonatomic, assign)BOOL shouldKeepAlive; // under special circumstances, you might need the view to disappear without stopping the player - this is an override that lets you do that.

@property (nonatomic, strong)NSString *videoTitle;

@property(nonatomic, assign)BOOL shouldPlayLiveStream;
@property(nonatomic, assign)BOOL shouldTrackQuartiles;
@property (nonatomic, assign)BOOL shouldOfferResume;  // this value is initialized with the appSpecificInformation value for @"TPResumeSupportedAcrossMultipleDevices".   You can override it for content that is too short to consider resume
@property(nonatomic, assign)BOOL shouldEnforceConcurrency;  // this value is initialized with the appSpecificInformation value for @"TPConcurrencySupported".   You can override it if you don't want this player to consider it for its content

@property(nonatomic, assign)BOOL shouldAllowAirplay;
@property(nonatomic, assign)BOOL suppressErrorAlert;
@property (nonatomic, assign)BOOL userIsAuthorized;
@property(nonatomic, assign, readonly)BOOL movieIsInProgress;
@property(nonatomic, assign)BOOL skipReset;
@property(nonatomic, strong)TPBookmark *bookmark;

// Values for alert that shows up if there play is restricted to use wiFi - not cellular
@property(nonatomic, strong)NSString *noWifiAlertTitle;  // if you don't provide a title, the alert says "WiFi Required" 
@property(nonatomic, strong)NSString *noWifiMessage;  // if you don't provide a message, the alert says "There is currently no WiFi Available."

//  TPPlayer is a wrapper that handles authentication, metadata, the playButton, a background imageView.
//  It manages several other ViewControllers to handle playing the video in addition to controls, closed captions, ads, and analytics 
//  There are basically 3 ways to organize the display
// 1)  The wrapper card is always fullScreen and the video it plays is always fullScreen
//      use isAlwaysFullScreen = YES and playsFullScreenOnly = YES
// 2)  The wrapper card is not fullScreen but the video it plays needs to be able to go back and forth between fullScreen
//      and the size of the TPPlayerViewController's view
//      use isAlwaysFullScreen = NO and playsFullScreenOnly = NO 
// IMPORTANT - ON IPAD - playsFullScreenOnly defaults to NO
// IMPORTANT - ON IPHONE - playsFullScreenOnly defaults to YES so you have to explicitly turn it off if you want to be able to play anything other than fullScreen landscape on iPhone.
// 3)  The wrapper card is not fullScreen, but you want the video to always be  played fullScreen.
//      use isAlwaysFullScreen = NO and shouldPlayFullScreenImmediately = YES;
// Note (isAlwaysFullScreen = YES and playsFullScreenOnly = NO is NOT AN OPTION)

@property(nonatomic, assign)BOOL isAlwaysFullScreen; 
@property(nonatomic, assign)BOOL playsFullScreenOnly;
@property(nonatomic, assign)BOOL shouldPlayFullScreenImmediately;  // plays immediately and returns immediately

@property(nonatomic, copy )NSString *playerControlClassName; // nil unless we want to subclass TPPlayerControls - then must be a subclass of TPPlayerControls
@property(nonatomic, weak)UIViewController *interestedParty;  // usually nil - only here for TPPlayerControls subclasses to use to get messages out - probably to the VC that owns TPPlayerVC
@property(nonatomic, assign)BOOL playerControlsRequireTapToHide; // default is NO, so if you don't use this, the controls default to fading out.  If you set this to YES, a gesture recognizer will cause the taps to hide and show.

@property(nonatomic, strong)NSArray *intervalsToMonitor;
@property(nonatomic, assign)NSUInteger fadeDuration;  // defaults to 1;
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *cancelButton;
@property(nonatomic, assign )BOOL directPlayForTesting;
@property(nonatomic, strong)NSArray *externalHeartbeatObservers;

// if the delegate wishes to control presentation and handling of alertViews rather than letting this class do it, set the 
//  specific cases you wish to handle and implement the associated methods in the TPPlayerFullScreenDelegate protocol
//  defined above
@property(nonatomic, assign)BOOL delegateShouldHandleConcurrencyAlert;
@property(nonatomic, assign)BOOL delegateShouldHandleLoadFailureAlert;
@property(nonatomic, assign)BOOL delegateShouldHandleNoWiFiAlert;


- ( void )loadReleaseURL:(NSString *)publicURLString withAdditionalParameters:( NSDictionary *)additionalParameters;
- ( void )playReleaseURL:(NSString *)publicURLString withAdditionalParameters:( NSDictionary *)additionalParameters;

- (IBAction)clickPlayButton:(id)sender;  
- (IBAction)userHitCancel:(id)sender;

- ( void )userChangedSelection:( TPContentItem * )newObject;
- ( void )reset;

// called when there is an error with the smil data
// can be called externally, ex. if the feed load failed.
- ( void )loadDataForURLFailed:( NSString *)errorMessage;

// subclasses can override if they want to do anything here.  On iOS7 call from delegate method.
- ( void )userReceivedLoadFailureAlert;

// subclasses can override if they want to do anything here.  On iOS7 call from delegate method.
- ( void )userReceivedNoWifiAlert;

// subclasses can override if they want to do anything here.  On iOS7 call from delegate method.
- ( void )userReceivedConcurrencyConflictAlert;


@end

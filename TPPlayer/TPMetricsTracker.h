//
//  TPMetricsTracker.h
//
//  Copyright © 2013, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)
//

#import <UIKit/UIKit.h>
#import "TPAnalyticsTracking.h"

@interface TPMetricsTracker : NSObject<TPAnalyticsTracking>

@property(nonatomic, copy )NSString *uniqueIdentifier;
@property(nonatomic, strong )NSDictionary *infoDictionary;

- ( id )initWithInfo:( NSDictionary *)info;
- ( NSDictionary *)parametersToReportFromDictionaryInfo:( NSDictionary *)controllerParams controller:( UIViewController *)reportingViewController;

//- ( void )trackPageView:( UIViewController *)reportingViewController;
//- ( void )trackVideoView:( UIViewController *)reportingController stage:( NSString *)videoStage;
//- ( void )trackVideoView:( UIViewController *)reportingController withTime:( NSUInteger)seconds stage:( NSString *)videoStage;
//- ( void)trackLink:( UIViewController *)reportingController linkName:( NSString *)linkName forType:( NSString *)type;
//- (void) trackSearch:(UIViewController *)reportingViewController searchTerm:(NSString *)searchTerm;

- ( void )didFinishLaunching;
- ( void )applicationWillResignActive;
- ( void )applicationDidEnterBackground;
- ( void )applicationWillEnterForeground;
- ( void )applicationDidBecomeActive;
- ( void )applicationWillTerminate;


@end

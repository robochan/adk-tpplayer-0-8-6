//
//  TPBookmarkManager.m
//  TPPlayer
//
//  Copyright © 2013, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)
//

#import "TPBookmarkManager.h"
#import "TPBookmark.h"
#import "TPUserListItem.h"
#import "TPMasterDataManager.h"
#import "TPEndUserServicesAgent.h"

@interface TPBookmarkManager()

@end

@implementation TPBookmarkManager

- ( void )makeBookmarkFromUserList:( TPUserList *)userList;
{
    self.userList = userList;
    self.bookmark = nil;
    TPUserListItem *userListItem = userList.userListItemsDictionary[self.aboutID];
    if (userListItem) {
        [ self makeBookmarkFromItem:userListItem ];
    }
    // otherwise there is no bookmark (yet)
    [ self.requestor bookmarkFetchComplete:self.bookmark];
    self.awaitingResponse = NO;
}

//- ( void )getBookmark;
//{
//    self.awaitingResponse = YES;
////    [ self getUserListForAboutID:self.aboutID ];
//}

- ( void )movieStopped:(NSString *)aboutID playheadPosition:( NSTimeInterval )playheadPosition duration:(NSTimeInterval )duration;
{
    // check self.userToken to see if the user is logged out
    DLog(@"movieStopped aboutID %@ playheadPosition = %g  duration = %g", aboutID, playheadPosition, duration);
    BOOL weAreNearlyAtTheEnd =  (duration - playheadPosition < [[ TPMasterDataManager sharedInstance ]ignoreSecondsAtEnd]);
    BOOL weHaveJustStarted = (playheadPosition < [[ TPMasterDataManager sharedInstance ]ignoreSecondsAtBeginning] );
    BOOL thereShouldBeNoBookmark = (weAreNearlyAtTheEnd || weHaveJustStarted);
    if (thereShouldBeNoBookmark ) {
        [ self removeBookmark ];
    }
    else{
        if (self.bookmark) {
            [ self updateBookmark:playheadPosition duration:duration ];
        }
        else{
            [ self createBookmarkForAboutID:aboutID playheadPosition:playheadPosition duration:duration];
        }
    }
}


- ( void )removeBookmark;
{
    DLog(@"removing my bookmark aboutID = %@", self.aboutID);
    if (self.bookmark) {
        [ self deleteUserListItem:self.bookmark ];
    }
}

- ( void )updateBookmark:( NSTimeInterval )playheadPosition duration:( NSTimeInterval)duration;
{
    self.bookmark.playheadPosition = playheadPosition;
    self.bookmark.duration = duration;
    [ self updateUserListItem:self.bookmark ];
}

- ( void )userListItemDeleteCompletedSuccessfully:( NSDictionary *)results;
{
    [ super userListItemDeleteCompletedSuccessfully:results];
    DLog(@"results = %@", results)
    [[ TPEndUserServicesAgent sharedInstance]bookMarkDeleted:self ];
}

- ( void )userListItemDeleteFailed:( NSDictionary *)results;
{
    [ super userListItemDeleteFailed:results];
    [[ TPEndUserServicesAgent sharedInstance]bookMarkDeleted:self ];
}

- ( void )userListItemCreationCompletedSuccessfully:( NSDictionary *)results;
{
    [ super userListItemCreationCompletedSuccessfully:results];
    self.bookmark.identifier = results[@"id"];
    TPUserList *genericList = [ TPEndUserServicesAgent sharedInstance].genericUserList;
    NSMutableDictionary *genericListDictionary = genericList.userListItemsDictionary;
    TPUserListItem *currentUserListItem = genericListDictionary[self.bookmark.aboutID];
    if (! currentUserListItem) {
        TPUserListItem *newListItem = [[ TPUserListItem alloc]init ];
        newListItem.aboutID = self.bookmark.aboutID;
        newListItem.identifier = self.bookmark.identifier;
        newListItem.itemDescription = self.bookmark.itemDescription;
        newListItem.title = self.bookmark.title;
        [genericListDictionary setObject:newListItem forKey:self.bookmark.aboutID ];
    }
    else
    {
        currentUserListItem.title = self.bookmark.title;
        currentUserListItem.itemDescription = self.bookmark.itemDescription;
        currentUserListItem.identifier = self.bookmark.identifier;
    }
}

- ( void )createBookmarkForAboutID:(NSString *)aboutID playheadPosition:( NSTimeInterval )playheadPosition duration:(NSTimeInterval )duration;
{
    self.bookmark = [[ TPBookmark alloc]init ];
    self.bookmark.aboutID = self.aboutID;
    self.bookmark.platformUserListID = self.userList.identifier;
    
    self.bookmark.playheadPosition = playheadPosition;
    self.bookmark.duration = duration;
    if (self.userList) {
        [ self createUserListItem:self.bookmark ];
    }
}



- ( void )makeBookmarkFromItem:( TPUserListItem *)userListItem;
{
    DLog(@"userListItem = %@", userListItem);
    self.bookmark = [ [ TPBookmark alloc]init];
    self.bookmark.aboutID = userListItem.aboutID;
    self.bookmark.identifier = userListItem.identifier;
    self.bookmark.platformUserListID = self.userList.identifier;
    NSUInteger valueAdjustment = [[ TPEndUserServicesAgent sharedInstance]shouldStoreTimeValuesAsSeconds] ? 1 : 1000;
    self.bookmark.playheadPosition = [userListItem.title longLongValue]/valueAdjustment;
    self.bookmark.duration = [userListItem.itemDescription longLongValue]/valueAdjustment;
    
//    DLog(@"playhead as long = %d itemDescription as int = %d", [userListItem.title intValue]/1000, [userListItem.itemDescription intValue]/1000);
//    DLog(@"playhead as long = %lld itemDescription as int = %lld", [userListItem.title longLongValue]/1000, [userListItem.itemDescription longLongValue]/1000);
    DLog(@"bookmark = %@", self.bookmark);

}







@end

//
//  TPSmilInfo.m
//
//  Copyright © 2013, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)
//


#import "TPSmilInfo.h"
#import "TPMasterDataManager.h"

@interface TPSmilInfo()

@property(nonatomic, assign)BOOL receivedRatingSource;
@property(nonatomic, assign)BOOL receivedAdvisorySource;
@property(nonatomic, weak)id delegate;
@property(nonatomic, assign)SEL selector;

@end

@implementation TPSmilInfo


- ( NSString *)description;
{
    return [ NSString stringWithFormat:@"source url is %@\n advisory url is %@\n ratings url is %@", self.source, self.advisoryURL, self.ratingURL];
}

-( BOOL)hasAllNecessarySMIL;
{
    BOOL needsAtLeastOne = self.ratingURL || self.advisoryURL;
    if (! needsAtLeastOne)  // usual case - we need neither one because we have no pre-roll
    {
        return YES;
    }
    BOOL ratingSourceIsOK = self.ratingURL ? self.receivedRatingSource : YES;
    BOOL advisorySourceIsOK = self.advisoryURL ? self.receivedAdvisorySource : YES;
    return ratingSourceIsOK && advisorySourceIsOK;
}

- ( void )getPreRollSmilInfoForDelegate:( id)delegate selector:( SEL)selector;
{
    self.delegate = delegate;
    self.selector = selector;
    
    if (self.ratingURL && ! self.receivedRatingSource) {
        [[ TPMasterDataManager sharedInstance ] loadSimpleSmilDataForUrl:self.ratingURL metadata:@"rating"  delegate:self selector:@selector(smilDataLoaded:)];
    }
    if (self.advisoryURL && ! self.receivedAdvisorySource) {
        [[ TPMasterDataManager sharedInstance ] loadSimpleSmilDataForUrl:self.advisoryURL metadata:@"advisory"  delegate:self selector:@selector(smilDataLoaded:)];
    }
}

- ( void )smilDataLoaded:( TPDataHandler *)dataHandler;
{
    NSError *error = dataHandler.error;
    // mark these whether or not there was an error
    if ([dataHandler.metadata isEqualToString:@"rating" ]) 
    {
        self.receivedRatingSource = YES;
    }
    else
    {
        self.receivedAdvisorySource = YES;
    }
    if (error == nil)
    {
        NSString *sourceURL = dataHandler.returnedObject;
        // see if this will produce a url at all...
        NSURL *testingURL = [ NSURL URLWithString:sourceURL];
        
        if ([dataHandler.metadata isEqualToString:@"rating" ]) 
        {
            self.ratingSource = testingURL ? sourceURL : nil;
        }
        else
        {
            self.advisorySource = testingURL ? sourceURL : nil;
        }
    }
    if (self.hasAllNecessarySMIL) {
        [self.delegate performSelectorOnMainThread:self.selector withObject:nil waitUntilDone:NO];
    }
}


@end

//
//  TPCCContent.h
//  TPPlayer
//
//  Copyright © 2013, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)

//

#import <Foundation/Foundation.h>

@interface TPCCContent : NSObject

@property(nonatomic, strong)NSArray *strings;
@property(nonatomic, strong)NSArray *fontStyles;

@end

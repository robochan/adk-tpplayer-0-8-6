//
//  TPVideoManagerVCConstants.m
//  TPPlayer
//
//  Copyright © 2013, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)
//

#import "TPVideoManagerVCConstants.h"

// When this notification is emitted, the object is a dictionary with an NSNumber for player's playerRate and an NSValue for the player's currentTime
NSString *const kTPPlayerDidChangeRateNotification = @"TPPlayerDidChangeRateNotification";
// When this notification is emitted, the object is the aboutID -- userInfo contains a dictionary with an NSNumber for the playbackTime and NSNumber for duration
NSString *const kTPPlayerResumeConditionNotification = @"TPPlayerResumeConditionNotification";

// If you aren't playing full-screen only, there is a play button that launches the video load/play process
// This replaces the old notification called TPPlayerDidPlayButtonHitNotification
NSString *const kTPTopLevelPlayButtonHitNotification = @"TPTopLevelPlayButtonHitNotification";

// When these notifications are emitted, the object is an NSValue with the player's currentTime
NSString *const kTPPlayButtonHitNotification = @"TPPlayButtonHitNotification";
NSString *const kTPPauseButtonHitNotification = @"TPPauseButtonHitNotification";

// When these notifications are emitted, there is no object
NSString *const kTPClosedCaptionButtonHitNotification = @"TPClosedCaptionButtonHitNotification";
NSString *const kTPPlayFromBeginningButtonHitNotification = @"TPPlayFromBeginningButtonHitNotification";
NSString *const kTPResumeButtonHitNotification = @"TPResumeButtonHitNotification";
NSString *const kTPMoviePlayedToEndNotification = @"TPMoviePlayedToEndNotification";
NSString *const kTPPlayerMovieAndAdsDidFinishNotification = @"TPPlayerMovieAndAdsDidFinishNotification";

// When this notification is emitted, the object contains the errorMessage
NSString *const kTPMovieFailedToLoadNotification = @"TPMovieFailedToLoadNotification";
// When this notification is emitted, there is no object -- if the url fails to load you will get this in addition to kTPMovieFailedToLoadNotification 
NSString *const kTPMovieNoContentNotification = @"TPMovieNoContentNotification";

// When this notification is emitted, the object contains the NSValue with the player's currentTime
NSString *const kTPMoviePassed25PerCentNotification = @"TPMoviePassed25PerCentNotification";
NSString *const kTPMoviePassed50PerCentNotification = @"TPMoviePassed50PerCentNotification";
NSString *const kTPMoviePassed75PerCentNotification = @"TPMoviePassed75PerCentNotification";

// When this notification is emitted, the object contains a dictionary with NSValues for the post-scrub time(postScrubValue) and the pre-scrub time(preScrubValue)
NSString *const kTPUserScrubbedNotification = @"TPUserScrubbedNotification";

// When this notification is emitted, the object is the new array of AVTimedMetadata
NSString *const kTPTimedMetaDataChangedNotification = @"TPTimedMetaDataChangedNotification";

// When this notification is emitted, the object is an NSValue with the CMTime duration
NSString *const kTPMovieReadyToPlayNotification = @"TPMovieReadyToPlayNotification";

// When this notification is emitted, the object is a TPContentItem
NSString *const kTPUserSelectedContentItemNotification  = @"TPUserSelectedContentItemNotification";

// When this notification is emitted, , there is no object
// This notification is sent the first time a closed caption is written to the view after the cc option has been turned on
//  It only applies to sidecar closed captions
NSString *const kTPClosedCaptionsFirstShownNotification = @"kTPClosedCaptionsFirstShownNotification";

// When these notifications are emitted, the object is a TPAdObject
NSString *const kTPAdStartedNotification = @"TPAdStartedNotification";
NSString *const kTPAdEndedNotification = @"TPAdEndedNotification";

// When these notifications are emitted, the object is a TPAdPod
NSString *const kTPAdSlotStartedNotification = @"TPAdSlotStartedNotification";
NSString *const kTPAdSlotEndedNotification = @"TPAdSlotEndedNotification";



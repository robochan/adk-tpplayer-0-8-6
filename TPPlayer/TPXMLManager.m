//
//  TPXMLManager.m
//  TPPlayer
//
//  Copyright © 2013, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)
//

#import "TPXMLManager.h"
#import "TPMasterDataManager.h"
#import "TPDataHandler.h"
#import "TPSmilInfo.h"
#import "TPClosedCaption.h"
#import "TPTrailer.h"
#import "TPVideoAsset.h"
#import "TPSmartImage.h"
#import <CoreText/CoreText.h>
#import "TPCCContent.h"

#import "TPPlaylist.h"
#import "TPClip.h"
#import "TPBaseClip.h"

//#import "TPCCStyle.h"

@interface TPXMLManager()

@property(nonatomic, strong)NSDictionary *smpteTTColors;
@property(nonatomic, strong)UIFont *font;
@property(nonatomic, strong)UIFont *boldFont;
@property(nonatomic, strong)UIFont *italicFont;
@property (nonatomic, strong )NSDictionary *namespaceDictionary;

@end

@implementation TPXMLManager
static TPXMLManager *defaultObject;

+ ( instancetype )sharedInstance;
{
    if( defaultObject == nil)
    {
        defaultObject = [ [ [ self class]alloc]init];
        [ defaultObject setup ];
    }
    return defaultObject;
}

- ( void )setup;
{
    // Create SMPTE+TT compliabt color table
    NSMutableDictionary *tempColors = [NSMutableDictionary dictionary];
    [tempColors setValue:[UIColor clearColor] forKey:@"transparent"];
    [tempColors setValue:[UIColor blackColor] forKey:@"black"];
    [tempColors setValue:[UIColor colorWithRed:192/255.0 green:192/255.0 blue:192/255.0 alpha:1.0] forKey:@"silver"];
    [tempColors setValue:[UIColor grayColor] forKey:@"gray"];
    [tempColors setValue:[UIColor whiteColor] forKey:@"white"];
    [tempColors setValue:[UIColor colorWithRed:128/255.0 green:0 blue:0 alpha:1.0] forKey:@"maroon"];
    [tempColors setValue:[UIColor redColor] forKey:@"red"];
    [tempColors setValue:[UIColor purpleColor] forKey:@"purple"];
    [tempColors setValue:[UIColor magentaColor] forKey:@"fuchsia"]; // identical to magenta per W3C spec section 8.3.10
    [tempColors setValue:[UIColor magentaColor] forKey:@"magenta"];
    [tempColors setValue:[UIColor colorWithRed:0 green:128/255.0 blue:0 alpha:1.0] forKey:@"green"];
    [tempColors setValue:[UIColor greenColor] forKey:@"lime"];
    [tempColors setValue:[UIColor colorWithRed:128/255.0 green:128/255.0 blue:0 alpha:1.0] forKey:@"olive"];
    [tempColors setValue:[UIColor yellowColor] forKey:@"yellow"];
    [tempColors setValue:[UIColor colorWithRed:0 green:0 blue:128/255.0 alpha:1.0] forKey:@"navy"];
    [tempColors setValue:[UIColor blueColor] forKey:@"blue"];
    [tempColors setValue:[UIColor colorWithRed:0 green:128/255.0 blue:128/255.0 alpha:1.0] forKey:@"teal"];
    [tempColors setValue:[UIColor cyanColor] forKey:@"aqua"]; // identical to cyan per W3C spec section 8.3.10
    [tempColors setValue:[UIColor cyanColor] forKey:@"cyan"];
    
    self.smpteTTColors = tempColors;
    
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ) {
        self.font = [ UIFont fontWithName:@"Courier" size:24.0];
    } else {
        self.font = [ UIFont fontWithName:@"Courier" size:16.0];
    }
        
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ) {
        self.italicFont = [ UIFont fontWithName:@"Courier-Oblique" size:24.0];
    } else {
        self.italicFont = [ UIFont fontWithName:@"Courier-Oblique" size:16.0];
    }
        
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ) {
        self.boldFont = [ UIFont fontWithName:@"Courier-Bold" size:24.0];
    } else {
        self.boldFont = [ UIFont fontWithName:@"Courier-Bold" size:16.0];
    }
}

- ( void )createObjectFromDataHandler:( TPDataHandler *)dataHandler;
{
    NSString *pathNameForObject = dataHandler.pathName;
    
    if( [pathNameForObject isEqualToString:@"SmilData" ] )
    {
//        [ self createSmilDataFromDataHandler:dataHandler ];
        [ self createPlaylistFromDataHandler:dataHandler];
        return;
    }
    if ([ pathNameForObject isEqualToString:@"CCData"]) {
        [ self createClosedCaptionDataFromDataHandler:dataHandler ];
        return;
    }
    if ([pathNameForObject isEqualToString:@"ClipsListData" ]) {
        [self createClipsFromDataHandler: dataHandler];
        return;
    }
//    if ([ pathNameForObject isEqualToString:@"GUIDAuth"]) {
//        [ self createGUIDAuthFromDataHandler:dataHandler ];
//        return;
//    }
    
}

- ( void )setupNamespaceDictionary:(GDataXMLDocument *)xmlDocument;
{
    NSArray *namespaces = [xmlDocument.rootElement namespaces ];
    NSMutableDictionary *tmpDict = [ NSMutableDictionary dictionary ];
    
    for( GDataXMLNode *namespaceNode in namespaces )
    {           
        NSString *pathName = [ namespaceNode stringValue ];
        NSString *nodeName = [ namespaceNode name ];
        
        if( nodeName && pathName )
        {
            [ tmpDict setObject:pathName forKey:nodeName ];
        }
    }
    self.namespaceDictionary = tmpDict;
}

- (void) createClipsFromDataHandler:(TPDataHandler *)dataHandler;
{
    GDataXMLDocument *xmlDoc = dataHandler.xmlDocument;
    
    if( xmlDoc )
    {
        [ self setupNamespaceDictionary:xmlDoc ];
        NSArray *clipsNodes = [xmlDoc nodesForXPath:@"//item" namespaces:self.namespaceDictionary error:nil];
        
        for (GDataXMLNode *clipNode in clipsNodes) {
            TPTrailer *tmpTrailer = [[TPTrailer alloc]init];
            tmpTrailer.parentId = [[clipNode nodeForXPath:@"./pl1:masterShowID" namespaces:self.namespaceDictionary error:nil] stringValue];
            tmpTrailer.houseId = [[clipNode nodeForXPath:@"./pl1:houseID" namespaces:self.namespaceDictionary error:nil] stringValue];
            tmpTrailer.title = [[clipNode nodeForXPath:@"./title" namespaces:self.namespaceDictionary error:nil] stringValue];
            
            // Get images
            NSArray *graphicsNodes  = [clipNode nodesForXPath:@"./media:thumbnail" namespaces:self.namespaceDictionary error:nil];
            NSMutableDictionary *tmpImages = [ NSMutableDictionary dictionary ];
            
            if (graphicsNodes != nil)
            {
                for( GDataXMLNode *graphicsNode in graphicsNodes )
                {
                    TPSmartImage *aSmartImage = [ self createSmartImageFromXMLNode:( GDataXMLNode *)graphicsNode withParentId:tmpTrailer.houseId ];
                    DLog(@"smartImage role = %@, path = %@", aSmartImage.role, aSmartImage.imagePath )
                    if (aSmartImage && aSmartImage.role  && aSmartImage.imagePath )
                    {
                        
                        [ tmpImages setObject:aSmartImage forKey:aSmartImage.role ];
                        
                    }
                }
                tmpTrailer.images = tmpImages;
            }
            
            // Get Video Assets
            NSArray *videoAssetNodes = [clipNode nodesForXPath:@"./media:group/media:content" namespaces:self.namespaceDictionary error:nil];
            NSMutableDictionary *tmpVideoAssetDictionary = [NSMutableDictionary dictionary];
            
            for (GDataXMLNode *videoAssetNode in videoAssetNodes) {
                
                if (videoAssetNode != nil) {
                    TPVideoAsset *tmpTPVideoAsset = [[TPVideoAsset alloc] init];
                    tmpTPVideoAsset.duration = [[[ (GDataXMLElement *)videoAssetNode attributeForName:@"duration"] stringValue] floatValue];
                    NSString *urlStringToEscape = [[[ (GDataXMLElement *)videoAssetNode attributeForName:@"url"] stringValue] stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding];
                    tmpTPVideoAsset.videoURLString = urlStringToEscape;
                    tmpTPVideoAsset.width = [[[(GDataXMLElement *)videoAssetNode attributeForName:@"width"] stringValue] intValue];
                    //                    DLog(@"Video width = %d", tmpTPVideoAsset.videoWidth);                    
                    NSNumber *videoWidthKey = [ NSNumber numberWithFloat:tmpTPVideoAsset.width];
                    [tmpVideoAssetDictionary setObject:tmpTPVideoAsset forKey:videoWidthKey];
                } else {
                    continue;
                }
                
            }
            tmpTrailer.videos = tmpVideoAssetDictionary;
            [[TPMasterDataManager sharedInstance] archiveTrailer:tmpTrailer];
        }
        
    }
}


- ( TPSmartImage *)createSmartImageFromXMLNode:( GDataXMLNode *)graphicsNode withParentId:( NSString *)parentId;
{
    DLog(@"%@", graphicsNode );
    TPSmartImage *newSmartImage = [ [ TPSmartImage alloc ]init ];
    newSmartImage.parentID = parentId;
    
    GDataXMLNode *node = [ graphicsNode nodeForXPath:@"./plfile:assetType" namespaces:self.namespaceDictionary error:nil];
    NSString *newSmartImageRole = [node stringValue];
    NSDictionary *rolesMatcher = @{ 
                                   @"Grid Retina" : @"gridTile_retina",
                                   @"FC Retina" : @"cardFront_retina",
                                   @"BC Retina" : @"cardBack_retina",
                                   @"BC Regular"  : @"cardBack",
                                   @"FC Regular" :  @"cardFront",
                                   @"Grid Regular" : @"gridTile",
                                   @"Grid Retina" : @"gridTile_retina",
                                   @"Video Regular TN" : @"videoThumbnailMD",
                                   @"Video Retina TN"  : @"videoThumbnailMD_retina",
                                   @"Image Regular" : @"photoImage",
                                   @"Image Retina" : @"photoImage_retina",
                                   @"Image Regular TN" : @"photoImageTN",
                                   @"Image Retina TN" : @"photoImageTN_retina",
                                   @"Poster Picture" : @"posterPicture",
                                   @"Poster Picture Retina"  : @"posterPicture_retina",
                                   @"Trailer Thumbnail Retina" : @"trailerThumbnail_retina",
                                   @"Trailer Thumbnail" : @"trailerThumbnail"
                                   };
    // This is temporary
//    if ([newSmartImageRole isEqualToString:@"Grid Retina" ])
//    {
//        newSmartImage.role = @"gridTile_retina";
//    }
//    if ([newSmartImageRole isEqualToString:@"Video Regular TN" ])
//    {
//        newSmartImage.role = @"videoThumbnailMD";
//    }
//    if ([newSmartImageRole isEqualToString:@"Video Retina TN" ])
//    {
//        newSmartImage.role = @"videoThumbnailMD_retina";
//    }
//    if ([newSmartImageRole isEqualToString:@"Image Regular" ])
//    {     
//        newSmartImage.role = @"photoImage";
//    }
//    if ([newSmartImageRole isEqualToString:@"Image Retina" ])
//    {     
//        newSmartImage.role = @"photoImage_retina";
//    }
//    if ([newSmartImageRole isEqualToString:@"Image Regular TN" ])
//    {     
//        newSmartImage.role = @"photoImageTN";
//    }
//    if ([newSmartImageRole isEqualToString:@"Image Retina TN" ])
//    {     
//        newSmartImage.role = @"photoImageTN_retina";
//    }
//    if ([newSmartImageRole isEqualToString:@"Poster Picture" ])
//    {     
//        newSmartImage.role = @"posterPicture";
//    }
//    if ([newSmartImageRole isEqualToString:@"Poster Picture Retina" ])
//    {
//        newSmartImage.role = @"posterPicture_retina";
//    }
//    if ([newSmartImageRole isEqualToString:@"Trailer Thumbnail Retina" ])
//    {
//        newSmartImage.role = @"trailerThumbnail_retina";
//    }
//    if ([newSmartImageRole isEqualToString:@"Trailer Thumbnail" ])
//    {
//        newSmartImage.role = @"trailerThumbnail";
//    }

//    if ([newSmartImageRole isEqualToString:@"Grid Retina" ])
//    {
//        newSmartImage.role = @"gridTile_retina";
//    }
//    if ([newSmartImageRole isEqualToString:@"FC Retina" ])
//    {
//        newSmartImage.role = @"cardFront_retina";
//    }
//    if ([newSmartImageRole isEqualToString:@"BC Retina" ])
//    {
//        newSmartImage.role = @"cardBack_retina";
//    }
//    if ([newSmartImageRole isEqualToString:@"BC Regular" ])
//    {
//        newSmartImage.role = @"cardBack";
//    }
//    if ([newSmartImageRole isEqualToString:@"FC Regular" ])
//    {
//        newSmartImage.role = @"cardFront";
//    }
//    if ([newSmartImageRole isEqualToString:@"Grid Regular" ])
//    {
//        newSmartImage.role = @"gridTile";
//    }
    if(newSmartImageRole)
    {
        newSmartImage.role = rolesMatcher[newSmartImageRole];
    }
    
    node = [ (GDataXMLElement *)graphicsNode attributeForName:@"url"];
    newSmartImage.imagePath = [node stringValue];
    
    NSURL *imageURL = [ [ TPMasterDataManager sharedInstance ].imagesDirectoryURL URLByAppendingPathComponent:newSmartImage.pathForImage ];  
    newSmartImage.imageLocalURL = imageURL;
    
    //    DLog(@"TPSmartImage imageURL = %@", imageURL );
    
    return newSmartImage;
}


- ( void )createSmilDataFromDataHandler:( TPDataHandler *)dataHandler;
{
    // dataHandler.error was checked before this method was called
    GDataXMLDocument *xmlDocument = dataHandler.xmlDocument;
    NSString *source = nil;
    NSString *advisoryURL = nil;
    NSString *ratingURL = nil;
    NSString *CCURL = nil;
    NSString *freewheelID = nil;
    
    if( xmlDocument )
    {
        NSDictionary *namespaces = [[NSDictionary alloc] initWithObjects:[NSArray arrayWithObject:@"http://www.w3.org/2005/SMIL21/Language"] forKeys:[NSArray arrayWithObject:@"df"]];
        
        // Now we check to be certain there is no error from mpx - For example, the request is valid, but the content is geoblocked for the user's current region
        GDataXMLNode *errorRefNode = [xmlDocument nodeForXPath:@"//df:body/df:seq/df:ref" namespaces:namespaces error:nil];
        
        // If there is a <param name="isException" value="true"> then we have an error
        GDataXMLNode *errorTestNode = [errorRefNode nodeForXPath:@"//df:param[@isException='true']" namespaces:namespaces error:nil];
        if (errorTestNode) {
            NSString *errorTitle = [[ (GDataXMLElement *)errorRefNode attributeForName:@"title"] stringValue];
            NSString *errorDescription = [[ (GDataXMLElement *)errorRefNode attributeForName:@"abstract"] stringValue];
            
            NSString *errorException = [[ (GDataXMLElement *)errorRefNode attributeForName:@"//exception"] stringValue];
            NSInteger responseCode = [[[ (GDataXMLElement *)errorRefNode attributeForName:@"//responseCode"] stringValue] integerValue];
            NSString *serverStackTrace = [[ (GDataXMLElement *)errorRefNode attributeForName:@"//serverStackTrace"] stringValue];
            
            NSMutableDictionary *errorDetails = [NSMutableDictionary dictionaryWithObjectsAndKeys:errorTitle, @"errorTitle", errorDescription, NSLocalizedDescriptionKey, errorException, @"errorException", responseCode, @"responseCode", serverStackTrace, @"serverStackTrace", nil];
            
            NSError *error = [NSError errorWithDomain:@"tpplayer" code:responseCode userInfo:errorDetails];
            DLog(@"Non-network error encountered in SMIL = %d, %@, %@", responseCode, errorTitle, errorDescription);

            dataHandler.error = error;
            [ [ TPMasterDataManager sharedInstance ]errorCreatingObject:dataHandler ];
            return;

        }
        
        DLog(@"SMIL file = %@", xmlDocument);

        // <meta> tag parse for concurrency
        NSArray *metaNodes = [xmlDocument nodesForXPath:@"//df:head/df:meta" namespaces:namespaces error:nil];
        TPConcurrencyInfo *concurrencyInfoObject = nil;
        
        if (metaNodes.count > 0)
        {
            NSMutableDictionary *metaTagDictionary = [NSMutableDictionary dictionary];
            for (GDataXMLNode *metaNode in metaNodes)
            {
                [   metaTagDictionary setObject:[[ (GDataXMLElement *)metaNode attributeForName:@"content"] stringValue] forKey:[[ (GDataXMLElement *)metaNode attributeForName:@"name"] stringValue]  ];
            }
            // there is other info in the metadata tag, so be sure we only make a concurrency Object
            // if we have the relevant data
            if (metaTagDictionary[@"lockId"] && metaTagDictionary[@"lockSequenceToken"] && metaTagDictionary[@"lock"]) {
                NSDictionary *finalMetaTagDictionary = metaTagDictionary;
                concurrencyInfoObject = [[TPConcurrencyInfo alloc] initWithDictionary:finalMetaTagDictionary];
            }
            
        }
        
        NSArray *paramNodes = [xmlDocument nodesForXPath:@"//df:body/df:seq/df:par/df:video/df:param" namespaces:namespaces error:nil];
        
        for (GDataXMLNode *paramNode in paramNodes) {
            if ([[[ (GDataXMLElement *)paramNode attributeForName:@"name"] stringValue] isEqualToString: @"freewheelId"]) {
                freewheelID = [[ (GDataXMLElement *)paramNode attributeForName:@"value"] stringValue];
                DLog(@"The Freewheel ID is %@", freewheelID);
            }
        }
        
        // Get Closed Caption (SMPTE-TT) File Url
        NSArray *textstreams = [xmlDocument nodesForXPath:@"//df:body/df:seq/df:par/df:textstream" namespaces:namespaces error:nil];
        DLog(@"there are %d textstreams", textstreams.count);
        
        for (GDataXMLNode *textstream in textstreams) {
            if ([[[ (GDataXMLElement *)textstream attributeForName:@"type"] stringValue] isEqualToString: @"application/smptett+xml"]) {
                CCURL = [[ (GDataXMLElement *)textstream attributeForName:@"src"] stringValue];
                DLog(@"The CC URL is %@", CCURL);
            }
            else if ([[[ (GDataXMLElement *)textstream attributeForName:@"type"] stringValue] isEqualToString: @"application/ttaf+xml"])
            {
                CCURL = [[ (GDataXMLElement *)textstream attributeForName:@"src"] stringValue];
                CCURL = [CCURL stringByDeletingPathExtension];
                CCURL = [CCURL stringByAppendingPathExtension:@"tt"];
                DLog(@"The CC URL is %@", CCURL);
            }
        }
        
        // Get pre-roll and bumper video for Advisory and Ratings
        NSArray *refNodes = [xmlDocument nodesForXPath:@"//df:body/df:seq/df:ref" namespaces:namespaces error:nil];
        DLog(@"there are %d refNodes", refNodes.count);
        
        for (GDataXMLNode *refNode in refNodes) {
            if ([[[ (GDataXMLElement *)refNode attributeForName:@"tags"] stringValue] isEqualToString: @"bumper"]) {
                ratingURL = [[ (GDataXMLElement *)refNode attributeForName:@"src"] stringValue];
                ratingURL = [ ratingURL stringByAppendingString:@""];
                DLog(@"Rating URL is %@", ratingURL);
            }
            else if ([[[ (GDataXMLElement *)refNode attributeForName:@"tags"] stringValue]  isEqualToString: @"preroll"]) {
                advisoryURL = [[ (GDataXMLElement *)refNode attributeForName:@"src"] stringValue];
                advisoryURL = [ advisoryURL stringByAppendingString:@""];
                DLog(@"Advisory URL is %@", advisoryURL);
            }
        }
        
        // Get the video URL
        GDataXMLNode *node = [xmlDocument nodeForXPath:@"//df:body/df:seq/df:par/df:video/@src" namespaces:namespaces error:nil];
        //        GDataXMLNode *node = [xmlDocument nodeForXPath:@"//df:body/df:video" namespaces:namespaces error:nil];
        if (node != nil)
        {
            source =[node stringValue];
            //            GDataXMLElement *sourceNode =[( GDataXMLElement *)node attributeForName:@"src"];
            //            source = [ sourceNode stringValue ];
        }
        else
        {
            node = [ xmlDocument nodeForXPath:@"//df:body/df:seq/df:video/@src" namespaces:namespaces error:nil];
            if (node != nil)
            {
                source =[node stringValue];
            }
            else
            {
                node = [xmlDocument nodeForXPath:@"//df:body/df:seq/df:par/df:switch/df:video/@src" namespaces:namespaces error:nil];
                if (node != nil)
                {
                    source =[node stringValue];
                }
                else
                {
                    node = [xmlDocument nodeForXPath:@"//df:body/df:seq/df:switch/df:video/@src" namespaces:namespaces error:nil];
                    if (node != nil)
                    {
                        source =[node stringValue];
                    }
                }
            }
        }
        // the object is created and populated, put it in the dataHandler and
        // notify the masterDataManager
        TPSmilInfo *smilInfo = [ [ TPSmilInfo alloc]init];
        if (source) {
            smilInfo.source = source;
            smilInfo.advisoryURL = advisoryURL;
            smilInfo.ratingURL = ratingURL;
            smilInfo.ccURL = CCURL;
            smilInfo.freewheelID = freewheelID;
            smilInfo.concurrencyInfo = concurrencyInfoObject;
        }
        dataHandler.returnedObject = smilInfo;
        [ [ TPMasterDataManager sharedInstance ]objectCreated:dataHandler ];
    }
    else
    {
        [ [ TPMasterDataManager sharedInstance ]errorCreatingObject:dataHandler ];
    }
}

- ( void )createPlaylistFromDataHandler:( TPDataHandler *)dataHandler;
{
    // dataHandler.error was checked before this method was called
    GDataXMLDocument *xmlDocument = dataHandler.xmlDocument;

    NSString *bumperURL = nil;
    NSString *prerollURL = nil;
    NSString *CCURL = nil;
    
    if( xmlDocument )
    {
        DLog(@"SMIL file = %@", xmlDocument);
        
        TPPlaylist *playlist = [[TPPlaylist alloc] init];
        TPBaseClip *baseClip = [[TPBaseClip alloc] init];
        playlist.baseClip = baseClip;
        
        DLog(@"SMIL file = %@", xmlDocument);
        
        NSDictionary *namespaces = [[NSDictionary alloc] initWithObjects:[NSArray arrayWithObject:@"http://www.w3.org/2005/SMIL21/Language"] forKeys:[NSArray arrayWithObject:@"df"]];
        
        GDataXMLNode *errorRefNode = [xmlDocument nodeForXPath:@"//df:body/df:seq/df:ref" namespaces:namespaces error:nil];
        // If there is a <param name="isException" value="true"> then we have an error -- the following really should work but I can't get it to
        GDataXMLNode *errorTestNode = [errorRefNode nodeForXPath:@"./df:param[@name='isException' and @value='true']" namespaces:namespaces error:nil];
        if (errorTestNode)
        {
            NSString *errorTitle = [[ (GDataXMLElement *)errorRefNode attributeForName:@"title"] stringValue];
            NSString *errorDescription = [[ (GDataXMLElement *)errorRefNode attributeForName:@"abstract"] stringValue];
            
            NSString *errorException = [[ (GDataXMLElement *)errorRefNode attributeForName:@"//exception"] stringValue];
            NSInteger responseCode = [[[ (GDataXMLElement *)errorRefNode attributeForName:@"//responseCode"] stringValue] integerValue];
            NSString *serverStackTrace = [[ (GDataXMLElement *)errorRefNode attributeForName:@"//serverStackTrace"] stringValue];
            
            NSMutableDictionary *errorDetails = [NSMutableDictionary dictionaryWithObjectsAndKeys:errorTitle, @"errorTitle", errorDescription, NSLocalizedDescriptionKey, errorException, @"errorException", responseCode, @"responseCode", serverStackTrace, @"serverStackTrace", nil];
            
            NSError *error = [NSError errorWithDomain:@"tpplayer" code:responseCode userInfo:errorDetails];
            DLog(@"Non-network error encountered in SMIL = %d, %@, %@", responseCode, errorTitle, errorDescription);
            
            dataHandler.error = error;
            [ [ TPMasterDataManager sharedInstance ]errorCreatingObject:dataHandler ];
            return;
            
        }
        
        // <meta> tag parse for concurrency
        NSArray *metaNodes = [xmlDocument nodesForXPath:@"//df:head/df:meta" namespaces:namespaces error:nil];
        TPConcurrencyInfo *concurrencyInfoObject = nil;
        
        if (metaNodes.count > 0)
        {
            NSMutableDictionary *metaTagDictionary = [NSMutableDictionary dictionary];
            for (GDataXMLNode *metaNode in metaNodes)
            {
                [   metaTagDictionary setObject:[[ (GDataXMLElement *)metaNode attributeForName:@"content"] stringValue] forKey:[[ (GDataXMLElement *)metaNode attributeForName:@"name"] stringValue]  ];
            }
            
            if (metaTagDictionary[@"lockId"] && metaTagDictionary[@"lockSequenceToken"] && metaTagDictionary[@"lock"]) {
                NSDictionary *finalMetaTagDictionary = metaTagDictionary;
                concurrencyInfoObject = [[TPConcurrencyInfo alloc] initWithDictionary:finalMetaTagDictionary];
            }
        }
        playlist.concurrencyInfo = concurrencyInfoObject;

        
        NSArray *videoNodes = [xmlDocument nodesForXPath:@"//df:video" namespaces:namespaces error:nil];
        if (videoNodes.count > 0) {
            GDataXMLNode *baseNode = [ videoNodes objectAtIndex:0 ];
            baseClip.url = [[ (GDataXMLElement *)baseNode attributeForName:@"src"] stringValue];
            baseClip.title = [[ (GDataXMLElement *)baseNode attributeForName:@"title"] stringValue];
            baseClip.abstract = [[ (GDataXMLElement *)baseNode attributeForName:@"abstract"] stringValue];
            baseClip.copyright = [[ (GDataXMLElement *)baseNode attributeForName:@"copyright"] stringValue];
            baseClip.type = [[ (GDataXMLElement *)baseNode attributeForName:@"type"] stringValue];
            
            // Get categories
            if (![[[ (GDataXMLElement *)baseNode attributeForName:@"categories"] stringValue] isEqualToString:@""]) {
                NSString *categoriesString = [[ (GDataXMLElement *)baseNode attributeForName:@"categories"] stringValue];
                NSArray *tmpCategories = [categoriesString componentsSeparatedByString:@","];
                baseClip.categories = tmpCategories;
            }
            
            if (![[[ (GDataXMLElement *)baseNode attributeForName:@"keywords"] stringValue] isEqualToString:@""]) {
                NSString *categoriesString = [[ (GDataXMLElement *)baseNode attributeForName:@"keywords"] stringValue];
                NSArray *tmpKeywords = [categoriesString componentsSeparatedByString:@","];
                baseClip.keywords = tmpKeywords;
            }
            
            // Capture some common values and attach to BaseClip
            NSArray *baseParamNodes = [baseNode nodesForXPath:@"//df:body/df:seq/df:video/df:param" namespaces:namespaces error:nil];
            for (GDataXMLNode *paramNode in baseParamNodes)
            {
                if ([[[ (GDataXMLElement *)paramNode attributeForName:@"name"] stringValue] isEqualToString: @"cdn"]) {
                    baseClip.cdn = [[ (GDataXMLElement *)paramNode attributeForName:@"value"] stringValue];
                }
                
                if ([[[ (GDataXMLElement *)paramNode attributeForName:@"name"] stringValue] isEqualToString: @"encryptScripts"]) {
                    NSString *encryptString = [[ (GDataXMLElement *)paramNode attributeForName:@"value"] stringValue];
                    BOOL usesEncryption = [encryptString isEqualToString:@"true"] ? YES : NO;
                    baseClip.encryptScripts = usesEncryption;
                }
                
                if ([[[ (GDataXMLElement *)paramNode attributeForName:@"name"] stringValue] isEqualToString: @"freewheelAssetId"]) {
                    baseClip.freewheelAssetID = [[ (GDataXMLElement *)paramNode attributeForName:@"value"] stringValue];
                }
            }
            

        }
        
        NSMutableArray *tmpClips = [NSMutableArray array];
        for (GDataXMLNode *videoNode in videoNodes) {
            TPClip *newClip = [[TPClip alloc] init];
            newClip.duration = [[ [ [(GDataXMLElement *)videoNode attributeForName:@"duration"] stringValue ] stringByReplacingOccurrencesOfString:@"ms" withString:@""] integerValue];
            
            
            
            NSString *clipBeginString = [ [(GDataXMLElement *)videoNode attributeForName:@"clipBegin"] stringValue ];
            newClip.clipBegin = [self millisecondsFromTimeCodeString:clipBeginString];
            
            NSString *clipEndString = [ [(GDataXMLElement *)videoNode attributeForName:@"clipEnd"] stringValue ];
            newClip.clipEnd = [self millisecondsFromTimeCodeString:clipEndString];
            
            newClip.title = [ [(GDataXMLElement *)videoNode attributeForName:@"title"] stringValue ];
            
            // Generalized version to capture all the user-customized parameters
            NSArray *baseParamNodes = [videoNode nodesForXPath:@"//df:param" namespaces:namespaces error:nil];
            NSMutableDictionary *tmpParamsDict = [NSMutableDictionary dictionary];
            for (GDataXMLNode *paramNode in baseParamNodes)
            {
                NSString *name = [ [ (GDataXMLElement *)paramNode attributeForName:@"name"] stringValue];
                NSString *value = [ [ (GDataXMLElement *)paramNode attributeForName:@"value"] stringValue];

                [ tmpParamsDict setObject:value forKey:name];
            }
            newClip.params = tmpParamsDict;
            
            [tmpClips addObject:newClip];
            
        }
        baseClip.clips = tmpClips;
        
                
        // Get Closed Caption (SMPTE-TT) File Url
        NSArray *textstreams = [xmlDocument nodesForXPath:@"//df:body/df:seq/df:par/df:textstream" namespaces:namespaces error:nil];
        DLog(@"there are %d textstreams", textstreams.count);
        
        for (GDataXMLNode *textstream in textstreams) {
            if ([[[ (GDataXMLElement *)textstream attributeForName:@"type"] stringValue] isEqualToString: @"application/smptett+xml"]) {
                baseClip.CCURL = [[ (GDataXMLElement *)textstream attributeForName:@"src"] stringValue];
                DLog(@"The CC URL is %@", CCURL);
            }
            else if ([[[ (GDataXMLElement *)textstream attributeForName:@"type"] stringValue] isEqualToString: @"application/ttaf+xml"])
            {
                CCURL = [[ (GDataXMLElement *)textstream attributeForName:@"src"] stringValue];
                CCURL = [CCURL stringByDeletingPathExtension];
                CCURL = [CCURL stringByAppendingPathExtension:@"tt"];
                baseClip.CCURL = CCURL;
                DLog(@"The CC URL is %@", CCURL);
            }
        }
        
        // Get pre-roll and bumper video for Advisory and Ratings
        NSArray *refNodes = [xmlDocument nodesForXPath:@"//df:body/df:seq/df:ref" namespaces:namespaces error:nil];
        DLog(@"there are %d refNodes", refNodes.count);
        
        for (GDataXMLNode *refNode in refNodes) {
            if ([[[ (GDataXMLElement *)refNode attributeForName:@"tags"] stringValue] isEqualToString: @"bumper"]) {
                bumperURL = [[ (GDataXMLElement *)refNode attributeForName:@"src"] stringValue];
                bumperURL = [ bumperURL stringByAppendingString:@""];
                baseClip.bumperURL = bumperURL;
                DLog(@"Rating URL is %@", bumperURL);
            }
            
            if ([[[ (GDataXMLElement *)refNode attributeForName:@"tags"] stringValue]  isEqualToString: @"preroll"]) {
                prerollURL = [[ (GDataXMLElement *)refNode attributeForName:@"src"] stringValue];
                prerollURL = [ prerollURL stringByAppendingString:@""];
                baseClip.preroll = prerollURL;
                DLog(@"Advisory URL is %@", prerollURL);
            }
        }
        
        // Get the video URL
//        GDataXMLNode *node = [xmlDocument nodeForXPath:@"//df:body/df:seq/df:par/df:video/@src" namespaces:namespaces error:nil];
//        //        GDataXMLNode *node = [xmlDocument nodeForXPath:@"//df:body/df:video" namespaces:namespaces error:nil];
//        if (node != nil)
//        {
//            source =[node stringValue];
//            //            GDataXMLElement *sourceNode =[( GDataXMLElement *)node attributeForName:@"src"];
//            //            source = [ sourceNode stringValue ];
//        }
//        else
//        {
//            node = [ xmlDocument nodeForXPath:@"//df:body/df:seq/df:video/@src" namespaces:namespaces error:nil];
//            if (node != nil)
//            {
//                source =[node stringValue];
//            }
//            else
//            {
//                node = [xmlDocument nodeForXPath:@"//df:body/df:seq/df:par/df:switch/df:video/@src" namespaces:namespaces error:nil];
//                if (node != nil)
//                {
//                    source =[node stringValue];
//                }
//                else
//                {
//                    node = [xmlDocument nodeForXPath:@"//df:body/df:seq/df:switch/df:video/@src" namespaces:namespaces error:nil];
//                    if (node != nil)
//                    {
//                        source =[node stringValue];
//                    }
//                }
//            }
//        }
        // the object is created and populated, put it in the dataHandler and
        // notify the masterDataManager
//        TPSmilInfo *smilInfo = [ [ TPSmilInfo alloc]init];
//        if (source) {
//            smilInfo.source = source;
//            smilInfo.advisoryURL = advisoryURL;
//            smilInfo.ratingURL = ratingURL;
//            smilInfo.ccURL = CCURL;
//            smilInfo.freewheelID = freewheelID;
//        }
        dataHandler.returnedObject = playlist;
        [ [ TPMasterDataManager sharedInstance ]objectCreated:dataHandler ];
    }
    else
    {
        [ [ TPMasterDataManager sharedInstance ]errorCreatingObject:dataHandler ];
    }
}

- (NSInteger )millisecondsFromTimeCodeString:(NSString *)timeCode
{
    NSArray *timeCodeComponents = [timeCode componentsSeparatedByString:@":"];
    switch (timeCodeComponents.count)
    {
        case 2:
        {
            int milliseconds = [ timeCodeComponents[1] floatValue ] *1000;      // the last component can be ss or ss.sss
            int minutes = ([timeCodeComponents[0] integerValue] * 60) * 1000;
            int totalMilliseconds = minutes + milliseconds;
            return totalMilliseconds; 
        }
            break;
            
        case 3:
        {
            int milliseconds = [ timeCodeComponents[2] floatValue ] *1000;      // the last component can be ss or ss.sss
            int minutes = ([timeCodeComponents[1] integerValue] * 60) * 1000;
            int hours = (([timeCodeComponents[0] integerValue] * 60) * 60 ) * 1000;
            int totalMilliseconds = hours + minutes + milliseconds;
            return totalMilliseconds;
        }
            
        default:
            break;
    }
    return 0;    
}

- ( void )createClosedCaptionDataFromDataHandler:( TPDataHandler *)dataHandler;
{
    GDataXMLDocument *xmlDocument = dataHandler.xmlDocument;
    if( xmlDocument )
    {
        // parse it out
        // the object is created and populated, put it in the dataHandler and
        // notify the masterDataManager
        TPClosedCaption *closedCaption = [ [ TPClosedCaption alloc]init];
        
        // populate the closedCaption here
        // Define namespaces
        NSDictionary *ttNamespaces = [[NSDictionary alloc] initWithObjects:[NSArray arrayWithObject:@"http://www.w3.org/ns/ttml"] forKeys:[NSArray arrayWithObject:@"tt"]];
        NSDictionary *stylingNamespaces = [[NSDictionary alloc] initWithObjects:[NSArray arrayWithObject:@"http://www.w3.org/ns/ttml#styling"] forKeys:[NSArray arrayWithObject:@"st"]];
        
        // Get styles
        NSArray *styles = [xmlDocument nodesForXPath:@"//tt:head/tt:styling/tt:style" namespaces:ttNamespaces error:nil];
        NSMutableDictionary *tempStyles = [NSMutableDictionary dictionary];
        
        for (GDataXMLNode *style in styles) {
            TPCCStyle *newStyle = [[TPCCStyle alloc] init];
            newStyle.styleID = [[ (GDataXMLElement *)style attributeForName:@"xml:id"] stringValue];
            newStyle.styleFontFamily = [[ (GDataXMLElement *)style attributeForName:@"tts:fontFamily"] stringValue];
            newStyle.styleFontSize = [[ (GDataXMLElement *)style attributeForName:@"tts:fontSize"] stringValue];
            newStyle.styleFontWeight = [[ (GDataXMLElement *)style attributeForName:@"tts:fontWeight"] stringValue];
            newStyle.styleFontStyle = [[ (GDataXMLElement *)style attributeForName:@"tts:fontStyle"] stringValue];
            
            NSString *colorName = [[style nodeForXPath:@"./@st:color" namespaces:stylingNamespaces error:nil] stringValue];
            NSString *backgroundColorName = [[ (GDataXMLElement *)style attributeForName:@"tts:backgroundColor"] stringValue];
            
            newStyle.styleColor = [self getUIColorForSMPTEName:colorName];
            newStyle.styleBackgroundColor = [self getUIColorForSMPTEName:backgroundColorName];
            
            [tempStyles setObject:newStyle forKey:newStyle.styleID];
            
        }
        
        closedCaption.styles = tempStyles;
        
        // Get Regions
        NSArray *regions = [xmlDocument nodesForXPath:@"//tt:head/tt:layout/tt:region" namespaces:ttNamespaces error:nil];
        NSMutableDictionary *tempRegions = [NSMutableDictionary dictionary];
        
        for (GDataXMLNode *region in regions) {
            TPCCRegion *newRegion = [[TPCCRegion alloc] init];
            newRegion.regionID = [[ (GDataXMLElement *)region attributeForName:@"xml:id"] stringValue];
            
            NSString *regionBackgroundColorName = [[ (GDataXMLElement *)region attributeForName:@"tts:backgroundColor"] stringValue];
            newRegion.regionBackgroundColor = [self getUIColorForSMPTEName:regionBackgroundColorName];
            
            newRegion.regionShowBackground = [[ (GDataXMLElement *)region attributeForName:@"tts:showBackground"] stringValue];
            
            [tempRegions setObject:newRegion forKey:newRegion.regionID];
        }
        
        closedCaption.layoutRegions = tempRegions;
        
        // Get CC Lines
        NSArray *ccLines = [xmlDocument nodesForXPath:@"//tt:body/tt:div/tt:p" namespaces:ttNamespaces error:nil];
        NSMutableArray *tempLines = [NSMutableArray array];
        
        for (GDataXMLNode *line in ccLines) {
            TPCCLine *newLine = [[TPCCLine alloc] init];
            newLine.beginTime = [NSStringWithCMTimeSupport convertCMTimeFromTimeCodeWithFrames:([[ (GDataXMLElement *)line attributeForName:@"begin"] stringValue])]; // need to convert from hh:mm:ss:hh
            newLine.endTime = [NSStringWithCMTimeSupport convertCMTimeFromTimeCodeWithFrames:([[ (GDataXMLElement *)line attributeForName:@"end"] stringValue])];  // need to convert from hh:mm:ss:hh
            newLine.regionID = [[ (GDataXMLElement *)line attributeForName:@"region"] stringValue];
            newLine.style = [[ (GDataXMLElement *)line attributeForName:@"style"] stringValue];
            
            // Convert origin floats from string
            NSString *originString = [[[ (GDataXMLElement *)line attributeForName:@"tts:origin"] stringValue] stringByReplacingOccurrencesOfString:@"%" withString:@""];
            NSArray *originComponentArray = [originString componentsSeparatedByString:@" "];
            NSString *originXSubString = nil;
            NSString *originYSubString = nil;
            if (originComponentArray.count > 0) {
                originXSubString = [originComponentArray objectAtIndex:0];
                originYSubString = [originComponentArray objectAtIndex:1];
            }
            newLine.originXPercent = [originXSubString floatValue] / 100.0;
            newLine.originYPercent = [originYSubString floatValue] / 100.0;
            
            // Convert extent floats from string
            NSString *extentString = [[[ (GDataXMLElement *)line attributeForName:@"tts:extent"] stringValue] stringByReplacingOccurrencesOfString:@"%" withString:@""];
            NSArray *extentComponentArray = [extentString componentsSeparatedByString:@" "];
            NSString *extentWidthSubString = nil;
            NSString *extentHeightSubString = nil;
            if (extentComponentArray.count > 0) {
                extentWidthSubString = [extentComponentArray objectAtIndex:0];
                extentHeightSubString = [extentComponentArray objectAtIndex:1];
            }
            newLine.widthPercent = [extentWidthSubString floatValue] / 100.0;
            newLine.heightPercent = [extentHeightSubString floatValue] / 100.0;
            
            // Get the line of dialogue
            //            NSMutableAttributedString *mutableFormattedLine = [[NSMutableAttributedString alloc] init];
            //            
            //            UIColor *lineBackgroundColor = [[closedCaption.styles objectForKey:newLine.style] styleBackgroundColor];
            //            newLine.backgroundColor = lineBackgroundColor;
            UIColor *lineTextColor = [[closedCaption.styles objectForKey:newLine.style] styleColor];
            if (!lineTextColor) {
                lineTextColor = [ UIColor whiteColor ];
            }
            NSString *html = [line XMLString] ;
            newLine.theLine = [ self contentFromHTML:html lineTextColor:lineTextColor];
            
            [tempLines addObject:newLine];
        }
        
        closedCaption.closedCaptionLines = tempLines;
        
        dataHandler.returnedObject = closedCaption;
        [ [ TPMasterDataManager sharedInstance ]objectCreated:dataHandler ];
    }
    else
    {
        [ [ TPMasterDataManager sharedInstance ]errorCreatingObject:dataHandler ];
    }
}



#pragma - mark helpful methods

- ( UIColor *)getUIColorForSMPTEName:(NSString *)colorName
{
    return [self.smpteTTColors objectForKey:colorName];
}

- ( TPCCContent *)contentFromHTML:( NSString *)html lineTextColor:(UIColor *)lineTextColor;
{
//    DLog(@"html = %@", html);

    TPCCContent *contentForLine = [ [ TPCCContent alloc]init];
    NSMutableArray *tmpStrings = [@[] mutableCopy];
    NSMutableArray *tmpFontStyles = [@[] mutableCopy];
    
    NSScanner *stringScanner = [[ NSScanner alloc] initWithString:html ];
    [ stringScanner setCharactersToBeSkipped:nil ];
    NSString *tagText = nil;
    // ignore the initial paragraph attributes for now
    [ stringScanner scanUpToString:@">" intoString:&tagText ];
    [ stringScanner scanString:@">" intoString:NULL ]; // just scan past the tag close
    
    BOOL showContent = YES;  // to turn debugging on and off
    NSString *currentAttributes = @"standard";

    while(! [ stringScanner isAtEnd ])
    {
        NSString *contentText;
        [ stringScanner scanUpToString:@"<" intoString:&contentText ];
        if (contentText.length) {
            [ tmpStrings addObject:[contentText copy ]];
            [ tmpFontStyles addObject:currentAttributes];
            if (showContent) {
//                DLog(@"contentText = %@ length = %d", contentText, contentText.length);
//                DLog(@"tmpStrings = %@", tmpStrings );
//                DLog(@"tmpFontStyles = %@", tmpFontStyles );
            }
        }
        
        // now we know we have another tag
        NSString *tagText;
        [ stringScanner scanUpToString:@">" intoString:&tagText];
        if ([tagText hasPrefix:@"<span tts:fontStyle="])  // we care about bold and italic - nothing else
        {
            if ([ tagText hasSuffix:@"\"italic\""]) {
                showContent = YES;
                currentAttributes = @"italic";
            }
            if ([ tagText hasSuffix:@"\"bold\""]) {
                showContent = YES;
                currentAttributes = @"bold";
            }  
        }
        else
        {
            currentAttributes = @"standard";
            showContent = NO;
        }
//        DLog(@"currentAttributes = %@", currentAttributes );
//        DLog(@"tagText = %@", tagText );
        [ stringScanner scanString:@">" intoString:NULL ]; // just scan past the tag close
    }   
    contentForLine.strings = tmpStrings;
    contentForLine.fontStyles = tmpFontStyles;
    return contentForLine;
}

- ( NSAttributedString *)stringFromHTML:( NSString *)html lineTextColor:( UIColor *)lineTextColor;
{
    NSDictionary *stringAttributes = @{   NSFontAttributeName: self.font, NSForegroundColorAttributeName: lineTextColor };
    NSDictionary *boldStringAttributes = @{   NSFontAttributeName: self.boldFont, NSForegroundColorAttributeName: lineTextColor };
    NSDictionary *italicStringAttributes = @{   NSFontAttributeName: self.italicFont, NSForegroundColorAttributeName: lineTextColor };
    
    NSScanner *stringScanner = [[ NSScanner alloc] initWithString:html ];
    [ stringScanner setCharactersToBeSkipped:nil ];
    NSMutableAttributedString *basicString = [[ NSMutableAttributedString alloc ]initWithString:@"" attributes:stringAttributes ];
    
    NSString *tagText = nil;
    // ignore the initial paragraph attributes for now
    [ stringScanner scanUpToString:@">" intoString:&tagText ];
    [ stringScanner scanString:@">" intoString:NULL ]; // just scan past the tag close
    
    // initially the currentAttributes are the basic ones
    NSDictionary *currentAttributes = stringAttributes;
    BOOL showContent = NO;
    
    while(! [ stringScanner isAtEnd ])
    {
        NSString *contentText;
        [ stringScanner scanUpToString:@"<" intoString:&contentText ];
        if (contentText.length) {
            NSAttributedString *attributedContentString = [[NSAttributedString alloc]initWithString:contentText attributes:currentAttributes ];
            [ basicString appendAttributedString:attributedContentString ];
            if (showContent) {
//                DLog(@"html = %@", html);
//                DLog(@"attributedContentString = %@", attributedContentString);
//                DLog(@"contentText = %@ length = %d", contentText, contentText.length);
//                DLog(@"basicString = %@", basicString );
            }
        }
        
        // now we know we have another tag
        NSString *tagText;
        [ stringScanner scanUpToString:@">" intoString:&tagText];
        if ([tagText hasPrefix:@"<span tts:fontStyle="])  // we care about bold and italic - nothing else
        {
            if ([ tagText hasSuffix:@"\"italic\""]) {
                showContent = YES;
                currentAttributes = italicStringAttributes;
            }
            if ([ tagText hasSuffix:@"\"bold\""]) {
                showContent = YES;
                currentAttributes = boldStringAttributes;
            }  
        }
        else
        {
            currentAttributes = stringAttributes;
            showContent = NO;
        }
        [ stringScanner scanString:@">" intoString:NULL ]; // just scan past the tag close
    }   
//    DLog(@"basic string is now %@", basicString );
    return basicString;
}


@end

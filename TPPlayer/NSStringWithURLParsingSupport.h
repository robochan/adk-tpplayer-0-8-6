//
//  NSStringWithURLParsingSupport.h
//  TPPlayer
//


#import <Foundation/Foundation.h>

@interface NSString (TP_URL_PARSING)

- ( NSMutableDictionary *)parametersDictionary;
- ( NSString *)baseURL;
- ( NSString *)urlStringWithNewParameters:( NSDictionary *)parameters;

@end

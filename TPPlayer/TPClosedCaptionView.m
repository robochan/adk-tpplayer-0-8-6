//
//  TPClosedCaptionView.m
//
//
//  Copyright © 2013, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)
//

#import "TPClosedCaptionView.h"

@interface TPClosedCaptionView()

@property(nonatomic, assign)CGFloat originXPercent;
@property(nonatomic, assign)CGFloat originYPercent;
@property(nonatomic, assign)CGFloat widthPercent;
@property(nonatomic, assign)CGFloat heightPercent;
@property(nonatomic, assign)CGRect movieContentViewFrame;

@end

@implementation TPClosedCaptionView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.label = [[UILabel alloc ]initWithFrame:self.bounds ];
//        self.label.numberOfLines = 0;  // only for subtitles
        self.label.backgroundColor = [ UIColor blackColor];
        [ self addSubview:self.label];
    }
    return self;
}

- ( void )adjustFrame:( TPCCLine *)lineToShow superviewFrame:( CGRect)superviewFrame;
{
    // we need to save these for rotation adjustments
    self.originXPercent = lineToShow.originXPercent;
    self.originYPercent = lineToShow.originYPercent;
//    self.widthPercent = lineToShow.widthPercent;
//    self.heightPercent = lineToShow.heightPercent;
    self.movieContentViewFrame = superviewFrame;
    [ self adjustFrame ];
    DLog(@"adjustFrame superview frame = %@ my frame = %@", NSStringFromCGRect(superviewFrame), NSStringFromCGRect(self.frame) );
}

- ( void )adjustFrame;
{
    CGRect frameToUse = self.movieContentViewFrame;
    
    CGFloat originX = CGRectGetMinX(frameToUse) + (CGRectGetWidth(frameToUse) * self.originXPercent);
    CGFloat originY = CGRectGetMinY(frameToUse)  + (CGRectGetHeight(frameToUse) * self.originYPercent);
//    CGFloat width = (frameToUse.size.width * self.widthPercent) * 0.80; // Testing reduced width - REMOVE or VALIDATE
//    CGFloat height = frameToUse.size.height * self.heightPercent;
    self.frame = CGRectIntegral( CGRectMake(originX  - self.cornerRadius, originY  - self.cornerRadius, CGRectGetWidth( self.bounds ), CGRectGetHeight( self.bounds )) );
    self.frame = CGRectIntegral( CGRectMake(originX, originY, CGRectGetWidth( self.bounds ), CGRectGetHeight( self.bounds )) );
    DLog(@"adjustFrame for view = %@",  self);
}


- ( void )adjustSizeForLine;
{
    CGRect currentFrame = self.frame;
    DLog(@"label frame before size to fit = %@", NSStringFromCGRect(self.label.frame));

    [ self.label sizeToFit];
    DLog(@"label frame after size to fit = %@", NSStringFromCGRect(self.label.frame));
    CGRect labelFrame = self.label.frame;

    labelFrame.origin = CGPointMake(self.cornerRadius, self.cornerRadius/2.0);
    self.label.frame = labelFrame;
    currentFrame.size = CGSizeMake(CGRectGetWidth(labelFrame) + ( 2 * self.cornerRadius), CGRectGetHeight(labelFrame) + ( self.cornerRadius));
    DLog(@"currentFrame = %@", NSStringFromCGRect(currentFrame));
    self.frame = currentFrame;
    if (self.cornerRadius > 0.0) {
        CALayer *viewLayer = self.layer;
        viewLayer.cornerRadius = self.cornerRadius;
        viewLayer.masksToBounds = YES;
    }
}

@end

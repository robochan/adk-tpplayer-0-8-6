//
//  TPMediaAccessibilityStyler.h
//  TPPlayer
//
//

#import <UIKit/UIKit.h>
#import "TPCCContent.h"

extern NSString *const kTPMediaAccessibilityStylerValuesChanged;

@interface TPMediaAccessibilityStyler : NSObject

@property(nonatomic, assign) CGFloat roundedCornerRadius; //The system setting for the caption window’s corner radius - not settable by users
@property(nonatomic, strong)UIColor *windowColor;  // preferred color displayed behind all other caption elements

+( TPMediaAccessibilityStyler *)sharedInstance;

- ( NSAttributedString *)styleCCContent:( TPCCContent *)lineToStyle isClosedCaption:( BOOL )stringIsClosedCaption;


@end

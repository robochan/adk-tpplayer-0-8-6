//
//  TPClosedCaptionManager.h
//
//
//  Copyright © 2013, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)
//
//
//  Compatibility: iOS 5.1+
//
//  SUMMARY
//  Top level controller class for Closed Captions. It interfaces directly to the Player,
//  so you should not need to directly call any method herein.



#import <UIKit/UIKit.h>
#import "TPClosedCaption.h"
#import "TPPlayerView.h"
#import <MediaPlayer/MPMoviePlayerController.h>
#import "TPHeartbeatObserver.h"
#import <AVFoundation/AVFoundation.h>

extern NSString *const kTPClosedCaptionManagerFrameChanged;

@interface TPClosedCaptionManager : NSObject<HeartbeatObserver>

@property( nonatomic, strong)TPClosedCaption *closedCaptionInfo;
@property (strong, nonatomic) TPPlayerView *playerView;
@property(nonatomic, assign)CGRect frameToUse;
@property(nonatomic, assign)NSUInteger pointSize;

// Apple reports the native video source sizes when the .naturalSize property is returned.
// However, the video as displayed on-screen may be scaled up or down. Thus it is
// necessary to calculate the actual visible rectangle of the video. This varies
// further in portrait, landscape, and in fullscreen for both orientations and additional
// calculation must be done hence the BOOL. The final correct rectangle is used for the
// scaling and positioning of the Closed Cpations per the SMPTE-TT specification (SMPTE ST 2052-1:2010).
- ( void )resetFrameToUse:( BOOL)showingFullScreen;

@end

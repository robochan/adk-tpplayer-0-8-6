//
//  TPVideoManagerViewController.h
//
//  Copyright © 2013, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)
//
//  Compatibility: iOS 5.1+
//
//  SUMMARY
//  Supports VOD and live-stream. Supports AdManager pre-, post-, mid-roll, and overlay ads.
//  Has quartile and heartbeat analytics reporting. Heartbeat can be configured in the intervalsToMonitor
//  array with variable length, endPoint, etc. See TPInterval for more configuration information.
//
//  Due to continuing Apple fullscreen composite bug (r. 12451302) the view controller will
//  spawn a separate view controller for fullscreen and move the player view into the new
//  view controller. On current devices this happens seamlessly.
//
//  DOCUMENTATION Notes (Remove when docs are complete)
//  HN - We need to be sure to explain the cycle structure of the IntervalManager
//  HN - Let's add an example timeline to visualize the event, quartile, and interval reporting
//
//  DEVELOPMENT Notes
//  - Pull Airplay, autoplay, etc. out of .m for easier configuration
//  DE - Review for encapsulation as we move to static linked lib

#import <UIKit/UIKit.h>
#import <MediaPlayer/MPMoviePlayerController.h>
#import "TPIntervalManager.h"
#import "TPAdManager.h"
#import "TPPlayerControls.h"
#import "TPClosedCaption.h"
#import "TPConcurrencyInfo.h"
#import "TPPlayerConfiguration.h"
#import "TPHeartbeatObserver.h"
#import "TPExternalWindowViewController.h"

@class TPVideoManagerViewController;

@protocol TPVideoManagerDelegate<NSObject>

// These are methods that are emitted to the delegate
- ( void )moviePlaybackDidFinish:(NSNotification *)notification;  // only sent when the movie reaches the end
//- ( void )moviePlaybackStateChanged:( NSUInteger )reason;
- ( void )videoEnteredFullScreen;
//- ( void )videoPaused;
//- ( void )videoEnded;
//- ( void )videoStarted;
//- ( void )videoStopped;
- ( void )timeHasPassedQuartile:( NSUInteger)quartile;
- ( void )moviePlaybackAndAdsDidFinish;
- ( NSString *)videoTitle;
//- ( void )storeCurrentPlaybackTime:( NSTimeInterval )currentPlaybackTime duration:( NSTimeInterval)duration;
- ( void )adStarted:( NSDictionary *)notificationDictionary adPosition:( NSString *)adPosition;
- ( void )adEnded:( NSDictionary *)notificationDictionary adPosition:( NSString *)adPosition;
- ( void )presentFullScreenVideoManagerVC:( TPVideoManagerViewController *)videoManagerViewController;
- ( void )restoreFullScreenVideoManagerVC:( TPVideoManagerViewController *)videoManagerViewController;
- ( void )videoStartedPlayingFromBeginning;
- ( void )videoStartedPlayingFromResume;

@optional
//- ( void )timeHasPassedOtherTimeMark:( NSUInteger)whichTimeMark;
- ( void )intervalFired:( TPInterval *)interval;
- ( void )userExitedFullscreen;
- ( BOOL )alwaysShowDoneButton;
- ( void )userSelectedDoneButton;
- ( void )userPausedPlayer;
- ( void )slotStarted:( TPAdPod *)adPod;
- ( void )slotEnded:( TPAdPod *)adPod;

- ( void )adStarted:( TPAdObject *)adObject;
- ( void )adEnded:( TPAdObject *)adObject;
- ( void )userStartedScrubbing;

- ( void )videoStalled;
- ( void )stallResolved;
- ( void )videoLoading:( BOOL )finished;

@end

@interface TPVideoManagerViewController : UIViewController<TPIntervalManagerDelegate, TPAdManagerOwner, TPPlayerControlsDelegate, UIAlertViewDelegate >

@property(nonatomic, strong)AVPlayer *player;
@property (copy, nonatomic) NSString *aboutID;
@property (weak, nonatomic )id<TPVideoManagerDelegate> delegate;
@property(nonatomic, assign)BOOL playsFullScreenOnly;
@property (nonatomic, assign)BOOL showsFullScreen;
@property (nonatomic, assign)BOOL clearedToDismiss;

@property(nonatomic, assign )BOOL shouldTrackQuartiles;
@property(nonatomic, strong )NSArray *intervalsToMonitor;
@property(nonatomic, assign)BOOL shouldShowAdTicks;
@property(nonatomic, assign)BOOL shouldAllowAirplay;
@property(nonatomic, assign)BOOL shouldOfferResume;
@property (assign, nonatomic)NSTimeInterval startTime;

@property(nonatomic, assign, readonly)NSTimeInterval currentPlaybackTime;
@property(nonatomic, assign, readonly)NSTimeInterval duration;

@property (strong, nonatomic)TPAdManager *adManager;  // optional
@property(nonatomic, strong)TPPlayerControls *playerControlsController;

// these next 3 are set by TPPlayerViewController
@property(nonatomic, copy )NSString *playerControlClassName; // nil unless we want to subclass TPPlayerControls - then must be a subclass of TPPlayerControls
@property(nonatomic, weak)UIViewController *interestedParty;  // usually nil - only here for TPPlayerControls subclasses to use to get messages out - probably to the VC that owns TPPlayerVC
@property(nonatomic, assign)BOOL playerControlsRequireTapToHide; // default is NO, so if you don't use this, the controls default to fading out.  If you set this to YES, a gesture recognizer will cause the taps to hide and show.

@property(nonatomic, strong)TPConcurrencyInfo *concurrencyInfo;
@property(nonatomic, strong )TPPlayerConfiguration *configuration;

@property (nonatomic, strong) UIWindow *externalWindow;
@property (nonatomic, strong) CADisplayLink *displayLink;
@property(nonatomic, strong)TPExternalWindowViewController *externalWindowViewController;
@property(nonatomic, assign)BOOL shouldKeepAlive; // under special circumstances, you might need the view to disappear without stopping the player - this is an override that lets you do that.
@property(nonatomic, strong)NSArray *externalHeartbeatObservers; // if there are external objects that need to get a heartbeat when the player is playing, make them conform to the HeartbeatObserver protocol (in TPHeartbeatObserver.h) and include them in the externalHeartbeatObservers array.
@property(nonatomic, assign)BOOL inlineClosedCaptionsPresent;


- (id)initWithFrame:(CGRect)frame;

//  Called by TPPlayerViewController when it receives a message by the same name
- ( void )reachabilityChanged:( BOOL )networkIsReachable;


// These are not called until authorization has been handled
- ( void )playVideoWithURL:( NSURL *)movieURL adManagerId:( NSString *)adManagerContentId;
- ( void )playVideoWithURL:( NSURL *)movieURL; // Non-AdManager

- ( void )stopVideo;
- ( void )checkQuartiles:( NSTimer *)quartileTimer;
- ( void )prepareForLiveStream:( BOOL)videoIsLiveStream;
- ( void)setUpClosedCaptioning:( TPClosedCaption *)closedCaptionInfo;

// methods called by AdManager if there is one
- (void)playContentMovie;
- (void)cleanup;

// heartbeat methods
- ( void )addHeartbeatObserver:( id )newObserver;
- ( void )removeHeartbeatObserver:( id )observerToRemove;


@end

//
//  TPExternalWindowViewController.m
//  TPPlayer
//
//  Copyright © 2014, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)
//

#import "TPExternalWindowViewController.h"

@interface TPExternalWindowViewController ()

@end

@implementation TPExternalWindowViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- ( void )viewDidAppear:(BOOL)animated  ;
{
    [ super viewDidAppear:animated];
    DLog( );
}

- ( void )viewWillDisappear:(BOOL)animated  ;
{
    [ super viewWillDisappear:animated];
    DLog( );
}

- ( void )viewWillLayoutSubviews;
{
    [ super viewWillLayoutSubviews];
    DLog( );
}


@end

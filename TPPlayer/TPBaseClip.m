//
//  TPBaseClip.m
//
//  Copyright © 2013, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)
//


#import "TPBaseClip.h"
#import "TPDataHandler.h"
#import "TPMasterDataManager.h"

@interface TPBaseClip()
@property(nonatomic, assign)BOOL receivedRatingSource;
@property(nonatomic, assign)BOOL receivedAdvisorySource;

@property(nonatomic, weak)id delegate;
@property(nonatomic, assign)SEL selector;

@end

@implementation TPBaseClip

-( BOOL)hasAllNecessarySMIL;
{
    BOOL needsAtLeastOne = self.bumperURL || self.preroll;
    if (! needsAtLeastOne)  // usual case - we need neither one because we have no pre-roll
    {
        return YES;
    }
    BOOL ratingSourceIsOK = self.bumperURL ? self.receivedRatingSource : YES;
    BOOL advisorySourceIsOK = self.preroll ? self.receivedAdvisorySource : YES;
    return ratingSourceIsOK && advisorySourceIsOK;
}

- ( void )getPreRollSmilInfoForDelegate:( id)delegate selector:( SEL)selector;
{
    self.delegate = delegate;
    self.selector = selector;
    
    if (self.bumperURL && ! self.receivedRatingSource) {
        [[ TPMasterDataManager sharedInstance ] loadSimpleSmilDataForUrl:self.bumperURL metadata:@"rating"  delegate:self selector:@selector(smilDataLoaded:)];
    }
    if (self.preroll && ! self.receivedAdvisorySource) {
        [[ TPMasterDataManager sharedInstance ] loadSimpleSmilDataForUrl:self.preroll metadata:@"advisory"  delegate:self selector:@selector(smilDataLoaded:)];
    }
}

- ( void )smilDataLoaded:( TPDataHandler *)dataHandler;
{
    NSError *error = dataHandler.error;
    // mark these whether or not there was an error
    if ([dataHandler.metadata isEqualToString:@"rating" ]) 
    {
        self.receivedRatingSource = YES;
    }
    else
    {
        self.receivedAdvisorySource = YES;
    }
    if (error == nil)
    {
        NSString *sourceURL = dataHandler.returnedObject;
        if ([dataHandler.metadata isEqualToString:@"rating" ]) 
        {
            self.ratingSource = [sourceURL length] > 0 ? sourceURL : nil;
        }
        else
        {
            self.advisorySource = [sourceURL length] > 0 ? sourceURL : nil;
        }
    }
    if (self.hasAllNecessarySMIL) {
        [self.delegate performSelectorOnMainThread:self.selector withObject:nil waitUntilDone:NO];
    }
}


- ( id )initWithCoder:(NSCoder *)aDecoder
{
    self = [ super init ];
    self.guid = [aDecoder decodeObjectForKey:@"guid"];
    self.url = [aDecoder decodeObjectForKey:@"url"];
    self.type = [aDecoder decodeObjectForKey:@"type"];
    self.title = [aDecoder decodeObjectForKey:@"title"];
    self.abstract = [aDecoder decodeObjectForKey:@"abstract"];
    self.freewheelAssetID = [aDecoder decodeObjectForKey:@"freewheelAssetID"];
    self.keywords = [aDecoder decodeObjectForKey:@"keywords"];
    self.categories = [aDecoder decodeObjectForKey:@"categories"];
    self.copyright = [aDecoder decodeObjectForKey:@"copyright"];
    self.cdn = [aDecoder decodeObjectForKey:@"cdn"];
    self.clips = [aDecoder decodeObjectForKey:@"clips"];
    self.CCURL = [aDecoder decodeObjectForKey:@"CCURL"];
    self.bumperURL = [aDecoder decodeObjectForKey:@"bumperURL"];
    self.preroll = [aDecoder decodeObjectForKey:@"preroll"];
    self.width = [aDecoder decodeIntForKey:@"width"];
    self.height = [aDecoder decodeIntForKey:@"height"];
    self.encryptScripts = [aDecoder decodeBoolForKey:@"encryptScripts"];    
    
    return self;
}

- ( void )encodeWithCoder:(NSCoder *)aCoder;
{
    [ aCoder encodeObject:self.guid forKey:@"guid" ];
    [ aCoder encodeObject:self.url forKey:@"url" ];
    [ aCoder encodeObject:self.type forKey:@"type" ];
    [ aCoder encodeObject:self.title forKey:@"title" ];
    [ aCoder encodeObject:self.abstract forKey:@"abstract" ];
    [ aCoder encodeObject:self.freewheelAssetID forKey:@"freewheelAssetID" ];
    [ aCoder encodeObject:self.keywords forKey:@"keywords" ];
    [ aCoder encodeObject:self.categories forKey:@"categories" ];
    [ aCoder encodeObject:self.copyright forKey:@"copyright" ];
    [ aCoder encodeObject:self.cdn forKey:@"cdn" ];
    [ aCoder encodeObject:self.clips forKey:@"clips" ];
    [ aCoder encodeObject:self.CCURL forKey:@"CCURL" ];
    [ aCoder encodeObject:self.bumperURL forKey:@"bumperURL" ];
    [ aCoder encodeObject:self.preroll forKey:@"preroll" ];
    [ aCoder encodeInteger:self.width forKey:@"width"];
    [ aCoder encodeInteger:self.height forKey:@"width"];
    [ aCoder encodeBool:self.encryptScripts forKey:@"encryptScripts"];
    
}

@end

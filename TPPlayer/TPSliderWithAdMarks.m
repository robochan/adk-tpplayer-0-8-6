//
//  TPSliderWithAdMarks.m
//
//  Copyright © 2013, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)
//
//

#import "TPSliderWithAdMarks.h"
#import "TPTickMarkOverlayView.h"

@interface TPSliderWithAdMarks()

@end

@implementation TPSliderWithAdMarks

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- ( void)awakeFromNib;
{
    self.backgroundColor = [ UIColor clearColor]; // on the off chance you forgot to do it in the nib
    if (self.datasource) {
        NSArray *marksArray = [ self.datasource tickMarksArray];
        [ self addOverlayView:marksArray];
    }
}

- ( void )layoutSubviews;
{
    TPTickMarkOverlayView *overlayView = self.horizontalSlider.overlayView;
    if (overlayView) {
        [ overlayView setSliderFrame:self.horizontalSlider.frame ];
    }
}

- ( void)setUpAdTicks:( NSArray *)tickMarkPositions;
{
    // on the off-chance this has already been done...
    for( UIView *subview in self.subviews)
    {
        if ( [ subview isKindOfClass:[ TPTickMarkOverlayView class ]]) {
            [ subview removeFromSuperview];
        }
    }
    [ self addOverlayView:tickMarkPositions];
}

- ( void )addOverlayView:( NSArray *)marksArray;
{
    if (self.datasource && marksArray.count) {
        NSString *imageName = [ self.datasource tickMarkImageName];        
        NSString *imageFileName = [ imageName stringByDeletingPathExtension];

        UIImage *tickImage = [ UIImage imageNamed:imageFileName];
        if (tickImage) {
            TPTickMarkOverlayView *overlayView = [[ TPTickMarkOverlayView alloc]initWithFrame:self.bounds ];
            overlayView.sliderFrame = self.horizontalSlider.frame;
            overlayView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
            overlayView.backgroundColor = [ UIColor clearColor];
            overlayView.opaque = NO;
            [ overlayView makeMarks:marksArray withImage:tickImage];
            overlayView.userInteractionEnabled = YES;
            self.horizontalSlider.overlayView = overlayView;
            [ self addSubview:overlayView];
            [ self setNeedsDisplay];
        }
    }
}


@end

//
//  TPSort.m
//
//  Copyright © 2013, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)
//


#import "TPSort.h"

@implementation TPSort

- ( id )copyWithZone:(NSZone *)zone;
{
    TPSort *newCopy = [[[ self class ]alloc ]init ];
    if (newCopy ) {
        newCopy.fieldName = [self.fieldName copy];
        newCopy.isDescending = self.isDescending;
    }
    return newCopy;
}

@end

//
//  TPVideoOwner.m
//
//  Copyright © 2013, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)
//

#import "TPVideoOwner.h"
#import "TPDataHandler.h"

@implementation TPVideoOwner
@synthesize videos = _videos;

- ( id )initWithCoder:(NSCoder *)aDecoder
{
    self = [ super initWithCoder:aDecoder ];
    self.videos = [ aDecoder decodeObjectForKey:@"videos" ];
    return self;
}

- ( void )encodeWithCoder:(NSCoder *)aCoder;
{
    [ super encodeWithCoder:aCoder ];
    [ aCoder encodeObject:self.videos forKey:@"videos" ];    
}
@end

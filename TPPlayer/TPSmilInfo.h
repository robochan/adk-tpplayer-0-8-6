//  TPSmilInfo.h
//
//
//  Copyright © 2013, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)
//
//  Compatibility: iOS 5.1+
//
//  SUMMARY
//  TPSmilInfo object contains smil data in the source field, but it can also contain other useful information including:
//  - a url for a closed caption smil file
//  - a url for an advisory smil file
//  - a url for a ratings smil file
//  - an id to use for freewheel ads
//  If there are urls for either the advisory or the ratings, they are constructed in situ and their source is included in 
//  the ratingSource field and the advisorySource field.  


#import <Foundation/Foundation.h>
#import "TPDataHandler.h"
#import "TPClosedCaption.h"
#import "TPConcurrencyInfo.h"


@interface TPSmilInfo : NSObject

@property(nonatomic, strong)NSString *source;
@property(nonatomic, strong)NSString *advisoryURL;
@property(nonatomic, strong)NSString *ratingURL;
@property(nonatomic, strong)NSString *ratingSource;
@property(nonatomic, strong)NSString *advisorySource;
@property(nonatomic, strong)NSString *freewheelID;
@property(nonatomic, strong)NSString *ccURL;
@property(nonatomic, strong)TPConcurrencyInfo *concurrencyInfo; // only present if the content has concurrency restrictions


- ( BOOL)hasAllNecessarySMIL;
- ( void )getPreRollSmilInfoForDelegate:( id)delegate selector:( SEL)selector;
- ( void )smilDataLoaded:( TPDataHandler *)dataHandler;

@end

//
//  TPJSONManager.h
//  TPPlayer
//
//
//  Copyright © 2013, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)
//

#import <Foundation/Foundation.h>

@class TPDataHandler;


@interface TPJSONManager : NSObject
+( TPJSONManager *)sharedInstance;
- ( void )createObjectFromDataHandler:( TPDataHandler *)dataHandler;

@end

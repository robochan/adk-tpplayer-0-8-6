//
//  FNResumeOverlay.h
//
//  Copyright © 2013, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)
//

#import <UIKit/UIKit.h>

@interface TPResumeOverlay : UIView

@property (weak, nonatomic) IBOutlet UIButton *playFromBeginningButton;
@property (weak, nonatomic) IBOutlet UIButton *resumePlayingButton;

+ ( TPResumeOverlay *)resumeOverlayView;

@end

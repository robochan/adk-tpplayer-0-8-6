//
//  TPContentItem.m
//
//  Copyright © 2013, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)
//

#import "TPContentItem.h"
#import "TPSmartImage.h"
#import "TPCategory.h"
#import "TPCredit.h"
#import "TPMasterDataManager.h"
#import "TPAppSpecificInformationManager.h"

@implementation TPContentItem

- ( id )initFromPreviewJson:( NSDictionary *)jsonObjects andURLString:( NSString *)basicURLString;
{
    if (self = [super init]) {
        self.title = jsonObjects[@"title"];
        _synopsis = jsonObjects[@"description"];
        _adManagerId = jsonObjects[@"pl1$freewheelAssetId"];
        _guid = jsonObjects[@"guid"];
        
        NSMutableDictionary *tmpDict = [ NSMutableDictionary dictionary ];
        TPSmartImage *defaultThumbnailSmartImage = [ TPSmartImage smartImageFromString:jsonObjects[@"defaultThumbnailUrl"] withParentId:_guid ];
        if (defaultThumbnailSmartImage) {
            [ tmpDict setObject:defaultThumbnailSmartImage forKey:@"defaultThumbnailUrl"];
        }
        TPSmartImage *thumbnailSmartImage = [  TPSmartImage smartImageFromString:jsonObjects[@"thumbnailUrl"] withParentId:_guid ];
        if (thumbnailSmartImage) {
                    [ tmpDict setObject:thumbnailSmartImage forKey:@"thumbnailURL"];
        }
        self.roleForDefaultThumbnail = @"defaultThumbnailUrl";
        self.roleForDefaultPosterImage = @"defaultThumbnailUrl";
        
        if (_guid) {
            self.images = [ self imageDictionaryFromJson:jsonObjects ];
        }
        
        NSArray *components = [ basicURLString componentsSeparatedByString:@"?"];
        _videoAsset = [ [ TPVideoAsset alloc ]initFromPreviewJson:jsonObjects andBasicURLString:components[0] ];
    }
    return self;
}

- ( id )initContentItemFromJson:( NSDictionary *)jsonObjects;
{
    if (self = [super init]) {
        [ self populateProperties:jsonObjects ];
    }
    return self;
}
    
- ( void )populateProperties:( NSDictionary *)jsonObjects;
{
    self.title = jsonObjects[@"title"];
    _textDescription = jsonObjects[@"description"];
    _identifier = [jsonObjects[@"id"] lastPathComponent]; // TODO - this is in the full URL form. Do we need an additional property to hold the mpxID by itself?
    _idURL = jsonObjects[@"id"];
    _guid = jsonObjects[@"guid"];
    
    // The following values are long's so they must be converted to double's rather than int's
    NSString *availableDateAsString = jsonObjects[@"media$availableDate"];
    NSTimeInterval availableDateAsTimeInterval = [availableDateAsString doubleValue]/1000;
    NSDate *availableDateAsDate = [[NSDate alloc] initWithTimeIntervalSince1970:availableDateAsTimeInterval];
    _availableDate = availableDateAsDate;
    
    NSString *expirationDateAsString = jsonObjects[@"media$expirationDate"];
    NSTimeInterval expirationDateAsTimeInterval = [expirationDateAsString doubleValue]/1000;
    NSDate *expirationDateAsDate = [[NSDate alloc] initWithTimeIntervalSince1970:expirationDateAsTimeInterval];
    _expirationDate = expirationDateAsDate;
    
    NSString *pubDateAsString = jsonObjects[@"pubDate"];
    NSTimeInterval pubDateAsTimeInterval = [pubDateAsString doubleValue]/1000;
    NSDate *pubDateAsDate = [[NSDate alloc] initWithTimeIntervalSince1970:pubDateAsTimeInterval];
    _pubDate = pubDateAsDate;
    
    _copyright = jsonObjects[@"media$copyright"];
    _copyrightUrl = jsonObjects[@"media$copyrightUrl"];
    
    _countries = jsonObjects[@"media$countries"];
    _excludeCountries = jsonObjects[@"media$excludeCountries"];
    
    NSArray *tmpKeywords = [jsonObjects[@"media$keywords"] componentsSeparatedByString:@","];
    _keywords = tmpKeywords;
    
    _text = jsonObjects[@"media$text"];
    
    _ratings = jsonObjects[@"media$ratings"];  // TODO - Do we need a TPRating object? or shall we just hold the dictionaries?
    
    // Create TPVideoAssets
    NSMutableArray *tmpVideoAssets = [NSMutableArray array];
    NSArray *mediaContentItems = jsonObjects[@"media$content"];
    for (NSDictionary *videoDictionary in mediaContentItems) {
        TPVideoAsset *tmpVideoAsset = [[TPVideoAsset alloc] initWithJsonDictionary:videoDictionary];
        [tmpVideoAssets addObject:tmpVideoAsset];
    }
    self.videoAssets = tmpVideoAssets;
           
    self.images = [ self imageDictionaryFromJson:jsonObjects ];
    
    // Create Categories array
    NSMutableArray *tmpCategories = [NSMutableArray array];
    NSArray *mediaCategories = jsonObjects[@"media$categories"];
    for (NSDictionary *categoryDictionary in mediaCategories) {
        TPCategory *tmpCategory = [[TPCategory alloc] initWithDictionaryFromJSON:categoryDictionary];
        [tmpCategories addObject:tmpCategory];
    }
    self.categories = tmpCategories;
    
    // Create Credits array
    NSMutableArray *tmpCredits = [NSMutableArray array];
    NSArray *mediaCredits = jsonObjects[@"media$credits"];
    for (NSDictionary *creditDictionary in mediaCredits) {
        TPCredit *tmpCredit = [[TPCredit alloc] initWithDictionaryFromJSON:creditDictionary];
        [tmpCredits addObject:tmpCredit];
    }
    self.credits = tmpCredits;
}

- ( NSDictionary *)imageDictionaryFromJson:( NSDictionary *)jsonObjects;
{
    // Create TPSmartImages
    NSMutableDictionary *tmpImagesDict = [ NSMutableDictionary dictionary ];
    NSString *defaultThumbnailURL = jsonObjects[@"plmedia$defaultThumbnailUrl"];
    
    NSArray *mediaThumbnailItems = jsonObjects[@"media$thumbnails"];
    for (NSDictionary *imageDictionary in mediaThumbnailItems) {
        TPSmartImage *tmpSmartImage = [[TPSmartImage alloc] initWithJsonDictionary:imageDictionary withParentId:_guid ];
        // subclasses can override to implement a scheme for assigning roles to the images
        [ self adjustRole:tmpSmartImage fromJsonDictionary:imageDictionary];
        // don't do this until the role is adjusted -- role is used in the path creation
        NSURL *imageURL = [ [ TPMasterDataManager sharedInstance ].imagesDirectoryURL URLByAppendingPathComponent:tmpSmartImage.pathForImage ];
        tmpSmartImage.imageLocalURL = imageURL;
        if ([tmpSmartImage.imagePath isEqualToString:defaultThumbnailURL ]) {
            self.roleForDefaultThumbnail = tmpSmartImage.role;
        }
        [tmpImagesDict setObject:tmpSmartImage forKey:tmpSmartImage.role];
    } 
    return tmpImagesDict;
}

- ( void )adjustRole:( TPSmartImage *)tmpSmartImage fromJsonDictionary:( NSDictionary *)imageDictionary;
{
    // subclasses can override 
}


- ( TPVideoAsset *)videoAsset;
{
    if (_videoAsset) {
        return _videoAsset;
    }
    return self.videoAssets.count ? self.videoAssets[0] : nil;
}

- ( NSString *)description;
{
    return [NSString stringWithFormat:@"title = %@\nidentifier = %@  --  imagesCount = %lu", self.title, self.identifier, (unsigned long)self.images.count ];
}

- ( void )setValue:(id)value forUndefinedKey:(NSString *)key;
{
    DLog(@"undefined Key = %@", key);
}

- ( NSString *)aboutID;
{
    return self.idURL.length ? self.idURL : self.guid ;
}

- ( id )initWithCoder:(NSCoder *)aDecoder
{
    self = [ super initWithCoder:aDecoder ];
    self.identifier = [ aDecoder decodeObjectForKey:@"identifier" ];
    self.adManagerId = [ aDecoder decodeObjectForKey:@"adManagerId" ];
    self.roleForDefaultThumbnail = [ aDecoder decodeObjectForKey:@"roleForDefaultThumbnail" ];
    self.textDescription = [ aDecoder decodeObjectForKey:@"textDescription" ];
    self.idURL = [ aDecoder decodeObjectForKey:@"idURL" ];
    self.guid = [ aDecoder decodeObjectForKey:@"guid" ];
    
    self.availableDate = [ aDecoder decodeObjectForKey:@"availableDate" ];
    self.categories = [ aDecoder decodeObjectForKey:@"categories" ];
    self.videoAssets = [ aDecoder decodeObjectForKey:@"videoAssets" ];
    self.copyright = [ aDecoder decodeObjectForKey:@"copyright" ];
    
    self.copyrightUrl = [ aDecoder decodeObjectForKey:@"copyrightUrl" ];
    self.synopsis = [ aDecoder decodeObjectForKey:@"synopsis" ];
    self.credits = [ aDecoder decodeObjectForKey:@"credits" ];
    self.countries = [ aDecoder decodeObjectForKey:@"countries" ];
    self.excludeCountries = [ aDecoder decodeObjectForKey:@"excludeCountries" ];
    self.expirationDate= [ aDecoder decodeObjectForKey:@"expirationDate" ];
    self.keywords = [ aDecoder decodeObjectForKey:@"keywords" ];
    self.ratings = [ aDecoder decodeObjectForKey:@"ratings" ];
    self.text = [ aDecoder decodeObjectForKey:@"text" ];
    self.pubDate = [ aDecoder decodeObjectForKey:@"pubDate" ];
    self.requiresAuth = [ aDecoder decodeBoolForKey:@"requiresAuth"];
    self.networkID = [ aDecoder decodeObjectForKey:@"networkID"];
    return self;
}

- ( void )encodeWithCoder:(NSCoder *)aCoder;
{
    [ super encodeWithCoder:aCoder ];
    [ aCoder encodeObject:self.identifier forKey:@"identifier" ];
    [ aCoder encodeObject:self.adManagerId forKey:@"adManagerId" ];
    [ aCoder encodeObject:self.roleForDefaultThumbnail forKey:@"roleForDefaultThumbnail" ];
    [ aCoder encodeObject:self.textDescription forKey:@"textDescription" ];
    [ aCoder encodeObject:self.idURL forKey:@"idURL" ];
    [ aCoder encodeObject:self.guid forKey:@"guid" ];
    
    [ aCoder encodeObject:self.availableDate forKey:@"availableDate" ];
    [ aCoder encodeObject:self.categories forKey:@"categories" ];
    [ aCoder encodeObject:self.videoAssets forKey:@"videoAssets" ];
    [ aCoder encodeObject:self.copyright forKey:@"copyright" ];
    
    [ aCoder encodeObject:self.copyrightUrl forKey:@"copyrightUrl" ];
    [ aCoder encodeObject:self.synopsis forKey:@"synopsis" ];
    [ aCoder encodeObject:self.credits forKey:@"credits" ];
    [ aCoder encodeObject:self.countries forKey:@"countries" ];
    [ aCoder encodeObject:self.excludeCountries forKey:@"excludeCountries" ];
    [ aCoder encodeObject:self.expirationDate forKey:@"expirationDate" ];
    [ aCoder encodeObject:self.keywords forKey:@"keywords" ];
    [ aCoder encodeObject:self.ratings forKey:@"ratings" ];
    [ aCoder encodeObject:self.text forKey:@"text" ];
    [ aCoder encodeObject:self.pubDate forKey:@"pubDate" ];
    [ aCoder encodeBool:self.requiresAuth forKey:@"requiresAuth"];
    [ aCoder encodeObject:self.networkID forKey:@"networkID"];
}
@end

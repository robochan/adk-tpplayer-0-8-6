//
//  TPClosedCaptionManager.m
//
//  Copyright © 2013, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)
//

#import "TPClosedCaptionManager.h"
#import "TPClosedCaptionView.h"
#import <QuartzCore/QuartzCore.h>
#import <CoreText/CoreText.h>
#import "TPCCLine.h"
#import "TPCCTimeInfo.h"
#import <AVFoundation/AVFoundation.h>
#import "TPMediaAccessibilityStyler.h"
#import "TPVideoManagerVCConstants.h"

NSString *const kTPClosedCaptionManagerFrameChanged = @"TPClosedCaptionManagerFrameChanged";

@interface TPClosedCaptionManager ()

@property(nonatomic, strong)NSArray *regionSubviews;
@property(nonatomic, strong) CATextLayer *backgroundTextLayer;
@property(nonatomic, strong)NSArray *captionStartTimes;
@property(nonatomic, assign)NSUInteger indexOfTargetTime;
@property(nonatomic, strong)NSArray *timeInfoCollection;
@property(nonatomic, strong)TPCCTimeInfo *targetTimeInfo;
@property(nonatomic, strong)TPCCTimeInfo *previousTimeInfo;
@property(nonatomic, strong)TPCCTimeInfo *nextTimeInfo;
@property(nonatomic, assign)BOOL timerIsRunning;
@property(nonatomic, strong)NSMutableArray *visibleViews;
@property(nonatomic, assign)CMTime currentPlayTime;
@property(nonatomic, assign)BOOL heartbeatStartedButNoPulseYet;
@property(nonatomic, assign)BOOL waitingForFirstRegion;
@end

@implementation TPClosedCaptionManager

- (id)init;
{
    if (self = [ super init]) {
        self.visibleViews = [@[] mutableCopy ];
    }
    return self;
}

- (void)removeNotificationCenterObserver
{
    NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
    [notificationCenter removeObserver:self];
}

- (void)dealloc
{
    [self performSelectorOnMainThread:@selector(removeNotificationCenterObserver) withObject:self waitUntilDone:YES];
}

- ( void)setClosedCaptionInfo:( TPClosedCaption *)closedCaptionInfo;
{
    _closedCaptionInfo = closedCaptionInfo;
    CGFloat yVal = 0.0;
    CGFloat cornerRadius = [ [ TPMediaAccessibilityStyler sharedInstance]roundedCornerRadius];
    UIColor *newWindowColor = [ [ TPMediaAccessibilityStyler sharedInstance]windowColor];

    [[ NSNotificationCenter defaultCenter]addObserver:self selector:@selector(stylerValuesChanged:) name:kTPMediaAccessibilityStylerValuesChanged object:nil ];
    NSMutableArray *tmpRegionSubviews = [ NSMutableArray array];
    for( TPCCRegion *region in closedCaptionInfo.layoutRegions.allValues )
    {
        // this size is pretty arbitrary -- pick a reasonable size to use
        TPClosedCaptionView *closedCaptionView = [[ TPClosedCaptionView alloc]initWithFrame:CGRectMake(0, yVal, 400, 48) ];
        closedCaptionView.cornerRadius = cornerRadius;
        closedCaptionView.backgroundColor = newWindowColor;
        closedCaptionView.label.backgroundColor = [ UIColor clearColor];
        
        region.regionSubview = closedCaptionView;
        [tmpRegionSubviews addObject:region.regionSubview ];
        yVal += 48;
//        DLog(@" region id = %@ region subview = %@", region.regionID, region.regionSubview)
    }
    self.regionSubviews = tmpRegionSubviews;
    self.timeInfoCollection = [ closedCaptionInfo closeCaptionTimeCollection];
    // set up initial values for timeInfo objects ( previousTimeInfo remains nil)
    self.indexOfTargetTime = 0;
    [ self setUpTimeInfoObjects ];

//    DLog(@"number of items in TimeCollection = %d", self.timeInfoCollection.count);
//    for (TPCCTimeInfo *timeInfo in self.timeInfoCollection) {
//        DLog(@"timeInfo = %@", timeInfo );
//    }
//    [ self setupCaptionTimes ];
}

- ( void )stylerValuesChanged:( NSNotification *)notification;
{
    CGFloat newRoundedCornerRadius = [ [ TPMediaAccessibilityStyler sharedInstance]roundedCornerRadius];
    UIColor *newWindowColor = [ [ TPMediaAccessibilityStyler sharedInstance]windowColor];
    for( TPClosedCaptionView *view in self.regionSubviews )
    {
        view.cornerRadius = newRoundedCornerRadius;
        view.backgroundColor = newWindowColor;
        view.label.backgroundColor = [ UIColor clearColor];
    }
}


- ( void )clearAllRegions;
{
    for( TPClosedCaptionView *view in self.regionSubviews )
    {
        [ self.visibleViews removeObject:view ];
        [view removeFromSuperview ];
    }
}

- ( void )heartbeatStarted;
{
    self.timerIsRunning = YES;
    self.waitingForFirstRegion = YES;
    self.heartbeatStartedButNoPulseYet = YES;
}


- ( void )heartbeatStopped;
{
    self.timerIsRunning = NO;
    self.waitingForFirstRegion = NO;
    [ self clearAllRegions];
}

- ( void )removeRegions:( CMTime )currentPlayTime;
{
    for( TPClosedCaptionView *regionSubview in self.regionSubviews )
    {
        BOOL currentPlayTimeIsLater=  CMTIME_COMPARE_INLINE(currentPlayTime, >=, regionSubview.timeToRemove);
        if (regionSubview.superview &&  currentPlayTimeIsLater) {
            [ self.visibleViews removeObject:regionSubview ];
            [ regionSubview removeFromSuperview];
        }
    }
}

- ( void )pulseHeartbeat:( CMTime)currentPlayTime;
{
    if (CMTIME_IS_NUMERIC(currentPlayTime))
    { 
        self.currentPlayTime = currentPlayTime;
        [ self removeRegions:currentPlayTime];
        if ( CMTimeGetSeconds(currentPlayTime) > 0) // don't bother until the playhead does something
        {
            if( self.heartbeatStartedButNoPulseYet )
            {
                self.heartbeatStartedButNoPulseYet = NO;
                DLog(@"userHasScrubbed");
                [ self clearAllRegions ];
                [ self findNextTimeIndexForCurrentPlayTime:currentPlayTime];
                [ self setUpTimeInfoObjects ];
           }
            BOOL timeHasArrived = CMTIME_COMPARE_INLINE(currentPlayTime, >=, self.targetTimeInfo.timeToShow);
//            DLog(@"**********--------------********** if timeHasArrived %@ --  playhead %g is after this line %g", timeHasArrived ? @"YES" : @"NO", CMTimeGetSeconds(currentPlayTime), CMTimeGetSeconds(self.targetTimeInfo.timeToShow));
            if ( timeHasArrived && [ self timeIsRight:self.targetTimeInfo.timeToShow currentPlayTime:currentPlayTime ]) {
//                DLog(@"**********--------------**********  targetTimeInfo = %@", self.targetTimeInfo);
               [ self showLines:self.targetTimeInfo ];
                self.indexOfTargetTime += 1;
                [ self setUpTimeInfoObjects ];
            }
        }
    }
}

- ( void )setUpTimeInfoObjects;
{
    BOOL thereIsACurrentTime = (self.indexOfTargetTime < self.timeInfoCollection.count );
    self.targetTimeInfo = thereIsACurrentTime ? [ self.timeInfoCollection objectAtIndex:self.indexOfTargetTime] : nil;
    
    BOOL thereIsANextTime = (self.indexOfTargetTime + 1 < self.timeInfoCollection.count);
    self.nextTimeInfo = thereIsANextTime ? [ self.timeInfoCollection objectAtIndex:self.indexOfTargetTime + 1] : nil;
    
    BOOL thereIsAPreviousTime = (self.indexOfTargetTime > 0 );
    self.previousTimeInfo = thereIsAPreviousTime ? [ self.timeInfoCollection objectAtIndex:self.indexOfTargetTime - 1] : nil;
}

- ( BOOL )userHasScrubbed:( CMTime )currentPlayTime;
{
    // see if they scrubbed backwards
    BOOL currentPlayTimeIsEarlier =  CMTIME_COMPARE_INLINE(currentPlayTime, <, self.targetTimeInfo.timeToShow);
    BOOL currentPlayTimeIsLater =  CMTIME_COMPARE_INLINE(currentPlayTime, >, self.targetTimeInfo.timeToShow);

    if(currentPlayTimeIsEarlier && self.previousTimeInfo) // there is a previous time to check
    {
        BOOL playHeadIsBeforePreviousLine = CMTIME_COMPARE_INLINE(currentPlayTime, <, self.previousTimeInfo.timeToShow );
//        if (playHeadIsBeforePreviousLine) {
//           DLog(@"**********--------------**********  playHeadIsBeforePreviousLine  playhead %g is before this line %g", CMTimeGetSeconds(currentPlayTime), CMTimeGetSeconds(self.previousTimeInfo.timeToShow));
//        }
        return playHeadIsBeforePreviousLine;
    }

    if (currentPlayTimeIsLater && self.nextTimeInfo ) {
        BOOL playHeadIsAfterSubsequentLine = CMTIME_COMPARE_INLINE(currentPlayTime, >, self.nextTimeInfo.timeToShow );
//        if (playHeadIsAfterSubsequentLine) {
//            DLog(@"**********--------------**********  playHeadIsAfterSubsequentLine  playhead %g is after this line %g", CMTimeGetSeconds(currentPlayTime), CMTimeGetSeconds(self.nextTimeInfo.timeToShow));
//        }
        return playHeadIsAfterSubsequentLine;
    }
    return NO;
}
    
- ( void )showLines:( TPCCTimeInfo *)timeInfo
{
    for (TPCCLine *lineToShow in timeInfo.ccLines ) {
        [ self showCaption:lineToShow];
    }
}


- ( void )findNextTimeIndexForCurrentPlayTime:( CMTime )currentPlayTime;
{
    for (int i = 0; i < self.timeInfoCollection.count; i++)
    {
        TPCCTimeInfo * timeInfo = [self.timeInfoCollection objectAtIndex:i ];
        BOOL playHeadIsIsAtThisLine = CMTIME_COMPARE_INLINE(currentPlayTime, ==, timeInfo.timeToShow );
        if (playHeadIsIsAtThisLine) {
            self.indexOfTargetTime = i;
            return;
        }
        BOOL playHeadIsAfterThisLine = CMTIME_COMPARE_INLINE(currentPlayTime, >, timeInfo.timeToShow );
        if ( ! playHeadIsAfterThisLine ) {
//            DLog(@"playhead %g is before this line %g", CMTimeGetSeconds(currentPlayTime), CMTimeGetSeconds(timeInfo.timeToShow));
                self.indexOfTargetTime = i;
                return;
        }
     }
}

- ( void )resetFrameToUse:( BOOL)showingFullScreen;
{
    CGSize naturalSize = self.playerView.playerNaturalSize;
    CGRect currentFrame = self.playerView.frame;
    CGFloat pointSizeAsFloatNumber = 0.0;
    DLog(@"before naturalSize = %@", NSStringFromCGSize(naturalSize));    
    DLog(@"before currentFrame = %@", NSStringFromCGRect(currentFrame));
    DLog(@"before newRect = %@", NSStringFromCGRect(self.frameToUse));
    
    if (!showingFullScreen) {
        // Inset
        self.frameToUse = currentFrame;
        if ( CGRectGetWidth(currentFrame) < naturalSize.width || naturalSize.width == 0) {
            pointSizeAsFloatNumber = CGRectGetWidth(currentFrame) * 0.028;
        } else {
            pointSizeAsFloatNumber = naturalSize.width * 0.028;
        }
    }
    else{
        //Fullscreen
        CGSize currentSize = currentFrame.size;
        CGSize scaledSize = currentSize;
        CGFloat newX = CGRectGetMinX(currentFrame);
        CGFloat newY = CGRectGetMinY(currentFrame);
        
        if (self.playerView.videoFillMode == AVLayerVideoGravityResizeAspect )
        {
            if (naturalSize.width) {
                CGFloat newHeight = currentSize.width * (naturalSize.height/ naturalSize.width);
                scaledSize.height = newHeight;
            }
            newY = (currentFrame.size.height - scaledSize.height)/2.0;

        }
        CGRect newRect = CGRectMake(newX, newY, scaledSize.width, scaledSize.height);
        self.frameToUse = CGRectIntegral(newRect);
        pointSizeAsFloatNumber = CGRectGetWidth(self.frameToUse) * 0.028 ;
    }
    
    self.pointSize = lroundf(pointSizeAsFloatNumber);

    DLog(@"self.pointSize %lu is before this line %@  pointSizeAsFloatNumber = %g", (unsigned long)self.pointSize, NSStringFromCGRect(self.frameToUse), pointSizeAsFloatNumber);

    NSDictionary *notificationDictionary = @{ @"fontSize" : [ NSNumber numberWithInt:(unsigned long)self.pointSize ] };
    [[ NSNotificationCenter defaultCenter]postNotificationName:kTPClosedCaptionManagerFrameChanged object:self userInfo:notificationDictionary ];
    
    if (self.timerIsRunning) // if we have stopped the timer, don't do this
    {
        if (CMTIME_IS_NUMERIC(self.currentPlayTime))
        {
            BOOL playHeadAfterStart = CMTIME_COMPARE_INLINE(self.currentPlayTime, >=, self.previousTimeInfo.timeToShow);
            if (playHeadAfterStart) {
                [ self showLines:self.previousTimeInfo ];
            }
        }
    }


    DLog(@"after naturalSize = %@", NSStringFromCGSize(naturalSize));    
    DLog(@"after currentFrame = %@", NSStringFromCGRect(currentFrame));
    DLog(@"after newRect = %@", NSStringFromCGRect(self.frameToUse));
}


- ( void )showCaption:( TPCCLine *)lineToShow;
{
    DLog(@"show caption %@", lineToShow.theLine);    
    TPCCRegion *regionToUse = [self.closedCaptionInfo.layoutRegions objectForKey:lineToShow.regionID ];
    TPClosedCaptionView *regionSubviewToUse = regionToUse.regionSubview;
    regionSubviewToUse.timeToRemove = lineToShow.endTime;
    // position the line
    [ regionSubviewToUse adjustFrame:lineToShow superviewFrame:self.frameToUse ];
    // style the line
    NSAttributedString *restyledLine = [ [ TPMediaAccessibilityStyler sharedInstance ]styleCCContent:lineToShow.theLine isClosedCaption:YES ];
//    DLog(@"lineToShow strings = %@", lineToShow.theLine.strings);
    regionSubviewToUse.label.attributedText = restyledLine;
    // now size the line
    [ regionSubviewToUse adjustSizeForLine ];
        
//    DLog(@" regionToUse id = %@ regionToUse subview = %@", regionToUse.regionID, regionToUse.regionSubview)
    [ self adjustForOverlap:regionSubviewToUse];
    DLog(@"about to add to superview regionSubviewToUse frame = %@", NSStringFromCGRect(regionSubviewToUse.frame));
    if (self.waitingForFirstRegion) {
        self.waitingForFirstRegion = NO;
        [[ NSNotificationCenter defaultCenter]postNotificationName:kTPClosedCaptionsFirstShownNotification object:nil ];
    }
    if (regionSubviewToUse.superview) {
//        DLog(@"region has superview")
        [ regionSubviewToUse setNeedsDisplay];
    }
    else{
//        DLog(@"add region")
        [ self addRegionSubviewToVisibleView:regionSubviewToUse];
//        CATextLayer *layerToUse = [ CATextLayer layer];
//        layerToUse.frame = regionSubviewToUse.frame;
//        layerToUse.string = restyledLine;
//        [ self.playerView.layer.superlayer addSublayer:layerToUse];
        [ self.playerView.superview addSubview:regionSubviewToUse];
//        UIView *testingView = [[ UIView alloc]initWithFrame:CGRectMake(100.0, 100.0, 200.0, 200.0)];
//        testingView.backgroundColor = [ UIColor yellowColor];
//        UIView *currentSuperview = self.playerView.superview;
//        [ currentSuperview addSubview:testingView ];
//        [ currentSuperview bringSubviewToFront:testingView];
        DLog(@"playerViewSuperview = %@ subviews", self.playerView.superview.subviews);
    }
}

- ( void )adjustForOverlap:( TPClosedCaptionView *)captionView;
{
    // don't bother if there are no other views
    CGRect currentFrame = captionView.frame;
    DLog(@"captionView frame = %@", NSStringFromCGRect(captionView.frame));
    for( TPClosedCaptionView *visibleView in self.visibleViews )
    {
        CGRect overlap = CGRectIntersection(captionView.frame, visibleView.frame);
        if ( !( CGRectIsNull(overlap ) || CGRectIsEmpty(overlap)) ) {
            currentFrame.origin.y += CGRectGetHeight(overlap);
            captionView.frame = currentFrame;
            DLog(@"overlap = %@", NSStringFromCGRect(overlap));
            DLog(@"visibleView frame = %@", NSStringFromCGRect(visibleView.frame));
            DLog(@"just changed captionView frame = %@", NSStringFromCGRect(captionView.frame));
        }
    }
    return;
}

- ( void )addRegionSubviewToVisibleView:( TPClosedCaptionView *)captionView;
{
    NSUInteger visibleViewsCount = self.visibleViews.count;
    if (visibleViewsCount) {
        NSComparator comparator = ^(TPClosedCaptionView *view1, TPClosedCaptionView *view2){
            CGFloat view1Y = view1.frame.origin.y;
            CGFloat view2Y = view2.frame.origin.y;
            if (view1Y == view2Y) {
                return (NSComparisonResult)NSOrderedSame;
            }
            else
            {
                return ( view1Y > view2Y ) ? (NSComparisonResult)NSOrderedDescending : (NSComparisonResult)NSOrderedAscending;
            }
        };
        NSUInteger newIndex = [self.visibleViews indexOfObject:captionView
                                     inSortedRange:(NSRange){0, visibleViewsCount}
                                           options:NSBinarySearchingInsertionIndex
                                   usingComparator:comparator];
        
        [self.visibleViews insertObject:captionView atIndex:newIndex];
    }
    else{
        [self.visibleViews addObject:captionView ];
    }

}

- ( void )removeCaptionView:( NSTimer *)timer;
{
    TPCCLine *lineToRemove = ( TPCCLine *)[ timer userInfo];
    TPCCRegion *regionToUse = [self.closedCaptionInfo.layoutRegions objectForKey:lineToRemove.regionID ];
    UIView *viewToRemove = regionToUse.regionSubview;
//    DLog(@" regionToRemove id = %@ regionToRemove subview = %@", regionToUse.regionID, regionToUse.regionSubview)
    [ self.visibleViews removeObject:viewToRemove ];
    [ viewToRemove removeFromSuperview];
    // we don't have to invalidate this timer - it is non-repeating
}

// this method is not called until we reach or pass the time
- ( BOOL )timeIsRight:( CMTime )captionTime currentPlayTime:( CMTime)currentPlayTime;
{
    CMTime diffTime = CMTimeAbsoluteValue( CMTimeSubtract(currentPlayTime, captionTime) );
    CMTime epsilon  = CMTimeMake ( 1 * 600, 600);


    // this is largely from Cypress
    // If we are +/- within our epsilon of an ad start time, notify to start the caption.
    if ( CMTimeCompare(diffTime, epsilon) != 1 )    // -1 == less than, 0 == equal, 1 == greater than. We want less than or equal.
    {
//        DLog(@"checkCaptionTimes    - close to Ad time  currentPlayTime: %f   %f", self.player.currentPlaybackTime, CMTimeGetSeconds(currentPlayTime));
//        DLog(@"                -                   diffTime: %f   epsilon: %f", CMTimeGetSeconds(diffTime), CMTimeGetSeconds(epsilon));
        return YES;
    }
    return NO;
}



@end

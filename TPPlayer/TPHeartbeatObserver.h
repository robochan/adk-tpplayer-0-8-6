//
//  TPHeartbeatObserver.h
//  TPPlayer
//
//  Copyright © 2014, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)
//

#import <Foundation/Foundation.h>

@protocol HeartbeatObserver

- ( void )pulseHeartbeat:( CMTime)newTime;
- ( void )heartbeatStarted;
- ( void )heartbeatStopped;

@end
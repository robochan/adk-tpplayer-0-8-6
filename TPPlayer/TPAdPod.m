//
//  TPAdPod.m
//  TPPlayer
//
//  Copyright © 2014, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)

//

#import "TPAdPod.h"

@implementation TPAdPod

- ( instancetype )initWithID:( NSString *)podID type:(TPAdType)adType;
{
    if (self = [ super init]) {
        _customID = podID;
        _adObjects = [@[] mutableCopy];
        _podType = adType;
    }
    return self;
}

@end

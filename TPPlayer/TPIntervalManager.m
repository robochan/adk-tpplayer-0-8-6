//
//  TPIntervalManager.m
//
//  Copyright © 2013, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)
//

#import "TPIntervalManager.h"

@interface TPIntervalManager()

@property(nonatomic, strong)TPInterval *activeInterval;
@property(nonatomic, assign )NSUInteger intervalIndex;
@property(nonatomic, strong)NSArray *intervalsToUse;

@end

@implementation TPIntervalManager

- ( id )initWithIntervals:( NSArray *)intervals;
{
    if (self = [ super init]) {
        [ self activateIntervals:intervals];
    }
    return self;
}

- ( void )activateIntervals:( NSArray *)intervals;
{
    self.intervalsToUse = intervals;
    self.intervalIndex = 0;
    if (self.intervalsToUse.count) {
        self.activeInterval = [ self.intervalsToUse objectAtIndex:0];
    }
    for (TPInterval *anInterval in intervals ) {
        anInterval.manager = self;
    }
}

- ( void )start;  // often really restart
{
    [ self.activeInterval start];
}

- ( void )restart;
{
    self.intervalIndex = 0;
    if (self.intervalsToUse.count) {
        self.activeInterval = [ self.intervalsToUse objectAtIndex:0];
    }
    // reset all the intervals
    for (TPInterval *anInterval in self.intervalsToUse ) {
        [ anInterval reset ];
    }
    [ self.activeInterval start];
}

- ( void )intervalFired:( TPInterval *)interval;
{
    [ self.delegate intervalFired:interval ];
    if (interval.isComplete) {
        DLog(@"manager interval fired interval is complete");
        self.intervalIndex += 1;
        if (self.intervalsToUse.count > self.intervalIndex) {
            self.activeInterval = [ self.intervalsToUse objectAtIndex:self.intervalIndex];
            self.activeInterval.accumulatedTime = interval.accumulatedTime;
            [ self start];
        }

    }
}

- ( void )pause;
{
    [ self.activeInterval pause];
}

- ( void )stopTiming;
{
    [ self.activeInterval stopTiming];
}

- ( CGFloat)intervalElapsedTime;
{
    if (self.activeInterval ) {
        return self.activeInterval.intervalElapsedTime;
    }
    return 0;
}



@end

//
//  TPBaseClip.h
//
//  Copyright © 2013, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)
//

#import <Foundation/Foundation.h>

@interface TPBaseClip : NSObject

@property(nonatomic, strong)NSString    *guid;  // guid
@property(nonatomic, strong)NSString    *url;   // URL of the BaseClip
@property(nonatomic, strong)NSString    *type;  // mime-type of the BaseClip [???]
@property(nonatomic, assign)NSUInteger  width;  // width of the BaseClip in pixels
@property(nonatomic, assign)NSUInteger  height; // height of the BaseClip in pixels
@property(nonatomic, strong)NSString    *title; // title of the BaseClip
@property(nonatomic, strong)NSString    *abstract;       // description of the BaseClip - named per Metafile specification
@property(nonatomic, strong)NSString    *freewheelAssetID;  // Freewheel ID
@property(nonatomic, strong)NSArray     *keywords;          // Array of keywords as strings
@property(nonatomic, strong)NSArray     *categories;        // Array of categories as strings
@property(nonatomic, strong)NSString    *copyright;     // copyright
@property(nonatomic, strong)NSString    *cdn;   // CDN reference
@property(nonatomic, assign)BOOL        encryptScripts; //Encrypted Y/N
@property(nonatomic, strong)NSArray     *clips;
@property(nonatomic, strong)NSString    *CCURL; // URL to retrieve the Closed Caption file
@property(nonatomic, strong)NSString    *bumperURL; // URL for bumper content
@property(nonatomic, strong)NSString    *preroll;   // URL for pre-roll content

// not archived
@property(nonatomic, strong)NSString *ratingSource;
@property(nonatomic, strong)NSString *advisorySource;

- ( BOOL)hasAllNecessarySMIL;
- ( void )getPreRollSmilInfoForDelegate:( id)delegate selector:( SEL)selector;

@end

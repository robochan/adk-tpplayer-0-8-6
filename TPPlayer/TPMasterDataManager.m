//
//  TPMasterDataManager.m
//
//  Copyright © 2013, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)
//

#import "TPMasterDataManager.h"
#import "TPAppSpecificInformationManager.h"
#import <UIKit/UIKit.h>

#import "TPContentList.h"
#import "TPDataHandler.h"
#import "TPImageLoading.h"
#import "TPNetworkMonitor.h"
#import "TPSmartImage.h"
#import "TPSmilInfo.h"
#import "TPVideoProgressInfo.h"
#import "GDataXMLNode.h"
#import "TPXMLManager.h"
#import "TPJSONManager.h"
#import "TPBaseClip.h"
#import "TPTrailer.h"


@interface TPMasterDataManager()

@property(nonatomic, strong )TPContentList *contentList;
@property(nonatomic, copy ) NSString *currentObjectPath;
@property(nonatomic, assign)NSUInteger minutesToExpire;  // defaults to 15

@property(nonatomic, strong )NSMutableSet *dataHandlers;
@property(nonatomic, strong )NSURL *dataSourceURL;
@property(nonatomic, strong)NSDictionary *defaultImages;
@property(nonatomic, strong )NSURL *videoProgressURL;
@property(nonatomic, strong ) dispatch_queue_t timeStoreQueue;

@end

@implementation TPMasterDataManager
static TPMasterDataManager *defaultObject;

+ ( instancetype )sharedInstance;
{
    if( defaultObject == nil)
    {
        defaultObject = [ [ [ self class]alloc]init];
        [ defaultObject setup ];
    }
    return defaultObject;
}

- ( void )setup;
{
    // make a container to hold the currently active dataHandlers
    self.minutesToExpire = 15;
    self.dataHandlers = [ [ NSMutableSet alloc ]init ];
    [ self setupFilePaths];
    [ self cleanStaleFilesInDirectory:self.imagesDirectoryURL];
    [ self cleanStaleFilesInDirectory:self.trailersDirectoryURL];
    NSNumber *minutesToExpireNumber = [[ TPAppSpecificInformationManager sharedInstance ]objectForKey:@"minutesToExpire"];
    self.minutesToExpire = minutesToExpireNumber && [minutesToExpireNumber integerValue ] ? [ minutesToExpireNumber integerValue] : 15;
    self.timeStoreQueue = dispatch_queue_create(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    self.ignoreSecondsAtEnd = 30;
    self.ignoreSecondsAtBeginning = 30;
}

- ( NSDictionary *)defaultImages;
{
    if( _defaultImages )
    {
        return _defaultImages;
    }
    NSMutableDictionary *tmpDict = [ NSMutableDictionary dictionary ];
    // NEED TO FILL THESE WITH SOMETHING
//    [ tmpDict setObject:[ self defaultWithName:gridTileDefaultName forRole:@"gridTile" ] forKey:@"gridTile" ];
//    [ tmpDict setObject:[ self defaultWithName:cardFrontDefaultName forRole:@"cardFront" ] forKey:@"cardFront" ];
//    [ tmpDict setObject:[ self defaultWithName:photoImageDefaultName forRole:@"photoImage" ] forKey:@"photoImage" ];
//    [ tmpDict setObject:[ self defaultWithName:videoThumbnailMDDefaultName forRole:@"videoThumbnailMD" ] forKey:@"videoThumbnailMD" ];
//    [ tmpDict setObject:[ self defaultWithName:videoThumbnailSMDefaultName forRole:@"videoThumbnailSM" ] forKey:@"videoThumbnailSM" ];
    
    _defaultImages = tmpDict;
    return _defaultImages;
}

- ( void )setupFilePaths;
{
    NSString *dataModelPath = @"DataModels";
    NSString *imagesPath = @"Images";
    NSString *trailersPath = @"Trailers";
    NSError *error;    
    // set up a NSURL that points to our folder in the applicationSupportDirectory - where we will keep our object
    NSFileManager *fileManager = [ NSFileManager defaultManager];
    DLog(@"applicationDataDirectory = %@", [ self applicationDataDirectory]);
    
    NSURL *appSupportURL = [ fileManager URLForDirectory:NSCachesDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:YES error:nil ];
    DLog(@"appSupportURL = %@", appSupportURL);
    
    NSURL* possibleURL = [[fileManager URLsForDirectory:NSCachesDirectory inDomains:NSUserDomainMask]firstObject ];
    DLog(@"a different way to get appSupportURL = %@", possibleURL);


    self.dataSourceURL = [ appSupportURL URLByAppendingPathComponent:dataModelPath ];
    BOOL ok = [ fileManager createDirectoryAtURL:self.dataSourceURL withIntermediateDirectories:YES attributes:nil error:&error ];
    DLog(@"dataSourceURL = %@", self.dataSourceURL);

    self.imagesDirectoryURL = [ appSupportURL URLByAppendingPathComponent:imagesPath ];
//    if ([fileManager fileExistsAtPath:[ self.imagesDirectoryURL path]]) {
//        NSArray *contents = [ fileManager contentsOfDirectoryAtPath:[ self.imagesDirectoryURL path] error:nil];
//        for( NSURL *url in contents)
//        {
//            DLog(@"url is %@", url);
//        }
//    }
    ok = [ fileManager createDirectoryAtURL:self.imagesDirectoryURL withIntermediateDirectories:YES attributes:nil error:&error ];    
    DLog(@"imagesDirectoryURL = %@", self.imagesDirectoryURL);
 
    self.trailersDirectoryURL = [ appSupportURL URLByAppendingPathComponent:trailersPath ];
    ok = [ fileManager createDirectoryAtURL:self.trailersDirectoryURL withIntermediateDirectories:YES attributes:nil error:&error ];    
    
    NSURL *pathForBackup = [ fileManager URLForDirectory:NSApplicationSupportDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:YES error:nil ];
    self.videoProgressURL = [ pathForBackup URLByAppendingPathComponent:@"ViewingExperience" ];
    [ fileManager createDirectoryAtURL:self.videoProgressURL withIntermediateDirectories:YES attributes:nil error:&error ];
    
}

- (NSURL*)applicationDataDirectory {
    NSFileManager* sharedFM = [NSFileManager defaultManager];
    NSArray* possibleURLs = [sharedFM URLsForDirectory:NSApplicationSupportDirectory
                                             inDomains:NSUserDomainMask];
    DLog(@"possibleURLs = %@", possibleURLs);
    NSURL* appSupportDir = nil;
    NSURL* appDirectory = nil;
    
    if ([possibleURLs count] >= 1) {
        // Use the first directory (if multiple are returned)
        appSupportDir = [possibleURLs objectAtIndex:0];
    }
    
    // If a valid app support directory exists, add the
    // app's bundle ID to it to specify the final directory.
    if (appSupportDir) {
        NSString* appBundleID = [[NSBundle mainBundle] bundleIdentifier];
        appDirectory = [appSupportDir URLByAppendingPathComponent:appBundleID];
    }
    
    return appDirectory;
}


- ( void )emptyCurrentDirectories:( NSString *)previousDesignator
{
    NSFileManager *fileManager = [ NSFileManager defaultManager];
    NSError *error;    
    if (previousDesignator.length) {
        [ fileManager removeItemAtURL:self.trailersDirectoryURL error:&error];
        [ fileManager removeItemAtURL:self.dataSourceURL error:&error];
        [ fileManager removeItemAtURL:self.imagesDirectoryURL error:&error];
    }
}


- ( void )cleanStaleFilesInDirectory:( NSURL *)directoryURL;
{
    NSError *error;    
    // clean stale image files
    NSFileManager *fileManager = [ NSFileManager defaultManager];
    
    NSDirectoryEnumerator *dirEnumerator = [ fileManager enumeratorAtURL:directoryURL includingPropertiesForKeys:[ NSArray arrayWithObjects:NSURLCreationDateKey, nil] options:NSDirectoryEnumerationSkipsSubdirectoryDescendants | NSDirectoryEnumerationSkipsHiddenFiles errorHandler:nil ];
    
    NSDate *staleFileDate = [ NSDate dateWithTimeIntervalSinceNow:( -1 * (60*60*24*30))];
    NSMutableSet *filesToDelete = [ NSMutableSet set ];
    for( NSURL *fileURL in dirEnumerator)
    {
        NSDate *createdAtDate;
        [ fileURL getResourceValue:&createdAtDate forKey:NSURLCreationDateKey error:nil];
        if ([createdAtDate isEqualToDate:[ createdAtDate earlierDate:staleFileDate] ] ) {
            [filesToDelete addObject:fileURL ];
        }
    }
    for (NSURL *fileURL in filesToDelete ) {
        [ fileManager removeItemAtURL:fileURL error:&error];
    }
}



- ( void )getImageForPath:( NSString *)url metadata:( id )metadata owner:( id )owner delegate:( id )delegate selector:( SEL ) selector;
{
    // make a datahandler for the imagePath -- save the owner too
    TPDataHandler *dataHandler = [  [TPDataHandler alloc ] initWithPath:nil url:url metadata:metadata delegate:delegate selector:selector contentType:DataManagerContentTypeData shouldEncodeURL:YES ];   
    dataHandler.owner = owner;
    [ self.dataHandlers addObject:dataHandler];
   [ dataHandler loadResource ];
}

- ( void )loadSmilDataForUrl:( NSString *)url metadata:( NSString *)metadata delegate:( id )delegate selector:( SEL ) selector;
{
    // make a datahandler for the imagePath -- save the owner too
    TPDataHandler *dataHandler = [ [TPDataHandler alloc ] initWithPath:@"SmilData" url:url metadata:metadata delegate:delegate selector:selector contentType:DataManagerContentTypeXml shouldEncodeURL:NO ];
    [ self.dataHandlers addObject:dataHandler];
    [ dataHandler loadResource ];
}

//- ( void )getCCDataForPath:( NSString *)url metadata:( TPSmilInfo *)metadata delegate:( id )delegate selector:( SEL ) selector;
- ( void )getCCDataForPath:( NSString *)url metadata:( TPBaseClip *)metadata delegate:( id )delegate selector:( SEL ) selector;
{
    // make a datahandler for the imagePath -- save the owner too
    TPDataHandler *dataHandler = [  [TPDataHandler alloc ] initWithPath:@"CCData" url:url metadata:metadata delegate:delegate selector:selector contentType:DataManagerContentTypeXml shouldEncodeURL:NO ];
    [ self.dataHandlers addObject:dataHandler];
    [ dataHandler loadResource ];
}

- ( void )getGUIDStringForPath:( NSString *)url metadata:( NSNumber *)metadata delegate:( id )delegate selector:( SEL ) selector;
{
    // make a datahandler for the imagePath -- save the owner too
    TPDataHandler *dataHandler = [ [TPDataHandler alloc ] initWithPath:@"GUIDAuth" url:url metadata:metadata delegate:delegate selector:selector contentType:DataManagerContentTypeData shouldEncodeURL:NO ];
    [ dataHandler loadResource ];
}

- ( void )getTrailersForURL:( NSString *)url requester:( id )requester selector:(SEL)selector path:( NSString *)objectPath;
{
    TPDataHandler *dataHandler = [ [TPDataHandler alloc ] initWithPath:objectPath url:url metadata:nil delegate:requester selector:selector contentType:DataManagerContentTypeXml shouldEncodeURL:YES ];
    [ self.dataHandlers addObject:dataHandler];
    [ dataHandler loadResource ];
}



- ( void )loadSimpleSmilDataForUrl:( NSString *)url metadata:( NSString *)metadata delegate:( id )delegate selector:( SEL ) selector;
{
    // make a datahandler for the imagePath -- save the owner too
    TPDataHandler *dataHandler = [ [TPDataHandler alloc ] initWithPath:@"SimpleSmilData" url:url metadata:metadata delegate:delegate selector:selector contentType:DataManagerContentTypeXml shouldEncodeURL:YES ];
    [ dataHandler loadResource ];
}

- ( void )errorCreatingObject:( TPDataHandler *)dataHandler;
{
    //    DLog();
    [self performSelectorForDelegate:dataHandler ];
}

- ( void )performSelectorForDelegate:( TPDataHandler *)dataHandler;
{
    [dataHandler.delegate performSelectorOnMainThread:dataHandler.selector withObject:dataHandler waitUntilDone:YES]; 
    dataHandler.delegate = nil;  // just in case
    dataHandler.owner = nil;
    [ self.dataHandlers removeObject:dataHandler ];
}

- ( void )cancelRequests:( id )requestor;
{
    NSMutableSet *handlersToRemove = [ NSMutableSet set ];
    for( TPDataHandler *dataHandler in self.dataHandlers )
    {
        if( dataHandler.delegate == requestor )
        {
            [ dataHandler cancelRequest ];
            [ handlersToRemove addObject:dataHandler ];
        }
    }
    [ self.dataHandlers minusSet:handlersToRemove ];
}

- ( void )dataHandlerLoaded:( TPDataHandler *)dataHandler;
{
    // calls the TPJSONManager or TPXMLManager to convert the data -- it will put the object into dataHandler.returnedObject and call objectCreated with the dataHandler
    
    NSError *error = dataHandler.error;
    
    if (error == nil)
    {
        if (dataHandler.contentType == DataManagerContentTypeData)  
        {    
            if ([dataHandler.pathName isEqualToString:@"GUIDAuth" ])
            {
                NSString *resultingString = [[ NSString alloc]initWithData:dataHandler.receivedData encoding:NSUTF8StringEncoding ];
                DLog(@"%@", resultingString );
                dataHandler.returnedObject = resultingString;
            }
            else
            {
                NSData *imageData = dataHandler.receivedData;       
                if( imageData )
                {
                    UIImage *newImage = [ [ UIImage alloc ]initWithData:imageData ];
                    dataHandler.returnedObject = newImage;
                    if ([dataHandler.owner respondsToSelector:@selector(setNewImage:)]) {
                        [ dataHandler.owner performSelector:@selector(setNewImage:) withObject:newImage ];  
                    }
                }
            }
            [ self objectCreated:dataHandler ];
        }
        else
        {
            if (dataHandler.contentType == DataManagerContentTypeXml) {
                [ [ TPXMLManager sharedInstance ] createObjectFromDataHandler:dataHandler ];
            }
            else // it is JSON
            {
                [ [ TPJSONManager sharedInstance ] createObjectFromDataHandler:dataHandler ];
            }
        }
    }
    else 
    {
        [ self errorCreatingObject:dataHandler ];
    }
}

- ( void )objectCreated:( TPDataHandler *)dataHandler;
{
    if ([dataHandler.returnedObject isKindOfClass:[  UIImage class ]]) {
        [ self archiveImage:dataHandler ];
    }
    else
    {
        if ( NSNotFound !=  [dataHandler.pathName rangeOfString:@"ContentList"].location )
        {
            TPContentList  *aContentList = ( TPContentList  *)dataHandler.returnedObject;
            if (aContentList.contentItems.count == 0) {
                aContentList.expiration = [ NSDate date];
            }
            else{
                [ aContentList setExpiration:[ self shortExpiration ] ];
            }
            [ self archiveObject:dataHandler ];
        } 
        else{
            if ([ dataHandler.pathName isEqualToString:@"Trailer"]) {
                TPTrailer *trailer = (TPTrailer *)dataHandler.returnedObject;
                [ self archiveTrailer:trailer];
            }
            if (dataHandler.classToBuild) {
                id< TPExpiringObject> newObject = dataHandler.returnedObject;
                NSTimeInterval secondsInMinutes = dataHandler.minutesToExpiration * 60;  
                [ newObject setExpiration:[ [ NSDate date ]dateByAddingTimeInterval:secondsInMinutes ] ];
                [ self archiveObject:dataHandler];
            }
        }
    }
//    if (dataHandler.contentType == DataManagerContentTypeXml || dataHandler.contentType == DataManagerContentTypeJSON )      
//    {
//        if (! ( [dataHandler.pathName isEqualToString:@"SmilData"] || [ dataHandler.pathName isEqualToString:@"VideoInfoData" ] ) )  // if it isn't SmilData or VideoInfoData, it is an object.  
//        {
//            if ( NSNotFound !=  [dataHandler.pathName rangeOfString:@"ContentList"].location ) {
//                TPContentList  *aContentList = ( TPContentList  *)dataHandler.returnedObject;
//                [ aContentList setExpiration:[ self shortExpiration ] ];
//                [ self archiveObject:dataHandler ];
//            } 
//        }
//    }
//    else // at the moment the only thing we see that isn't xml is imageData
//    {
//        [ self archiveImage:dataHandler ];
//    }
    [self performSelectorForDelegate:dataHandler ];
}

- ( id )objectFromArchive:( NSString *)path;
{
    DLog( @"path = %@", path );
    id newObject = nil;
    
    NSURL *objectURL = [ self.dataSourceURL URLByAppendingPathComponent:path ];
    
    NSData *objectData = [[ NSData alloc ]initWithContentsOfURL:objectURL ];
    if( objectData )
    {
        newObject = [ NSKeyedUnarchiver unarchiveObjectWithData:objectData ];
    }
    DLog(@"newObject = %@", newObject);
    return newObject;
}

- ( void )archiveObject:( TPDataHandler *)dataHandler;
{
    //    DLog();
    if (dataHandler.returnedObject)
    {
        NSString *path = dataHandler.pathName;
        NSURL *objectURL = [ self.dataSourceURL URLByAppendingPathComponent:path];
        NSData *objectData = [ NSKeyedArchiver archivedDataWithRootObject:dataHandler.returnedObject ];
        [ objectData writeToURL:objectURL atomically:NO ];
    }
}

- ( TPTrailer *)trailerFromArchive:( NSString *)path;
{
    //    DLog();
    TPTrailer *newObject = nil;
    
    NSURL *objectURL = [ self.trailersDirectoryURL URLByAppendingPathComponent:path ];
    
    NSData *objectData = [[ NSData alloc ]initWithContentsOfURL:objectURL ];
    if( objectData )
    {
        newObject = [ NSKeyedUnarchiver unarchiveObjectWithData:objectData ];
    }
    return newObject;
}


- ( void )archiveContentItem:( TPContentItem *)contentItem path:( NSString *)path;
{
    NSURL *objectURL = [ self.dataSourceURL URLByAppendingPathComponent:path];
    NSData *objectData = [ NSKeyedArchiver archivedDataWithRootObject:contentItem ];
    [ objectData writeToURL:objectURL atomically:NO ];
}

- ( void )archiveImage:( TPDataHandler *)dataHandler;
{
    if (dataHandler.receivedData && dataHandler.owner)
    {
        id<TPImageLoading> owner = dataHandler.owner;
        
        NSString *pathForImage = [ owner pathForImage ]; 
        NSURL *imageURL = [ self.imagesDirectoryURL URLByAppendingPathComponent:pathForImage ];      
        DLog(@"imageURL = %@\nowner = %@", imageURL, owner );
        NSData *imageData = dataHandler.receivedData;
        [ imageData writeToURL:imageURL atomically:NO ];    
        
        NSError *error;
        NSString *pathForSourceName = [ NSString stringWithFormat:@"%@_sourceName.txt", [ pathForImage stringByDeletingPathExtension ] ];
        NSURL *imageSourceNameURL = [ self.imagesDirectoryURL URLByAppendingPathComponent:pathForSourceName];
        [dataHandler.url writeToURL:imageSourceNameURL atomically:YES encoding:NSUTF8StringEncoding error:&error ];
    }
}

- ( void )archiveTrailer:(TPTrailer *)trailer; 
{
    NSString *pathForTrailer = [trailer pathForTrailer];
    NSURL *trailerURL = [ self.trailersDirectoryURL URLByAppendingPathComponent:pathForTrailer ];
    NSData *trailerData = [ NSKeyedArchiver archivedDataWithRootObject:trailer];
    [ trailerData writeToURL:trailerURL atomically:NO ];
}

- ( NSDate *)shortExpiration;
{
    
    NSTimeInterval secondsInFourHours = self.minutesToExpire * 60;  // 15 minute Feed Data expiration per request April 2013
    
    return [ [ NSDate date ]dateByAddingTimeInterval:secondsInFourHours ];
}

- ( TPDataHandler *)addJSONDataHandlerForDelegate:(id )delegate selector:( SEL )selector;
{
    TPDataHandler *dataHandler = [ [ TPDataHandler alloc ]init ];
    // for now, always use DataManagerCacheTypeNone, we aren't caching data handlers
    dataHandler.cacheType = DataManagerCacheTypeNone;
    dataHandler.contentType = DataManagerContentTypeJSON;
    dataHandler.delegate = delegate;
    dataHandler.selector = selector;
    [self.dataHandlers addObject:dataHandler ];
    return dataHandler;
}

- ( void )wrapupAndLaunchJSONDataHandler:( TPDataHandler *)dataHandler withURL:( NSString  *)url path:( NSString *)objectPath;
{
    DLog( );
    dataHandler.pathName = objectPath;
    dataHandler.url = url;
    dataHandler.contentType = DataManagerContentTypeJSON;
    dataHandler.shouldEncodeUrl = YES;
    [ dataHandler loadResource ];
}

- ( TPContentList * )readAContentList:( TPDataHandler *)dataHandler path:( NSString *)objectPath url:( NSString *)url;
{
    TPContentList *newContentList = [ self objectFromArchive:objectPath ];
    if( newContentList && [newContentList isFresh ] )
    {
        //            DLog(@"didn't have object - unload it");
        dataHandler.returnedObject =newContentList;
        [self performSelectorForDelegate:dataHandler ];
        return newContentList;
    }
    else
    {
        //            DLog(@"didn't have archived object - go get it");
        [ self wrapupAndLaunchJSONDataHandler:dataHandler withURL:url path:objectPath ];
    }
    return nil;
}

- (  id<TPExpiringObject> )readExpiringObject:(NSString *)classToRead requester:( id )requester selector:(SEL)selector path:( NSString *)objectPath url:( NSString *)url minutesToExpiration:( NSUInteger)minutesToExpiration;
{
    TPDataHandler *dataHandler = [self addJSONDataHandlerForDelegate:requester selector:selector ];
    dataHandler.classToBuild = classToRead;
    dataHandler.minutesToExpiration = minutesToExpiration;
    
    id<TPExpiringObject> newObject = [ self objectFromArchive:objectPath ];
    if( newObject && [newObject isFresh ] )
    {
        //            DLog(@"didn't have object - unload it");
        dataHandler.returnedObject =newObject;
        [self performSelectorForDelegate:dataHandler ];
        return newObject;
    }
    else
    {
        //            DLog(@"didn't have archived object - go get it");
        [ self wrapupAndLaunchJSONDataHandler:dataHandler withURL:url path:objectPath ];
    }
    return nil;
}


// this is really for contentLists that contain TPContentItem subclass objects (I think) 
// currently it is not accessible from anywhere else
- ( void )getCustomObjectForURL:( NSString *)url requester:( id )requester selector:(SEL)selector path:( NSString *)objectPath contentItemSubclass:( NSString *)contentItemSubclass;
{
    TPDataHandler *dataHandler = [self addJSONDataHandlerForDelegate:requester selector:selector ];
    dataHandler.contentItemSubclass = contentItemSubclass;
    if( self.currentObjectPath == objectPath &&  self.contentList && [self.contentList isFresh] )
    {
        //        DLog(@"already have object");
        dataHandler.returnedObject = self.contentList;
        [ self performSelectorForDelegate:dataHandler ];
    }
    else
    {
        // read it from file
        self.contentList = [ self readAContentList:dataHandler path:objectPath url:url ];
        if( self.contentList )
        {
            self.currentObjectPath = objectPath;
        }
        // otherwise we have to wait for the load
    }
}

- ( void )getContentItemForURL:( NSString *)url requester:( id )requester selector:(SEL)selector path:( NSString *)objectPath;
{
    TPDataHandler *dataHandler = [self addJSONDataHandlerForDelegate:requester selector:selector ];
    // doesn't try to read it locally - just gets it from network
    [ self wrapupAndLaunchJSONDataHandler:dataHandler withURL:url path:objectPath ];
}

- ( void )getContentListForURL:( NSString *)url requester:( id )requester selector:(SEL)selector path:( NSString *)objectPath contentItemSubclass:( NSString *)contentItemSubclass;
{
    TPDataHandler *dataHandler = [self addJSONDataHandlerForDelegate:requester selector:selector ];
    dataHandler.contentItemSubclass = contentItemSubclass;
    if( self.currentObjectPath == objectPath &&  self.contentList && [self.contentList isFresh] )
    {
        //        DLog(@"already have object");
        dataHandler.returnedObject = self.contentList;
        [ self performSelectorForDelegate:dataHandler ];
    }
    else
    {
        // read it from file
        self.contentList = [ self readAContentList:dataHandler path:objectPath url:url ];
        if( self.contentList )
        {
            self.currentObjectPath = objectPath;
        }
        // otherwise we have to wait for the load
    }
}

- ( void )getClass:( NSString *)classToBuild forURL:( NSString *)url requester:( id )requester selector:(SEL)selector path:( NSString *)objectPath;
{
    TPDataHandler *dataHandler = [self addJSONDataHandlerForDelegate:requester selector:selector ];
    dataHandler.classToBuild = classToBuild;
    // doesn't try to read it locally - just gets it from network
    [ self wrapupAndLaunchJSONDataHandler:dataHandler withURL:url path:objectPath ];
}

- ( NSString *)adjustedRole:( NSString *)originalRole;
{
    if ( [ self prefersRetina ] )
    {
        return [ NSString stringWithFormat:@"%@_retina", originalRole ];
    }
    return originalRole;
}

- ( BOOL )prefersRetina;
{
    BOOL deviceIsRetinaDisplay = ( [ [ UIScreen mainScreen] respondsToSelector:@selector(scale)] && [[UIScreen mainScreen] scale] == 2.00);
    if (deviceIsRetinaDisplay && self.hasWiFiAccess )
    {
        return YES;
    }
    return NO;
}

- ( BOOL )hasWiFiAccess;
{
    return [ TPNetworkMonitor sharedInstance].currentNetworkStatus == TPReachableViaWiFi;
}

#pragma mark -- default images

- ( TPSmartImage *)defaultForRole:( NSString *)role;
{
    // we don't have to adjust here.  We named the images properly - the right one will be used automatically
    return [ self.defaultImages objectForKey:role ];
}

- ( NSDictionary *)dictionaryOfDefaultImages;
{
    return self.defaultImages;
}

- ( TPSmartImage *)defaultWithName:( NSString *)imageName forRole:( NSString  *)roleName;
{
    UIImage *aDefaultImage = [ UIImage imageNamed:imageName ];
    TPSmartImage *smartImageForDefault = [ [ TPSmartImage alloc ] init ];
    smartImageForDefault.image = aDefaultImage;
    smartImageForDefault.role = roleName;
    return smartImageForDefault;
}

//
//- ( NSTimeInterval)timeToStart:( TPVideoAsset *)videoAsset;
//{
//    NSString *assetID = videoAsset.guid;
//    if (assetID.length == 0) {
//        return 0.0;
//    }
//    NSURL *objectURL = [ self.videoProgressURL URLByAppendingPathComponent:assetID ];
//    NSData *objectData = [[ NSData alloc ]initWithContentsOfURL:objectURL ];
//    if( !objectData )
//    {
//        return 0.0;
//    }
//    TPVideoProgressInfo *videoProgressInfo = [ NSKeyedUnarchiver unarchiveObjectWithData:objectData ];
//    return videoProgressInfo.resumeTime;
//}
//
//- ( void )removeTimeToStartForVideoAsset:( TPVideoAsset *)videoAsset;
//{
//    NSError *error = nil;
//    NSString *assetID = videoAsset.guid;
//    if (assetID.length == 0) {
//        return;
//    }
//    NSURL *objectURL = [ self.videoProgressURL URLByAppendingPathComponent:assetID ];
//    [[ NSFileManager defaultManager] removeItemAtURL:objectURL error:&error ];
//    DLog(@"just removed time for videoAsset %@", videoAsset.guid);
//}
//
//- ( void )saveTimeToStart:( NSTimeInterval)playheadTime forVideoAsset:( TPVideoAsset *)videoAsset;
//{
//    DLog();
//    // some videos don't qualify to this -- they don't have guids -- ignore them.
//    NSString *assetID = videoAsset.guid;
//    if (assetID.length == 0) {
//        return;
//    }
//    BOOL farEnoughFromEnd = (videoAsset.duration - playheadTime > [ self ignoreSecondsAtEnd ]);
//    BOOL farEnoughFromBeginning =  (playheadTime > [ self ignoreSecondsAtBeginning]);
//    
//    if (farEnoughFromEnd && farEnoughFromBeginning)
//    {
//        NSURL *objectURL = [ self.videoProgressURL URLByAppendingPathComponent:assetID ];
//        TPVideoProgressInfo *videoProgressInfo = [[ TPVideoProgressInfo alloc ]init ];
//        videoProgressInfo.resumeTime = playheadTime;
//        NSData *objectData = [ NSKeyedArchiver archivedDataWithRootObject:videoProgressInfo ];
//        dispatch_async(self.timeStoreQueue, ^{
//                           [ objectData writeToURL:objectURL atomically:NO ];
//                       });
//        DLog(@"just wrote time for videoAsset %@ %g", videoAsset.guid, playheadTime);
//    }
//    else  // at this point there is no information worth saving - remove the file if it exists
//    {
//        [ self removeTimeToStartForVideoAsset:videoAsset];
//    }
//}



@end

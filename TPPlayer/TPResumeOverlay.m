//
//  FNResumeOverlay.m
//
//  Copyright © 2013, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)
//

#import "TPResumeOverlay.h"

@implementation TPResumeOverlay

+ ( TPResumeOverlay *)resumeOverlayView;
{    
//    NSString *resourceBundlePath = [ [ NSBundle mainBundle]pathForResource:@"TPPlayer" ofType:@"bundle"];
//    NSBundle *resourceBundle = [ NSBundle bundleWithPath:resourceBundlePath];
//    NSString *nibName = @"TPResumeOverlay";
    NSString *nibName = NSStringFromClass([ self class]);
    NSBundle *resourceBundle = [ NSBundle mainBundle];
    UINib *nib = [ UINib nibWithNibName:nibName bundle:resourceBundle ];
    NSArray *nibObjects = [ nib instantiateWithOwner:nil options:nil ];
    NSAssert2( [ nibObjects count ] > 0 && [ [ nibObjects objectAtIndex:0 ] isKindOfClass:[ self class ] ], @"Nib %@ does not contain top level view of type %@", nibName, nibName );
    return [ nibObjects objectAtIndex:0 ];
}

- ( id )initWithCoder:(NSCoder *)aDecoder;
{
    if( self = [ super initWithCoder:aDecoder])
    {
        [self.playFromBeginningButton setTitle:NSLocalizedString(@"Play From Beginning", @"Play From Beginning") forState:UIControlStateNormal ];
        [self.resumePlayingButton setTitle:NSLocalizedString(@"Resume Playing", @"Resume Playing") forState:UIControlStateNormal ];
    }
    return self;
}

@end

//
//  TPAppSpecificInformationManager.m
//
//  Copyright © 2013, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)
//

#import "TPAppSpecificInformationManager.h"
#import <UIKit/UIKit.h>

@interface TPAppSpecificInformationManager()

@property(nonatomic, strong)NSDictionary *appSpecificInformation;

@end


@implementation TPAppSpecificInformationManager

+( TPAppSpecificInformationManager *)sharedInstance
{
    static dispatch_once_t once;
    static TPAppSpecificInformationManager *shared = nil;
    
    dispatch_once( &once, ^{
        shared = [ [ TPAppSpecificInformationManager alloc ]init ];
        [ shared resetForRegion ];
    } );
    return shared;
}

- ( id )objectForKey:( NSString *)keyToUse;
{
    id objectToUse = [self.appSpecificInformation objectForKey:keyToUse ];
    if (objectToUse) {
        return objectToUse;
    }
    NSString *alternateKey = nil;
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ) {
        alternateKey = [ NSString stringWithFormat:@"%@~ipad", keyToUse];
    }
    else{
        alternateKey = [ NSString stringWithFormat:@"%@~iphone", keyToUse];
    }
    return [ self.appSpecificInformation objectForKey:alternateKey];
}

- ( BOOL )booleanForKey:( NSString *)keyToUse;
{
    NSNumber *booleanAsNumber = [ self objectForKey:keyToUse];
    return [ booleanAsNumber boolValue];
}

- ( void )resetForRegion;
{
//    NSURL *localURL = [ [ NSBundle mainBundle] URLForResource:currentRegionDesignator withExtension:@"plist" subdirectory:@"regionSpecificInformation"];
    NSURL *localURL = [ [ NSBundle mainBundle ] URLForResource:@"AppSpecificInformation"  withExtension:@"plist"];
    NSDictionary *topLevelDictionary = [ [ NSDictionary alloc ]initWithContentsOfURL:localURL ];
    BOOL environmentIsProduction = [[ topLevelDictionary objectForKey:@"environmentIsProduction" ] boolValue];
    NSString *environmentToUse = environmentIsProduction ? @"production" : @"staging" ;
    NSString *specificBuildDesignator = topLevelDictionary[@"TPSpecificBuildDesignator"];
    if (specificBuildDesignator) {
        NSDictionary *specificBuildInfo = topLevelDictionary[specificBuildDesignator ];
        if (specificBuildInfo.count) {
            self.appSpecificInformation = specificBuildInfo;
            return;
        }
    }
    self.appSpecificInformation =  [topLevelDictionary objectForKey:environmentToUse];
}

@end

//
//  TPSlider.h
//
//  Copyright © 2013, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)
//
//   We really only need this class to get access to the thumbRect.  
//   When that changes, it sends it to the overlayView to make the right tick marks visible/invisible
//

#import <UIKit/UIKit.h>
#import "TPTickMarkOverlayView.h"

@interface TPSlider : UISlider

@property(nonatomic, assign)CGRect thumbRect;
@property (nonatomic, strong)TPTickMarkOverlayView *overlayView;  // owned by the superview

@end

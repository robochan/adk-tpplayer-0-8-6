//
//  TPTrailer.h
//  TPPlayer
//
//  Copyright © 2013, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)
//

#import "TPVideoOwner.h"
@class TPVideoAsset;

@interface TPTrailer : TPVideoOwner


@property(nonatomic, strong)TPSmartImage    *thumbnail;
//@property(nonatomic, strong)NSDictionary    *videos;
@property(nonatomic, strong)NSString        *parentId;
@property(nonatomic, strong)NSString        *houseId;
//@property(nonatomic, strong)NSDictionary    *images;
//@property(nonatomic, strong)NSString        *title;
@property(nonatomic, assign)BOOL requiresAuth;

- (NSString *)pathForTrailer;
- ( TPVideoAsset *)videoAsset;  // just returns the first one

@end

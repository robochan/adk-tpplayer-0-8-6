//
//  TPVideoProgressInfo.m
//
//  Copyright © 2013, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)
//

#import "TPVideoProgressInfo.h"

@implementation TPVideoProgressInfo

- ( id )initWithCoder:(NSCoder *)aDecoder
{
    self = [ super init ];
    self.resumeTime = [ aDecoder decodeDoubleForKey:@"resumeTime" ];
    return self;
}

- ( void )encodeWithCoder:(NSCoder *)aCoder;
{
    [ aCoder encodeDouble:self.resumeTime forKey:@"resumeTime"  ];    
}


@end

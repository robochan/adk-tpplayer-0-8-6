//
//  TPConcurrencyInfo.m
//  TPPlayer
//
//  Copyright (c) 2014 thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)
//

#import "TPConcurrencyInfo.h"

@implementation TPConcurrencyInfo

- ( id )initWithDictionary:(NSDictionary *)metaTagDictionary
{
    if (self = [super init])
    {
        self.updateLockInterval = metaTagDictionary[@"updateLockInterval"];
        self.concurrencyServiceURL = metaTagDictionary[@"concurrencyServiceUrl"];
        self.lockID = metaTagDictionary[@"lockId"];
        self.lockSequenceToken = metaTagDictionary[@"lockSequenceToken"];
        self.lockContent = metaTagDictionary[@"lock"];
    }
    
    return self;
}

- ( NSString *)description;
{
    return [ NSString stringWithFormat:@"updateLockInterval = %@  concurrenceServiceURL = %@  lockID = %@ lockSequenceToken=  %@ lockContent = %@", self.updateLockInterval, self.concurrencyServiceURL, self.lockID, self.lockSequenceToken, self.lockContent ];
}

-(  NSString *)unlockURL;
{
    if (self.concurrencyServiceURL) {
        NSString *unlockURLString = [ NSString stringWithFormat:@"%@/web/Concurrency/unlock", self.concurrencyServiceURL ];
        return unlockURLString;
    }
    return nil;
}

-(  NSString *)updateURL;
{
    if (self.concurrencyServiceURL) {
        NSString *updateURLString = [ NSString stringWithFormat:@"%@/web/Concurrency/update", self.concurrencyServiceURL ];
        return updateURLString;
    }
    return nil;
}

@end

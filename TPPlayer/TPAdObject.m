//
//  TPAdObject.m
//  TPPlayer
//
//  Copyright © 2014, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)
//

#import "TPAdObject.h"

@implementation TPAdObject

- ( NSString *)description;
{
    return [ NSString stringWithFormat:@"type = %@ position = %lu duration = %g primaryAsset name = %@  adPOD = %@ ad id = %@", [self adTypeAsString ], (unsigned long)self.adPodPosition, self.adDuration, self.primaryAssetName, self.adPodID, self.adID ];
}

- ( NSString *)adTypeAsString;
{
    switch (self.adType) {
        case TPAdTypeUnknown:
            return @"Unknown Type";
            break;
        case TPAdTypePreRoll:
            return @"PreRoll";
            break;
        case TPAdTypeOverlay:
            return @"Overlay";
            break;
        case TPAdTypeMidRoll:
            return @"MidRoll";
            break;
        case TPAdTypePostRoll:
            return @"PostRoll";
            break;
    }
    return @"Unknown Type";
}

@end

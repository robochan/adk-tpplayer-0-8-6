//
//  TPPlayerViewController.m
//
//  Copyright © 2013, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)
//

#import "TPPlayerViewController.h"
#import "TPAnalyticsCommander.h"
#import "TPMasterDataManager.h"
#import "TPDataHandler.h"
#import "TPContentItem.h"
#import "TPVideoAsset.h"
#import "TPVideoManagerViewController.h"
#import "TPResumeOverlay.h"
#import "TPBaseClip.h"
#import "TPPlaylist.h"
#import "NSStringWithURLParsingSupport.h"
#import "TPIdentityServicesViewController.h"
#import "TPAppSpecificInformationManager.h"
#import "TPEndUserServicesAgent.h"

NSString *const TPPlayerDidReceiveContentItemNotification = @"TPPlayerDidReceiveContentItemNotification";

@interface TPPlayerViewController ()<TPVideoManagerDelegate>

@property(nonatomic, strong )UIImage *currentVideoImage;
@property(nonatomic, strong )TPVideoManagerViewController *videoManagerViewController;
@property(nonatomic, assign)BOOL shouldShowPlayButton;
@property(nonatomic, strong)NSDate *timeOfLastBeacon;
@property(nonatomic, assign, readwrite)BOOL movieIsInProgress;
@property(nonatomic, assign)BOOL networkUnavailable;

@end

@implementation TPPlayerViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization


        self.shouldOfferResume = [[ TPAppSpecificInformationManager sharedInstance]booleanForKey:@"TPResumeSupportedAcrossMultipleDevices" ];
        self.shouldEnforceConcurrency= [[ TPAppSpecificInformationManager sharedInstance]booleanForKey:@"TPConcurrencySupported" ];
        // to allow iphone to play embedded uncomment the following 2 lines. - check the iphone xib file too.
        BOOL deviceIsPhone = ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone);
        self.playsFullScreenOnly = deviceIsPhone;  // set default to YES if device is iPhone - but it is merely default no enforced
//        self.view.backgroundColor = [ UIColor greenColor];

    }
    return self;
}

- ( void )viewDidLoad;
{
    [ super viewDidLoad];
//    self.mainActivityIndicator.backgroundColor = [ UIColor whiteColor];
    self.cancelButton.hidden = !self.playsFullScreenOnly;
    [[ NSNotificationCenter defaultCenter]addObserver:self selector:@selector(reachabilityOfInterestChanged:) name:kTPReachabilityOfInterestChangedNotification object:nil ];
}

- ( void )viewDidLayoutSubviews;
{
    [ super viewDidLayoutSubviews];
    CGPoint realCenter = CGPointMake(CGRectGetMidX(self.view.bounds), CGRectGetMidY(self.view.bounds) );
    self.mainActivityIndicator.center = realCenter;
}

- (void)removeNotificationCenterObserver
{
    NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
    [notificationCenter removeObserver:self];
}

- (void)dealloc
{
    DLog(@"before");
    [self performSelectorOnMainThread:@selector(removeNotificationCenterObserver) withObject:self waitUntilDone:YES];
    DLog(@"after");
}


- ( void )viewWillDisappear:(BOOL)animated;
{
    [ super viewWillDisappear:animated];
    DLog( );
    // don't reset everything if we get covered by a presented VC.
    if (self.skipReset) {
        return;
    }

    if (self.videoManagerViewController.showsFullScreen ) {
        return;
    }
    [[ TPMasterDataManager sharedInstance ]cancelRequests:self ];
    [ self reset];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- ( void )loadReleaseURL:(NSString *)publicURLString withAdditionalParameters:( NSDictionary *)additionalParameters;
{
    NSMutableDictionary *parametersToUse = [ NSMutableDictionary dictionaryWithDictionary:additionalParameters];
    parametersToUse[@"format"] = @"Preview";
    parametersToUse[@"width"] = [ NSNumber numberWithFloat:self.view.bounds.size.width];
    parametersToUse[@"height"] = [ NSNumber numberWithFloat:self.view.bounds.size.height];
    
    NSString *initialAdjustedURLString = [ publicURLString urlStringWithNewParameters:parametersToUse ];

    if (self.metadaUrlAdjusters.count) {
        [ self.metadaUrlAdjusters[0] rewriteMetadataUrl:initialAdjustedURLString forRequester:self  isPreview:YES ];
    }
    else{
        [ self metaDataUrlAdjuster:nil applyMetadataURL:initialAdjustedURLString isPreview:YES ];
    }
}

- ( void )metaDataUrlAdjuster:( id <TPMetaDataURLAdjuster>)adjuster applyMetadataURL:(NSString *)stringForURL isPreview:( BOOL )isPreview;
{
    if ( self.metadaUrlAdjusters.count && [self.metadaUrlAdjusters lastObject ] != adjuster) 
    {
        NSUInteger index = [ self.metadaUrlAdjusters indexOfObject:adjuster];
        id<TPMetaDataURLAdjuster> nextURLAdjuster = self.metadaUrlAdjusters[index + 1];
        [ nextURLAdjuster rewriteMetadataUrl:stringForURL forRequester:self isPreview:isPreview ];
    }
    else
    {
        // on the off chance you got here without a videoAsset
        // ex.  directly from playReleaseURL...
        // you need to make one to use
        if (! self.videoAsset) {
            self.videoAsset = [ [ TPVideoAsset alloc]init];
        }
        self.videoAsset.videoURLString = stringForURL;
        if (isPreview) {
            [ self getContentItem:stringForURL];
        }
        else
        {
            [ self checkForResume ];
        }
    }
}

// was userHitPlayButton - changed for parity with pdk
- (IBAction)clickPlayButton:(id)sender;
 {
    DLog( );
    [ [ NSNotificationCenter defaultCenter]postNotificationName:@"TPTopLevelPlayButtonHitNotification" object:nil ];
    [ self showMainActivityIndicator ];
    if (self.metadataView) {
        [ self handleMetadataViewDisplay:NO ];
    }
    NSString *initialAdjustedURLString = self.contentItem.videoAsset.videoURLString;
    [ self playReleaseURL:initialAdjustedURLString withAdditionalParameters:nil ];
}

- ( void )showMainActivityIndicator;
{
    self.mainActivityIndicator.hidden = NO;
    [self.mainActivityIndicator startAnimating];      
    self.shouldShowPlayButton = NO;
    [ self handlePlayButton];
}


- ( void )checkForResume;
{
    if (self.bookmark) {
        NSTimeInterval startTime = 0.0;
        // protection from switching between using milliseconds and using seconds or any other bizarre issues
        if ([ self bookmarkPlayHeadPositionContainsReasonableValue]) {
            startTime = self.bookmark.playheadPosition;
        }
        if (startTime > 0) {
            [ self showResumeButton ];
            return;
        }
    }
    [ self getVideoData ];
}

- ( void )showResumeButton;
{
    [self.mainActivityIndicator stopAnimating];
    TPResumeOverlay *resumeOverlay = [ TPResumeOverlay resumeOverlayView ];
    [ resumeOverlay.resumePlayingButton addTarget:self action:@selector(resumePlay: ) forControlEvents:UIControlEventTouchUpInside ];
    [ resumeOverlay.playFromBeginningButton addTarget:self action:@selector(playFromBeginning: ) forControlEvents:UIControlEventTouchUpInside ];
    resumeOverlay.frame = self.view.bounds;

//    resumeOverlay.center = self.view.center;
    [self.view addSubview:resumeOverlay ];
    [ self.view bringSubviewToFront:self.cancelButton];
}

- ( void )resumePlay:( UIButton * )sender;
{
    [[ NSNotificationCenter defaultCenter]postNotificationName:kTPResumeButtonHitNotification object:nil ];
    self.mainActivityIndicator.hidden = NO;
    [self.mainActivityIndicator startAnimating]; 
    [ sender.superview removeFromSuperview ];
    // proceed with playing using the stored start value
    [ self getVideoData];
}

- ( void )playFromBeginning:( UIButton * )sender;
{
    [[ NSNotificationCenter defaultCenter]postNotificationName:kTPPlayFromBeginningButtonHitNotification object:nil ];
    self.mainActivityIndicator.hidden = NO;
    [self.mainActivityIndicator startAnimating]; 
    [ sender.superview removeFromSuperview ];
    // remove stored start value
    [[ TPEndUserServicesAgent sharedInstance]userOptedToPlayFromBeginning:self.bookmark ];
    [ self getVideoData];
}

- ( void )presentFullScreenVideoManagerVC:( TPVideoManagerViewController *)videoManagerViewController;
{
    [ videoManagerViewController willMoveToParentViewController:nil ];
    [ videoManagerViewController.view removeFromSuperview ];
    [ videoManagerViewController removeFromParentViewController];
    // this is for debugging
//    videoManagerViewController.view.backgroundColor = [ UIColor redColor];
//    self.view.backgroundColor = [ UIColor blueColor];
    self.videoManagerViewController.clearedToDismiss = NO;
    [ self presentViewController:videoManagerViewController animated:NO completion:^{
        videoManagerViewController.clearedToDismiss = YES;
    } ];
}

- ( void )restoreFullScreenVideoManagerVC:( TPVideoManagerViewController *)videoManagerViewController;
{
    [ self dismissViewControllerAnimated:YES completion:^{
        [ self addChildViewController:videoManagerViewController];
        videoManagerViewController.view.frame = self.view.bounds;
        [self.view addSubview:videoManagerViewController.view];
        [ self.view bringSubviewToFront:self.cancelButton];
        [ videoManagerViewController didMoveToParentViewController:self ];
    }];
}

- ( void )getVideoData;
{
    self.videoManagerViewController = [ [ TPVideoManagerViewController alloc ]initWithFrame:self.view.bounds ];
    self.videoManagerViewController.shouldOfferResume = (self.shouldOfferResume && [[ TPEndUserServicesAgent sharedInstance]resumeIsAvailable]);
    if (self.shouldOfferResume) {
        self.videoManagerViewController.aboutID = self.contentItem.aboutID;
    }
//    self.videoManagerViewController.playsFullScreenOnly = self.playsFullScreenOnly;
    self.videoManagerViewController.delegate = self;
    self.videoManagerViewController.interestedParty = self.interestedParty;
    self.videoManagerViewController.playerControlClassName = self.playerControlClassName;
    self.videoManagerViewController.playerControlsRequireTapToHide = self.playerControlsRequireTapToHide;
    self.videoManagerViewController.externalHeartbeatObservers = self.externalHeartbeatObservers;
    
//    BOOL deviceIsPhone = ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone);
//    self.videoManagerViewController.playsFullScreenOnly = deviceIsPhone ? YES : self.playsFullScreenOnly;
    
    // to make iphone play embedded - delete the 2 lines above and use this instead
    self.videoManagerViewController.playsFullScreenOnly = self.playsFullScreenOnly;
    self.videoManagerViewController.inlineClosedCaptionsPresent = self.contentItem.videoAsset.inlineClosedCaptionsPresent;
    
    // TESTING - set this when you create the playerViewController
//    self.shouldAllowAirplay = YES;
    self.videoManagerViewController.shouldAllowAirplay = self.shouldAllowAirplay;
    self.videoManagerViewController.shouldTrackQuartiles = self.shouldTrackQuartiles;
    
    // protection from switching between using milliseconds and using seconds or any other bizarre issues
    if ( [ self bookmarkPlayHeadPositionContainsReasonableValue]) {
        self.videoManagerViewController.startTime = self.bookmark.playheadPosition;
        DLog(@"time to start = %d", (int)self.videoManagerViewController.startTime);
    }
    else{
        self.videoManagerViewController.startTime = 0;
    }
    
    self.videoManagerViewController.adManager =  self.adManager;
    
    // we want this to happen before videoManagerViewController's viewDidLoad
    if( self.shouldPlayLiveStream )  
    {
        self.videoManagerViewController.intervalsToMonitor = self.intervalsToMonitor;
        [self.videoManagerViewController prepareForLiveStream:YES];
    }
    if ((!self.isAlwaysFullScreen) && self.videoManagerViewController.playsFullScreenOnly) {
        self.videoManagerViewController.clearedToDismiss = NO;
        [ self presentViewController:self.videoManagerViewController animated:NO completion:^{
            self.videoManagerViewController.clearedToDismiss = YES;
        }];
    }
    else
    {
        [ self addChildViewController:self.videoManagerViewController];
        self.videoManagerViewController.view.frame = self.view.bounds;
        [self.view addSubview:self.videoManagerViewController.view];
        [ self.view bringSubviewToFront:self.cancelButton];
        [ self.videoManagerViewController didMoveToParentViewController:self ];
    }
    if (self.shouldPlayLiveStream) {
//        NSURL *movieURL = [ NSURL URLWithString:self.videoAsset.videoURLString];
//        [self.videoManagerViewController playVideoWithURL:movieURL];
//        [ [TPAnalyticsCommander sharedInstance ] trackVideoView:self stage:@"liveStream video start" ];
        [ [TPAnalyticsCommander sharedInstance ] trackVideoView:self withTime:0 stage:@"liveStream video start" ];
        self.timeOfLastBeacon = [ NSDate date];
    }
    if (self.directPlayForTesting) {
        NSURL *movieURL = [ NSURL URLWithString:self.videoAsset.videoURLString];
        [self.videoManagerViewController playVideoWithURL:movieURL];
    }
        [[ TPMasterDataManager sharedInstance ] loadSmilDataForUrl:self.videoAsset.videoURLString metadata:self.videoAsset.videoURLString delegate:self selector:@selector(smilDataLoaded:)];  
}

- ( BOOL )bookmarkPlayHeadPositionContainsReasonableValue
{
    BOOL bookmarkPlayHeadPositionContainsReasonableValue = (self.bookmark && self.contentItem.videoAsset && self.bookmark.playheadPosition > 0 && self.bookmark.playheadPosition < self.contentItem.videoAsset.duration);
    DLog(@"bookmarkPlayHeadPositionContainsReasonableValue = %@", @(bookmarkPlayHeadPositionContainsReasonableValue) );
    return bookmarkPlayHeadPositionContainsReasonableValue;
}

- ( void )videoStartedPlayingFromBeginning;
{
    [ [TPAnalyticsCommander sharedInstance ] trackVideoView:self stage:@"video start" ];
}

- ( void )videoStartedPlayingFromResume;
{
    [ [TPAnalyticsCommander sharedInstance ] trackVideoView:self stage:@"video resume" ];
}

- ( void )userStartedScrubbing;
{
    [ [TPAnalyticsCommander sharedInstance ] trackVideoView:self stage:@"user scrubbing" ];
}


- ( void )getContentItem:( NSString *)urlForContentItem;
{
    DLog(@"urlForContentItem = %@", urlForContentItem);
    [ [ TPMasterDataManager sharedInstance ]getContentItemForURL:urlForContentItem requester:self selector:@selector(contentItemLoaded:) path:@"ContentItem" ];
}

- ( void )smilDataLoaded:( TPDataHandler *)dataHandler;
{
    NSError *error = dataHandler.error;
    if (error == nil)
    {
        self.movieIsInProgress = YES;
        TPPlaylist *playlist = dataHandler.returnedObject;
        if(self.shouldEnforceConcurrency)
        {
            self.videoManagerViewController.concurrencyInfo = playlist.concurrencyInfo;
        }
        TPBaseClip *baseClip = playlist.baseClip;
        NSString *sourceURL = baseClip.url;
        if (baseClip.freewheelAssetID) {
            self.contentItem.adManagerId = baseClip.freewheelAssetID;
            self.contentItem.videoAsset.adManagerId = baseClip.freewheelAssetID;
        }
        if (sourceURL.length) {
            BOOL needSideCarCaptions = !self.contentItem.videoAsset.inlineClosedCaptionsPresent;
            if (baseClip.CCURL && needSideCarCaptions) {
                [ [ TPMasterDataManager sharedInstance] getCCDataForPath:baseClip.CCURL metadata:baseClip delegate:self selector:@selector(ccDataLoaded:) ];
            }
            else
            {
                [ self adjustBaseClip:baseClip ];
            }
        }
        else {
            [ self showNoContent:YES ];
            [ self.mainActivityIndicator stopAnimating ];  // hide on stop defined in nib
        }
    }
    else {
        NSString *errorMessage = [ error localizedDescription];
        NSString *errorToShow = errorMessage.length ? errorMessage : @"No source in the smil data";
        [ self loadDataForURLFailed:errorToShow];
        [ self.mainActivityIndicator stopAnimating ];  // hide on stop defined in nib
    }
}

- ( void )adjustBaseClip:( TPBaseClip *)baseClip
{
    if (self.urlAdjusters.count) {
        [ self.urlAdjusters[0] rewriteURL:baseClip forRequester:self ];
    }
    else{
        [ self urlAdjuster:nil applyClip:baseClip ];
    }
}

// clip adjustment is completed
- ( void )urlAdjuster:( id <TPURLAdjuster>)adjuster applyClip:(TPBaseClip *)baseClip;
{
    if ( self.urlAdjusters.count  && ([self.urlAdjusters lastObject ] != adjuster)) 
    {
        NSUInteger index = [ self.urlAdjusters indexOfObject:adjuster];
        id<TPURLAdjuster> nextURLAdjuster = self.urlAdjusters[index + 1];
        [ nextURLAdjuster rewriteURL:baseClip forRequester:self ];
    }
    else{
        if (baseClip) {
            [ self playVideo:baseClip];
        }
        else
        {
            [ self loadDataForURLFailed:@"You may not have access to this content." ];
        }
    }
}

- ( void )showNoContent:( BOOL)contentIsMissing;
{
    [[ NSNotificationCenter defaultCenter]postNotificationName:kTPMovieNoContentNotification object:nil ];
    [ self stopVideo];
    self.noContentLabel.hidden = !contentIsMissing;
    [self.view bringSubviewToFront:self.noContentLabel ];
    self.shouldShowPlayButton = !contentIsMissing;
    DLog(@"self.shouldShowPlayButton = %@", YES_OR_NO(self.shouldShowPlayButton));
    [ self handlePlayButton];
}

- ( void )loadDataForURLFailed:( NSString *)errorMessage;
{
    [[ NSNotificationCenter defaultCenter]postNotificationName:kTPMovieFailedToLoadNotification object:errorMessage ];
    DLog(@"loadDataForURLFailed >>>>>>>>>>>>> %@", errorMessage);
    [ self stopVideo];
    
    [self.mainActivityIndicator stopAnimating];
    [ self showNoContent:YES];
    
    if (self.delegateShouldHandleLoadFailureAlert) {
        [ self.delegate userReceivedLoadError:errorMessage ];
        return;
    }
    [ self showErrorAlert:errorMessage];
}

- ( void )showErrorAlert:( NSString *)alertMessage;
{
    if (!self.suppressErrorAlert) {
        NSString *loadFailureTitle = NSLocalizedString(@"Load Failure", @"load failure");
        if (NSClassFromString(@"UIAlertController")) {
            UIAlertController *alertController = [ UIAlertController alertControllerWithTitle:loadFailureTitle message:alertMessage preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *cancelAction = [ UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
                DLog(@"user says OK");
                [ self userReceivedLoadFailureAlert  ];
            }];
            [ alertController addAction:cancelAction];
            [ self presentViewController:alertController animated:NO completion:nil ];
        }
        else{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:loadFailureTitle message:alertMessage  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
    }
}

- ( void )userReceivedLoadFailureAlert;
{
    // subclasses can override if they want to do anything here.  On iOS7 call from delegate method.
}

/*
- ( void )videoLoading:( BOOL )finished;
{
    DLog(@"video loading likelyToKeepUp = %@", finished ? @"YES" : @"NO");
    if (finished) {
        [self.mainActivityIndicator stopAnimating ];
        DLog(@"video loading likelyToKeepUp = YES stopAnimating" );
    }
    else
    {
        self.mainActivityIndicator.hidden = NO;
        [ self.view bringSubviewToFront:self.mainActivityIndicator];
        [self.mainActivityIndicator startAnimating ];
        DLog(@"video loading likelyToKeepUp = NO started spinner" );
   }
}
 */

- ( void )videoStalled;
{
    DLog(@"video stalled started spinner");
    self.mainActivityIndicator.hidden = NO;
    [ self.view bringSubviewToFront:self.mainActivityIndicator];
    [self.mainActivityIndicator startAnimating ];
}

- ( void )stallResolved;
{
    DLog(@"stallResolved stopAnimating" );
    [self.mainActivityIndicator stopAnimating ];
}

// This is called if this class is made an interestedParty to TPNetworkMonitor
// or from the method below
- ( void )reachabilityChanged:( BOOL )networkIsReachable;
{
    DLog(@"reachabilityChanged networkIsReachable = %@", networkIsReachable ? @"YES" : @"NO");
    [ self.videoManagerViewController reachabilityChanged:networkIsReachable];
    if (networkIsReachable) {
        [self.mainActivityIndicator stopAnimating ];
    }
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    self.networkUnavailable = !networkIsReachable;
    [ self setNeedsStatusBarAppearanceUpdate ];
}

// This is called because we signed up for the notification from TPNetworkMonitor
// we might not want to be the interested party because there can be only one at any one time.
- ( void)reachabilityOfInterestChanged:( NSNotification *)notification;
{
    BOOL networkIsReachable = [[ TPNetworkMonitor sharedInstance]networkIsReachable];
    [ self reachabilityChanged:networkIsReachable];
}

- ( BOOL )prefersStatusBarHidden;
{
    DLog(@"self.networkUnavailable = %@ so prefers hidden = %@", self.networkUnavailable ? @"YES" : @"NO", (!self.networkUnavailable) ? @"YES" : @"NO" )
    return ! self.networkUnavailable ;
}

- ( BOOL)modalPresentationCapturesStatusBarAppearance;
{
    return YES;
}


- ( void )contentItemLoaded:( TPDataHandler *)dataHandler;
{
    NSError *error = dataHandler.error;
    
    if (error == nil)
    {
        TPContentItem *contentItemToUse = dataHandler.returnedObject;
        
        [[ NSNotificationCenter defaultCenter]postNotificationName:TPPlayerDidReceiveContentItemNotification object:contentItemToUse ];
        // get the poster image from the contentItem
        self.contentItem = contentItemToUse;

        // posterImage could be nil
        [ self userChangedSelection:contentItemToUse ];
    }
}

- ( void )playVideo:( TPBaseClip *)baseClip
{
    //    NSURL *movieURL = [ NSURL URLWithString:smilInfo.source ]; // copy
    NSURL *movieURL = [ NSURL URLWithString:baseClip.url ]; // copy
    DLog(@"movieURL = %@", movieURL);
//    [ [TPAnalyticsCommander sharedInstance ] trackVideoView:self stage:@"video start" ];
    [ [TPAnalyticsCommander sharedInstance ] trackVideoView:self stage:@"video load starting" ];
    self.cancelButton.hidden = YES;

    if (self.adManager && self.contentItem.adManagerId.length)
    {
        [self.videoManagerViewController playVideoWithURL:movieURL adManagerId:self.contentItem.adManagerId];
    }
    else
    {
        [self.videoManagerViewController playVideoWithURL:movieURL];
    }
}

- ( void)ccDataLoaded:(TPDataHandler *)dataHandler
{
    NSError *error = dataHandler.error;
    if (error == nil)
    {
        TPClosedCaption *closedCaptionObject = dataHandler.returnedObject;
        NSArray *closedCaptionLines = closedCaptionObject.closedCaptionLines;
        if ([closedCaptionLines count] )
        {
            [ self.videoManagerViewController setUpClosedCaptioning:closedCaptionObject ];
        }
    }
    TPBaseClip *baseClip = dataHandler.metadata;
    [ self playVideo:baseClip];
}

- ( void )moviePlaybackAndAdsDidFinish;
{
    [ self clearVideoManagerViewController];
    [ [ NSNotificationCenter defaultCenter]postNotificationName:kTPPlayerMovieAndAdsDidFinishNotification object:nil ];
    if (self.shouldPlayFullScreenImmediately) {
        self.cancelButton.hidden = YES; // it is flashing on dismiss
        [ self.delegate playbackDidFinish ];
    }
    else
    {
        self.shouldShowPlayButton = (self.contentItem.videoAsset !=nil);
        DLog(@"self.shouldShowPlayButton = %@", YES_OR_NO(self.shouldShowPlayButton));
        // more fading code
        if (self.currentVideoImage && self.videoImage.alpha == 0.0f) {
            [ UIView animateWithDuration:self.fadeDuration animations:^{
                self.videoImage.alpha = 1.0f;
            }
                              completion:^(BOOL finished) {
                                  self.playButton.hidden = NO;
                              } ];
        }
        [ self handlePlayButton];
    }
}

// we only get t his is the user play to the end - the notification object is the AVPlayerItem
- ( void )moviePlaybackDidFinish:(NSNotification *)notification;
{
    DLog( );
    [ self videoEnded ];

    // moviePlaybackAndAdsDidFinish was called as part of the cleanup if there was an adManager
    BOOL thereIsNoAdManagerActive = !(self.videoManagerViewController.adManager);
    BOOL thisItemHasNoAdManagerId = !(self.contentItem.adManagerId.length);
    if (thereIsNoAdManagerActive || thisItemHasNoAdManagerId)  
    {
        [ self moviePlaybackAndAdsDidFinish];
    }
    DLog( );
}

- ( void )videoPaused;
{
    if (self.shouldPlayLiveStream) 
    {
        NSUInteger secondsFromLastBeacon = -1 * [self.timeOfLastBeacon timeIntervalSinceNow ];
        // reset the time
        DLog(@"secondsFromLastBeacon = %lu  timeOfLastBeacon = %@", (unsigned long)secondsFromLastBeacon, self.timeOfLastBeacon);
        [ [TPAnalyticsCommander sharedInstance ] trackVideoView:self withTime:secondsFromLastBeacon stage:@"liveStream video pause" ];
    }
    self.timeOfLastBeacon = [ NSDate date];
}

- ( void )userSelectedDoneButton;
{
    [ [TPAnalyticsCommander sharedInstance ] trackVideoView:self stage:@"video stopped" ];
}

- ( void )userPausedPlayer;
{
    [ [TPAnalyticsCommander sharedInstance ] trackVideoView:self stage:@"video pause" ];
}

- ( void )userChangedSelection:( TPContentItem * )contentItemToUse;
{

    [ self reset];
    // don't make bookmarks if we are playing live
    if (self.shouldOfferResume && !self.shouldPlayLiveStream) {
        [[ TPEndUserServicesAgent sharedInstance ]contentChanged:contentItemToUse.aboutID requester:self ];
    }
    self.contentItem = contentItemToUse;
    self.videoAsset = contentItemToUse.videoAsset;
    self.currentVideoImage  = [ contentItemToUse imageForRole:contentItemToUse.roleForDefaultPosterImage  ];
    if (self.currentVideoImage )  // otherwise it will show up in videoImageLoaded:
    {
        self.videoImage.alpha = 1.0;
        self.videoImage.image = self.currentVideoImage ;
        self.imageActivityIndicator.hidden = YES;
    }
    else
    {
        [ contentItemToUse goGetImageForRole:contentItemToUse.roleForDefaultThumbnail  metadata:nil delegate:self selector:@selector(videoImageLoaded:) activityIndicator:self.imageActivityIndicator];
    }
    BOOL weHaveAnAsset = contentItemToUse.videoAsset != nil;
    self.noContentLabel.hidden = (weHaveAnAsset);
    if ([ [ TPNetworkMonitor sharedInstance]networkIsAvailableForVideoPlay]) 
    {
        if (weHaveAnAsset && self.shouldPlayFullScreenImmediately) {
            [ self clickPlayButton:nil ];
        }
        else{
            if (self.metadataView) {
                [ self.metadataView populateFromContentItem:self.contentItem ];
                [ self handleMetadataViewDisplay:YES ];
            }
            
            DLog( );
            self.shouldShowPlayButton = YES;
            DLog(@" user changed Selection self.shouldShowPlayButton = %@", YES_OR_NO(self.shouldShowPlayButton));
            [ self handlePlayButton];
            //    self.videoURLString = contentItemToUse.videoAsset.videoURLString;
            
            DLog(@"poster image is %@", self.currentVideoImage);
            [ self handleAuthViewDisplay];
        }
        //    self.videoURLString = contentItemToUse.videoAsset.videoURLString;
    }
    else{
        self.shouldShowPlayButton = NO;
        [ self handlePlayButton];
        if (self.delegateShouldHandleNoWiFiAlert) {
            [ self.delegate userReceivedNoWifiCondition ];
        }
        else{
            [ self showNoWifiAlert ];
        }
    }
    DLog(@"poster image is %@", self.currentVideoImage);
}

- ( void )showNoWifiAlert;
{
    if (self.noWifiMessage.length == 0) {
        self.noWifiMessage = @"WiFi Required";
    }
    if (self.noWifiAlertTitle.length == 0) {
        self.noWifiAlertTitle = @"There is currently no WiFi Available.";
    }
    NSString *loadFailureTitle = NSLocalizedString(@"Load Failure", @"load failure");
    if (NSClassFromString(@"UIAlertController")) {
        UIAlertController *alertController = [ UIAlertController alertControllerWithTitle:self.noWifiAlertTitle message:self.noWifiMessage preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancelAction = [ UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
            DLog(@"user says OK");
            [ self userReceivedNoWifiAlert  ];
        }];
        [ alertController addAction:cancelAction];
        [ self presentViewController:alertController animated:NO completion:nil ];
    }
    else{
    UIAlertView *alertView = [[ UIAlertView alloc]initWithTitle:self.noWifiAlertTitle message:self.noWifiMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil ];
    [ alertView show];
    }
}

- ( void )userReceivedNoWifiAlert;
{
    // subclasses can override if they want to do anything here.  On iOS7 call from UIAlertView's delegate method, if you make one.
}

- ( void )bookmarkFetchComplete:( TPBookmark *)bookmark;
{
    DLog(@"********  BOOKMARK FETCH COMPLETE bookmark = %@", bookmark);
        self.bookmark = bookmark;
}

- ( void )setBookmark:(TPBookmark *)bookmark;
{
    // We already have a bookmark, we are being passed a new one.
    // Take the more recent of the two
    if (self.bookmark && bookmark && self.bookmark != bookmark) {
        if (NSOrderedDescending == [[self.bookmark dateUpdated] compare:[ bookmark dateUpdated]]) {
            // don't replace the one we have
            DLog(@"using the existing bookmark %@", _bookmark);
            return;
        }
    }
    DLog(@"using the replacement bookmark %@", bookmark);
    _bookmark = bookmark;
}

- ( void )handleMetadataViewDisplay:( BOOL)shouldShow;
{
    if (shouldShow ) {
        if ( self.metadataView.alpha < 0.9) {
            [ UIView animateWithDuration:self.fadeDuration animations:^{
                self.metadataView.alpha = 1.0f;
            }
                              completion:^(BOOL finished) {
                                  //                                  self.metadataView.hidden = NO;
                              } ];
        }
    }
    else // fade it out
    {
        if ( self.metadataView.alpha > 0.1) {
            [ UIView animateWithDuration:self.fadeDuration animations:^{
                self.metadataView.alpha = 0.0f;
            }
                              completion:^(BOOL finished) {
                                  //                                  self.metadataView.hidden = YES;
                              } ];            
        }
    }
}


- (void) videoImageLoaded:(TPDataHandler *) dataHandler
{
    self.currentVideoImage = dataHandler.returnedObject;
    if (self.currentVideoImage)
    {
        self.videoImage.image = self.currentVideoImage;
    }
    else{
        self.videoImage.image = [self defaultVideoImage ];
    }
    [ self.imageActivityIndicator stopAnimating]; // hide on stop defined in nib
}

- ( UIImage *)defaultVideoImage;
{
    return nil;
}

- ( void )setUserIsAuthorized:(BOOL)userIsAuthorized;
{
    _userIsAuthorized = userIsAuthorized;
    self.videoManagerViewController.shouldOfferResume = (self.shouldOfferResume && [[ TPEndUserServicesAgent sharedInstance]resumeIsAvailable] );

    DLog(@"self.shouldShowPlayButton = %@", YES_OR_NO(self.shouldShowPlayButton));
    [ self handleAuthViewDisplay];
    [ self handlePlayButton];
}

- ( void )handlePlayButton;
{
    if (self.contentItem.videoAsset.needsAuthorization) {
        DLog(@"self.contentItem.videoAsset.needsAuthorization");
        if( self.userIsAuthorized && self.shouldShowPlayButton)
        {
            self.playButton.hidden = NO;
        }
        else
        {
            self.playButton.hidden = YES;
        }
    }
    else{  // if we are here authorization is not an issue
        DLog(@"authorization is not an issue");
        self.playButton.hidden = !self.shouldShowPlayButton;
    }
 }

- ( void )reset;
{
    [ self stopVideo ];
    // until we have either a video asset or a reason to beleive we can't get it, hide the play button and the no content label
    self.noContentLabel.hidden = YES;
    self.shouldShowPlayButton = NO;
    self.playButton.hidden = YES;
    self.videoAsset = nil;
    self.currentVideoImage = nil;
    self.videoImage.image = nil;
    [self.imageActivityIndicator stopAnimating ];
    [self.mainActivityIndicator stopAnimating ];
    self.cancelButton.hidden = !self.playsFullScreenOnly;
}

// if an exception is returned from a concurrency update
- ( void )receivedConcurrencyConflict;
{
    [self.videoManagerViewController stopVideo];
    [ self clearVideoManagerViewController];
    [ self videoStopped ]; 
    self.movieIsInProgress = NO;
    
    NSString *concurrencyConflictTitle = NSLocalizedString(@"Concurrency Conflict", @"Concurrency Conflict");
    NSString *concurrencyConflictMessage= NSLocalizedString(@"The maximum number of devices showing this content has been exceeded", @"The maximum number of devices showing this content has been exceeded");
    
    if (self.delegateShouldHandleConcurrencyAlert) {
        [ self.delegate userReceivedConcurrencyError:concurrencyConflictMessage];
        return;
    }
    
    if (NSClassFromString(@"UIAlertController")) {
        UIAlertController *alertController = [ UIAlertController alertControllerWithTitle:concurrencyConflictTitle message:concurrencyConflictMessage preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancelAction = [ UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
            DLog(@"user says OK");
            [ self userReceivedConcurrencyConflictAlert  ];
        }];
        [ alertController addAction:cancelAction];
        [ self presentViewController:alertController animated:NO completion:nil ];
    }
    else{
        UIAlertView *alertView = [[ UIAlertView alloc]initWithTitle:concurrencyConflictTitle message:concurrencyConflictMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil ];
        [ alertView show];
    }
}

- ( void )userReceivedConcurrencyConflictAlert;
{
    // subclasses can override if they want to do anything here.  On iOS7 call from UIAlertView's delegate method, if you make one.
}


- (void) stopVideo
{
    self.movieIsInProgress = NO;
    
    if (self.videoManagerViewController != nil)
    {
        [self.videoManagerViewController stopVideo];
        [ self clearVideoManagerViewController];
        [ self videoStopped ];
    }
}

- ( void)clearVideoManagerViewController;
{
    self.movieIsInProgress = NO;
    if( self.videoManagerViewController.parentViewController )
    {
        [ self.videoManagerViewController willMoveToParentViewController:nil ];
        [ self.videoManagerViewController.view removeFromSuperview];
        [ self.videoManagerViewController removeFromParentViewController];
        self.videoManagerViewController.delegate = nil;
        self.videoManagerViewController = nil;
    }
    else{
        if (self.videoManagerViewController.presentingViewController) {
            [ self.videoManagerViewController.presentingViewController dismissViewControllerAnimated:NO completion:^{
                self.videoManagerViewController.delegate = nil;
                self.videoManagerViewController = nil;
            } ];
        }
    }

}


- ( void )playReleaseURL:(NSString *)publicURLString withAdditionalParameters:( NSDictionary *)additionalParameters;
{
    NSMutableDictionary *parametersToUse = [ NSMutableDictionary dictionaryWithDictionary:additionalParameters];
    parametersToUse[@"format"] = @"SMIL";
//    parametersToUse[@"embedded"] = @"true";
    parametersToUse[@"tracking"] = @"true";
    
    if (self.shouldEnforceConcurrency ) {
        NSString *userToken = [[ TPEndUserServicesAgent sharedInstance]userToken ];
        NSString *clientID = [ [ TPEndUserServicesAgent sharedInstance]clientID ];
        if (userToken && clientID) {
            parametersToUse[@"clientId"]= clientID;
            parametersToUse[@"auth" ]= userToken;
            //            parametersToUse[@"traceto"] = @"mailto:dave.browne@theplatform.com";
        }
    }
    
    NSString *initialAdjustedURLString = [ publicURLString urlStringWithNewParameters:parametersToUse ];
    if (self.shouldEnforceConcurrency && parametersToUse[@"auth" ]) {
        initialAdjustedURLString = [initialAdjustedURLString stringByReplacingOccurrencesOfString:@"http://" withString:@"https://" ];
    }
    if (self.metadaUrlAdjusters.count) {
        [ self.metadaUrlAdjusters[0] rewriteMetadataUrl:initialAdjustedURLString forRequester:self isPreview:NO ];
    }
    else{
        [ self metaDataUrlAdjuster:nil applyMetadataURL:initialAdjustedURLString isPreview:NO ];
    }
}

//- ( void )storeCurrentPlaybackTime:( NSTimeInterval )currentPlaybackTime duration:( NSTimeInterval)duration;
//{
//    self.videoAsset.duration = duration;
//    [ [ TPMasterDataManager sharedInstance]saveTimeToStart:currentPlaybackTime forVideoAsset:self.videoAsset];
//}

- ( void )videoEnteredFullScreen;
{
    [ [TPAnalyticsCommander sharedInstance ] trackVideoView:self stage:@"full screen view" ];
}

- ( void )userExitedFullscreen;
{
    
}

- (void)setShouldKeepAlive:( BOOL )shouldKeepAlive;
{
    if (_shouldKeepAlive != shouldKeepAlive) {
        self.videoManagerViewController.shouldKeepAlive = shouldKeepAlive;
        _shouldKeepAlive = shouldKeepAlive;
    }
}


- ( void )videoStarted;
{
    DLog("Media Video Player - Video Started");
    // fade code
//    BOOL shouldFade = self.isAlwaysFullScreen || ! self.videoManagerViewController.playsFullScreenOnly;
    self.cancelButton.hidden = YES;
    BOOL shouldFade = self.videoManagerViewController.parentViewController != nil;
    if( shouldFade )
    {
        if (self.currentVideoImage && self.videoImage.alpha > 0.0 ) // don't do this if the movie is playing (which means we have set the videoImage.alpha to 0.0
        {
            [ self.mainActivityIndicator stopAnimating];
            [ UIView animateWithDuration:self.fadeDuration animations:^{
            self.videoImage.alpha = 0.0f;
            }
                            completion:^(BOOL finished) {
                                [self.view addSubview:self.videoManagerViewController.view];
                            } ];
        }
    }
    //    else{
    //        [self.view addSubview:self.videoManagerViewController.view];
    //    }
//    if (self.shouldPlayLiveStream) {
//        [ [TPAnalyticsCommander sharedInstance ] trackVideoView:self withTime:0 stage:@"liveStream video start" ];
//    }
//    else
//    {
//            [ [TPAnalyticsCommander sharedInstance ] trackVideoView:self stage:@"video start" ];
//    }
        self.timeOfLastBeacon = [ NSDate date];
}

- ( void )videoStopped
{
    if (self.shouldPlayLiveStream) 
    {
        NSUInteger secondsFromLastBeacon = lround(fabs([self.timeOfLastBeacon timeIntervalSinceNow ]));
        // reset the time
        DLog(@"secondsFromLastBeacon = %lu  timeOfLastBeacon = %@", (unsigned long)secondsFromLastBeacon, self.timeOfLastBeacon);
        [ [TPAnalyticsCommander sharedInstance ] trackVideoView:self withTime:secondsFromLastBeacon stage:@"liveStream video stopped" ];
    }
    else{
        [ [TPAnalyticsCommander sharedInstance ] trackVideoView:self stage:@"video stopped" ];
    }
        self.timeOfLastBeacon = [ NSDate date];
}

- ( void )timeHasPassedQuartile:( NSUInteger)quartile;
{
    
     switch (quartile) {
     case 0:
     [ [TPAnalyticsCommander sharedInstance ] trackVideoView:self stage:@"video 25 percent" ];
     break;
     case 1:
     [ [TPAnalyticsCommander sharedInstance ] trackVideoView:self stage:@"video 50 percent" ];
     break;
     case 2:
     [ [TPAnalyticsCommander sharedInstance ] trackVideoView:self stage:@"video 75 percent" ];
     break;
     default:
     break;
     }
     
}

- ( void )adStarted:( NSDictionary *)notificationDictionary adPosition:( NSString *)adPosition;
{
    [ [TPAnalyticsCommander sharedInstance ] trackVideoAdStarted:self  adPosition:adPosition additionalInformation:notificationDictionary];
}

- ( void )adEnded:( NSDictionary *)notificationDictionary adPosition:( NSString *)adPosition;
{
    [ [TPAnalyticsCommander sharedInstance ] trackVideoAdCompleted:self adPosition:adPosition additionalInformation:notificationDictionary];
}

- ( void )slotStarted:( TPAdPod *)adPod;
{
    [ self.mainActivityIndicator stopAnimating];
    [ [TPAnalyticsCommander sharedInstance ] trackVideoAdPodStarted:self adPod:adPod ];
    if (adPod.podType == TPAdTypeMidRoll) {
        [[ TPAnalyticsCommander sharedInstance]trackVideoView:self stage:@"pause for ad" ];
    }
}
- ( void )slotEnded:( TPAdPod *)adPod;
{
    [ [TPAnalyticsCommander sharedInstance ] trackVideoAdPodCompleted:self adPod:adPod ];
}
- ( void )adStarted:( TPAdObject *)adObject;
{
    [ [TPAnalyticsCommander sharedInstance ] trackVideoAdStarted:self adObject:adObject ];
}
- ( void )adEnded:( TPAdObject *)adObject;
{
    [ [TPAnalyticsCommander sharedInstance ] trackVideoAdCompleted:self adObject:adObject ];
}


- ( void )videoEnded;
{
        [ [TPAnalyticsCommander sharedInstance ] trackVideoView:self stage:@"video complete" ];
        self.timeOfLastBeacon = [ NSDate date];
}

// this is liveStream only
- ( void )intervalFired:( TPInterval *)interval;
{
    NSString *stageString = [ interval messageForStage];
    [ [TPAnalyticsCommander sharedInstance ] trackVideoView:self withTime:interval.intervalLength stage:stageString ];
    self.timeOfLastBeacon = [ NSDate date];
    DLog(@"TPPlayer interval fired new date = %@", self.timeOfLastBeacon );
}

#pragma  - mark methods for hiding and displaying authorization overlay

- ( void )handleAuthViewDisplay;
{
    DLog(@"handleAuthViewDisplay");
    //  if the user is already authorized or if this contentItem doesn't require authentication, take the view down if necessary
    if (self.userIsAuthorized || ! self.contentItem.requiresAuth) {
        [ self removeAuthView ];
        return;
    }
    // the user is not authorized and the content requires authorization
    // However, if the movie is in progress, don't show the overlay
    if ( !self.movieIsInProgress )
    {
        [ self reinstateAuthView ];
    }
    else{
        DLog(@"self.playerViewController.movieIsInProgress");
    }
}

- ( void )reinstateAuthView;
{
    if (self.identityServicesViewController && !(self.identityServicesViewController.parentViewController)) {
        DLog(@"adding to parentVC");
        [ self addChildViewController:self.identityServicesViewController];
        CGRect frame = self.view.bounds;
        self.identityServicesViewController.view.frame = frame;
        [ self.view addSubview:self.identityServicesViewController.view];
        [ self.identityServicesViewController didMoveToParentViewController:self ];
    }
//    if (!self.authorizationOverlay.superview) {
//        CGRect frame = self.view.bounds;
//        self.authorizationOverlay.frame = frame;
//        [ self.view addSubview:self.authorizationOverlay ];
//    }
}

- ( void )removeAuthView;
{
    if (self.identityServicesViewController && (self.identityServicesViewController.parentViewController)) {
        DLog(@"removing from parentVC");
        [ self.identityServicesViewController willMoveToParentViewController:nil ];
        [ self.identityServicesViewController.view removeFromSuperview];
        [ self.identityServicesViewController removeFromParentViewController];
    }
//    if (self.authorizationOverlay.superview) {
//        [ self.authorizationOverlay removeFromSuperview];
//    }
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}



- (NSUInteger)supportedInterfaceOrientations;
{
    return UIInterfaceOrientationMaskLandscape;
}

// this should onlyl be possible if the player is in fullScreenOnly mode
- (IBAction)userHitCancel:(id)sender {
//    [ self dismissViewControllerAnimated:YES completion:nil];
    if (self.videoManagerViewController) {
        [ self.videoManagerViewController stopVideo];
    }
    [ self.delegate playerCancelled ];
}

- ( void )setContentItem:(TPContentItem *)contentItem;
{
    if (_contentItem != contentItem) {
        _contentItem = contentItem;
        [[ NSNotificationCenter defaultCenter]postNotificationName:kTPUserSelectedContentItemNotification object:contentItem];
    }
}

@end

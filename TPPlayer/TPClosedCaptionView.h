//
//  TPClosedCaptionView.h
//
//  Compatibility: iOS 5.1+
//
//
//  Copyright © 2013, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)
//
//  SUMMARY
//  View subclass used to host CATextLayers
//  The view is positioned in accord with region data
//  and a CATextLayer( which can support NSAttributedString in ios 5.1 ) is added
//  to show a closed caption line of text
//
//
//  DEVELOPMENT Notes
//

#import <UIKit/UIKit.h>
#import <CoreMedia/CoreMedia.h>
#import "TPCCLine.h"

@interface TPClosedCaptionView : UIView

@property(nonatomic, assign)CMTime timeToRemove;
@property(nonatomic, strong)UILabel *label;
@property(nonatomic, assign)NSUInteger cornerRadius;

//  position the view for lineToShow  -- it stores the percents from the TPCCLine, so it automatically handles rotation
- ( void )adjustFrame:( TPCCLine *)lineToShow superviewFrame:( CGRect)superviewFrame;
// size the line to fit the label as much as is possible
- ( void )adjustSizeForLine;

@end

//
//  NSStringWithCMTimeSupport.m
//  
//
//  
//

#import "NSStringWithCMTimeSupport.h"

@implementation NSStringWithCMTimeSupport

+ (NSString *)timeStringFromSeconds:(int)totalSeconds
{
    int seconds = totalSeconds % 60;
    int minutes = (totalSeconds / 60) % 60;
    int hours = totalSeconds / 3600;
    
    if (hours > 0) {
        return [NSString stringWithFormat:@"%02d:%02d:%02d",hours, minutes, seconds];
    }
    return [NSString stringWithFormat:@"%02d:%02d", minutes, seconds];
}

+ ( CMTime )convertCMTimeFromTimeCode:(NSString *)timeCode
{
    // Parse timecode string into CMTime object. Assumes hh:mm:ss:ff
    int hours = 0;
    int minutes = 0;
    int seconds = 0;
    int thousandths = 0;
    if (timeCode.length) {
        NSArray *timePieces = [ timeCode componentsSeparatedByCharactersInSet:[ NSCharacterSet characterSetWithCharactersInString:@":."]];
        if (timePieces.count == 4) {
            hours = [timePieces[0] intValue];
            minutes = [timePieces[1] intValue];
            seconds = [timePieces[2] intValue];
            thousandths = [timePieces[3] intValue];
        }
        if (timePieces.count == 3) {
            hours = [timePieces[0] intValue];
            minutes = [timePieces[1] intValue];
            seconds = [timePieces[2] intValue];
        }
    }
    NSTimeInterval timeInSeconds = (hours * 60 * 60) + (minutes * 60) + seconds + ((thousandths) * 0.001); 
    return  CMTimeMakeWithSeconds(timeInSeconds, CC_NSEC_PER_SEC);
}

+ ( CMTime )convertCMTimeFromTimeCodeWithFrames:(NSString *)timeCode
{
    // Parse timecode string into CMTime object. Assumes hh:mm:ss:ff
    int hours = 0;
    int minutes = 0;
    int seconds = 0;
    int frames = 0;
    if (timeCode.length) {
        NSArray *timePieces = [ timeCode componentsSeparatedByCharactersInSet:[ NSCharacterSet characterSetWithCharactersInString:@":."]];
        if (timePieces.count == 4) {
            hours = [timePieces[0] intValue];
            minutes = [timePieces[1] intValue];
            seconds = [timePieces[2] intValue];
            frames = [timePieces[3] intValue];
        }
        if (timePieces.count == 3) {
            hours = [timePieces[0] intValue];
            minutes = [timePieces[1] intValue];
            seconds = [timePieces[2] intValue];
        }
    }
    NSTimeInterval timeInSeconds = (hours * 60 * 60) + (minutes * 60) + seconds + ((frames) * 1.0f/30.0f); // Assumes 30 fps video rate
    return  CMTimeMakeWithSeconds(timeInSeconds, CC_NSEC_PER_SEC);
}


//+ ( CMTime )convertCMTimeFromTimeCode:(NSString *)timeCode
//{
//    // Parse timecode string into CMTime object. Assumes hh:mm:ss:ff
//    int hours = [[timeCode substringWithRange:NSMakeRange(0, 2)] intValue];
//    int minutes = [[timeCode substringWithRange:NSMakeRange(3, 2)] intValue];
//    int seconds = [[timeCode substringWithRange:NSMakeRange(6, 2)] intValue];
//    int frames = [[timeCode substringWithRange:NSMakeRange(9, 2)] intValue];
//    
//    NSTimeInterval timeInSeconds = (hours * 60 * 60) + (minutes * 60) + seconds + ((frames) * 1.0f/30.0f); // Assumes 30 fps video rate
//    
//    return  CMTimeMakeWithSeconds(timeInSeconds, CC_NSEC_PER_SEC);
//}

@end

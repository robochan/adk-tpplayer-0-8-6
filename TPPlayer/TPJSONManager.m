//
//  TPJSONManager.m
//  TPPlayer
//
//
//  Copyright © 2013, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)
//

#import "TPJSONManager.h"
#import "TPContentList.h"
#import "TPFeed.h"
#import "TPDataHandler.h"
#import "TPMasterDataManager.h"

@implementation TPJSONManager

+( TPJSONManager *)sharedInstance;
{
    static dispatch_once_t once;
    static TPJSONManager *shared = nil;
    
    dispatch_once( &once, ^{
        shared = [ [ TPJSONManager alloc ]init ];
        [ shared setup ];
    } );
    return shared;
}

- ( void )setup;
{
    // might need this
}

- ( void )createObjectFromDataHandler:( TPDataHandler *)dataHandler;
{
    // we could store stuff in a dictionary and use selectors
    NSString *pathNameForObject = dataHandler.pathName;
    
    if( NSNotFound != [pathNameForObject rangeOfString:@"ContentList" ].location )
    {
        [ self createContentList:dataHandler ];
        return;
    }
    if( [pathNameForObject isEqualToString:@"ContentItem" ] )
    {
        [ self createContentItem:dataHandler ];
        return;
    }
    else{
        if (dataHandler.classToBuild) {
            [ self createCustomObject:dataHandler];
        }
    }

}

- ( void )createContentList:( TPDataHandler *)dataHandler;
{
    id jsonObjects  = dataHandler.jsonObjects;
    NSString *classToUse = dataHandler.contentItemSubclass ? dataHandler.contentItemSubclass : @"TPContentItem";
    
    if( jsonObjects )
    {
        NSArray *entries = jsonObjects[@"entries"];
        NSMutableArray *contentItems = [ NSMutableArray arrayWithCapacity:entries.count];
        
        for(NSDictionary *entryDictionary in entries ) 
        {
            TPContentItem * contentItem = [[ NSClassFromString(classToUse) alloc ]initContentItemFromJson:entryDictionary];
            [contentItems addObject:contentItem ];
        };
        TPFeed *aContentList = [ [ TPFeed alloc ]init ];
        aContentList.contentItems = contentItems;
        // apply values from dataHandle.jsonObjects
        dataHandler.returnedObject = aContentList;

        [ [ TPMasterDataManager sharedInstance ]objectCreated:dataHandler ];
    }
    else
    {
        [ [ TPMasterDataManager sharedInstance ]errorCreatingObject:dataHandler ];
    }
}

- ( void )createCustomObject:( TPDataHandler *)dataHandler;
{
    id jsonObjects  = dataHandler.jsonObjects;
    NSString *classToUse = dataHandler.classToBuild;
    
    if( jsonObjects )
    {
        id<TPExpiringObject> newObject = [[ NSClassFromString(classToUse) alloc ]initContentItemFromJson:jsonObjects];
        // apply values from dataHandle.jsonObjects
        dataHandler.returnedObject = newObject;
        [ [ TPMasterDataManager sharedInstance ]objectCreated:dataHandler ];
    }
    else
    {
        [ [ TPMasterDataManager sharedInstance ]errorCreatingObject:dataHandler ];
    }
}

// this is for the one-off contentItems we get from a releaseURL with the format = Preview
- ( void )createContentItem:( TPDataHandler *)dataHandler;
{
    id jsonObjects  = dataHandler.jsonObjects;
    
    if( jsonObjects )
    {
        TPContentItem *aContentItem = [ [ TPContentItem alloc ]initFromPreviewJson:jsonObjects andURLString:dataHandler.url ];

        // apply values from dataHandle.jsonObjects
        dataHandler.returnedObject = aContentItem;
        
        [ [ TPMasterDataManager sharedInstance ]objectCreated:dataHandler ];
    }
    else
    {
        [ [ TPMasterDataManager sharedInstance ]errorCreatingObject:dataHandler ];
    }
}



@end

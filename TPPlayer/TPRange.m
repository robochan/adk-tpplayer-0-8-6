//
//  TPRange.m
//
//  Copyright © 2013, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)
//


#import "TPRange.h"

@implementation TPRange

- ( id )copyWithZone:(NSZone *)zone;
{
    TPRange *newCopy = [ [ [ self class]alloc]init ];
    if (newCopy) {
        newCopy.startIndex = self.startIndex;
        newCopy.endIndex = self.endIndex;
        newCopy.itemCount = 0;
        newCopy.totalCount = 0;
    }
    return newCopy;
}

@end

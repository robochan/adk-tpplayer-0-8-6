//
//  FNContentList.m
//
//  Copyright © 2013, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)
//

#import "TPContentList.h"

@implementation TPContentList


- ( BOOL )isFresh;
{
    NSUInteger nowInt = [ [ NSDate date ] timeIntervalSinceReferenceDate ];
    return ( nowInt <= [ self.expiration timeIntervalSinceReferenceDate ] );
}

- ( id )initWithCoder:(NSCoder *)aDecoder
{
    self = [ super init ];
    self.contentItems = [ aDecoder decodeObjectForKey:@"contentItems" ];
    self.expiration = [ aDecoder decodeObjectForKey:@"expiration" ];
    return self;
}

- ( void )encodeWithCoder:(NSCoder *)aCoder;
{
    [ aCoder encodeObject:self.contentItems forKey:@"contentItems" ];
    [ aCoder encodeObject:self.expiration forKey:@"expiration" ];
}

@end

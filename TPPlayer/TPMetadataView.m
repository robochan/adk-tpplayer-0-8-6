//
//  TPMetadataView.m
//
//  Copyright © 2013, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)
//

#import "TPMetadataView.h"
#import "TPContentItem.h"

@implementation TPMetadataView

+ ( TPMetadataView *)metadataView;
{
//    NSString *resourceBundlePath = [ [ NSBundle mainBundle]pathForResource:@"TPPlayer" ofType:@"bundle"];
//    NSBundle *resourceBundle = [ NSBundle bundleWithPath:resourceBundlePath];
    NSString *nibName = NSStringFromClass([ self class]);
    NSBundle *resourceBundle = [ NSBundle mainBundle];

    UINib *nib = [ UINib nibWithNibName:nibName bundle:resourceBundle ];
    NSArray *nibObjects = [ nib instantiateWithOwner:nil options:nil ];
    NSAssert2( [ nibObjects count ] > 0 && [ [ nibObjects objectAtIndex:0 ] isKindOfClass:[ self class ] ], @"Nib %@ does not contain top level view of type %@", nibName, nibName );
    return [ nibObjects objectAtIndex:0 ];
}

- ( void )populateFromContentItem:( TPContentItem *)contentItem;
{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
    
    if (!contentItem.expirationDate ) {
        self.availabilityValue.text = NSLocalizedString(@"NA", @"If there is no content expiration date available");
    }
    else
    {
        self.availabilityValue.text = [dateFormatter stringFromDate:contentItem.expirationDate];
    }
    
    if (!contentItem.availableDate ) {
        self.availableFromValue.text = NSLocalizedString(@"NA", @"If there is no content available date available");
    }
    else
    {
        self.availableFromValue.text = [dateFormatter stringFromDate:contentItem.availableDate];
    }
    
    NSString *runtimeFormattedString = [self timeFormatted:(int)contentItem.videoAsset.duration ];
    self.runtimeValue.text = [ NSString stringWithFormat:@"%@",runtimeFormattedString];

    
    self.synopsis.text = contentItem.textDescription;
//    [self.synopsis setVerticalAlignmentTop];
    self.contentTitle.text = contentItem.title;

}



- (NSString *)timeFormatted:(int)totalSeconds
{
    
    int seconds = totalSeconds % 60;
    int minutes = (totalSeconds / 60) % 60;
    int hours = totalSeconds / 3600;
    
    return [NSString stringWithFormat:@"%02d:%02d:%02d",hours, minutes, seconds];
}

@end

//
//  TPMasterDataManager.h
//
//  Copyright © 2013, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)
//

#import <UIKit/UIKit.h>

@class TPDataHandler;
@class TPSmartImage;
@class TPVideoAsset;
@class TPSmilInfo;
@class TPBaseClip;
@class TPTrailer;

@protocol TPExpiringObject

-   (BOOL )isFresh;
-   (void )setExpiration:( NSDate *)expiration;

@end

@interface TPMasterDataManager : NSObject

// path where images are stored locally
@property(nonatomic, strong )NSURL *imagesDirectoryURL;
@property(nonatomic, strong)NSURL *trailersDirectoryURL;
@property(nonatomic, assign)CGFloat ignoreSecondsAtEnd;
@property(nonatomic, assign)CGFloat ignoreSecondsAtBeginning;


+ ( instancetype )sharedInstance;

- ( void )getContentItemForURL:( NSString *)url requester:( id )requester selector:(SEL)selector path:( NSString *)objectPath;
- ( void )getContentListForURL:( NSString *)url requester:( id )requester selector:(SEL)selector path:( NSString *)objectPath contentItemSubclass:( NSString *)contentItemSubclass;
- ( void )getImageForPath:( NSString *)url metadata:( id )metadata owner:( id )owner delegate:( id )delegate selector:( SEL ) selector;
- ( void )getGUIDStringForPath:( NSString *)url metadata:( NSNumber *)metadata delegate:( id )delegate selector:( SEL ) selector;
// classToBuild is the class for the object dataHandler will return - it must have a method for initContentItemFromJson:
// objectPath needs to be unique for each object
- ( void )getClass:( NSString *)classToBuild forURL:( NSString *)url requester:( id )requester selector:(SEL)selector path:( NSString *)objectPath;
// checks first for local copy
- (  id<TPExpiringObject> )readExpiringObject:(NSString *)classToRead requester:( id )requester selector:(SEL)selector path:( NSString *)objectPath url:( NSString *)url minutesToExpiration:( NSUInteger)minutesToExpiration;

- ( void )loadSmilDataForUrl:( NSString *)url metadata:( NSString *)metadata delegate:( id )delegate selector:( SEL ) selector;
- ( void )loadSimpleSmilDataForUrl:( NSString *)url metadata:( NSString *)metadata delegate:( id )delegate selector:( SEL ) selector;
//- ( void )getCCDataForPath:( NSString *)url metadata:( TPSmilInfo *)metadata delegate:( id )delegate selector:( SEL ) selector;
- ( void )getCCDataForPath:( NSString *)url metadata:( TPBaseClip *)metadata delegate:( id )delegate selector:( SEL ) selector;

- ( void )dataHandlerLoaded:( TPDataHandler *)dataHandler;
- ( void )objectCreated:( TPDataHandler *)dataHandler;
- ( void )errorCreatingObject:( TPDataHandler *)dataHandler;
- ( void )cancelRequests:( id )requestor;

- ( NSDate *)shortExpiration;

// checks for retina conditions and returns the appropriate role
- ( NSString *)adjustedRole:( NSString *)originalRole;
- ( TPSmartImage *)defaultForRole:( NSString *)role;

// local resume play support has been deprecated in favor of cross-device support

// trailer support
- ( void )getTrailersForURL:( NSString *)url requester:( id )requester selector:(SEL)selector path:( NSString *)objectPath;
- ( TPTrailer *)trailerFromArchive:( NSString *)path;
- ( void )archiveTrailer:(TPTrailer *)trailer; 

- ( void )emptyCurrentDirectories:( NSString *)previousDesignator;


- ( BOOL )prefersRetina;
- ( BOOL )hasWiFiAccess;

@end

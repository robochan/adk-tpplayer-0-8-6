    //
//  TPEndUserServicesAgent.m
//  TPPlayer
//
//  Copyright © 2013, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)
//
//  Generally we check to be sure resumeIsAvailable, but we don't rely on it.  We check here as well.

#import "TPEndUserServicesAgent.h"
#import "TPBookmarkManager.h"
#import "TPPlayerViewController.h"
#import "TPAppSpecificInformationManager.h"
#import "TPUserListItem.h"

NSString *const kTPResumeTimeInfoWasRefreshedNotification = @"TPResumeTimeInfoWasRefreshedNotification";

@interface TPEndUserServicesAgent()

@property(nonatomic, strong )NSTimer *userListRefreshTimer;
@property(nonatomic, assign) BOOL shouldRefreshForEveryItem;

@property(nonatomic, strong)NSMutableDictionary *bookmarkManagers;
@property(nonatomic, strong)NSString *activeAboutID;

@property( nonatomic, strong) NSString *loginID;
@property(nonatomic, strong)NSString *loginPassword;
@property(nonatomic, assign)NSTimeInterval userTokenDuration;
@property(nonatomic, strong)NSString *userID;
@property(nonatomic, strong, readwrite)NSString *userToken;
@property(nonatomic, strong, readwrite)NSString *clientID;

@property( nonatomic, strong) NSString *accountID;
@property( nonatomic, strong) NSString *userNamePrefix;
@property( nonatomic, strong) NSString *context;
@property( nonatomic, strong) NSString *adminToken;
@property(nonatomic, strong)NSString *userListTitle;


@property(nonatomic, strong)NSString *signInUrl;
@property(nonatomic, strong)NSString *signOutUrl;
@property(nonatomic, strong)NSString *basicCreateUserUrl;
@property(nonatomic, strong)NSString *adminSignInUrl;
@property(nonatomic, strong)NSString *adminSignOutUrl;
@property(nonatomic, strong)NSString *userListGetURL;
@property(nonatomic, strong)NSString *userListPostUrl;
@property(nonatomic, strong, readwrite)NSString *userListItemPostUrl;
@property(nonatomic, strong, readwrite)NSString *userListItemDeleteUrl;
@property(nonatomic, assign)BOOL appWasBackgrounded;

@end

@implementation TPEndUserServicesAgent

+( TPEndUserServicesAgent *)sharedInstance
{
    static dispatch_once_t once;
    static TPEndUserServicesAgent *shared = nil;
    
    dispatch_once( &once, ^{
        shared = [ [ TPEndUserServicesAgent alloc ]init ];
    } );
    return shared;
}

- ( void )setup;
{
    self.shouldStoreTimeValuesAsSeconds = [[ TPAppSpecificInformationManager sharedInstance]booleanForKey:@"TPShouldStoreTimeValuesAsSeconds" ];
    self.signInUrl =  @"https://euid.theplatform.com/idm/web/Authentication/signIn?schema=1.0&form=json";
    self.signOutUrl = @"https://euid.theplatform.com/idm/web/Authentication/signOut?schema=1.0&form=json";
    self.adminSignInUrl = @"https://identity.auth.theplatform.com/idm/web/Authentication/signIn?schema=1.0&form=json";
    self.adminSignOutUrl = @"https://identity.auth.theplatform.com/idm/web/Authentication/signOut?schema=1.0";
    self.basicCreateUserUrl =  @"https://euid.theplatform.com/idm/data/User";
    self.userListGetURL = @"http://data.userprofile.community.theplatform.com/userprofile/data/UserList?schema=1.2.0&form=json&fields=id,context,userId,items,items.id,items.userListId,items.aboutId,items.title,items.description,items.updated";
    self.userListPostUrl = @"http://data.userprofile.community.theplatform.com/userprofile/data/UserList?schema=1.2.0&form=cjson&method=post";
    self.userListItemPostUrl = @"http://data.userprofile.community.theplatform.com/userprofile/data/UserListItem?schema=1.2.0&form=cjson&method=post";
    self.userListItemDeleteUrl = @"http://data.userprofile.community.theplatform.com/userprofile/data/UserListItem?schema=1.2.0&form=cjson&method=delete";
    self.shouldRefreshForEveryItem = [[ TPAppSpecificInformationManager sharedInstance]booleanForKey:@"TPResumeShouldRefreshForEveryItem" ];
    self.bookmarkManagers = [@{} mutableCopy];
    [[ NSNotificationCenter defaultCenter]addObserver:self selector:@selector(appResignedActive:) name:UIApplicationWillResignActiveNotification object:nil ];
    [[ NSNotificationCenter defaultCenter]addObserver:self selector:@selector(appBecameActive:) name:UIApplicationDidBecomeActiveNotification object:nil ];
     [ self signInAsAdmin];
}

-( void )appResignedActive:( NSNotification *)notification;
{
    self.appWasBackgrounded = YES;
    if (self.userListRefreshTimer) {
        [self.userListRefreshTimer invalidate];
        self.userListRefreshTimer =  nil;
    }
}

- ( void )appBecameActive:( NSNotification *)notification;
{
    if (self.appWasBackgrounded) {
        [ self getUserList:nil ];
    }
    self.appWasBackgrounded = NO;
}


- ( BOOL )resumeIsAvailable;
{
    return self.genericUserList !=nil;
}

- ( void )getTimeToStart:( NSString *)aboutID interestedParty:( id<TPResumeTimeConsumer>)interestedParty metadata:( id )metadata; 
{
    if (interestedParty) {
        if (self.genericUserList ) {
            NSValue *timeToStartRangeValue = [self.genericUserList timeToStartRangeValue:aboutID ];
            NSDictionary *timeToStartValues = timeToStartRangeValue ? @{aboutID : timeToStartRangeValue } : nil;
            [ interestedParty resumeTimeInfoIsAvailable:timeToStartValues metadata:metadata ];
            return;
        }
     }
}

- ( void )registerUser:( NSString *)loginID password:( NSString *)loginPassword;
{
    if (loginID.length) {
        DLog(@"loginID %@ password = %@", loginID, loginPassword );
        self.loginID = loginID;
        self.loginPassword = loginPassword;
        NSString *stringForSignInURL = [ NSString stringWithFormat:@"%@&userName=%@/%@&password=%@", self.signInUrl, self.userNamePrefix, loginID, loginPassword ];
        NSURL *url = [ NSURL URLWithString:stringForSignInURL];
        NSURLSession *session = [ NSURLSession sharedSession];
        NSURLSessionDownloadTask *task = [ session downloadTaskWithURL:url completionHandler:^(NSURL *location, NSURLResponse *response, NSError *error) {
            if ([ self responseHasProblems:response error:error ]) {
                return;
            }
            NSData *downloadedData = [ NSData dataWithContentsOfURL:location];
            [ self signinComplete:downloadedData];
        }];
        [ task resume ];
    }
    else{
        DLog(@"user logged out");
        if (self.userToken) {
            [ self signOut ];
        }
    }
}

- ( BOOL )responseHasProblems:( NSURLResponse *)response error:( NSError *)error;
{
    if (error) {
        DLog(@"%@", error);
        return YES;
    }
    NSInteger status = [(NSHTTPURLResponse *)response statusCode];
    DLog(@"response status = %li", (long)status);
    if (status != 200) {
        DLog(@"%@", @"oh well");
        return YES;
    }
    return NO;
}


- ( void )signinComplete:( NSData *)response;
{
    NSError *anError = nil;
    NSDictionary *signInResults = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:&anError];
    DLog(@"signInResults = %@", signInResults);
    if (signInResults[@"isException"]) {
        if (self.adminToken) {
            [ self createUser ];
        }
        else
        {
            DLog(@"no adminToken - can't create user");
        }
    }
    else{
        self.userToken = signInResults[@"signInResponse"][@"token"];
        self.userTokenDuration = [signInResults[@"signInResponse"][@"duration"]doubleValue]/1000;
        self.userID = signInResults[@"signInResponse"][@"userId"];        
        [ self getUserList:nil ];
    }
}

- ( NSString *)clientID;
{
    if (!_clientID) {
        NSString *clientIDPrefix = [[ TPAppSpecificInformationManager sharedInstance]objectForKey:@"TPClientIDPrefix" ];
        NSUUID *deviceID = [ UIDevice currentDevice].identifierForVendor;
        _clientID = [[ NSString stringWithFormat:@"%@_%@", clientIDPrefix, [deviceID UUIDString]]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding ];
//        _clientID = [ NSString stringWithFormat:@"%@_%@", clientIDPrefix, [deviceID UUIDString]];
//        _clientID = @"thisIsATest";
    }
    return _clientID;
}

//  DO THIS FIRST - preferably when application launches
- ( void )signInAsAdmin;
{
    NSString *adminUserName = [ [ TPAppSpecificInformationManager sharedInstance]objectForKey:@"TPmpxAdminUserName" ];
    NSString *adminPassword = [ [ TPAppSpecificInformationManager sharedInstance]objectForKey:@"TPmpxAdminPassword" ];
    
    if (adminUserName && adminPassword ) {
        DLog(@"adminUserName %@", adminUserName);
        NSString *stringForSignInURL = [ NSString stringWithFormat:@"%@&userName=%@&password=%@", self.adminSignInUrl, adminUserName, adminPassword ];
        NSURL *url = [ NSURL URLWithString:stringForSignInURL];
        NSURLSession *session = [ NSURLSession sharedSession];
        NSURLSessionDownloadTask *task = [ session downloadTaskWithURL:url completionHandler:^(NSURL *location, NSURLResponse *response, NSError *error) {
            if ([ self responseHasProblems:response error:error ]) {
                return;
            }
            NSData *downloadedData = [ NSData dataWithContentsOfURL:location];
            [ self adminSignInComplete:downloadedData];
        }];
        [ task resume ];
    }
}

- ( void )adminSignInComplete:( NSData *)response;
{
    NSError *anError = nil;
    NSDictionary *signInResults = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:&anError];
    DLog(@"signInResults = %@", signInResults);
    if (signInResults[@"isException"]) {
        DLog(@"Can't sign in as admin");
    }
    else{
        self.adminToken = signInResults[@"signInResponse"][@"token"];
    }
}

- ( void )signOut;
{
    NSString *stringForSignInURL = [ NSString stringWithFormat:@"%@&_token=%@", self.signOutUrl, self.userToken ];
    NSURL *url = [ NSURL URLWithString:stringForSignInURL];
    NSURLSession *session = [ NSURLSession sharedSession];
    NSURLSessionDownloadTask *task = [ session downloadTaskWithURL:url completionHandler:^(NSURL *location, NSURLResponse *response, NSError *error) {
        if (error) {
            DLog(@"%@", error);
            return;
        }
        NSInteger status = [(NSHTTPURLResponse *)response statusCode];
        DLog(@"response status = %li", (long)status);
        if (status != 200) {
            DLog(@"%@", @"oh well");
            return;
        }
        NSData *downloadedData = [ NSData dataWithContentsOfURL:location];
        [ self signOutComplete:downloadedData];
    }];
    [ task resume ];

}

- ( void )signOutComplete:( NSData *)response;
{
    NSError *anError = nil;
    NSDictionary *signOutResults = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:&anError];
    DLog(@"signOutResults = %@", signOutResults);
    if (signOutResults[@"isException"]) {
        DLog(@"%@", anError);
    }
    else{
        DLog(@"signOutResults has no exceptions -- success %@", signOutResults);
        self.userToken = nil;
        self.userTokenDuration = 0;
        if (self.userListRefreshTimer) {
            [self.userListRefreshTimer invalidate];
            self.userListRefreshTimer =  nil;
        }
        self.clientID = nil;
        self.loginID = nil;

    }
}
- ( void )createUser;
{
    NSString *	baseCreateUserUrl =  [ NSString stringWithFormat:@"%@/%@?schema=1.0&method=post&form=cjson", self.basicCreateUserUrl, self.userNamePrefix ];
    NSString *createUrlString = [ NSString stringWithFormat:@"%@&_userName=%@&_password=%@&token=%@&account=%@", baseCreateUserUrl, self.loginID, self.loginPassword, self.adminToken, self.accountID ];
    DLog(@"createURLString = %@", createUrlString);
    NSURL *createURL = [ NSURL URLWithString:createUrlString];
    NSURLSession *session = [ NSURLSession sharedSession];
    NSURLSessionDownloadTask *task = [ session downloadTaskWithURL:createURL completionHandler:^(NSURL *location, NSURLResponse *response, NSError *error) {
        if (error) {
            DLog(@"%@", error);
            return;
        }
        NSInteger status = [(NSHTTPURLResponse *)response statusCode];
        DLog(@"response status = %li", (long)status);
        // here a 201 status is a good thing
        if (status > 204) {
            DLog(@"%@", @"oh well");
            return;
        }
        NSData *dataFromDownload = [ NSData dataWithContentsOfURL:location ];
        [ self creationComplete:dataFromDownload];
    }];
    [ task resume ];
    
}

- ( void )creationComplete:( NSData *)response;
{
    NSError *anError = nil;
    NSDictionary *creationResults = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:&anError];
    DLog(@"creationResults = %@", creationResults);
    if (creationResults[@"id"]) {
        [ self registerUser:self.loginID password:self.loginPassword ];
    }
    else{
        DLog(@"creation failed");
    }
}

- ( void )getUserList:( NSTimer *)timer;
{
    if (self.userToken) {
        DLog(@"getting user list");
        NSString *userListURL = [ NSString stringWithFormat:@"%@&byContext=%@&byUserId=%@&account=%@&token=%@", self.userListGetURL, self.context, self.userID, self.accountID, self.userToken ];
        DLog(@"getUserList url = %@", userListURL);
        NSURL *url = [ NSURL URLWithString:userListURL];
        NSURLSession *session = [ NSURLSession sharedSession];
        NSURLSessionDownloadTask *task = [ session downloadTaskWithURL:url completionHandler:^(NSURL *location, NSURLResponse *response, NSError *error) {
            if ([ self responseHasProblems:response error:error ]) {
                return;
            }
            NSData *downloadedData = [ NSData dataWithContentsOfURL:location];
            [ self getUserListComplete:downloadedData];
        }];
        [ task resume ];
    }
    else{
        DLog(@"no user token available - can't get userList");
    }
}

- ( void )getUserListComplete:( NSData *)response;
{
    NSError *anError = nil;
    
    NSDictionary *getUserListResults = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:&anError];
    DLog(@"getUserListResults = %@", getUserListResults);
    if (getUserListResults[@"isException"]) {
        if (self.userToken) {
            [ self createUserList ];
        }
        else
        {
            DLog(@"no token - can't create userList");
        }
    }
    else{
        NSArray *userListEntries = getUserListResults[@"entries"];
        if (userListEntries.count) {
            [ self resetRefreshTimer ];
            self.genericUserList = [[ TPUserList alloc]initFromDictionary:[ getUserListResults[@"entries"] firstObject]];
            [ self associatePendingBookmark ];
        }
        else{
            [ self createUserList];
        }
    }
}

- ( void )associatePendingBookmark;
{
    TPBookmarkManager *activeManager = self.bookmarkManagers[self.activeAboutID];
    DLog(@"activeManager bookmark  = %@", activeManager.bookmark);
    
    if (activeManager && ! activeManager.bookmark) {
        [ activeManager makeBookmarkFromUserList:self.genericUserList ]; 
        DLog(@"activeManager has no bookmark - making one from new list");
    }
    [[ NSNotificationCenter defaultCenter]postNotificationName:kTPResumeTimeInfoWasRefreshedNotification object:nil ];
}


- ( void )resetRefreshTimer;
{
    NSNumber *appSpecificRefreshTime = [[ TPAppSpecificInformationManager sharedInstance]objectForKey:@"TPResumeUserListRefreshMinutes" ];
    if (appSpecificRefreshTime) {
        if (self.userListRefreshTimer) {
            [ self.userListRefreshTimer invalidate];
            self.userListRefreshTimer = nil;
        }
        CGFloat refreshTime = [appSpecificRefreshTime integerValue]  * 60.0;
        if (refreshTime > 0) {
            self.userListRefreshTimer = [ NSTimer timerWithTimeInterval:refreshTime target:self selector:@selector(getUserList:) userInfo:nil repeats:YES ];
            [[ NSRunLoop mainRunLoop]addTimer:self.userListRefreshTimer forMode:NSDefaultRunLoopMode ];
        }
    }
}

- ( void)createUserList;
{
    if (self.userToken) {
        NSString *userListURL = [ NSString stringWithFormat:@"%@&_title=%@&_context=%@&account=%@&token=%@", self.userListPostUrl, self.userListTitle, self.context, self.accountID, self.userToken ];
        DLog(@"userListURL url = %@", userListURL);
        NSURL *url = [ NSURL URLWithString:userListURL];
        NSURLSession *session = [ NSURLSession sharedSession];
        NSURLSessionDownloadTask *task = [ session downloadTaskWithURL:url completionHandler:^(NSURL *location, NSURLResponse *response, NSError *error) {
            if (error) {
                DLog(@"%@", error);
                return;
            }
            NSInteger status = [(NSHTTPURLResponse *)response statusCode];
            DLog(@"response status = %li", (long)status);
            // here a 201 status is a good thing
            if (status > 204) {
                DLog(@"%@", @"oh well");
                return;
            }
            NSData *downloadedData = [ NSData dataWithContentsOfURL:location];
            [ self createUserListComplete:downloadedData];
        }];
        [ task resume ];
    }
    else{
        DLog(@"no user token available - can't create userList");
    }
    
}

- ( void )createUserListComplete:( NSData *)response;
{
    NSError *anError = nil;
    
    NSDictionary *createUserListResults = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:&anError];
    DLog(@"createUserListResults = %@", createUserListResults);
    if (createUserListResults[@"isException"]) {
        DLog(@"can't create userList");
    }
    else{
        NSArray *results = createUserListResults[@"entries"];
        if (results.count) {
            self.genericUserList = [[ TPUserList alloc]initFromDictionary:[ createUserListResults[@"entries"] firstObject]];
            [ self resetRefreshTimer ];
            [ self associatePendingBookmark ];
        }
        else
        {
            [ self getUserList:nil ];
        }
    }
}

/*

- ( void )getUserListForAboutID:( NSString *)aboutID;
{
    if (self.userToken) {
        NSString *userListURL = [ NSString stringWithFormat:@"%@&byAboutId=%@&byContext=%@&byUserId=%@&account=%@&token=%@", self.userListGetURL, aboutID, self.context, self.userID, self.accountID, self.userToken ];
        DLog(@"userListURL url = %@", userListURL);
        NSURL *url = [ NSURL URLWithString:userListURL];
        NSURLSession *session = [ NSURLSession sharedSession];
        NSURLSessionDownloadTask *task = [ session downloadTaskWithURL:url completionHandler:^(NSURL *location, NSURLResponse *response, NSError *error) {
            if ([ self responseHasProblems:response error:error ]) {
                return;
            }
            NSData *downloadedData = [ NSData dataWithContentsOfURL:location];
            [ self getUserListForAboutIDComplete:downloadedData];
        }];
        [ task resume ];
    }
    else{
        DLog(@"no user token available - can't get userList");
    }
}

- ( void )getUserListForAboutIDComplete:( NSData *)response;
{
    NSError *anError = nil;
    TPBookmarkManager *activeManager = self.bookmarkManagers[self.activeAboutID];

    NSDictionary *getUserListResults = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:&anError];
    DLog(@"getUserListResults = %@", getUserListResults);
    if (getUserListResults[@"isException"]) {
        DLog(@"no userList");
        [ activeManager makeBookmarkFromUserList:self.genericUserList isGeneric:YES ];
    }
    else{
        NSArray *userListEntries = getUserListResults[@"entries"];
        if ( userListEntries.count) {
            TPUserList *specificUserList = [[ TPUserList alloc]initFromDictionary:[ getUserListResults[@"entries"] firstObject]];
            [ activeManager makeBookmarkFromUserList:specificUserList isGeneric:NO ];
        }
        else
        {
            [ activeManager makeBookmarkFromUserList:self.genericUserList isGeneric:YES ];
        }
    }
//    [ self userListEntriesFetchComplete ];
}
*/


- ( void )contentChanged:( NSString *)aboutID requester:(id<TPBookmarkConsumer>)requester;
{
    DLog(@"aboutID %@", aboutID);
    if (self.activeAboutID) {
        TPBookmarkManager *currentlyActiveBookmarkManager = self.bookmarkManagers[self.activeAboutID];
        if (! currentlyActiveBookmarkManager.isProcessingListItem) {
            [ self.bookmarkManagers removeObjectForKey:self.activeAboutID];
        }
    }
    if (aboutID) {
        self.activeAboutID = aboutID;
        if (self.userToken) {
            TPBookmarkManager *bookmarkManager = [ self makeBookMarkManager:aboutID ];
            bookmarkManager.requestor = requester;

            if (self.shouldRefreshForEveryItem || ! self.genericUserList) {
                [ self getUserList:nil ];  // this sends a message to the bookmarkManager when it finishes
            }
            else{
                [ bookmarkManager makeBookmarkFromUserList:self.genericUserList ];
            }
        }
    }
}

- ( TPBookmarkManager *)makeBookMarkManager:( NSString *)aboutID;
{
    TPBookmarkManager *bookmarkManager = [ [ TPBookmarkManager alloc]init ];
    self.bookmarkManagers[aboutID] = bookmarkManager;
    bookmarkManager.aboutID = aboutID;
    bookmarkManager.userID = self.userID;
    bookmarkManager.userToken = self.userToken;
    return bookmarkManager;
}

- ( void )movieStopped:(NSString *)aboutID playheadPosition:( NSTimeInterval )playheadPosition duration:(NSTimeInterval )duration;
{
    TPBookmarkManager *bookmarkManager = self.bookmarkManagers[aboutID];
    if (self.userToken && !bookmarkManager) {
        bookmarkManager = [ self makeBookMarkManager:aboutID];
        if (self.genericUserList) {
            [ bookmarkManager makeBookmarkFromUserList:self.genericUserList ];
        }
    }
    [ bookmarkManager movieStopped:aboutID playheadPosition:playheadPosition duration:duration ];
}

- ( void )userOptedToPlayFromBeginning:( TPBookmark *)bookmark;
{
    // check self.userToken to see if the user is logged out
    DLog(@"aboutID %@", bookmark.aboutID);
    if (bookmark) {
        NSString *aboutID = bookmark.aboutID;
        bookmark.playheadPosition = 0.0;
        TPBookmarkManager *bookmarkManager = self.bookmarkManagers[aboutID];
        if ( bookmarkManager) {
            [ bookmarkManager removeBookmark ];
        }
        else
        {
            DLog(@"THIS SHOULD NEVER BE POSSIBLE");
        }
    }
}

- ( void )unlockConcurrency:( TPConcurrencyInfo *)concurrencyInfo;
{
    if (concurrencyInfo) 
    {
        concurrencyInfo.concurrencyObserver = nil;
       NSString *unlockURLString = [ concurrencyInfo unlockURL];
        NSString *unlockURL = [ NSString stringWithFormat:@"%@?schema=1.0&form=json&_clientId=%@&_id=%@&_sequenceToken=%@&_encryptedLock=%@", unlockURLString, self.clientID, concurrencyInfo.lockID, concurrencyInfo.lockSequenceToken, concurrencyInfo.lockContent ];
        DLog(@"unlock url = %@", unlockURL);
        NSURL *url = [ NSURL URLWithString:unlockURL];
        NSURLSession *session = [ NSURLSession sharedSession];
        NSURLSessionDownloadTask *task = [ session downloadTaskWithURL:url completionHandler:^(NSURL *location, NSURLResponse *response, NSError *error) {
            if ([ self responseHasProblems:response error:error ]) {
                return;
            }
            NSData *downloadedData = [ NSData dataWithContentsOfURL:location];
            [ self concurrencyUnlockComplete:downloadedData];
        }];
        [ task resume ];
    }
    else{
        DLog(@"no user token available - can't get userList");
    }
}

- ( void )concurrencyUnlockComplete:( NSData *)response;
{
    NSError *anError = nil;    
    NSDictionary *concurrencyUnlockResults = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:&anError];
    DLog(@"concurrencyUnlockResults = %@", concurrencyUnlockResults);
    if (concurrencyUnlockResults[@"isException"]) {
        DLog(@"no concurrencyUnlockResults");
    }
    else{
        DLog(@"unlock appears to have worked");
    }
    
}


- ( void )updateConcurrency:( TPConcurrencyInfo *)concurrencyInfo;
{
    if (concurrencyInfo) 
    {
        NSString *updateURLString = [ concurrencyInfo updateURL];
        NSCharacterSet *alphaCharacterSet = [ NSCharacterSet alphanumericCharacterSet];
        NSString *encodedLockSequenceToken = [concurrencyInfo.lockSequenceToken stringByAddingPercentEncodingWithAllowedCharacters:alphaCharacterSet];
        NSString *encodedLockContent = [ concurrencyInfo.lockContent stringByAddingPercentEncodingWithAllowedCharacters:alphaCharacterSet ];
        NSString *updateURL = [ NSString stringWithFormat:@"%@?schema=1.0&form=json&_clientId=%@&_id=%@&_sequenceToken=%@&_encryptedLock=%@", updateURLString, self.clientID, concurrencyInfo.lockID, encodedLockSequenceToken, encodedLockContent];
//        NSString *encodedUpdateURL = [ updateURL stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding ];
        DLog(@"update url = %@", updateURL);
//        DLog(@"encoded update url = %@", encodedUpdateURL);
        NSURL *url = [ NSURL URLWithString:updateURL];
        NSURLSession *session = [ NSURLSession sharedSession];
        NSURLSessionDownloadTask *task = [ session downloadTaskWithURL:url completionHandler:^(NSURL *location, NSURLResponse *response, NSError *error) {
            if ([ self responseHasProblems:response error:error ]) {
                return;
            }
            NSData *downloadedData = [ NSData dataWithContentsOfURL:location];
            [ self concurrencyUpdateComplete:downloadedData concurrencyInfo:concurrencyInfo];
        }];
        [ task resume ];
    }
    else{
        DLog(@"no user token available - can't updateConcurrency");
    }
}

- ( void )concurrencyUpdateComplete:( NSData *)response concurrencyInfo:( TPConcurrencyInfo *)concurrencyInfo;
{
    NSError *anError = nil;    
    NSDictionary *concurrencyUpdateResults = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:&anError];
    DLog(@"concurrencyUpdateResults = %@", concurrencyUpdateResults);
    DLog(@"concurrencyInfo = %@", concurrencyInfo);
    // shouldForceConcurrencyException is just for testing -- 
    // change it to YES to force the exception behavior
    BOOL shouldForceConcurrencyException = NO;   
    if ( shouldForceConcurrencyException || concurrencyUpdateResults[@"isException"]) {
        DLog(@"isException for concurrencyUpdateResults");
        dispatch_async(dispatch_get_main_queue(), ^{
            [concurrencyInfo.concurrencyObserver receivedConcurrencyConflict];
        });
    }
    else{
        concurrencyInfo.lockSequenceToken = concurrencyUpdateResults[@"updateResponse"][@"sequenceToken"];
        concurrencyInfo.lockContent = concurrencyUpdateResults[@"updateResponse"][@"encryptedLock"];
        concurrencyInfo.lockID = concurrencyUpdateResults[@"updateResponse"][@"id"];
    }
}


- ( void )bookMarkDeleted:( TPBookmarkManager *)bookmarkManager;
{
    [ self.bookmarkManagers removeObjectForKey:bookmarkManager.aboutID];
}

- ( void )dealloc
{
    // log out the amin user
    [self performSelectorOnMainThread:@selector(removeNotificationCenterObserver) withObject:self waitUntilDone:YES];

    DLog(@"signing out admin user");
    NSString *stringForSignInURL = [ NSString stringWithFormat:@"%@&_token=%@", self.adminSignOutUrl, self.adminToken ];
    NSURL *url = [ NSURL URLWithString:stringForSignInURL];
    NSURLSession *session = [ NSURLSession sharedSession];
    // we just get an empty response here - so we don't really care about the completion handler
    NSURLSessionDownloadTask *task = [ session downloadTaskWithURL:url completionHandler:nil];
    [ task resume ];
}


- (void)removeNotificationCenterObserver
{
    NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
    [notificationCenter removeObserver:self];
}



- ( NSString *)userNamePrefix;
{
    if (! _userNamePrefix) {
        _userNamePrefix = [[ TPAppSpecificInformationManager sharedInstance]objectForKey:@"TPDirectory" ];
    }
    return _userNamePrefix;
}

- ( NSString *)accountID;
{
    if (! _accountID) {
        _accountID = [[ TPAppSpecificInformationManager sharedInstance]objectForKey:@"TPmpxAccountID" ];
    }
    return _accountID;
}


- ( NSString *)context;
{
    if (! _context) {
        _context =[[[ TPAppSpecificInformationManager sharedInstance]objectForKey:@"TPResumeContext" ]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding ];
//        _context = @"public";
    }
    return _context;
}

- ( NSString *)userListTitle;
{
    if (! _userListTitle) {
        _userListTitle = [[[ TPAppSpecificInformationManager sharedInstance]objectForKey:@"TPmpxUserListTitle" ]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding ];
    }
    return _userListTitle;
}

@end

//
//  TPVideoAsset.h
//
//  Copyright © 2013, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)
//
//

#import <UIKit/UIKit.h>

@class TPClosedCaption;

@interface TPVideoAsset : NSObject


@property(nonatomic, strong )NSString *videoURLString;
@property(nonatomic, strong)TPClosedCaption *closedCaptionInfo; // not currently archiving this
@property(nonatomic, assign)BOOL needsAuthorization;  // need to figure out how to tell if this is true
@property(nonatomic, strong)NSString *guid;  // it is convenient to store this here as well
@property(nonatomic, strong)NSString *adManagerId;
@property(nonatomic, assign)BOOL inlineClosedCaptionsPresent; // if this is true, player will not use sidecar closed captions.

// tP properties with strict naming from JSON
@property(nonatomic, assign)NSInteger   bitrate;
@property(nonatomic, assign)CGFloat     duration;
@property(nonatomic, assign)NSInteger   filesize;
@property(nonatomic, assign)NSInteger   height;
@property(nonatomic, assign)NSInteger   width;



- ( id )initFromPreviewJson:( NSDictionary *)jsonObjects andBasicURLString:( NSString *)basicURLString;
- ( id )initWithJsonDictionary:(NSDictionary *)videoDictionary;

@end

//
//  TPClip.m
//
//  Copyright © 2013, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)
//

#import "TPClip.h"

@implementation TPClip

- ( id )initWithCoder:(NSCoder *)aDecoder
{
    self = [ super init ];
    self.duration = [ aDecoder decodeIntegerForKey:@"duration" ];
    self.title = [ aDecoder decodeObjectForKey:@"title" ];
    self.clipBegin = [ aDecoder decodeIntegerForKey:@"clipBegin" ];
    self.clipEnd = [ aDecoder decodeIntegerForKey:@"clipEnd" ];
    self.baseClip = [ aDecoder decodeObjectForKey:@"baseClip"]; // TODO - Discuss with tP team
    self.params = [ aDecoder decodeObjectForKey:@"params"];

    return self;
}

- ( void )encodeWithCoder:(NSCoder *)aCoder;
{
    [ aCoder encodeInteger:self.duration forKey:@"duration" ];
    [ aCoder encodeObject:self.title forKey:@"title" ];
    [ aCoder encodeInteger:self.clipBegin forKey:@"clipBegin" ];
    [ aCoder encodeInteger:self.clipEnd forKey:@"clipEnd" ];
    [ aCoder encodeObject:self.baseClip forKey:@"baseClip" ];
    [ aCoder encodeObject:self.params forKey:@"params" ];


}

@end

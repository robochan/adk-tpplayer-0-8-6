//
//  TPContentItem.h
//
//  Copyright © 2013, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)
//


#import <Foundation/Foundation.h>
#import "TPVideoAsset.h"
#import "TPImagesOwner.h"

// guid is a REQUIRED FIELD
@interface TPContentItem : TPImagesOwner

@property(nonatomic, strong)TPVideoAsset *videoAsset; // this is currently defined as the first object in videoAssets so it is not archived
@property(nonatomic, strong)NSString *synopsis;
@property(nonatomic, strong)NSString *adManagerId;
@property(nonatomic, strong)NSString *roleForDefaultThumbnail;
@property(nonatomic, strong)NSString *roleForDefaultPosterImage;

// Strict tP properties using nomenclature from feed responses in JSON
@property(nonatomic, strong)NSString    *textDescription;
@property(nonatomic, strong)NSString    *identifier;    // Exception - cannot us "id" iOS reserved word
@property(nonatomic, strong)NSString    *idURL;         // The full id URL from the feed "id" key
@property(nonatomic, strong)NSString    *guid;
@property(nonatomic, strong)NSDate      *availableDate; // as an NSDate (from ms since 1970)
@property(nonatomic, strong)NSArray     *categories;    // Array of TPCategory objects
@property(nonatomic, strong)NSArray     *videoAssets;   // Array of TPVideoAsset objects
@property(nonatomic, strong)NSString    *copyright;
@property(nonatomic, strong)NSString    *copyrightUrl;
@property(nonatomic, strong)NSArray     *countries;     // Array of strings
@property(nonatomic, strong)NSArray     *credits;       // Array of TPCredit objects
@property(nonatomic, strong)NSArray     *excludeCountries;  // Array of strings
@property(nonatomic, strong)NSDate      *expirationDate;// as an NSDate (from ms since 1970)
@property(nonatomic, strong)NSArray     *keywords;      // Array of strings from comma delimited string
@property(nonatomic, strong)NSArray     *ratings;       // Array of dictionaries
@property(nonatomic, strong)NSString    *text;          // TODO - no model input, verify content format
@property(nonatomic, strong)NSDate      *pubDate;       // as an NSDate (from ms since 1970)
@property(nonatomic, assign)BOOL      requiresAuth;       // true if this content requires authorization
@property(nonatomic, strong)NSString *networkID;  // if this is restricted content

- ( id )initFromPreviewJson:( NSDictionary *)jsonObjects andURLString:( NSString *)basicURLString;
- ( id )initContentItemFromJson:( NSDictionary *)jsonObjects;

// override this to parse out the images differently
- ( NSDictionary *)imageDictionaryFromJson:( NSDictionary *)jsonObjects;

// subclasses can override to implement a scheme for assigning roles to the images
- ( void )adjustRole:( TPSmartImage *)tmpSmartImage fromJsonDictionary:( NSDictionary *)imageDictionary;

- ( NSString *)aboutID;



@end

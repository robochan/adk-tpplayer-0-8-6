//
//  TPAdManager.m
//
//  Copyright © 2013, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)
//

#import "TPAdManager.h"
#import "TPPlayerControls.h"


@implementation TPAdManager


- (void)removeNotificationCenterObserver
{
    NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
    [notificationCenter removeObserver:self];
}

- (void)dealloc
{
    [self performSelectorOnMainThread:@selector(removeNotificationCenterObserver) withObject:self waitUntilDone:YES];
}

- (void)submitAdRequest:( NSString *)adManagerId videoManager: (UIViewController<TPAdManagerOwner> *)videoManager;
{
    self.videoManager = videoManager;
    // subclasses override & can call super
    [[ NSNotificationCenter defaultCenter]addObserver:self selector:@selector(playerStateChanged:) name:kTPPlayerDidChangeRateNotification object:nil ];
}

- ( void )playerStateChanged:( NSNotification *)notification;
{
    // subclasses override
}

- ( void )userScrubbedForward:( NSTimeInterval)endTime playbackTimeAtStart:( NSTimeInterval)playbackTimeAtStart;
{
    // subclasses override
}

- ( void )onSlotStarted:( NSNotification *)notification;
{
    DLog( );
    [ self.videoManager slotStarted:[ notification userInfo] adPosition:self.adTypePlaying ];
}

- ( void )onSlotEnded:( NSNotification *)notification;
{
    [ self.videoManager slotEnded:[ notification userInfo] adPosition:self.adTypePlaying ];
}

- ( void )onAdStarted:( NSNotification *)notification;
{
    [ self.videoManager adStarted:[ notification userInfo] adPosition:self.adTypePlaying ];
}

- ( void )onAdEnded:( NSNotification *)notification;
{
    [ self.videoManager adEnded:[ notification userInfo] adPosition:self.adTypePlaying];
}


- ( void )onAdError:( NSNotification *)notification;
{
    DLog( );
    // this might come in handy - explore it.
}



- ( void )heartbeatStarted;
{
    DLog(@"heartbeatStartedr");
}

- ( void )heartbeatStopped;
{
    DLog(@"heartbeatStopped");
}

- ( void )pulseHeartbeat:( CMTime)currentPlayTime;
{
    DLog(@"pulseHeartbeat");
}


- ( void )cleanup;
{   
    [[NSNotificationCenter defaultCenter] removeObserver:self ];
    [ self.videoManager cleanup ];

    // subclasses override
}


- ( void )displayBaseChanged;
{
    // subclasses override
}
- ( void )closedCaptionsAreNowShowing:( BOOL )closedCaptionsAreShowing;
{
    // subclasses override
}

- ( void )playerDidReachEnd;
{
    // subclasses override
}

@end

//
//  TPContentList.h
//
//  Copyright © 2013, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)
//

#import <Foundation/Foundation.h>
#import "TPContentItem.h"

@interface TPContentList : NSObject

@property(nonatomic, strong )NSArray *contentItems; 
@property(nonatomic, retain )NSDate *expiration; 

- ( BOOL )isFresh;

@end

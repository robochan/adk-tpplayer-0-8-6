//
//  FNImageLoading.h
//
//
//  Copyright © 2013, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)
//
//  Compatibility: iOS 5.1+
//
//  SUMMARY
//
//
//  DOCUMENTATION Notes (Remove when docs are complete)
//
//
//  DEVELOPMENT Notes
//

#import <Foundation/Foundation.h>

@protocol TPImageLoading <NSObject>

- ( NSString * )pathForImage;
- ( void )setNewImage:( UIImage *)newImage;

@end

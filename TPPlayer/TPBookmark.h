//
//  TPBookmark.h
//  TPPlayer
//
//  Copyright © 2013, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)
//

#import <Foundation/Foundation.h>
#import "TPUserListItem.h"

@interface TPBookmark : TPUserListItem

@property(nonatomic, strong)NSString *platformUserListID;
@property(nonatomic, assign)NSInteger playheadPosition;
@property(nonatomic, assign)NSInteger duration;

// returns now if this is self.updated is nil
- ( NSDate *)dateUpdated;

@end

//
//  TPPlayerView.h
//  tryAVPlayer
//
//  Copyright © 2014, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)

//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface TPPlayerView : UIView

@property (nonatomic, strong) AVPlayer *player;
@property(copy) NSString *videoFillMode;
@property(readonly) CGSize playerNaturalSize;

@end

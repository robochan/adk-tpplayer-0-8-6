//
//  TPRange.h
//
//  Copyright © 2013, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)
//


#import <Foundation/Foundation.h>

@interface TPRange : NSObject<NSCopying>

@property(nonatomic, assign)NSUInteger startIndex;  // populate prior to request
@property(nonatomic, assign)NSUInteger endIndex;  // populate prior to request
@property(nonatomic, assign)NSUInteger itemCount;  // TPFeedRequest populates on load
@property(nonatomic, assign)NSUInteger totalCount;  // TPFeedRequest populates on load

@end

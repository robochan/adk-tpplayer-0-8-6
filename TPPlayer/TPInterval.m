//
//  TPInterval.m
//
//  Copyright © 2013, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)
//

#import "TPInterval.h"
#import "TPIntervalManager.h"

@interface TPInterval()
@property(nonatomic, assign)CGFloat timeRemaining;
@property(nonatomic, strong)NSTimer *intervalTimer;
@end

@implementation TPInterval

@synthesize intervalLength = _intervalLength;
@synthesize endPoint = _endPoint;
@synthesize hasEndpoint = _hasEndpoint;
@synthesize manager = _manager;
@synthesize timeRemaining = _timeRemaining;
@synthesize accumulatedTime = _accumulatedTime;
@synthesize isComplete = _isComplete;

- ( id )initWithLength:( CGFloat)anInterval endPoint:( CGFloat)anEndPoint;
{
    if (self = [ super init]) {
        self.intervalLength = anInterval;
        self.endPoint = anEndPoint;
    }
    return self;
}

- ( void )start;  // could be restart
{
    if (self.timeRemaining > 0) // restart
    {
            self.intervalTimer = [ NSTimer scheduledTimerWithTimeInterval:self.timeRemaining target:self selector:@selector(partialTimerFired:) userInfo:nil repeats:NO];
    }
    else
    {
           self.intervalTimer = [ NSTimer scheduledTimerWithTimeInterval:self.intervalLength target:self selector:@selector(timeIntervalElapsed:) userInfo:nil repeats:YES]; 
    }
}

- ( void )timeIntervalElapsed:( NSTimer *)aTimer;
{
    // the timer will keep repeating unless we stop it
    self.accumulatedTime += self.intervalLength;
    DLog(@"accumulatedTime = %g", self.accumulatedTime);
    if ( self.hasEndpoint && self.accumulatedTime >= self.endPoint) {
        [ self stopTiming];
        self.isComplete = YES;
        DLog(@"interval is complete %@", self);
    }
    [ self.manager intervalFired:self ];
}

- ( void )partialTimerFired:( NSTimer *)aTimer;
{
    DLog( );
    [ aTimer invalidate];  // no matter what, we are done with this timer
    self.timeRemaining = 0;
    self.accumulatedTime += self.intervalLength;
    if ( self.hasEndpoint && self.accumulatedTime >= self.endPoint) {
        self.intervalTimer = nil;
        self.isComplete = YES;
        DLog(@"interval is complete %@", self);
    }
    else 
    {
        // not finished, need to start up the repeating timer
        self.intervalTimer = [ NSTimer scheduledTimerWithTimeInterval:self.intervalLength target:self selector:@selector(timeIntervalElapsed:) userInfo:nil repeats:YES]; 
    }
    [ self.manager intervalFired:self ];
}

- ( void )pause;
{
    self.timeRemaining = [self.intervalTimer.fireDate timeIntervalSinceNow ];
    DLog(@"timeRemaining = %g fireDate = %@ now = %@", self.timeRemaining, self.intervalTimer.fireDate, [ NSDate date ] );
    [ self stopTiming];
}

- ( void )reset;
{
    DLog(@"resetting");
    if (self.intervalTimer) {
        [ self.intervalTimer invalidate];
        self.intervalTimer = nil;
    }
    self.timeRemaining = 0;
    self.accumulatedTime = 0;
    self.isComplete = NO;
}

- ( void )stopTiming;
{
    DLog( );
    [self.intervalTimer invalidate ];
    self.intervalTimer = nil;
}

- ( NSString *)messageForStage;
{
    if (self.isComplete) {
        NSUInteger integerLength = self.endPoint/60;
        return [ NSString stringWithFormat:@"%lu minute mark", (unsigned long)integerLength ];
    }
    else{
        NSUInteger integerLength = self.intervalLength/60;
        return [ NSString stringWithFormat:@"%lu minute mark", (unsigned long)integerLength ];
    }
}

- ( void )setEndPoint:(CGFloat)endPoint;
{
    _endPoint = endPoint;
    self.hasEndpoint = (endPoint > 0);
    DLog(@"setEndPoint = %@", YES_OR_NO(self.hasEndpoint));
}

- ( NSString *)description;
{
    return [ NSString stringWithFormat:@"intervalLength = %g endPoint = %g hasEndPoint = %@", self.intervalLength, self.endPoint, YES_OR_NO(self.hasEndpoint)];
}

- (CGFloat )intervalElapsedTime;
{
    return self.intervalLength - self.timeRemaining;
}

- ( void )dealloc
{
    if (self.intervalTimer) {
        [ self.intervalTimer invalidate];
        self.intervalTimer = nil;
    }
}

@end

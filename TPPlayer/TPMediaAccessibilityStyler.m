//
//  TPMediaAccessibilityStyler.m
//  TPPlayer
//
//

#import "TPMediaAccessibilityStyler.h"
#import "TPClosedCaptionManager.h"
#import <MediaAccessibility/MediaAccessibility.h>

NSString *const kTPMediaAccessibilityStylerValuesChanged = @"TPMediaAccessibilityStylerValuesChanged";

@interface TPMediaAccessibilityStyler()
@property(nonatomic, strong)UIFontDescriptor *fontDescriptor; //  name of the preferred font for the specified style - from docs
@property(nonatomic, strong)UIColor *foregroundColor; // preferred color for caption text.
@property(nonatomic, assign )CGFloat foregroundOpacity; // ranging from 0.0 to 1.0, representing the opacity of the color for text opacity.

@property(nonatomic, assign)CGFloat relativeCharacterSize; // font scaling preference, as a multiplier, for the specified style; ranging from 0.0 to 2.0.
@property(nonatomic, assign )MACaptionAppearanceTextEdgeStyle textEdgeStyle; // preferred text edge style, such as Raised or Drop Shadow

@property(nonatomic, strong)UIColor *backgroundColor; // preferred color shown behind the text and above the window color
@property(nonatomic, assign )CGFloat backgroundOpacity; // ranging from 0.0 to 1.0, representing the opacity of the color behind the text and above the window color.

@property(nonatomic, assign )CGFloat windowOpacity; // ranging from 0.0 to 1.0, for the opacity of the color behind all other caption elements.

@property(nonatomic, strong) NSDictionary *fontAttributes;
@property(nonatomic, strong)NSDictionary *italicFontAttributes;
@property(nonatomic, strong)NSDictionary *boldFontAttributes;
@property(nonatomic, strong)NSDictionary *additionalAttributes;

@end

@implementation TPMediaAccessibilityStyler

+( TPMediaAccessibilityStyler *)sharedInstance;
{
    static dispatch_once_t once;
    static TPMediaAccessibilityStyler *shared = nil;
    
    dispatch_once( &once, ^{
        shared = [ [ TPMediaAccessibilityStyler alloc ]init ];
        [ shared setup ];
    } );
    return shared;
}

- ( void )setup;
{
    [ [ NSNotificationCenter defaultCenter]addObserver:self selector:@selector(styleChanged:) name:(NSString *)kMACaptionAppearanceSettingsChangedNotification object:nil ];
    [[ NSNotificationCenter defaultCenter]addObserver:self selector:@selector(screenSizeChanged:) name:kTPClosedCaptionManagerFrameChanged object:nil ];
    [ self resetStyleComponents];
}

- ( void )styleChanged:( NSNotification *)notification;
{
    [ self resetStyleComponents ];
}

- ( void )resetStyleComponents;
{
    self.fontDescriptor = (__bridge UIFontDescriptor *)(MACaptionAppearanceCopyFontDescriptorForStyle ( kMACaptionAppearanceDomainUser, NULL, kMACaptionAppearanceFontStyleDefault));
    
    self.foregroundColor = [ UIColor colorWithCGColor:(MACaptionAppearanceCopyForegroundColor (kMACaptionAppearanceDomainUser, NULL  )) ];
    self.foregroundOpacity = MACaptionAppearanceGetForegroundOpacity (kMACaptionAppearanceDomainUser, NULL );
    
    self.relativeCharacterSize = MACaptionAppearanceGetRelativeCharacterSize (kMACaptionAppearanceDomainUser,NULL );
    self.textEdgeStyle = MACaptionAppearanceGetTextEdgeStyle (kMACaptionAppearanceDomainUser,NULL );
    
    self.backgroundColor = [ UIColor colorWithCGColor:(MACaptionAppearanceCopyBackgroundColor (kMACaptionAppearanceDomainUser, NULL  )) ];
    self.backgroundOpacity = MACaptionAppearanceGetBackgroundOpacity (kMACaptionAppearanceDomainUser, NULL );
    
    self.windowColor = [ UIColor colorWithCGColor:(MACaptionAppearanceCopyWindowColor (kMACaptionAppearanceDomainUser, NULL  ))];
    self.windowOpacity = MACaptionAppearanceGetWindowOpacity (kMACaptionAppearanceDomainUser, NULL );
    self.roundedCornerRadius = MACaptionAppearanceGetWindowRoundedCornerRadius (kMACaptionAppearanceDomainUser,NULL );

    NSArray *preferredCaptioningMediaCharacteristics = (__bridge NSArray *)MACaptionAppearanceCopyPreferredCaptioningMediaCharacteristics(kMACaptionAppearanceDomainUser);
    DLog(@"preferredCaptioningMediaCharacteristics = %@", preferredCaptioningMediaCharacteristics );
    DLog(@"fontDescriptor attributes = %@", self.fontDescriptor.fontAttributes);
    DLog(@"foregroundColor before = %@", self.foregroundColor);
    DLog(@"foregroundOpacity before = %g", self.foregroundOpacity);
    DLog(@"backgroundColor before = %@", self.backgroundColor);
    DLog(@"backgroundOpacity before = %g", self.backgroundOpacity);
    DLog(@"windowColor before = %@", self.windowColor);
    DLog(@"windowOpacity before = %g", self.windowOpacity);
    DLog(@"textEdgeStyle before = %ld", self.textEdgeStyle);
    DLog(@"roundedCornerRadius before = %g", self.roundedCornerRadius);

    // now package these so they are reusable
    NSUInteger fontSize = ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ) ? 16 : 12;

    if (self.foregroundOpacity > 0 && self.foregroundOpacity < 1.0) // otherwise don't bother
    {
        CGFloat red;
        CGFloat green;
        CGFloat blue;
        CGFloat alpha;
        [ self.foregroundColor getRed:&red green:&green blue:&blue alpha:&alpha ];
        self.foregroundColor = [ UIColor colorWithRed:red green:green blue:blue alpha:self.foregroundOpacity ];
    }
    
//    if (self.windowOpacity > 0 && self.windowOpacity < 1.0) // otherwise don't bother
//    {
        CGFloat windowRed;
        CGFloat windowGreen;
        CGFloat windowBlue;
        CGFloat windowAlpha;
        [ self.windowColor  getRed:&windowRed green:&windowGreen blue:&windowBlue alpha:&windowAlpha ];
        self.windowColor = [ UIColor colorWithRed:windowRed green:windowGreen blue:windowBlue alpha:self.windowOpacity ];
//    }
    
//    if (self.backgroundOpacity > 0 && self.backgroundOpacity < 1.0) // otherwise don't bother
//    {
        CGFloat backgkroundRed;
        CGFloat backgroundGreen;
        CGFloat backgroundBlue;
        CGFloat backgroundAlpha;
        [ self.backgroundColor  getRed:&backgkroundRed green:&backgroundGreen blue:&backgroundBlue alpha:&backgroundAlpha ];
        self.backgroundColor = [ UIColor colorWithRed:backgkroundRed green:backgroundGreen blue:backgroundBlue alpha:self.backgroundOpacity ];
//    }
    
    [ self resetBasicAttributes ];
    [ self resetFontDescriptors:fontSize ];

    [[ NSNotificationCenter defaultCenter]postNotificationName:kTPMediaAccessibilityStylerValuesChanged object:nil ];
}

- ( void )screenSizeChanged:( NSNotification *)notification;
{
    NSDictionary *userInfo = [ notification userInfo];
    CGFloat fontSize = [userInfo[@"fontSize"]integerValue ];
    DLog(@"point size = %@  fontSize = %g", userInfo[@"fontSize"], fontSize );
    [ self resetFontDescriptors:fontSize ];
}

- ( void )resetBasicAttributes;
{
    NSMutableDictionary *tmpDictionary = [@{NSForegroundColorAttributeName : self.foregroundColor, NSBackgroundColorAttributeName : self.backgroundColor } mutableCopy ];

    NSShadow *shadow = [ [ NSShadow alloc]init ];
    shadow.shadowColor = [ UIColor darkGrayColor ];
    shadow.shadowBlurRadius = 3.0;
    
    switch( self.textEdgeStyle){
    case kMACaptionAppearanceTextEdgeStyleUndefined:
            shadow = nil;
        break;
    case kMACaptionAppearanceTextEdgeStyleNone:
            shadow = nil;
        break;
    case kMACaptionAppearanceTextEdgeStyleRaised:
            shadow.shadowOffset = CGSizeMake(-4.0, 4.0);
        break;
    case kMACaptionAppearanceTextEdgeStyleDepressed:
            shadow.shadowOffset = CGSizeMake(4.0, -4.0);
        break;
    case kMACaptionAppearanceTextEdgeStyleUniform:
            shadow = nil;
            tmpDictionary[NSTextEffectAttributeName] = NSTextEffectLetterpressStyle;
            break;
    case kMACaptionAppearanceTextEdgeStyleDropShadow:
            shadow.shadowOffset = CGSizeMake(0.0, -4.0);
        break;
    default:
        break;
    }
    if (shadow) {
        tmpDictionary[NSShadowAttributeName] = shadow;
    }
    self.additionalAttributes = tmpDictionary;

}

- ( void )resetFontDescriptors:( NSUInteger)fontSize;
{
    NSMutableDictionary *tmpAttributes = [ self.additionalAttributes mutableCopy];
    NSMutableDictionary *tmpBoldAttributes = [ self.additionalAttributes mutableCopy];
    NSMutableDictionary *tmpItalicAttributes = [ self.additionalAttributes mutableCopy];

    NSUInteger fontSizeToUse = fontSize;
    if (self.relativeCharacterSize) {
        fontSizeToUse  = lroundf(fontSize * self.relativeCharacterSize);
    }
    DLog(@"fontSize = %lu relativeCharacterSize = %g fontSizeToUse = %lu", fontSize, self.relativeCharacterSize, (unsigned long)fontSizeToUse);
    DLog(@"fontDescriptor before = %@", self.fontDescriptor);
    UIFont *fontToUse = [ UIFont fontWithDescriptor:self.fontDescriptor size:(CGFloat)fontSizeToUse ];
    DLog(@"fontToUse pointSize = %g", fontToUse.pointSize);
    tmpAttributes[NSFontAttributeName] = fontToUse;
    self.fontAttributes = tmpAttributes;

    UIFontDescriptor *boldFontDescriptor = [ self.fontDescriptor fontDescriptorWithSymbolicTraits:UIFontDescriptorTraitBold];
    UIFont *boldFontToUse = [ UIFont fontWithDescriptor:boldFontDescriptor size:(CGFloat)fontSizeToUse ];
    tmpBoldAttributes[NSFontAttributeName] = boldFontToUse;
    self.boldFontAttributes = tmpBoldAttributes;
    
    UIFontDescriptor *italicFontDescriptor = [ self.fontDescriptor fontDescriptorWithSymbolicTraits:UIFontDescriptorTraitItalic];
    UIFont *italicFontToUse = [ UIFont fontWithDescriptor:italicFontDescriptor size:(CGFloat)fontSizeToUse ];
    tmpItalicAttributes[NSFontAttributeName] = italicFontToUse;
    self.italicFontAttributes = tmpItalicAttributes;

}


- ( NSAttributedString *)styleCCContent:( TPCCContent *)lineToStyle isClosedCaption:( BOOL )stringIsClosedCaption;
{
    if (lineToStyle.strings.count == 1) {
        NSDictionary *fontAttributes = [ self fontAttributes:[ lineToStyle.fontStyles firstObject]];
        NSAttributedString *stringToUse = [[ NSAttributedString alloc]initWithString:[ lineToStyle.strings firstObject] attributes:fontAttributes ];
        return stringToUse;
    }
    NSMutableAttributedString *finalString = [[ NSMutableAttributedString alloc]initWithString:@"" ];
    [ lineToStyle.strings enumerateObjectsUsingBlock:^(NSString *string, NSUInteger idx, BOOL *stop) {
        NSAttributedString *partialString = [[ NSAttributedString alloc]initWithString:string attributes:[ self fontAttributes:lineToStyle.fontStyles[idx]] ];
        [ finalString appendAttributedString:partialString];
    }];
//    NSMutableAttributedString *styledString = [ [NSMutableAttributedString alloc ] initWithString:[ lineToStyle.strings firstObject] ];
//    [ styledString addAttributes:self.fontAttributes range:NSMakeRange(0, styledString.length ) ];
    DLog(@"attributedString = %@", finalString);
    return finalString;
}


- ( NSDictionary *)fontAttributes:( NSString *)styleToUse;
{
    if ([ styleToUse isEqualToString:@"italic"]) {
        return self.italicFontAttributes;
    }
    if ([ styleToUse isEqualToString:@"bold"]) {
        return self.boldFontAttributes;
    }
    return self.fontAttributes;
}


- (void)removeNotificationCenterObserver
{
    NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
    [notificationCenter removeObserver:self];
}

- (void)dealloc
{
    [self performSelectorOnMainThread:@selector(removeNotificationCenterObserver) withObject:self waitUntilDone:YES];
}

@end

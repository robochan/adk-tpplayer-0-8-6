//
//  TPIdentityServicesViewController.m
//  TPPlayer
//
//
//  Copyright © 2013, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)
//

#import "TPIdentityServicesViewController.h"

@interface TPIdentityServicesViewController ()



@end

@implementation TPIdentityServicesViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- ( void )registerForNotifications;
{
    
}


- (void)removeNotificationCenterObserver
{
    NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
    [notificationCenter removeObserver:self];
}

- (void)dealloc
{
    [self performSelectorOnMainThread:@selector(removeNotificationCenterObserver) withObject:self waitUntilDone:YES];
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    [ self configureView ];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- ( void )configureView;
{
//    NSString *localContinueWord = NSLocalizedString(@"CONTINUE", @"Label for authorization button - ALL CAPS" );
//    [ self.continueButton setTitle:localContinueWord forState:UIControlStateNormal];
//    self.continueButton.titleLabel.textAlignment = NSTextAlignmentCenter;
//    UIImage *continueBackgroundImage = [[UIImage imageNamed:@"large_green_stretchable_button"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 5, 0, 5)];
//    [self.continueButton setBackgroundImage:continueBackgroundImage forState:UIControlStateNormal];
//    [self.continueButton setBackgroundImage:continueBackgroundImage forState:UIControlStateSelected];
//    [self.continueButton setBackgroundImage:continueBackgroundImage forState:UIControlStateHighlighted];
//    
//    NSString *accessIntructionsLabelText = NSLocalizedString(@"If you have a subscription with a participating Television Provider, you can be among the first to watch the most recently aired episodes within this iPad application. Sign in for full access.", @"Authorization information");
//    self.authorizationDescriptionText.text = accessIntructionsLabelText;
//    
//    NSString *getAccessText = NSLocalizedString(@"Get Access", @"Title for the authorization instructions in the video player");
//    self.getAccessLabel.text = getAccessText;
}

- ( void )handleAccess;
{
    // if user has access, remove the lock screen 
    // otherwise put it up unless the movie is already playing
    [ self checkAuthentication];
}

- ( void )showAuthorizationError:( NSString *)errorDescription
{
    // app can register another class to handle this error
    if (self.delegateWillHandleAuthorizationError) {
        [ self.delegate userReceivedAuthorizationError:errorDescription];
        return;
    }
    //    self.authorizationLabel.hidden = NO;
    //    self.playButton.hidden = YES;
    if (NSClassFromString(@"UIAlertController")) {
        UIAlertController *alertController = [ UIAlertController alertControllerWithTitle:@"Can't access content" message:errorDescription preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancelAction = [ UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
            DLog(@"user says OK");
            [ self.delegate userReceivedAuthorizationError ];
                 }];
        [ alertController addAction:cancelAction];
        [ self presentViewController:alertController animated:NO completion:nil ];
    }
    else{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Can't access content" 
                                                        message:errorDescription 
                                                       delegate:self 
                                              cancelButtonTitle:@"OK" 
                                              otherButtonTitles:nil];
        [alert show];    }

}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{    
    [ self.delegate userReceivedAuthorizationError ];
}

#pragma  - useful auth methods
- ( void )authorizationCheckComplete:( NSNotification *)notification;
{
    // subclasses handle according to the appropriate authManager
}

- ( void )userLoggedIn:( NSNotification *)notification;
{
    // subclasses handle according to the appropriate authManager
}

- ( void )userLoggedOut:( NSNotification *)notification;
{
    // subclasses handle according to the appropriate authManager
}



#pragma - mark methods that handle UI -- pretty simple for this one

- ( void )accessAuthorizationChanged:(BOOL)userIsNowAuthorized;
{
    self.userHasAccessAuthorization = userIsNowAuthorized;
    DLog(@"accessAuthorizationChanged  self.userHasAccessAuthorization = %@", YES_OR_NO(self.userHasAccessAuthorization));
//    if (  self.view.superview) {
//        [ self handleLockScreenDisplay ];
//    }
    [ self.delegate userIsAuthorizedToPlay:userIsNowAuthorized ];
}

//- ( void )handleLockScreenDisplay;
//{
//    if (! self.userHasAccessAuthorization) {
//        self.view.frame = self.parentViewController.view.bounds;
//        [ self.parentViewController.view addSubview:self.view ];
//    }
//    else{
//        if( self.view.superview)
//        {
//            [ self.view removeFromSuperview];
//        }
//    }
//}

#pragma - mark general authentication methods

- (  void )checkAuthentication;
{
    // subclasses contact their appropriate authManager
}

- ( void )getAuthentication;
{
    // subclasses contact their appropriate authManager
}

- ( void )logout;
{
    // subclasses contact their appropriate authManager
}

#pragma - mark methods that handle local nib interaction

- (IBAction)userSelectedAuthorizationButton:(id)sender 
{
    [ self getAuthorization ];
}

- ( void )getAuthorization;
{
    // if you can't reach the internet, don't bother
    if ( ! [ [ TPNetworkMonitor sharedInstance] networkIsReachable  ] ) 
    {
        [[ TPNetworkMonitor sharedInstance ]showNoConnectivityAlert ];
    }
    else 
    {
        [ self getAuthentication];
    }
    
}

- ( void )reachabilityChanged:(BOOL)networkIsReachable;
{
    // don't do anything from here.
}

#pragma - mark metadatURLAdjuster method

// this one just returns the string immediately with no change 
// subclasses can override to do something more

- ( void )rewriteMetadataUrl:( NSString *)stringForURL forRequester:( id<TPMetaDataURLAdjustmentRequester>)requester isPreview:( BOOL )isPreview;
{
    // default is do nothing -- override to modify it
    [ requester metaDataUrlAdjuster:self applyMetadataURL:stringForURL isPreview:isPreview];
}

@end

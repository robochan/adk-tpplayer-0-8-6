//
//  TPClip.h
//
//  Copyright © 2013, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)
//

#import <Foundation/Foundation.h>
#import "TPBaseClip.h"

@interface TPClip : NSObject

@property(nonatomic, assign)NSUInteger      duration;   // Length of the clip in ms - named per Metafile specification
@property(nonatomic, strong)NSString        *title;     // Title of the clip
@property(nonatomic, weak)TPBaseClip        *baseClip;  // Baseclip this clip links to [???]
@property(nonatomic, assign)NSInteger       clipBegin;  // clip start time converted to ms from string
@property(nonatomic, assign)NSInteger       clipEnd;    // clip end time converted to ms from string
@property(nonatomic, strong)NSDictionary    *params;    // Dictionary of custom params fields, keyed on "name"

@end

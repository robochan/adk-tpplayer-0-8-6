//
//  TPAppSpecificInformationManager.h
//
//
//  Copyright © 2013, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)
//

#import <Foundation/Foundation.h>

#define ENVIRONMENT_IS_PRODUCTION 0

@interface TPAppSpecificInformationManager : NSObject

+( TPAppSpecificInformationManager *)sharedInstance;

- ( id )objectForKey:( NSString *)keyToUse;
- ( BOOL )booleanForKey:( NSString *)keyToUse;

@end

//
//  TPUserListItemManager.m
//  TPPlayer
//
//  Copyright (c) 2014 thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)


#import "TPUserListItemManager.h"
#import "TPEndUserServicesAgent.h"
#import "TPAppSpecificInformationManager.h"
#import "TPUserListItem.h"

@interface TPUserListItemManager()

@property(nonatomic, strong)NSString *userListItemPostUrl;
@property(nonatomic, strong)NSString *userListItemDeleteUrl;
@property( nonatomic, strong) NSString *accountID;
@property( nonatomic, strong) NSString *context;
@property(nonatomic, assign)NSInteger numberUserListItemsAllowed ;

@end

@implementation TPUserListItemManager

- ( void )createUserListItem:( TPUserListItem *)prototypeItem;
{
    if (self.userToken) {
        [ self removeLastUserListItemBeyondLimit ];
        // first add it to our local copy of the userList
        [self.userList addUserListItem:prototypeItem ];
        // then take care of getting it added to the server-side
        self.isProcessingListItem = YES;
        NSString *userListURL = [ self createUserListItemURL:prototypeItem ];
        NSURL *url = [ NSURL URLWithString:userListURL];
        DLog(@"url  = %@", url);
        
        NSURLSession *session = [ NSURLSession sharedSession];
        NSURLSessionDownloadTask *task = [ session downloadTaskWithURL:url completionHandler:^(NSURL *location, NSURLResponse *response, NSError *error) {
            if (error) {
                DLog(@"%@", error);
                return;
            }
            NSInteger status = [(NSHTTPURLResponse *)response statusCode];
            DLog(@"response status = %ld", (long)status);
            // here a 201 status is a good thing
            if (status > 204) {
                DLog(@"%@", @"oh well");
                return;
            }
            NSData *downloadedData = [ NSData dataWithContentsOfURL:location];
            [ self createUserListItemComplete:downloadedData];
        }];
        [ task resume ];
    }
    else{
        DLog(@"no user token available - can't create userList");
    }
}

// subclasses should probably override this
- ( NSString *)createUserListItemURL:( TPUserListItem *)userListItem;
{
    NSString *userListURL = [ NSString stringWithFormat:@"%@&_aboutId=%@&_userListId=%@&account=%@&token=%@&_title=%@&_description=%@", self.userListItemPostUrl, userListItem.aboutID, self.userList.identifier, self.accountID, self.userToken, userListItem.title, userListItem.itemDescription ];
    return userListURL;
}

- ( void )createUserListItemComplete:( NSData *)response;
{
    NSError *anError = nil;
    
    NSDictionary *getUserListItemResults = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:&anError];
    DLog(@"getUserListItemResults = %@", getUserListItemResults);
    if (getUserListItemResults[@"isException"]) {
        [ self userListItemCreationFailed:getUserListItemResults];
    }
    else{
        [ self userListItemCreationCompletedSuccessfully:getUserListItemResults];
    }
    self.isProcessingListItem = NO;
}

- ( void )userListItemCreationCompletedSuccessfully:( NSDictionary *)results;
{
    self.isProcessingListItem = NO;
    DLog(@"results = %@", results)
    // subclasses override to handle the results
}

- ( void )userListItemCreationFailed:( NSDictionary *)results;
{
    self.isProcessingListItem = NO;
    DLog(@"isException for Creation");
}

// according to the docs, if there is an existing object, it will update
// if not, it will create it.  This is of concern in case this object
// was deleted since we fetched it.
- ( void )updateUserListItem:( TPUserListItem *)prototypeItem;
{
    if (self.userToken)
    {
        [ self.userList updateUserListItem:prototypeItem];
        self.isProcessingListItem = YES;
        NSString *userListURL = [ self updateUserListItemURL:prototypeItem ];
        NSURL *url = [ NSURL URLWithString:userListURL];
        DLog(@"update userListURL  = %@", url);
        
        NSURLSession *session = [ NSURLSession sharedSession];
        NSURLSessionDownloadTask *task = [ session downloadTaskWithURL:url completionHandler:^(NSURL *location, NSURLResponse *response, NSError *error) {
            if (error) {
                DLog(@"%@", error);
                return;
            }
            NSInteger status = [(NSHTTPURLResponse *)response statusCode];
            DLog(@"response status = %ld", (long)status);
            // here a 201 status is a good thing ?
            if (status > 204) {
                DLog(@"%@", @"oh well");
                return;
            }
            NSData *downloadedData = [ NSData dataWithContentsOfURL:location];
            [ self updateUserListItemComplete:downloadedData];
        }];
        [ task resume ];
    }
    else{
        DLog(@"no user token available - can't update userListItem");
    }
}

// subclasses should probably override this
- ( NSString *)updateUserListItemURL:( TPUserListItem *)userListItem;
{
    NSString *userListURL = [ NSString stringWithFormat:@"%@&_title=%@&_description=%@&_id=%@&account=%@&token=%@", self.userListItemPostUrl,  userListItem.title, userListItem.itemDescription, userListItem.identifier, self.accountID, self.userToken ];
    return userListURL;
}


- ( void )updateUserListItemComplete:( NSData *)response;
{
    NSError *anError = nil;
    
    NSDictionary *updateUserListItemResults = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:&anError];
    DLog(@"getUserListResults = %@", updateUserListItemResults);
    if (updateUserListItemResults[@"isException"]) {
        DLog(@"no update on userListItem");
        [ self userListItemUpdateFailed:updateUserListItemResults];
    }
    else{
        [ self userListItemUpdateCompletedSuccessfully:updateUserListItemResults ];
    }
}

- ( void )userListItemUpdateCompletedSuccessfully:( NSDictionary *)results;
{
    self.isProcessingListItem = NO;
    DLog(@"results = %@", results)
    // subclasses override to handle the results
}

- ( void )userListItemUpdateFailed:( NSDictionary *)results;
{
    self.isProcessingListItem = NO;
    DLog(@"isException for Update");
}

- ( void )removeLastUserListItemBeyondLimit;
{
    if (self.userList.userListItems.count > self.numberUserListItemsAllowed) {
        TPUserListItem *lastListItem = [self.userList.userListItems lastObject];
        [ self deleteUserListItem:lastListItem ];
    }
 }

- ( NSInteger)numberUserListItemsAllowed
{
    if (! _numberUserListItemsAllowed) {
        NSNumber *appNumberOfUserListItemsAllowed = [[ TPAppSpecificInformationManager sharedInstance]objectForKey:@"TPResumeNumberOfUserListItemsAllowed" ];
        if (appNumberOfUserListItemsAllowed) {
           _numberUserListItemsAllowed = [ appNumberOfUserListItemsAllowed integerValue];
        }
        if (_numberUserListItemsAllowed <= 0) {
            _numberUserListItemsAllowed = 500;
        }
    }
    return _numberUserListItemsAllowed;
}




- ( void )deleteUserListItem:( TPUserListItem *)prototypeItem;
{
    if (self.userToken)
    {
        // first remove it from our local copy of the userList
        [self.userList removeUserListItem:prototypeItem ];
        // then take care of getting it removed from the server-side
        self.isProcessingListItem = YES;
        NSString *userListURL = [ self deleteUserListItemURL:prototypeItem ];
        NSURL *url = [ NSURL URLWithString:userListURL];
        DLog(@"url  = %@", url);
        
        NSURLSession *session = [ NSURLSession sharedSession];
        NSURLSessionDownloadTask *task = [ session downloadTaskWithURL:url completionHandler:^(NSURL *location, NSURLResponse *response, NSError *error) {
            if (error) {
                DLog(@"%@", error);
                return;
            }
            NSInteger status = [(NSHTTPURLResponse *)response statusCode];
            DLog(@"response status = %ld", (long)status);
            // here a 201 status is a good thing ?
            if (status > 204) {
                DLog(@"%@", @"oh well");
                return;
            }
            NSData *downloadedData = [ NSData dataWithContentsOfURL:location];
            [ self deleteUserListItemComplete:downloadedData];
        }];
        [ task resume ];
    }
    else{
        DLog(@"no user token available - can't update userListItem");
    }
}

- ( void )deleteUserListItemComplete:( NSData *)response;
{
    NSError *anError = nil;
    
    NSDictionary *deleteUserListItemResults = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:&anError];
    DLog(@"deleteUserListItemResults = %@", deleteUserListItemResults);
    if (deleteUserListItemResults[@"isException"]) {
        [ self userListItemDeleteFailed:deleteUserListItemResults];
    }
    else{
        [ self userListItemDeleteCompletedSuccessfully:deleteUserListItemResults ];
    }
}

// subclasses should probably override this
- ( NSString *)deleteUserListItemURL:( TPUserListItem *)userListItem;
{
    NSString *userListURL = [ NSString stringWithFormat:@"%@&byAboutID=%@&account=%@&token=%@", self.userListItemDeleteUrl, userListItem.aboutID, self.accountID, self.userToken ];
    
    return userListURL;
}

- ( void )userListItemDeleteCompletedSuccessfully:( NSDictionary *)results;
{
    self.isProcessingListItem = NO;
    DLog(@"results = %@", results)
    // subclasses override to handle the results
}

- ( void )userListItemDeleteFailed:( NSDictionary *)results;
{
    self.isProcessingListItem = NO;
    DLog(@"isException for Creation");
}


- ( BOOL )responseHasProblems:( NSURLResponse *)response error:( NSError *)error;
{
    if (error) {
        DLog(@"%@", error);
        return YES;
    }
    NSInteger status = [(NSHTTPURLResponse *)response statusCode];
    DLog(@"response status = %ld", (long)status);
    if (status != 200) {
        DLog(@"%@", @"oh well");
        return YES;
    }
    return NO;
}



- ( NSString *)context;
{
    if (! _context) {
        _context =[[[ TPAppSpecificInformationManager sharedInstance]objectForKey:@"TPResumeContext" ]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding ];
        //        _context = @"public";
    }
    return _context;
}

- ( NSString *)accountID;
{
    if (! _accountID) {
        _accountID = [[ TPAppSpecificInformationManager sharedInstance]objectForKey:@"TPmpxAccountID" ];
    }
    return _accountID;
}



- ( NSString *)userListItemPostUrl;
{
    if (! _userListItemPostUrl) {
        _userListItemPostUrl =[[ TPEndUserServicesAgent sharedInstance]userListItemPostUrl ];
    }
    return _userListItemPostUrl;
}

- ( NSString *)userListItemDeleteUrl;
{
    if (! _userListItemDeleteUrl) {
        _userListItemDeleteUrl =[[ TPEndUserServicesAgent sharedInstance]userListItemDeleteUrl ];
    }
    return _userListItemDeleteUrl;
}

@end

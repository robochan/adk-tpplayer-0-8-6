//
//  TPTrailer.m
//  TPPlayer
//
//  Copyright © 2013, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)
//

#import "TPTrailer.h"

@implementation TPTrailer

- (NSString *)pathForTrailer;
{
    // I'm not sure why houseId includes _Trailer
    return [NSString stringWithFormat:@"%@", self.houseId];
}

- ( BOOL )requiresAuth;
{
    return NO;  // default is that video requires authorization
}

- ( TPVideoAsset *)videoAsset;
{
    return [[self.videos allValues] firstObject ];
}

- ( id )initWithCoder:(NSCoder *)aDecoder
{
    self = [ super initWithCoder:aDecoder ];  // takes care of title and videos and images
    self.thumbnail = [ aDecoder decodeObjectForKey:@"thumbnail" ];
    self.parentId = [ aDecoder decodeObjectForKey:@"parentId" ];
    self.houseId = [ aDecoder decodeObjectForKey:@"houseId" ];

    
    return self;
}

- ( void )encodeWithCoder:(NSCoder *)aCoder;
{
    [ super encodeWithCoder:aCoder ]; // takes care of title and videos and images
    [ aCoder encodeObject:self.thumbnail forKey:@"thumbnail" ];
    [ aCoder encodeObject:self.parentId forKey:@"parentId" ];
    [ aCoder encodeObject:self.houseId forKey:@"houseId" ];

    
}

@end
